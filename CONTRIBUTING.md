# Quartus Template Structure

This is the default template for Quartus project, you may fork this to start a new project.

# Table of Contents

1. [Overview](#overview)
1. [Adding QSYS Components](#adding-qsys-components)
1. [Adding Megafunctions](#adding-megafunctions)
1. [Adding HDL](#adding-hdl)
1. [Adding Scripts](#adding-scripts)
1. [Adding Documentation](#adding-documentation)
1. [Adding Binary Files](#adding-binary-files)
1. [Adding Testbenches](#adding-testbenches)

## Overview

The purpose of this template is to provide a bare-bones skeleton to be used as a starting off point for new Quartus Projects, and to reduce inconsistencies between project layouts.

## Adding QSYS Components

All QSys components will be placed under **/hdl/ip/\<qsys_component_name\>/**. No other work is necessary to have them appear in QSys. Software only QSys components will need to be manually enabled inside the BSP once added. 

## Adding Megafunctions

All Megafunctions will be placed under **/hdl/mf/** and must be inside their own directory. 

Ex. A NATIVE_PHY is created called "ethernet_phy", a directory will be created **/hdl/mf/ethernet_phy/** and the megafunction will be placed inside it.

## Adding HDL

All HDL files will be placed under **/hdl/**. Libary files taken from other projects will be placed in **/hdl/lib/\<name of library-version\>/**. For project related HDL modules, subdirectories may be created as necessary to group related module files. 

## Adding Scripts

All TCL scripts related to the build process will placed under **/scripts/**. Additional subdirectories may be created to group related scripts. 

## Adding Documentation

All project documentation shall be placed under **/docs**, this includes datasheets, test results, images, etc.

## Adding Binary Files

The Quartus project will place all binary output files such as .sof, .pof, .jic, .rbf, in the **/bin/**. If outside files are required to build a .POF, .JIC or other binary file, they will be placed in the proper format and included in this directory. 

**NOTE: Intermediary files should not be placed in the repository**. 

## Adding Testbenches

All testbench related files, including HDL, scripting, and executables shall be placed in **/tb/**. Testbench reports should stay within the /tb/ structure. The /tb/ subdirectory structure has not yet been formalized.  