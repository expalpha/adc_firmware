# ALPHAg ADC 16 Module (ALPHA16) - REV1

Welcome to the ALPHAg ALPHA16 Project 

# Table of Contents

* [Overview](#overview)
* [Inital Setup](#initial-setup)
    * [Quartus Setup](#quartus-setup)
        * [Create the .cdf](#create-the-cdf)
    * [NIOS Setup](#nios-Setup)
        * [Create the project files](#create-the-project-files) 
* [Build Instructions](#build-instructions)
    * [Building from Quartus GUI](#building-from-quartus-gui)
        * [Generate the QSys File](#generate-the-qsys-file)
        * [Compile the NIOSII Project](#compile-the-niosii-project)
        * [Compile the Quartus Project](#compile-the-quartus-project)
    * [Building from Scripts](#building-from-scripts)
    * [Installing from JTAG](#installing-from-jtag)
    * [Installing from ESPER](#installing-from-esper)
* [Components](#components)
* [Drivers](#drivers)
* [Libaries](#libraries)
* [FAQ](#faq)

# Overview

The ALPHA16 is a 16channel digitizer, with a FMC32 card attached, to provide a total of 48 channels.

# Initial Setup

### Warning: Please use Quartus Prime Standard Edition 16.1

## Quartus Setup

### Create the .cdf
1. Open Quartus Prime Standard Edition
2. Click on **Tools -> Programmer**. The **Programmer** window will appear
3. In the **Programmer** window, click **Auto Detect**. A **Select Device** dialog box will appear
    1. (Optional) If **Auto Detect** is greyed out, click on **Hardware Setup**. The **Hardware Setup** dialog box will appear
    2. Under **Hardware Settings**, double-click on the USB JTAG hardware you wish to use
    3. Click **Close**. The **Hardware Setup** dialog box will close
5. Click on **File -> Save As**. The **Save As** dialog box will appear
6. In the **Save As** dialog box, browse to the **/\<project_dir\>/bin/** directory
7. Make sure the **Add file to current project** checkbox is checked
7. In the **File name** input box, type "alpha16.cdf" and click **Save**
8. Close the **Programmer** window
9. Done!

## NIOS Setup

### Create the project files
1. Open Quartus Prime Standard Edition
2. Open the NIOS II Software Build Tools by clicking **Tools -> NIOS II Software Build Tools for Eclipse**
3. The **Workspace Launcher** window will appear, click *OK* to accept the default workspace.
4. Click **File -> Import...**. The **Import** dialog box will appear
5. Click on **General -> Existing Projects into Workspace**
6. Click **Next \>**
7. Next to **Select root directory** click **Browse...**. A **Browse for Folder** dialog box will appear
8. Browse to **/\<project_dir\>/hdl/software** and click **OK**
8. Four Projects should appear in the **Projects** panel: 
    * alpha16
    * alpha16\_bsp 
    * alpha16\_bootloader
    * alpha16\_bootloader\_bsp
9. Click **Select All**
10. Click **Finish**
11. Done!
 
# Build Instructions

## Building from Quartus GUI

**Warning: Please use Quartus Prime Standard Edition 16.1**
### Generate the QSys File
1. Open Quartus Standard Edition 16.1
1. Open QSys by clicking **Tools -> QSys**
2. Click on **File -> Open** (CTRL+O). An **Open File** dialog box should appear.
3. Select the **management.qsys** file from the **/\<project_dir\>/hdl** directory and click **Open**
4. Modify as necessary. NOTE: Any change to the QSys, even cosmetic such as expanding or collapsing components will cause the QSys to desire a re-generate. 
5. Click **File -> Save** (CTRL+S)
6. Click **Generate HDL**. A **Generation** dialog box will appear 
7. Select the settings you desire to be changed, if any
8. Click **Generate**
9. Wait for the generation to complete.
10. Done!

### Compile the NIOSII Project
1. Open the NIOS II Software Build Tools by clicking **Tools -> NIOS II Software Build Tools for Eclipse**
2. (Optional) If the QSys file has been re-generated performing the following
    1. Right-click on **alpha16\_bsp** and select **NIOS II -> Generate BSP**. The BSP will regenerate
    2. Right-click on **alpha16\_bootloader\_bsp** and select **NIOS II -> Generate BSP**. The BSP will regenerate
3. Modify files as needed. WARNING: Do not modify any BSP files by hand! All changes will be lost on next generate.
4. Click **Project -> Build All** (CTRL+B). If your workspace as multiple projects, it is recommend to close all but the ones related to the ALPHA16.
5. Right-click on **alpha16\_bootloader** and select **Make Targets -> Build...**. The **Make Targets** dialog box will appear
6. In the **Make Targets** dialog box, select **mem\_init\_generate** and click **Build**
7. Done!

### Compile the Quartus Project
1. Open Quartus Standard Edition 16.1
1. Under **Processing** click **Start Compilation** (CTRL+L)
2. Wait about 16-30 minutes.
3. Done!
  
**Note:** If Quartus crashed while compiling, delete the **\<project_dir\>/hdl/db** and **\<project_dir\>/hdl/incremental_db** directories and try again
  
## Building from Scripts
1. Scripts are not yet implemented.

## Installing from JTAG
1. Generate the .jic file using the provided /bin/alpha16.cof
    1. In the main Quartus window, click **File -> Convert Programming Files**, A **Convert Programming Files** window will appear
    2. In the **Convert Programming Files** window, click **Open Conversion Setup Data**. An **Open** dialog box will appear
    3. In the **Open** dialog box, select the **alpha16.cof** file located in **/bin/**
    4. Click **Open**. The **Open dialog box will close
        * Note the **File name** field, it should display /\<project\_dir\>/bin/alpha16.jic
    5. In the **Convert Programming Files** window, click **Generate**
    6. When the .jic is done being generated, a small dialog box will appear, click **OK**
    7. In the **Convert Programming Files** window, click **Close**
2. Load the .JIC file using the Programmer
    1. In the main Quartus window in the **Project Navigator** drop-down input select **Files**.
    2. Scroll down the window and find the **alpha16.cdf**, and double-click it. The **Programmer** window will open
    3. Click on the icon labelled **5AGXFB3H4F35C4**
    4. Click on the **Change File** button. The **Select New Programming File** dialog box will appear
    5. Browse to **/\<project_dir\>/bin/** and select **alpha16.jic**
    6. Click Open. The **Select New Programming File** dialog box will close
    7. In the main panel, check the **Program/Configure** box on the line that starts **./bin/alpha16.jic**
    8. Click the **Start** button
    9. Wait for the **Progress** bar to go to 100% and stop
    10. Done!

## Installing from ESPER
1. To be implemented
 
# Components

Major components of the firmware

# Drivers

Drivers used in this project

# Libraries

Libraries used in this project

# FAQ

Frequently Asked Questions