#########################################################
# TRIUMF Electronic Development Group Firmware Makefile #
#########################################################

##################################
# ALPHAg PWB Project, Revision 1 #
##################################

#
# before running this script, execute
# /opt/intelFPGA/17.0/nios2eds/nios2_command_shell.sh
#

#############
# VARIABLES #
#############

hdl_dir := hdl
qsys_name := management
nios_all_sources := $(wildcard $(hdl_dir)/software/*)
nios_bsp_sources := $(wildcard $(hdl_dir)/software/*_bsp)
nios_proj_sources := $(filter-out $(nios_bsp_sources), $(nios_all_sources)) # Separate the projects from the BSPs
quartus_project := alpha16
quartus_project_rev := alpha16

.PHONY: help all clean firmware
.PHONY: qsys clean_qsys
.PHONY: nios compile_nios generate_nios_bsp clean_nios $(nios_all_sources)
.PHONY: quartus clean_quartus regenerate_jic

###########
# Targets #
###########

default: help

all: ## Build everything
all: qsys nios quartus

firmware: ## Build Firmware (Quartus+Qsys+NIOS II)
firmware: qsys nios quartus

clean: ## Clean everything
clean: quartus_clean qsys_clean nios_clean

help: ## List available targets
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

# QSYS Target
qsys: ## Makes the top-level Qsys component
qsys:
	@echo "Generating Qsys"
	@qsys-generate  $(hdl_dir)/$(qsys_name).qsys --synthesis=VERILOG --output-directory=$(hdl_dir)/$(qsys_name)

qsys_clean: ## Removes the synthesized Qsys files
qsys_clean:
	@echo "Cleaning Qsys"
	@rm -rf $(hdl_dir)/$(qsys_name)

##################
# NIOSII Targets #
##################

nios: ## Build all NIOS II projects
nios: nios_generate_bsp nios_compile

nios_clean: ## Clean all NIOS II projects
	@echo "Cleaning NIOS II Projects"
	@for proj in $(nios_all_sources) ; do \
	make clean -C $$proj ; \
	done

# Break nios build into two steps, we want to generate the BSPs first, then compile the nios projects

nios_generate_bsp: ## Generate the BSPs for NIOS II projects
	@echo "Generating BSP files"
	@for proj in $(nios_bsp_sources) ; do \
	nios2-bsp-generate-files --silent --settings=$$proj/settings.bsp --bsp-dir=$$proj ; \
	done

nios_compile: ## Compile NIOS II projects
	@echo "Compiling NIOS II projects"
	@for proj in $(nios_proj_sources) ; do \
	make --quiet -C $$proj all; \
	done

###################
# Quartus Targets #
###################

quartus: ## Build Quartus project
quartus:	quartus_compile

quartus_clean: ## Remove temporary Quartus files
	@cd $(hdl_dir) ; quartus_sh --clean $(quartus_project)
	@rm -rf $(hdl_dir)/db
	@rm -rf $(hdl_dir)/incremental_db

quartus_compile: ## Full compile of Quartus project
	@cd $(hdl_dir) ; quartus_sh --flow compile $(quartus_project) -c $(quartus_project_rev)

quartus_map: ## Perform Analysis and Synthesis 
	@cd $(hdl_dir) ; quartus_map $(quartus_project) -c $(quartus_project_rev)

quartus_fit: ## Perform Fitter
	@cd $(hdl_dir) ; quartus_fit $(quartus_project) -c $(quartus_project_rev)

quartus_asm: ## Generate Assembly files and update memory contents
	@cd $(hdl_dir) ; quartus_cdb --update_mif $(quartus_project) -c $(quartus_project_rev)
	@cd $(hdl_dir) ; quartus_asm $(quartus_project) -c $(quartus_project_rev)

quartus_cof: ## Run Quartus COF file for project
	@cd $(hdl_dir) ; quartus_cpf -c ../bin/$(quartus_project)\_$(quartus_project_rev).cof

quartus_sta: ## Perform TimeQuest Timing Analysis
	@cd $(hdl_dir) ; quartus_sta $(quartus_project) -c $(quartus_project_rev)	

quartus_eda: ## Run EDA Netlist Writer
	@cd $(hdl_dir) ; quartus_eda $(quartus_project) -c $(quartus_project_rev)	
