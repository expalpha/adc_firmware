#!/bin/sh

SCRIPTDIR=$(dirname "$(readlink -f "$0")")
#PROJECT_PATH=$(realpath --relative-to $(pwd) $SCRIPTDIR/../hdl )
PROJECT_PATH=$SCRIPTDIR/../hdl
NIOS_PATH=$PROJECT_PATH"/software"
NIOS_PROJECT="alpha16"
NIOS_PROJECT_BSP=$NIOS_PROJECT"_bsp"
BOOTLOADER_PROJECT="alpha16_bootloader"
BOOTLOADER_PROJECT_BSP=$BOOTLOADER_PROJECT"_bsp"
INIT_PROJECT="alpha16_init"
INIT_PROJECT_BSP=$INIT_PROJECT"_bsp"

# Might as well recreate the bootloader if necessary
echo "Generating ALPHA16 Bootloader BSP"
nios2-bsp-generate-files --silent --settings=$NIOS_PATH/$BOOTLOADER_PROJECT_BSP/settings.bsp --bsp-dir=$NIOS_PATH/$BOOTLOADER_PROJECT_BSP
echo "Making ALPHA16 Bootloader Project"
make -C $NIOS_PATH/$BOOTLOADER_PROJECT all mem_init_generate 

echo "Generating ALPHA16 Init BSP"
nios2-bsp-generate-files --silent --settings=$NIOS_PATH/$INIT_PROJECT_BSP/settings.bsp --bsp-dir=$NIOS_PATH/$INIT_PROJECT_BSP
echo "Making ALPHA16 Init Project"
make -C $NIOS_PATH/$INIT_PROJECT all mem_init_generate 

# Then compile the main project
echo "Generating ALPHA16 BSP"
nios2-bsp-generate-files --silent --settings=$NIOS_PATH/$NIOS_PROJECT_BSP/settings.bsp --bsp-dir=$NIOS_PATH/$NIOS_PROJECT_BSP
echo "Making ALPHA16 Project"
make -C $NIOS_PATH/$NIOS_PROJECT all 
echo "compile_nios.sh - done"
