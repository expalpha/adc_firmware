#!/usr/bin/perl -w
#
# program PWB with given SOF file
#

$| = 1;

my $f = shift @ARGV;

die "Cannot read $f: $!\n" unless -r $f;

#my $quartus = "/home/quartus/quartus7.2";
#my $quartus = "/home/olchansk/altera7.2/quartus7.2";
#my $quartus = "/triumfcs/trshare/olchansk/altera/altera9.1/quartus";

my $j = `jtagconfig`;
print $j;

#my $q = `$quartus/bin/quartus_pgm -l`;
my $q = `quartus_pgm -l`;
print $q;

$q =~ /1\) (.*)\n/m;
my $b = $1;
print "$b\n";

#my $cmd = "$quartus/bin/quartus_pgm -c \"$b\" -m JTAG -o \"p;$f\@1\"";
my $cmd = "quartus_pgm -c \"$b\" -m JTAG -o \"p;$f\"";
print "Running $cmd\n";
system $cmd;

exit 0;
#end
