#!/bin/sh
# FILE: make_build_info.sh

# version holds the major release number which does not get incremented.
# it is read from a file called major_version.
# If it doesn't exsist then is will be created with a default 0.0
if [ -e build/major_version ]
then    major_version="`sed  's/^ *//' build/major_version`"
else    major_version="0"
        echo $major_version > build/major_version
fi

if [ -e build/minor_version ]
then    minor_version="`sed  's/^ *//' build/minor_version`"
else    minor_version="0"
        echo $minor_version > build/minor_version
fi

version="$major_version.$minor_version"

# buildnum holds the major release number which does get incremented.
# it is read from a file called build.number
# if it doesn't exsist then it is creatd with a default value of 0
if [ -e build/build_number  ]
then    buildnum="`sed  's/^ *//' build/build_number` "
else    buildnum=0;
fi
#increment the buildnum by 1
buildnum=`expr $buildnum + 1`

#write it to a file
echo $buildnum  > build/build.number.temp

#copy over the original
mv build/build.number.temp build/build_number

#print it to the screen
# echo Build $buildnum

timestamp=$(date +%s)
timestamp_fmt=$(date -d@$timestamp)

#version..
#why - just because
echo "$version.$buildnum - $timestamp_fmt" > build/version_number

#header
#now for the real work
echo "#ifndef BUILD_NUMBER_STR" > src/build_number.h
echo "#define BUILD_NUMBER_STR \"$buildnum\"" >> src/build_number.h
echo "#endif" >> src/build_number.h
echo >> src/build_number.h

echo "#ifndef BUILD_NUMBER" >> src/build_number.h
echo "#define BUILD_NUMBER $buildnum" >> src/build_number.h
echo "#endif" >> src/build_number.h
echo >> src/build_number.h

echo "#ifndef BUILD_TIMESTAMP" >> src/build_number.h
echo "#define BUILD_TIMESTAMP $timestamp" >> src/build_number.h
echo "#endif" >> src/build_number.h
echo >> src/build_number.h

echo "#ifndef VERSION_MAJOR" >> src/build_number.h
echo "#define VERSION_MAJOR $major_version" >> src/build_number.h
echo "#endif" >> src/build_number.h
echo >> src/build_number.h

echo "#ifndef VERSION_MINOR" >> src/build_number.h
echo "#define VERSION_MINOR $minor_version" >> src/build_number.h
echo "#endif" >> src/build_number.h
echo >> src/build_number.h

echo "#ifndef VERSION_STR" >> src/build_number.h
echo "#define VERSION_STR \"Ver $version  Build $buildnum - $timestamp_fmt\"" >> src/build_number.h
echo "#endif" >> src/build_number.h
echo >> src/build_number.h

echo "#ifndef VERSION_STR_SHORT" >> src/build_number.h
echo "#define VERSION_STR_SHORT \"$version.$buildnum\"" >> src/build_number.h
echo "#endif" >> src/build_number.h
echo >> src/build_number.h

git_hash="$(git rev-parse --verify HEAD)"
retval=$?
if [ $retval -eq 0 ]; then 
    echo "#ifndef GIT_HASH_STR" >> src/build_number.h
    echo "#define GIT_HASH_STR \"${git_hash}\"" >> src/build_number.h
    echo "#endif" >> src/build_number.h
    echo >> src/build_number.h
else 
    echo "#ifndef GIT_HASH_STR" >> src/build_number.h
    echo "#define GIT_HASH_STR \"Unknown\"" >> src/build_number.h
    echo "#endif" >> src/build_number.h
    echo >> src/build_number.h
fi

git_branch="$(git rev-parse --abbrev-ref HEAD)"
retval=$?
if [ $retval -eq 0 ]; then 
    echo "#ifndef GIT_BRANCH_STR" >> src/build_number.h
    echo "#define GIT_BRANCH_STR \"${git_branch}\"" >> src/build_number.h
    echo "#endif" >> src/build_number.h
    echo >> src/build_number.h
else 
    echo "#ifndef GIT_BRANCH_STR" >> src/build_number.h
    echo "#define GIT_BRANCH_STR \"Unknown\"" >> src/build_number.h
    echo "#endif" >> src/build_number.h
    echo >> src/build_number.h
fi

git_tag="$(git tag --points-at ${git_hash})"
retval=$?
if [ $retval -eq 0 ]; then 
    echo "#ifndef GIT_TAG_STR" >> src/build_number.h
    echo "#define GIT_TAG_STR \"${git_tag}\"" >> src/build_number.h
    echo "#endif" >> src/build_number.h
    echo >> src/build_number.h
else 
    echo "#ifndef GIT_TAG_STR" >> src/build_number.h
    echo "#define GIT_TAG_STR \"\"" >> src/build_number.h
    echo "#endif" >> src/build_number.h
    echo >> src/build_number.h
fi

user_name=`git config user.name`
retval=$?
if [ $retval -eq 0 ]; then 
    echo "#ifndef BUILT_BY_USER_STR" >> src/build_number.h
    echo "#define BUILT_BY_USER_STR \"$user_name\"" >> src/build_number.h
    echo "#endif" >> src/build_number.h
    echo >> src/build_number.h
else 
    echo "#ifndef BUILT_BY_USER_STR" >> src/build_number.h
    echo "#define BUILT_BY_USER_STR \"First Last\"" >> src/build_number.h
    echo "#endif" >> src/build_number.h
    echo >> src/build_number.h
fi

user_email=`git config user.email`
retval=$?
if [ $retval -eq 0 ]; then 
    echo "#ifndef BUILT_BY_EMAIL_STR" >> src/build_number.h
    echo "#define BUILT_BY_EMAIL_STR \"$user_email\"" >> src/build_number.h
    echo "#endif" >> src/build_number.h
    echo >> src/build_number.h
else 
    echo "#ifndef BUILT_BY_EMAIL_STR" >> src/build_number.h
    echo "#define BUILT_BY_EMAIL_STR \"noreply@example.com\"" >> src/build_number.h
    echo "#endif" >> src/build_number.h
    echo >> src/build_number.h
fi