#!/usr/bin/perl -w
#
# program PWB with given jic file
#

$| = 1;

my $f = shift @ARGV;

die "Cannot read $f: $!\n" unless -r $f;

my $j = `jtagconfig`;
print $j;

my $q = `quartus_pgm -l`;
print $q;

$q =~ /1\) (.*)\n/m;
my $b = $1;
print "$b\n";

my $xsof = "/opt/intelFPGA/17.0/quartus/common/devinfo/programmer/sfl_enhanced_02_02b030dd.sof";

my $cmd = "quartus_pgm -c \"$b\" -m JTAG -o \"p;$xsof\"";
print "Running $cmd\n";
system $cmd;

my $cmd = "quartus_pgm -c \"$b\" -m JTAG -o \"p;$f\"";
print "Running $cmd\n";
system $cmd;

exit 0;
#end
