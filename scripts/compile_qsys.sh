#!/bin/sh

ROOT=/opt/intelFPGA/17.0
export PATH=$PATH:$ROOT/quartus/sopc_builder/bin

SCRIPTDIR=$(dirname "$(readlink -f "$0")")
PROJECT_PATH=$(realpath --relative-to $(pwd) $SCRIPTDIR/../hdl )
QSYS_NAME="management"

ip-generate $PROJECT_PATH/$QSYS_NAME.qsys \
--remove-qsys-generate-warning \
--file-set=QUARTUS_SYNTH \
--output-dir=$PROJECT_PATH/$QSYS_NAME/synthesis \
--project-directory=$PROJECT_PATH \
--report-file=sopcinfo:$PROJECT_PATH/$QSYS_NAME.sopcinfo \
--report-file=regmap:$PROJECT_PATH/$QSYS_NAME/synthesis/$QSYS_NAME.regmap \
--report-file=qip:$PROJECT_PATH/$QSYS_NAME/synthesis/$QSYS_NAME.qip \
--report-file=debuginfo:$PROJECT_PATH/$QSYS_NAME/synthesis/$QSYS_NAME.debuginfo
