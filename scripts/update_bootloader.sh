#!/bin/sh

echo This script will update the bootloader and the ELF file inside the SOF, RPD and JIC files

cd hdl
quartus_cdb --read_settings_files=off --write_settings_files=off --update_mif alpha16
quartus_asm --read_settings_files=off --write_settings_files=off alpha16
quartus_cpf -c ../bin/alpha16_alpha16.cof

echo Done
