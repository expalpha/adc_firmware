#!/bin/sh

SCRIPTDIR=$(dirname "$(readlink -f "$0")")
BIN_PATH=$(realpath --relative-to $(pwd) $SCRIPTDIR/../bin )
PROJECT_NAME="alpha16"

sof2flash --input $BIN_PATH/$PROJECT_NAME.sof --output $BIN_PATH/$PROJECT_NAME.sof.hex --epcq
nios2-elf-objcopy -I srec $BIN_PATH/$PROJECT_NAME.sof.hex -O binary $BIN_PATH/$PROJECT_NAME.sof.bin
nios2-elf-objcopy -I srec $BIN_PATH/$PROJECT_NAME.webpkg.hex -O binary $BIN_PATH/$PROJECT_NAME.webpkg.bin
nios2-elf-objcopy -I srec $BIN_PATH/$PROJECT_NAME.elf.flash.hex -O binary $BIN_PATH/$PROJECT_NAME.elf.bin
