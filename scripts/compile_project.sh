#!/bin/sh

SCRIPTDIR=$(dirname "$(readlink -f "$0")")

$SCRIPTDIR/compile_qsys.sh
$SCRIPTDIR/compile_nios.sh
$SCRIPTDIR/clean_quartus.sh
$SCRIPTDIR/compile_quartus.sh
