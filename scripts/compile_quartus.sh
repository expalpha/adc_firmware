#!/bin/sh

ROOT=/opt/intelFPGA/17.0
export PATH=$PATH:$ROOT/quartus/bin

SCRIPTDIR=$(dirname "$(readlink -f "$0")")
PROJECT_PATH=$(realpath --relative-to $(pwd) $SCRIPTDIR/../hdl )
PROJECT_NAME="alpha16"

# go to project dir
cd $PROJECT_PATH
#quartus_sh --clean $PROJECT_NAME
/usr/bin/time quartus_sh --flow compile $PROJECT_NAME
