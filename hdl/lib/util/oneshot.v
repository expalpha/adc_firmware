module oneshot ( 
	clk,
	rst,
	d,
	q
);

input wire clk;
input wire rst; 
input wire d;
output reg q;

reg d_prev;

always@(posedge rst, posedge clk) begin 
	if(rst) begin 
		q 	<= 1'b0;
		d_prev <= 1'b0;
	end else begin 
		d_prev <= d;
		q 	<= ({d_prev,d} == 2'b01) ? 1'b1 : 1'b0;
	end 
end

endmodule

module oneshot_extend ( 
	clk,
	rst,
	d,
	q
);

parameter TICKS_TO_EXTEND = 1;

localparam SZ_TICKS = $clog2(TICKS_TO_EXTEND+1);

input wire clk;
input wire rst; 
input wire d;
output reg q;

wire start_delayer;
reg [SZ_TICKS-1:0] cnt;

oneshot u0 (
	.clk 	( clk ),
	.rst		( rst ),
	.d		( d ),
	.q 		( start_delayer )	
);

always@(posedge rst, posedge clk) begin 
	if(rst) begin 
		q 	<= 1'b0;
		cnt  <= 0;
	end else begin 
		if(start_delayer) begin 
			q 	<= 1'b1;
			cnt 	<= TICKS_TO_EXTEND[SZ_TICKS-1:0];
		end else begin 
			if(cnt > {SZ_TICKS{1'b0}}) begin
				q	 	<= 1'b1;
				cnt 	<= cnt - 1'b1;
			end else begin 
				q 	<= 1'b0;
				cnt 	<= {SZ_TICKS{1'b0}};
			end
		end
	end 
end

endmodule
