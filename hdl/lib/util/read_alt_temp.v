// This module should hopefully work with both the Arria10 and ArriaV/StratixV/StratixIV devices
module read_alt_temp (
	clk,
	rst,
	q,
	// Control lines for temperature sensor
	temp_done,
	temp_rdbk,
	temp_rst
);

parameter SZ_TEMP = 8; 

input wire 			clk;
input wire 			rst;
output reg [7:0] 	q;
input wire 			temp_done;
input wire [7:0]	temp_rdbk;
output reg 			temp_rst;

reg [1:0] r_done;

always@(posedge clk) r_done <= { r_done[0], temp_done };

always@(posedge clk, posedge rst) begin 
	if(rst) begin 
		q 					<= 8'h0;
		temp_rst 	<= 1'b1;
	end else begin 
		// On done, capture temperature
		if(r_done == 2'b01) begin 
			q 				<= temp_rdbk;
			temp_rst <= 1'b0;
		// After one clock past initial done, reset temperature sensor logic
		end else if ((r_done == 2'b11)  || (r_done == 2'b10)) begin 
			q					<= q;
			temp_rst <= 1'b1;
		// Not yet done, keep acquiring
		end else begin 
			q					<= q;
			temp_rst <= 1'b0;
		end 
	end
end 

endmodule
