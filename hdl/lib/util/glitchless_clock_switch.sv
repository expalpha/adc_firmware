module glitchless_clock_switch (
	rst,
	inclk0,
	inclk1,
	outclk,
	select
);

input wire rst;
input wire inclk0;
input wire inclk1;
output wire outclk;
input wire select;

wire select0;
wire select1;
wire and0;
wire and1;

reg sync0_0 = 1'b0;
reg sync0_1 = 1'b0;
reg sync1_0 = 1'b0;
reg sync1_1 = 1'b0;

always_comb begin 
	select0 = !select & !sync1_1;
	select1 = select & !sync0_1;
	and0    = sync0_1 & inclk0;
	and1    = sync1_1 & inclk1;
	outclk  = and0 | and1;
end

always_ff@(posedge rst, posedge inclk0) begin 
	if(rst) begin 
		sync0_0 <= 1'b0;
	end else begin 
		sync0_0 <= select0;
	end 
end

always_ff@(posedge rst, negedge inclk0) begin 
	if(rst) begin 	
		sync0_1 <= 1'b0;
	end else begin 
		sync0_1 <= sync0_0;
	end 
end 

always_ff@(posedge rst, posedge inclk1) begin 
	if(rst) begin 	
		sync1_0 <= 1'b0;
	end else begin 
		sync1_0 <= select1;
	end 
end

always_ff@(posedge rst, negedge inclk1) begin 
	if(rst) begin 	
		sync1_1 <= 1'b0;
	end else begin 
		sync1_1 <= sync1_0;	
	end
end 


endmodule 