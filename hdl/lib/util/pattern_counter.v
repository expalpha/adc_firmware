// This module can count incoming patterns, or more commonly, be used as an edge counter 
module pattern_counter (
	clk,
	clk_cap,
	rst,
	pattern,	// Pattern to match against
	d,			// Input signal to pattern match 
	q			// Output Count
);

parameter SZ_PATTERN = 0;  // This is an invalid size, so it should error if forgotten!
parameter SZ_COUNTER = 32;

input wire clk;
input wire clk_cap;
input wire rst;
input wire [SZ_PATTERN-1:0] pattern;
input wire d;
output wire [SZ_COUNTER-1:0] q;

wire rst_cap;
wire pattern_found;
reg [SZ_COUNTER-1:0] r_count;

// Synchronize reset to clk_cap
synchronizer #( 
	.SZ_DATA(1),
	.NUM_SYNC(3)
) sync_rst (
	.clk	( clk_cap ),
	.rst  ( 1'b0 ),
	.d    ( rst ),
	.q 	( rst_cap )
);

// Runs on faster capture clock, finds the pattern for us
pattern_matcher #(
	.SZ_PATTERN(SZ_PATTERN)
) matcher (
	.clk		( clk_cap ),
	.rst		( rst_cap ),
	.pattern	( pattern ),
	.d			( d ),
	.q			( pattern_found )
);

// Increment counter on found pattern, within fast capture clock domain
always@(posedge rst_cap, posedge clk_cap) begin 
	if(rst_cap) begin 
		r_count <= {SZ_COUNTER{1'b0}};
	end else begin 
		r_count <= (pattern_found == 1'b1) ? r_count + 1'b1 : r_count;
	end
end

// Send captured count back to clk domain via graycode synchronizer 
synchronizer_counter #( 
	.SZ_WIDTH(SZ_COUNTER) 
) sync_count (
	.clk			( clk ),
	.rst			( rst ),
	.d_clk		( clk_cap ),
	.d 			( r_count ),
	.q 			( q )
);

endmodule
