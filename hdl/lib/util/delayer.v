// Goes 'active' after incoming signal is 'active' for delay count time. 
// Goes 'inactive' immediately on low, and holds the counter in reset until incoming signal goes high again

module delayer (
	clk,
	rst,
	d,
	q
);

parameter DELAY_CNT	= 100;

localparam SZ_DELAY 	= $clog2(DELAY_CNT+1);

input wire clk;
input wire rst;
input wire d;
output wire q;

reg [SZ_DELAY-1:0] delay_cnt 	= 0;
reg r_q 								= 0;
reg r_d;

assign q = r_q;

// 'gate' reset input, designed for altera pll locked signal 
always@(posedge rst, posedge clk) begin 
	if(rst) begin 		
		delay_cnt 	<= {SZ_DELAY{1'b0}};
		r_q <= 1'b0;
	end else begin 
		r_d <= d;
		if(r_d) begin 			
			if(delay_cnt < DELAY_CNT[SZ_DELAY-1:0]) begin
				delay_cnt <= delay_cnt + 1'b1;
				r_q <= 1'b0;
			end else begin 
				delay_cnt <= delay_cnt;
				r_q <= 1'b1;
			end 						
		end else begin 
			delay_cnt <= {SZ_DELAY{1'b0}};
			r_q <= 1'b0;
		end 
	end 
end 

endmodule
