module pattern_matcher (
	clk,
	rst,
	pattern,
	d,
	q
);

parameter SZ_PATTERN = 0;
localparam SZ_DELAY = $clog2(SZ_PATTERN+1);

input wire clk;
input wire rst;
input wire [SZ_PATTERN-1:0] pattern;
input wire d;
output reg q;

wire pattern_found;
wire active;

reg [SZ_PATTERN-1:0] r_cap;
reg [SZ_PATTERN-1:0] r_pattern;
reg [SZ_DELAY-1:0] r_start_delay;

// Flag the pattern when found
assign pattern_found = (r_cap == r_pattern) ? 1'b1 : 1'b0;
// Delay start until we've captured SZ_PATTERN bits to avoid erronous pattern_found flagging
assign active = (r_start_delay < SZ_PATTERN[SZ_DELAY-1:0] ) ? 1'b0 : 1'b1;

// These registers must not be reset! Capture-during-reset ensures we don't accidentally match coming out of reset
always@(posedge clk) begin 
	r_pattern	<= pattern;	// Constantly capture pattern to search for	
	r_cap 		<= { r_cap[0 +: SZ_PATTERN-1], d }; // Shift in captured signal into pattern matcher, and register pattern input
end	
	
// Register output
always@(posedge rst, posedge clk) begin 
	if(rst) begin 
		q 				<= 1'b0;
		r_start_delay 	<= {SZ_DELAY{1'b0}};
	end else begin 
		q 				<= (active) ? pattern_found : 1'b0;
		r_start_delay 	<= (active) ? r_start_delay : r_start_delay + 1'b1;
	end
end 

endmodule
