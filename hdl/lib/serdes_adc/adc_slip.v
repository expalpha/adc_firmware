module adc_slip (
	clk,
	training,
	bitslip,
	pattern,
	data_in,
	aligned
);

parameter SZ_DATA = 8;

localparam SZ_CNT = 4;

input 	wire clk;
input 	wire training;
input 	wire [SZ_DATA-1:0] pattern;
input 	wire [SZ_DATA-1:0] data_in;
output 	reg aligned;
output 	reg bitslip;

reg [SZ_CNT-1:0] r_count;
wire is_aligned = (data_in == pattern) ? 1'b1 : 1'b0;

always @(posedge clk) begin 		
	r_count 	<= (is_aligned) ? {SZ_CNT{1'b0}} : r_count + 1'b1;
	aligned 	<= (is_aligned) ? 1'b1 : 1'b0;
	bitslip 	<= ((training) && (!is_aligned) && (r_count == {SZ_CNT{1'b1}})) ? 1'b1 : 1'b0;
end

endmodule
