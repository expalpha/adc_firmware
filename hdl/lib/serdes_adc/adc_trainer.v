`default_nettype none
module adc_trainer (
	clk,
	training,
	aligned,
	bitslip,	
	pattern,
	d_in
);

parameter NUM_CH = 9;
parameter SZ_CH = 8;

input wire clk;
input wire training;
input wire [NUM_CH-1:0][SZ_CH-1:0] pattern;
input wire [NUM_CH-1:0][SZ_CH-1:0] d_in;

output wire [NUM_CH-1:0] aligned;
output wire [NUM_CH-1:0] bitslip;

genvar n;
generate 
	for(n=0; n<NUM_CH; n++) begin: gen_slip
		adc_slip #(
			.SZ_DATA(SZ_CH)
		) adc_slip_inst (
			.clk		( clk ),
			.training       ( training ),
			.bitslip	( bitslip[n] ),
			.pattern	( pattern[n] ),
			.data_in	( d_in[n] ),
			.aligned	( aligned[n] )
		);
	end // gen_slip
endgenerate 

endmodule
