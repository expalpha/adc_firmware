// Basic Aavalon-ST packet mux 
// Performs a round-robin with a one channel 'look-ahead'
// Expects Req-ack (look-ahead) type FIFOs!

module st_mux (
	clk,
	rst,
	d_dat,
	d_sop,
	d_eop,
	d_val,
	d_ack,
	d_empty,
	d_req,
	
	q_ch,
	q_ch_oh,
	q_dat,
	q_sop,
	q_eop,
	q_val,
	q_empty,
	q_ack
);

parameter NUM_CH = 16;
parameter WIDTH  = 32;

localparam SZ_CH = $clog2(NUM_CH);

input wire clk;
input wire rst;
input wire [NUM_CH-1:0][WIDTH-1:0] d_dat;
input wire [NUM_CH-1:0] d_sop;
input wire [NUM_CH-1:0] d_eop;
input wire [NUM_CH-1:0] d_val;
input wire [NUM_CH-1:0] d_req;
input wire [NUM_CH-1:0][1:0] d_empty;
output wire [NUM_CH-1:0] d_ack;
	
output reg [SZ_CH-1:0] q_ch;
output reg [NUM_CH-1:0] q_ch_oh;
output reg [WIDTH-1:0] q_dat;
output reg q_sop;
output reg q_eop;
output reg q_val;
output reg [1:0] q_empty;
input wire q_ack;

wire [SZ_CH-1:0] 	granted_ch;
wire [NUM_CH-1:0] granted_ch_oh;
wire 					grant_valid;

genvar n;
generate 
for(n=0; n<NUM_CH; n++) begin : gen_ack
	assign d_ack[n] = granted_ch_oh[n] & q_ack;
end
endgenerate

basic_rr_arbiter #(
	.NUM_CH( NUM_CH )
) arbiter (
	.clk		( clk ),
	.rst		( rst ),
	.ack		( d_eop[granted_ch] && d_val[granted_ch] && grant_valid && q_ack),
	.request	( d_req ),
	.grant	( granted_ch ),
	.grant_oh( granted_ch_oh ),
	.val		( grant_valid )
);

always@(posedge rst, posedge clk) begin 
	if(rst) begin 
		q_ch		<= {SZ_CH{1'b0}};
		q_ch_oh 	<= {NUM_CH{1'b0}};
		q_sop 	<= 1'b0;
		q_eop 	<= 1'b0;
		q_dat		<= {WIDTH{1'b0}};
		q_val 	<= 1'b0;	
		q_empty	<= 2'b00;
	end else begin 
		if(q_ack) begin
			if(d_val[granted_ch] && grant_valid) begin 
				q_ch		<= granted_ch;
				q_ch_oh	<= granted_ch_oh;
				q_val 	<= d_val[granted_ch];
			end else begin
				q_ch		<= {SZ_CH{1'b0}};
				q_ch_oh 	<= {NUM_CH{1'b0}};
				q_val 	<= 1'b0;						
			end
			q_sop 	<= d_sop[granted_ch];
			q_eop 	<= d_eop[granted_ch];
			q_empty  <= d_empty[granted_ch];
			q_dat		<= d_dat[granted_ch];
		end else begin 
			q_ch		<= q_ch;
			q_ch_oh 	<= q_ch_oh;
			q_sop 	<= q_sop;
			q_eop 	<= q_eop;
			q_dat		<= q_dat;
			q_empty  <= q_empty;
			q_val 	<= q_val;					
		end
	end
end

endmodule
