module link_rx (
	rst,
	clk,
	link_status,

	rx_data,
	rx_valid,
	rx_k,
	rx_err,

	source_valid,
	source_data,
	source_sop,
	source_eop,
	source_ready,
	source_error,
	source_empty,
	
	stat_err_crc,
	stat_err_810,
	stat_pkt_rcv	
);

input  wire 		rst;
input  wire 		clk;
input  wire 		link_status;

input  wire [7:0] rx_data;
input  wire  		rx_valid;
input  wire  		rx_k;
input  wire  		rx_err;

output wire 		source_valid;
output wire [31:0] source_data;
output wire 		source_sop;
output wire 		source_eop;
output wire 		source_error;
output wire [1:0]	source_empty;
input  wire 		source_ready;

output reg [31:0] stat_err_crc;
output reg [31:0] stat_err_810;
output reg [31:0] stat_pkt_rcv;

localparam RX_STATE_IDLE 				= 9'b000000001;
localparam RX_STATE_DATA_START 		= 9'b000000010;
localparam RX_STATE_DATA0				= 9'b000000100;
localparam RX_STATE_DATA1				= 9'b000001000;
localparam RX_STATE_DATA2				= 9'b000010000;
localparam RX_STATE_DATA3				= 9'b000100000;
localparam RX_STATE_DATA_EOP_CRC0	= 9'b001000000;
localparam RX_STATE_DATA_EOP_CRC1	= 9'b010000000;
localparam RX_STATE_DATA_EOP_END		= 9'b100000000;

localparam K_IDLE 	= 8'hBC; // Use K.28.5 as IDLE
localparam K_START 	= 8'hFC; // Use K.28.7 as Start of Packet 
localparam K_END 		= 8'h3C; // Use K.28.1 as End of Packet
localparam K_CTRL		= 8'h5C; // Use K.28.2 as WAIT (do not send until ) 

reg [8:0] state;
reg [31:0] dat;
reg [31:0] r_dat;
reg val;
reg sop;
reg eop;
reg r_sop;
reg [1:0] empty;
wire rdy;

wire [7:0] d_in;
wire k_in;

assign d_in 	= rx_data;
assign k_in 	= rx_k;

assign source_valid	= val;
assign source_data	= r_dat;
assign source_sop		= r_sop;
assign source_eop		= eop;
assign source_empty	= r_empty;
assign rdy 				= source_ready;
assign source_error	= error;

// Calculate CRC16 
wire crc_rst = (state == RX_STATE_IDLE) ? 1'b1 : 1'b0; // Reset CRC when state transitions just before data is to be sent		

reg crc_ena;
reg [7:0] crc_d;

reg [1:0] r_empty;

wire [15:0] calc_crc;
reg  [15:0] data_crc;
crc16 crc (
	.clk		( clk ),
	.rst		( crc_rst ),
	.ena		( crc_ena ),
	.data_in	( crc_d ),
	.crc_out	( calc_crc )
);				

// Calculate Error
wire crc_match = (data_crc == calc_crc) ? 1'b1 : 1'b0;

reg error;

always@(posedge clk, posedge rst) begin 
	if(rst) begin 
		error <= 1'b0;
		stat_err_crc <= 0;
		stat_err_810 <= 0;		
	end else begin
		error				<= ((state == RX_STATE_DATA_EOP_END) && (!crc_match)) ? 1'b1 : 1'b0;
		stat_err_crc 	<= ((state == RX_STATE_DATA_EOP_END) && (!crc_match)) ? stat_err_crc + 1'b1 : stat_err_crc;
		stat_err_810 	<= (link_status && rx_err) ? stat_err_810 + 1'b1 : stat_err_810;		
	end
end

// Reception
always@(posedge clk, posedge rst) begin 
	if(rst) begin 
		state 			<= RX_STATE_IDLE;
		val 				<= 1'b0;
		dat  				<= 32'h0;
		sop 				<= 1'b0;
		r_dat				<= 32'h0;
		r_sop				<= 1'b0;
		eop 				<= 1'b0;
		empty				<= 2'b00;
		r_empty			<= 2'b00;
		stat_pkt_rcv 	<= 0;
	end else begin 
		data_crc	<= data_crc;
		r_dat			<= dat;
		r_sop			<= sop;
		r_empty		<= empty;
		if(link_status == 1'b0) begin 
			state	<= RX_STATE_IDLE;
			val 	<= 1'b0;
			dat  	<= 8'h0;
			sop 	<= 1'b0;
			eop 	<= 1'b0;
			empty	<= 2'b00;
		end else begin 					
			case(state)
				RX_STATE_IDLE: begin	
					if((k_in == 1'b1) && (d_in == K_START) && (rdy == 1'b1)) begin 
						state <= RX_STATE_DATA_START;
						stat_pkt_rcv <= stat_pkt_rcv + 1'b1;
					end else begin
						state <= RX_STATE_IDLE;
					end					
					val 		<= 1'b0;
					dat 		<= 8'h0;
					sop 		<= 1'b0;
					eop 		<= 1'b0;							
					crc_ena	<= 1'b0;
					empty		<= 2'b11;
				end
								
				RX_STATE_DATA_START: begin 
					// it would be invalid to send a two-byte packet, the minimum size should be at least 4-bytes!
					// we do not want to output unless we have received valid data on this frame (maintain pipeline!)
					if(k_in == 1'b1) begin 
						state 	<= RX_STATE_DATA_START;							
						crc_ena	<= 1'b0;
					end else begin 
						state 	<= RX_STATE_DATA1;							
						crc_ena	<= 1'b1;
					end
					crc_d			<= d_in;
					val 			<= 1'b0;
					dat[31:24] 	<= d_in;
					sop 			<= 1'b1;						
					eop 			<= 1'b0;	
					empty			<= 2'b11;
				end
				
				RX_STATE_DATA0: begin
					// Have to watch for the K_END symbol, anything else, we should stay here
					if(k_in == 1'b1) begin 
						state		<= (d_in == K_END) ? RX_STATE_DATA_EOP_CRC0 : RX_STATE_DATA1;
						val 		<= 1'b0;
						crc_ena	<= 1'b0;
						dat		<= dat;
						empty		<= empty;
					end else begin 
						state 		<= RX_STATE_DATA1;
						val 			<= 1'b1;
						crc_ena		<= 1'b1;
						dat[31:24] 	<= d_in;
						empty			<= 2'b11;
					end							
					crc_d			<= d_in;					
					sop 			<= 1'b0;
					eop 			<= eop;					
				end

				RX_STATE_DATA1: begin
					// Have to watch for the K_END symbol, anything else, we should stay here
					if(k_in == 1'b1) begin 
						state		<= (d_in == K_END) ? RX_STATE_DATA_EOP_CRC0 : RX_STATE_DATA2;
						crc_ena	<= 1'b0;
						dat		<= dat;
						empty		<= empty;
					end else begin 
						state 		<= RX_STATE_DATA2;						
						crc_ena		<= 1'b1;
						dat[23:16] 	<= d_in;
						empty			<= 2'b10;
					end
					val 			<= 1'b0;
					crc_d			<= d_in;
					sop 			<= sop;
					eop 			<= 1'b0;
				end

				RX_STATE_DATA2: begin
					// Have to watch for the K_END symbol, anything else, we should stay here
					if(k_in == 1'b1) begin 
						state		<= (d_in == K_END) ? RX_STATE_DATA_EOP_CRC0 : RX_STATE_DATA3;
						crc_ena	<= 1'b0;
						dat		<= dat;
						empty		<= empty;
					end else begin 
						state 	<= RX_STATE_DATA3;
						crc_ena	<= 1'b1;						
						dat[15:8]<= d_in;
						empty		<= 2'b01;
					end
					val 			<= 1'b0;					
					crc_d			<= d_in;
					sop 			<= sop;
					eop 			<= 1'b0;					
				end
				
				RX_STATE_DATA3: begin
					// Have to watch for the K_END symbol, anything else, we should stay here
					if(k_in == 1'b1) begin 
						state		<= (d_in == K_END) ? RX_STATE_DATA_EOP_CRC0 : RX_STATE_DATA0;						
						crc_ena	<= 1'b0;
						dat		<= dat;
						empty		<= empty;
					end else begin 
						state 	<= RX_STATE_DATA0;
						crc_ena	<= 1'b1;						
						dat[7:0]	<= d_in;
						empty		<= 2'b00;
					end							
					val 			<= 1'b0;					
					crc_d			<= d_in;
					sop 			<= sop;
					eop 			<= 1'b0;
				end

				RX_STATE_DATA_EOP_CRC0: begin 
					state		<= (k_in == 1'b1) ? RX_STATE_DATA_EOP_CRC0 : RX_STATE_DATA_EOP_CRC1;
					data_crc[15:8] <= d_in;					
					crc_ena	<= 1'b0;
					crc_d		<= 8'h0;
					val 		<= 1'b0;
					dat 		<= dat;
					sop 		<= 1'b0;
					eop 		<= 1'b0;
					empty		<= empty;
				end 
				
				RX_STATE_DATA_EOP_CRC1: begin 
					state		<= (k_in == 1'b1) ? RX_STATE_DATA_EOP_CRC1 : RX_STATE_DATA_EOP_END;
					data_crc[7:0] <= d_in;					
					crc_ena	<= 1'b0;
					crc_d		<= 8'h0;
					val 		<= 1'b0;
					dat 		<= dat;
					sop 		<= 1'b0;
					eop 		<= 1'b0;
					empty		<= empty;
				end
										
				RX_STATE_DATA_EOP_END: begin
					state		<= RX_STATE_IDLE;
					crc_ena	<= 1'b0;
					crc_d		<= 8'h0;
					val 		<= 1'b1;
					dat 		<= dat;
					sop 		<= 1'b0;
					eop 		<= 1'b1;
					empty		<= empty;
				end
			endcase
		end
	end
end 
		
endmodule
