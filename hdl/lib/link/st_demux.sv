module st_demux (
	clk,
	rst, 
	
	d_sel,
	d_dat,
	d_sop,
	d_eop,
	d_val,
	d_empty,
	d_ack,
	
	q_dat,
	q_sop,
	q_eop,
	q_val,
	q_empty,
	q_error,
	q_ack
);

parameter NUM_CH = 16;
parameter WIDTH  = 32;

input wire 						clk;
input wire 						rst;

input wire 	[NUM_CH-1:0]	d_sel;
input wire 	[WIDTH-1:0] 	d_dat;
input wire 	 					d_sop;
input wire 	 					d_eop;
input wire 	 					d_val;
input wire [1:0]				d_empty;
output wire  					d_ack;
	
output reg 	[NUM_CH-1:0][WIDTH-1:0] q_dat;
output reg 	[NUM_CH-1:0]	q_sop;
output reg 	[NUM_CH-1:0]	q_eop;
output reg 	[NUM_CH-1:0]	q_val;
output reg  [NUM_CH-1:0][1:0]	q_empty;
output reg  [NUM_CH-1:0] 	q_error;
input wire 	[NUM_CH-1:0]	q_ack;

reg [NUM_CH-1:0] ack;

assign d_ack = |ack;

genvar n;
generate 
	for(n=0; n<NUM_CH; n++) begin: gen_ch
		always@(posedge rst, posedge clk) begin 
			if(rst) begin 
				q_dat[n] <= {WIDTH{1'b0}};
				q_sop[n] <= 1'b0;
				q_eop[n] <= 1'b0;
				q_val[n] <= 1'b0;
				q_empty[n] <= 2'b00;
				ack[n]	<= 1'b0;			
				q_error[n] <= 1'b0;
			end else begin
				if(d_sel[n] == 1'b1) begin 
					q_dat[n] <= d_dat;
					q_sop[n] <= d_sop;
					q_eop[n] <= d_eop;
					q_val[n] <= d_val;
					q_empty[n] <= d_empty;
					ack[n]	<= q_ack[n];
					q_error[n] <= (q_ack[n] == 1'b0) ? 1'b1 : 1'b0; // going high even once during a transmission should be enough, the 'smart' fifo will take care of the rest of the packet
				end else begin 
					q_dat[n] <= {WIDTH{1'b0}};
					q_sop[n] <= 1'b0;
					q_empty[n] <= 2'b00;
					q_eop[n] <= 1'b0;
					q_val[n] <= 1'b0;
					ack[n]	<= 1'b0;
					q_error[n] <= 1'b0;
				end 
			end 
		end		
	end 
endgenerate 

endmodule
