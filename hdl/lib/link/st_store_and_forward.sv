module st_store_and_forward_fifo ( 
	clk,
	rst,
		
	snk_dat,
	snk_val,
	snk_sop,
	snk_eop,
	snk_rdy,
	snk_err,
	snk_empty,
	
	src_dat,
	src_val,
	src_sop,
	src_eop,
	src_empty,
	src_rdy,
	src_avail // indicates there is a packet to be transmitted
);

parameter DEPTH = 256;

localparam PKT_WIDTH = 36;
localparam WIDTH = 32;
localparam SZ_DEPTH = $clog2(DEPTH);
localparam ST_IDLE 		= 2'b01;
localparam ST_PACKET 	= 2'b10;

localparam ST_OUT_IDLE 		= 4'b0001;
localparam ST_OUT_DELAY0 	= 4'b0010;
localparam ST_OUT_DELAY1 	= 4'b0100;
localparam ST_OUT_RUN0		= 4'b1000;

input wire clk;
input wire rst;

input wire [WIDTH-1:0] snk_dat;
input wire snk_val;
input wire snk_sop;
input wire snk_eop;
input wire snk_err;
input wire [1:0] snk_empty;
output wire snk_rdy;

output reg [WIDTH-1:0] src_dat;
output reg src_val;
output reg src_sop;
output reg src_eop;
output reg [1:0] src_empty;
output reg src_avail;
input wire src_rdy;

reg [WIDTH-1:0] src0_dat;
reg src0_val;
reg src0_sop;
reg src0_eop;
reg [1:0] src0_empty;

reg [SZ_DEPTH-1:0] in_words;
reg [SZ_DEPTH-1:0] out_words;
reg [SZ_DEPTH-1:0] wr_pos;
reg [SZ_DEPTH-1:0] wr_start;
reg [SZ_DEPTH-1:0] rd_pos;

reg [SZ_DEPTH-1:0] in_pkts;
reg [SZ_DEPTH-1:0] out_pkts;

reg [PKT_WIDTH-1:0] wr_d; // add two for SOP, EOP
reg wr_ena;
reg [1:0] in_state;
reg [3:0] out_state;

wire [PKT_WIDTH-1:0] rd_d; // add two for SOP, EOP

assign snk_rdy = ((in_words - out_words) != {SZ_DEPTH{1'b1}}) ? 1'b1 : 1'b0;

always@(posedge rst, posedge clk) begin 
	if(rst) begin 
		in_words 	<= {SZ_DEPTH{1'b0}};
		out_words 	<= {SZ_DEPTH{1'b0}};
		wr_pos 		<= {SZ_DEPTH{1'b1}}; // rolled over on first write!
		wr_start 	<= {SZ_DEPTH{1'b1}};
		rd_pos 		<= {SZ_DEPTH{1'b0}};
		in_pkts		<= {SZ_DEPTH{1'b0}};		
		out_pkts		<= {SZ_DEPTH{1'b0}};
		src_val		<= 1'b0;
		wr_ena		<= 1'b0;		
		in_state    <= ST_IDLE;
		out_state	<= ST_OUT_IDLE;
		src_avail	<= 1'b0;
	end else begin 	
		case(in_state)
			ST_IDLE: begin 
				// waiting for SOP 
				if(snk_val && snk_sop && snk_rdy) begin
					// if this is a 4 byte packet, end immediately
					in_state 	<= (snk_eop) ? ST_IDLE : ST_PACKET;
					in_words 	<= in_words + 1'b1;
					wr_ena 		<= 1'b1;
					wr_pos 		<= wr_pos + 1'b1;
					wr_start 	<= wr_pos;
				end else begin 
					in_state 	<= ST_IDLE;
					in_words 	<= in_words;
					wr_ena 		<= 1'b0;
					wr_pos 		<= wr_pos;
					wr_start 	<= wr_start;
				end 				
				wr_d   			<= {snk_sop, snk_eop, snk_empty, snk_dat};
				in_pkts 			<= in_pkts;
			end 
				
			ST_PACKET: begin 
				if(snk_val && snk_rdy) begin 
					// ERROR, SOP received wile in packet 
					if(snk_sop || snk_err) begin 
						in_state <= ST_IDLE;
						in_pkts 	<= in_pkts;
						in_words <= in_words - (wr_pos - wr_start);
						wr_ena 	<= 1'b0;
						wr_pos 	<= wr_start;
					end else begin 
						in_state <= (snk_eop) ? ST_IDLE : ST_PACKET;
						in_pkts 	<= (snk_eop) ? in_pkts + 1'b1 : in_pkts;
						in_words <= in_words + 1'b1;
						wr_ena 	<= 1'b1;
						wr_pos 	<= wr_pos + 1'b1;
					end 
					wr_d   		<= {snk_sop, snk_eop, snk_empty, snk_dat};
				end else begin 
					in_state <= ST_PACKET;
					in_pkts 	<= in_pkts;
					in_words <= in_words;
					wr_ena 	<= 1'b0;
					wr_d   	<= wr_d;
					wr_pos 	<= wr_pos;
				end 
				wr_start <= wr_start;
			end 
		endcase
		
		case(out_state)
			ST_OUT_IDLE: begin 
				// if we have data available, start the pipeline , not yet valid
				if(in_pkts != out_pkts) begin
					src_avail	<= 1'b1;
					rd_pos  		<= rd_pos + 1'b1;
					out_state 	<= ST_OUT_DELAY0;	
				end else begin 
					src_avail	<= 1'b0;	
					rd_pos  		<= rd_pos;
					out_state 	<= ST_OUT_IDLE;	
				end 
				src_val 		<= 1'b0;
				out_words 	<= out_words;
				src_dat 		<= rd_d[WIDTH-1:0];
				src_empty 	<= rd_d[WIDTH+1:WIDTH];
				src_sop 		<= rd_d[WIDTH+3];
				src_eop 		<= rd_d[WIDTH+2];					
			end 
			
			ST_OUT_DELAY0: begin 
				// continue to build pipeline, not yet valid 
				src_avail	<= 1'b1;
				rd_pos  		<= rd_pos + 1'b1;
				out_state 	<= ST_OUT_DELAY1;	
				out_words 	<= out_words;
				src_val 		<= 1'b1;
				src_dat 		<= rd_d[WIDTH-1:0];
				src_empty 	<= rd_d[WIDTH+1:WIDTH];
				src_sop 		<= rd_d[WIDTH+3];
				src_eop 		<= rd_d[WIDTH+2];		
			end 
			
			ST_OUT_DELAY1: begin 
				// stall the pipeline, output first cached data until we get a ready signal
				if(src_rdy) begin 
					if(src_eop) begin 
						out_pkts <= out_pkts + 1'b1;
						if(out_pkts == (in_pkts - 1'b1)) begin 
							rd_pos  		<= rd_pos - 1'h1;			
							out_state 	<= ST_OUT_IDLE;
							src_val	 	<= 1'b0;
							src_avail	<= 1'b0;	
						end else begin 
							rd_pos  		<= rd_pos + 1'b1;			
							out_state 	<= ST_OUT_RUN0;						
							src_val	 	<= 1'b1;
							src_avail	<= 1'b1;
						end 	
					end else begin 
						rd_pos  		<= rd_pos + 1'b1;			
						out_state 	<= ST_OUT_RUN0;
						src_val	 	<= 1'b1;
						src_avail	<= 1'b1;
					end 
					out_words 	<= out_words + 1'b1;					
				end else begin 
					rd_pos  		<= rd_pos - 2'h2;				
					out_state 	<= ST_OUT_IDLE;
					out_words 	<= out_words;
					src_val	 	<= 1'b0;
					src_avail	<= 1'b1;
				end 	
				
				src_dat 		<= rd_d[WIDTH-1:0];
				src_empty 	<= rd_d[WIDTH+1:WIDTH];
				src_sop 		<= rd_d[WIDTH+3];
				src_eop 		<= rd_d[WIDTH+2];					
			end 
			
			ST_OUT_RUN0: begin 
				if(src_rdy) begin 
					if(src_eop) begin 
						out_pkts <= out_pkts + 1'b1;
						if(out_pkts == (in_pkts - 1'b1)) begin 
							rd_pos  		<= rd_pos - 1'h1;			
							out_state 	<= ST_OUT_IDLE;
							src_val	 	<= 1'b0;
							src_avail	<= 1'b0;	
						end else begin 
							rd_pos  		<= rd_pos + 1'b1;			
							out_state 	<= ST_OUT_RUN0;						
							src_val	 	<= 1'b1;
							src_avail	<= 1'b1;
						end 	
					end else begin 
						rd_pos  		<= rd_pos + 1'b1;			
						out_state 	<= ST_OUT_RUN0;
						src_val	 	<= 1'b1;
						src_avail	<= 1'b1;
					end 
					out_words 	<= out_words + 1'b1;					
				end else begin 
					rd_pos  		<= rd_pos - 2'h2;				
					out_state 	<= ST_OUT_IDLE;
					out_words 	<= out_words;
					src_val	 	<= 1'b0;
					src_avail	<= 1'b1;
				end 		
				src_dat 		<= rd_d[WIDTH-1:0];
				src_empty 	<= rd_d[WIDTH+1:WIDTH];
				src_sop 		<= rd_d[WIDTH+3];
				src_eop 		<= rd_d[WIDTH+2];	
			end 			
		endcase
	end 
end 
	
simple_dual_port_ram #(
	.ADDR_WIDTH( SZ_DEPTH ),
	.DATA_WIDTH( PKT_WIDTH )
) mem (
	.clk		( clk ),
	.we		( wr_ena ),
	.waddr	( wr_pos ),
	.raddr	( rd_pos ),
	.d			( wr_d ),
	.q			( rd_d )
);

endmodule 

