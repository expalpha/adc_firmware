// Quartus Prime SystemVerilog Template
//
// Simple Dual-Port RAM with different read/write addresses and single read/write clock
// and with a control for writing single bytes into the memory word; byte enable

module simple_dual_port_ram ( 
	clk,
	we,
	waddr,
	raddr,
	d, 
	q
);

parameter ADDR_WIDTH = 6;
parameter DATA_WIDTH = 32;

localparam int WORDS = 1 << ADDR_WIDTH;

input  wire clk;
input  wire we;
input  wire [ADDR_WIDTH-1:0] waddr;
input  wire [ADDR_WIDTH-1:0] raddr;
input  wire [DATA_WIDTH-1:0] d;
output wire [DATA_WIDTH-1:0] q;

// use a multi-dimensional packed array to model individual bytes within the word
logic [DATA_WIDTH-1:0] ram [0:WORDS-1];
reg 	[ADDR_WIDTH-1:0] addr_reg;

assign q = ram[addr_reg];

always_ff@(posedge clk) begin
	if(we) begin
		ram[waddr] <= d;
	end
	addr_reg <= raddr;
end

endmodule
