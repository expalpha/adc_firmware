module st_router (
	clk,	
	rst,
	
	ch_net,
	src_id,
	dst_id,
	dst_id_val,
	ch_sel,
	msg_ch,

	tx_dat,
	tx_empty,	
	tx_sop,
	tx_eop,
	tx_val,
	tx_rdy,
	tx_error,
	
	rx_dat,
	rx_sop,
	rx_eop,
	rx_val,
	rx_empty,
	rx_avail,
	rx_rdy
);


parameter NUM_CH = 1;

localparam SZ_CH = $clog2(NUM_CH);

localparam SZ_DATA = 32;

input wire clk;
input wire rst;


output wire [NUM_CH-1:0][SZ_DATA-1:0] tx_dat;
output wire [NUM_CH-1:0] tx_sop;
output wire [NUM_CH-1:0] tx_eop;
output wire [NUM_CH-1:0] tx_val;
output wire [NUM_CH-1:0][1:0] tx_empty;
output wire [NUM_CH-1:0] tx_error;
input  wire [NUM_CH-1:0] tx_rdy;

output wire [15:0] dst_id; // DestinationID of current packet 
output wire [15:0] src_id;
input  wire [NUM_CH-1:0][15:0] ch_net;
output wire	dst_id_val;
input  wire [NUM_CH-1:0] ch_sel;

input  wire [NUM_CH-1:0][SZ_DATA-1:0] rx_dat;
input  wire [NUM_CH-1:0] rx_sop;
input  wire [NUM_CH-1:0] rx_eop;
input  wire [NUM_CH-1:0] rx_val;
input  wire [NUM_CH-1:0][1:0] rx_empty;
input  wire [NUM_CH-1:0] rx_avail;
output wire [NUM_CH-1:0] rx_rdy;
output wire [SZ_CH-1:0] msg_ch;


wire [NUM_CH-1:0][SZ_DATA-1:0] tx_temp_dat;

wire [NUM_CH-1:0]		into_router_sel;
wire [SZ_DATA-1:0] 	into_router_dat;
wire 						into_router_sop;
wire						into_router_eop;
wire						into_router_val;
wire						into_router_rdy;
wire [1:0]				into_router_empty;

assign dst_id_val = into_router_val & into_router_sop;
assign dst_id = into_router_dat[31:16];
assign src_id = into_router_dat[15:0];

// Mux packets into the router decision maker 
st_mux #(
	.NUM_CH( NUM_CH ),
	.WIDTH( SZ_DATA )	
) rx_mux (
	.clk		( clk ),
	.rst		( rst ),
	.d_dat	( rx_dat ),
	.d_sop	( rx_sop ),
	.d_eop	( rx_eop ),
	.d_empty	( rx_empty ),
	.d_val	( rx_val ),
	.d_req 	( rx_avail ),
	.d_ack	( rx_rdy ),
	.q_ch		( msg_ch ), // which channel message came from 
	.q_ch_oh	( into_router_sel  ), // one-hot of which channel it came from 
	.q_dat	( into_router_dat ),
	.q_sop	( into_router_sop ),
	.q_eop	( into_router_eop ),
	.q_empty ( into_router_empty ),
	.q_val	( into_router_val ),
	.q_ack	( 1'b1 )					// we always send through the router, it is up to the receiver to keep up! (or lose packets)
);

reg [NUM_CH-1:0] 	from_router_sel;
reg [NUM_CH-1:0]	into_router_sel0;
reg [SZ_DATA-1:0] from_router_dat;
reg 					from_router_sop;
reg 					from_router_eop;
reg [1:0]			from_router_empty;
reg 					from_router_val;
reg [SZ_DATA-1:0] from_router_dat0;
reg 					from_router_sop0;
reg 					from_router_eop0;
reg [1:0]			from_router_empty0;
reg 					from_router_val0;

// Select the channel to send to, must mask off incoming channel (not allowed to send to where we came within here, to prevent eternal broadcasts)
always@(posedge rst, posedge clk) begin 
	if(rst) begin 
		from_router_sel 	<= {NUM_CH{1'b0}};
		from_router_dat 	<= {SZ_DATA{1'b0}};
		from_router_sop 	<= 1'b0;
		from_router_eop 	<= 1'b0;
		from_router_val 	<= 1'b0;
		from_router_empty <= 2'b00;
	end else begin		
		into_router_sel0	<= into_router_sel;
		from_router_dat0 	<= into_router_dat;
		from_router_sop0 	<= into_router_sop;
		from_router_eop0 	<= into_router_eop;
		from_router_val0 	<= into_router_val;		
		from_router_empty0 <= into_router_empty;
		
		from_router_sel  	<= (~into_router_sel0) & ch_sel; // enforce a channel never sending to itself
		from_router_dat 	<= from_router_dat0;
		from_router_sop 	<= from_router_sop0;
		from_router_eop 	<= from_router_eop0;
		from_router_val 	<= from_router_val0;		
		from_router_empty <= from_router_empty0;		
	end
end 

// Router makes a decision, and sends the packet to one or all (minus sender) destinations!
// Demux router packet into appropriate channel
// The router never 'stalls', it always sends, regardless if the channel is ready or not! Otherwise we risk lockup
// FIFO full issues must be resolved elsewhere, anything connecting must do its best to avoid droppage
// Droppage must be counted per channel 
// A transmission must not be partially grabbed, but this is up to the connecting component to resolve
st_demux #(
	.NUM_CH( NUM_CH ),
	.WIDTH( SZ_DATA )	
) tx_demux (
	.clk		( clk ),
	.rst		( rst ),
	.d_sel	( from_router_sel ),
	.d_dat	( from_router_dat ),
	.d_sop	( from_router_sop ),
	.d_eop	( from_router_eop ),
	.d_val	( from_router_val ),
	.d_empty ( from_router_empty ),
	.d_ack	( ),
	.q_dat	( tx_temp_dat ),
	.q_sop	( tx_sop ),
	.q_eop	( tx_eop ),
	.q_val	( tx_val ),
	.q_empty ( tx_empty ),
	.q_error	( tx_error ),
	.q_ack	( tx_rdy )
);

genvar n;
generate 
for(n=0; n<NUM_CH; n++) begin : gen_set_src_id 
	assign tx_dat[n][31:16] = tx_temp_dat[n][31:16];
	assign tx_dat[n][15:0]  = ((tx_sop[n] == 1'b1) && (tx_temp_dat[n][31:16] == 16'hFFFF)) ? tx_temp_dat[n][15:0] | ch_net[n] : tx_temp_dat[n][15:0];
end 
endgenerate 

endmodule
