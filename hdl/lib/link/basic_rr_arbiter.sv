module basic_rr_arbiter (
	clk,
	rst,
	ack,
	request,
	grant,
	grant_oh,
	val
);

parameter NUM_CH = 2;

localparam SZ_CH = $clog2(NUM_CH);

input wire 					clk;
input wire 					rst;
input wire 					ack;
input wire [NUM_CH-1:0] request;
output reg [SZ_CH-1:0] 	grant;
output reg [NUM_CH-1:0] grant_oh;
output reg 					val;

reg [SZ_CH-1:0] 	next_ch;
reg [NUM_CH-1:0] 	next_oh;
reg 					found;
reg 					restart;
reg [NUM_CH-1:0] 	mask_ch;

wire [NUM_CH-1:0] masked_requests;

genvar n;
generate
for(n=0; n<NUM_CH; n++) begin : mask_requests
	assign masked_requests[n] = request[n] & (~mask_ch[n]);
end 
endgenerate

// Requestor Search Logic
always@(posedge rst, posedge clk) begin 
	if(rst) begin 
		next_ch 	<= {SZ_CH{1'b0}};
		next_oh	<= {{(NUM_CH-1){1'b0}},1'b1};
		found		<= 1'b0;
	end else begin 
		// Search for next requestor
		if(!found) begin 
			if(masked_requests[next_ch] == 1'b1) begin 
				found 	<= 1'b1;
				next_ch 	<= next_ch;
				next_oh	<= next_oh;
			end else begin 
				found		<= 1'b0;
				next_ch 	<= (next_ch < (NUM_CH-1)) ? next_ch + 1'b1 : {SZ_CH{1'b0}};
				next_oh	<= {next_oh[NUM_CH-2:0], next_oh[NUM_CH-1]};
			end
		// We're in a 'found' state 
		end else begin 
			if(restart || (masked_requests[next_ch] == 1'b0)) begin 
				found 	<= 1'b0;
				next_ch 	<= (next_ch < (NUM_CH-1)) ? next_ch + 1'b1 : {SZ_CH{1'b0}};
				next_oh	<= {next_oh[NUM_CH-2:0], next_oh[NUM_CH-1]};	// left shift next_oh with wrap 			
			end else begin 
				found 	<= 1'b1;
				next_ch 	<= next_ch;
				next_oh	<= next_oh;
			end
		end
	end 
end 

always@(posedge rst, posedge clk) begin 
	if(rst) begin 
		grant 	<= {SZ_CH{1'b0}};
		val 		<= 1'b0;	
		restart 	<= 1'b0;
		grant_oh <= {NUM_CH{1'b0}};
		mask_ch 	<= {NUM_CH{1'b0}};
	end else begin 		
		if(ack) begin 
			// Grab last channel used, on startup, this will be zero, so channel 0 will have temporary priority 
			// switch to the next found channel
			if(found && (request[next_ch] == 1'b1)) begin 
				grant 	<= next_ch;
				grant_oh <= next_oh;
				mask_ch	<= next_oh;
				val		<= 1'b1;
				restart 	<= 1'b1;
			// continue search 
			end else begin 
				grant 	<= {SZ_CH{1'b0}};
				grant_oh <= {NUM_CH{1'b0}};
				mask_ch 	<= {NUM_CH{1'b0}};			
				val 		<= 1'b0;
				restart 	<= 1'b0;
			end
		end else begin 
			// Switch to found channel
			if(!val && found && (request[next_ch] == 1'b1)) begin 
				grant 	<= next_ch;
				grant_oh <= next_oh;
				mask_ch	<= next_oh;
				val 		<= 1'b1;
				restart 	<= 1'b1;
			// Last channel has data, go right back to it since we haven't yet found a new channel to switch to!
			end else if(!val && !found && (request[grant] == 1'b1) && (grant_oh != {SZ_CH{1'b0}}))  begin 
				grant 	<= grant;
				grant_oh <= grant_oh;
				mask_ch	<= mask_ch;
				val 		<= 1'b1;
				restart 	<= 1'b0;		
			// Hold steady (either transmitting or still searching)
			end else begin 
				grant 	<= grant;
				grant_oh	<= grant_oh;	
				mask_ch	<= mask_ch;
				val 		<= val;
				restart 	<= 1'b0;
			end
		end 
	end 		
end 

endmodule 
