module link_tx (
	rst,
	clk,
	link_status,

	tx_data,
	tx_ena,
	tx_k,
	sink_valid,
	sink_data,
	sink_sop,
	sink_eop,
	sink_empty,
	sink_ready,
	
	stat_pkts_sent
);

// TX States
localparam TX_STATE_IDLE 			= 8'b00000001;
localparam TX_STATE_DATA0 			= 8'b00000010;
localparam TX_STATE_DATA1			= 8'b00000100;
localparam TX_STATE_DATA2 			= 8'b00001000;
localparam TX_STATE_DATA3			= 8'b00010000;
localparam TX_STATE_EOP 			= 8'b00100000;
localparam TX_STATE_EOP_CRC0		= 8'b01000000;
localparam TX_STATE_EOP_CRC1		= 8'b10000000;

localparam K_IDLE 		= 8'hBC; // Use K.28.5 as IDLE
localparam K_START 		= 8'hFC; // Use K.28.7 as Start of Packet 
localparam K_END 			= 8'h3C; // Use K.28.1 as End of Packet
localparam K_CTRL			= 8'h5C; // Use K.28.2 as CTRL, currently ACK response

input  wire 		rst;
input  wire 		clk;
input  wire 		link_status;

output wire	[7:0] tx_data;
output wire 		tx_ena;
output wire 		tx_k;
input  wire 		sink_valid;
input  wire [31:0] sink_data;
input  wire 		sink_sop;
input  wire 		sink_eop;
input  wire [1:0] sink_empty;
output wire 		sink_ready;

output reg [31:0] stat_pkts_sent;

reg  [3:0]	ackID;				
reg  [11:0] state;
reg  [2:0] 	ctrl_state;
reg 			rdy;
wire [31:0]	dat;
wire 			eop;
wire [1:0]	empty;
reg [7:0] 	q;
reg 			k;

assign sink_ready = rdy;
assign dat = r_d[31:0];
assign eop = r_d[34];
assign empty = r_d[33:32];
assign tx_data = q;
assign tx_k    = k;
assign tx_ena  = 1'b1;

// Calculate CRC16 
reg crc_rst; //= (state == TX_STATE_START) ? 1'b1 : 1'b0; // Reset CRC when state transitions just before data is to be sent		
reg crc_ena;// = (((state == TX_STATE_DATA) && (val == 1'b1) ) || (state == TX_STATE_EOP))  ? 1'b1 : 1'b0; // Only update CRC when valid data is presented
wire [15:0] dat_crc;

// Calculate Data Length		
reg [35:0] r_d;
			

crc16 crc (
	.clk		( clk ),
	.rst		( crc_rst ),
	.ena		( crc_ena ),
	.data_in	( q ),
	.crc_out	( dat_crc )
);		

always@(posedge clk, posedge rst) begin 
	if(rst) begin 
		stat_pkts_sent <= 0;
		state	<= TX_STATE_IDLE;				
		rdy	<= 1'b0;				
		q 		<= K_IDLE;
		k 		<= 1'b1;
		crc_rst	<= 1'b1;
		crc_ena 	<= 1'b0;										
	end else begin
		r_d <= r_d;		
		rdy <= 1'b0;
		// if the link goes down, discard messages 
		if(link_status == 1'b0) begin 
			state <= TX_STATE_IDLE;
			q 		<= K_IDLE;
			k 		<= 1'b1;
			rdy 	<= 1'b1;
			crc_rst	<= 1'b1;
			crc_ena 	<= 1'b0;
		end else begin 	
			case(state)
				TX_STATE_IDLE: begin	
					crc_rst	<= 1'b1;
					crc_ena 	<= 1'b0;
					// Send receipt if we haven't yet, and if we are prepared to receive more messages!
					// Otherwise we will delay receipt until we can (or until time out occurs)
					if((sink_valid == 1'b1) && (sink_sop == 1'b1)) begin 
						state <= TX_STATE_DATA0;
						q		<= K_START;
						k		<= 1'b1;	
						rdy 	<= (rdy) ? 1'b0 : 1'b1; // toggle ready/ack if necessary so we pre-advance to next item
						r_d   <= { sink_sop, sink_eop, sink_empty, sink_data }; // dat will be the NEXT item in the FIFO after this clock cycle, so hold on to the word 
						stat_pkts_sent <= stat_pkts_sent + 1'b1;
					// Link is up, but there is no valid start of packet, we should allow the FIFO to flow until start condition is met					
					end else begin 
						state	<= TX_STATE_IDLE;
						rdy	<= 1'b1;
						q		<= K_IDLE;
						k		<= 1'b1;
					end
				end	

				TX_STATE_DATA0: begin
					crc_rst	<= 1'b0;
					crc_ena	<= 1'b1;
					state <= ((eop == 1'b1) && (empty == 2'b11)) ? TX_STATE_EOP : TX_STATE_DATA1;
					q		<= dat[31:24];
					k		<= 1'b0;					
				end
				
				TX_STATE_DATA1: begin
					crc_rst	<= 1'b0;
					crc_ena 	<= 1'b1;				
					state <= ((eop == 1'b1) && (empty == 2'b10)) ? TX_STATE_EOP : TX_STATE_DATA2;
					q		<= dat[23:16];
					k		<= 1'b0;					
				end
				
				TX_STATE_DATA2: begin
					crc_rst	<= 1'b0;
					crc_ena 	<= 1'b1;				
					state <= ((eop == 1'b1) && (empty == 2'b01)) ? TX_STATE_EOP : TX_STATE_DATA3;
					q		<= dat[15:8];
					k		<= 1'b0;	
				end
				
				TX_STATE_DATA3: begin
					crc_rst	<= 1'b0;
					crc_ena 	<= 1'b1;				
					state <= ((eop == 1'b1) && (empty == 2'b00)) ? TX_STATE_EOP : TX_STATE_DATA0;
					rdy	<= ((eop == 1'b1) && (empty == 2'b00)) ? 1'b0 : 1'b1;
					r_d   <= { sink_sop, sink_eop, sink_empty, sink_data }; // dat will be the NEXT item in the FIFO after this clock cycle, so hold on to the word 
					q		<= dat[7:0];
					k		<= 1'b0;	
				end
				
				TX_STATE_EOP: begin
					crc_rst	<= 1'b0;
					crc_ena 	<= 1'b0;								
					state	<= TX_STATE_EOP_CRC0;
					q		<= K_END;
					k		<= 1'b1;			
				end
				
				TX_STATE_EOP_CRC0: begin
					crc_rst	<= 1'b0;
					crc_ena 	<= 1'b0;								
					state	<= TX_STATE_EOP_CRC1;
					q		<= dat_crc[15:8];
					k		<= 1'b0;			
				end
				
				TX_STATE_EOP_CRC1: begin
					crc_rst	<= 1'b0;
					crc_ena 	<= 1'b0;								
					state	<= TX_STATE_IDLE;
					q		<= dat_crc[7:0];
					k		<= 1'b0;			
				end
			endcase
		end
	end
end

endmodule
