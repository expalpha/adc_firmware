module link_sar (
	link_status,
	
	tx_clk,
	tx_rst,
	tx_data,
	tx_ena,
	tx_k,
	
	rx_clk,
	rx_rst,
	rx_data,
	rx_valid,
	rx_k,
	rx_err,
	
	sink_error,
	sink_valid,
	sink_data,
	sink_sop,
	sink_eop,
	sink_ready,
	sink_empty,
	
	source_valid,
	source_data,
	source_sop,
	source_eop,
	source_ready,
	source_avail,
	source_empty,
	
	stat_err_crc,
	stat_err_810,
	stat_pkt_rcv,
	stat_pkts_sent	
);

parameter TX_FIFO_DEPTH = 1024;
parameter RX_FIFO_DEPTH = 1024;

input  wire 		 link_status;

input  wire 		 tx_clk;
input  wire			 tx_rst;
output wire	[7:0]  tx_data;
output wire 		 tx_ena;
output wire 		 tx_k;

input  wire        rx_clk;
input  wire        rx_rst;
input  wire [7:0]  rx_data;
input  wire  		 rx_valid;
input  wire  		 rx_k;
input  wire  		 rx_err;

input  wire 		 sink_error;
input  wire 		 sink_valid;
input  wire [31:0] sink_data;
input  wire 		 sink_sop;
input  wire 		 sink_eop;
output wire 		 sink_ready;
input wire [1:0]   sink_empty;

output wire 		 source_valid;
output wire [31:0] source_data;
output wire 		 source_sop;
output wire 		 source_eop;
output wire [1:0]  source_empty;
output wire 		 source_avail;
input  wire 		 source_ready;

output wire [31:0] stat_err_crc;
output wire [31:0] stat_err_810;
output wire [31:0] stat_pkt_rcv;
output wire [31:0] stat_pkts_sent;	

wire rx_src_val;
wire [31:0] rx_src_dat;
wire rx_src_sop;
wire rx_src_eop;
wire rx_src_rdy;
wire rx_src_err;
wire [1:0] rx_src_empty;

wire [31:0] tx_src_dat;
wire tx_src_val;
wire tx_src_sop;
wire tx_src_eop;
wire tx_src_rdy;
wire [1:0] tx_src_empty;

st_store_and_forward_fifo #(
	.DEPTH( TX_FIFO_DEPTH )
) tx_smart_fifo (
	.clk			( tx_clk ),
	.rst			( tx_rst | ~link_status ),
	
	.snk_dat		( sink_data ),
	.snk_val		( sink_valid ),
	.snk_sop		( sink_sop ),
	.snk_eop		( sink_eop ),
	.snk_rdy		( sink_ready ),
	.snk_err		( sink_error ),
	.snk_empty	( sink_empty ),
	
	.src_dat		( tx_src_dat ),
	.src_val		( tx_src_val ),
	.src_sop		( tx_src_sop ),
	.src_eop		( tx_src_eop ),
	.src_rdy		( tx_src_rdy ),
	.src_empty	( tx_src_empty )
);

link_tx tx ( 
	.rst				( tx_rst ),
	.clk				( tx_clk ),
	.link_status	( link_status ),
	.tx_data			( tx_data ),
	.tx_ena			( tx_ena ),
	.tx_k				( tx_k ),
	.sink_valid		( tx_src_val),
	.sink_data		( tx_src_dat ),
	.sink_empty 	( tx_src_empty ),
	.sink_sop		( tx_src_sop ),
	.sink_eop		( tx_src_eop ),
	.sink_ready		( tx_src_rdy ),
	.stat_pkts_sent( stat_pkts_sent )
);

link_rx rx (
	.rst				( rx_rst ),
	.clk				( rx_clk),
	.link_status	( link_status ),
	.rx_data			( rx_data ),
	.rx_valid		( rx_valid ),
	.rx_k				( rx_k ),
	.rx_err			( rx_err ),	
	.source_valid	( rx_src_val ),
	.source_data	( rx_src_dat ),
	.source_sop		( rx_src_sop ),
	.source_eop		( rx_src_eop ),
	.source_ready	( rx_src_rdy ),
	.source_empty	( rx_src_empty ),
	.source_error	( rx_src_err ),
	.stat_err_crc	( stat_err_crc ),
	.stat_err_810	( stat_err_810 ),
	.stat_pkt_rcv	( stat_pkt_rcv )
);

st_store_and_forward_fifo #(
	.DEPTH( RX_FIFO_DEPTH )
) rx_smart_fifo (
	.clk			( rx_clk ),
	.rst			( rx_rst | ~link_status ),
	
	.snk_dat		( rx_src_dat ),
	.snk_val		( rx_src_val ),
	.snk_sop		( rx_src_sop ),
	.snk_eop		( rx_src_eop ),
	.snk_rdy		( rx_src_rdy ),
	.snk_err		( rx_src_err ),
	.snk_empty	( rx_src_empty ),
	
	.src_dat		( source_data ),
	.src_val		( source_valid ),
	.src_sop		( source_sop ),
	.src_eop		( source_eop ),
	.src_empty	( source_empty ),
	.src_avail	( source_avail ),
	.src_rdy		( source_ready )
);

endmodule
