/* 
	Packet Length Prepender
	
	Takes in a Avalon-ST packet and prepends the length of the packet
	shifting the avalon-st packet bytes by 16 bits. Stores packets 
	before transmission (due to need to count), up to NUM_PACKETS. 
	
	Purpose is for use with Altera UDP Offloader, as it requires
	the length of a packet in bytes to be given in the upper 16 bits of the
	first word. 
	
	A variable one or two cycle delay occurs between receiving a packet, and it's availability to be read out, 
		1 cycle if the last word is 1 or 2 bytes (empty > 1), 2 cycles otherwise ('overflow' write is required)
	
	Conditions Handled:
	- SOP within packet, if the Avalon-ST stream sends a SOP before an EOP, a 'restart' occurs,
		discarding the 'old' packet, and starting from where the new SOP has been received
	
	- length exceeded, if the packet exceeds the maximum length given by PKT_DEPTH, the packet will be discarded,
		and will continue to discard words until a SOP is received again
		
	- single word SOP+EOP packets are allowed
	
*/

module packet_length_prepender (
	clk,
	rst,
	
	in_sop,
	in_eop,
	in_dat,
	in_val,
	in_empty,
	in_rdy,

	out_sop,
	out_eop,
	out_val,
	out_dat,
	out_empty,
	out_rdy
);

parameter PKT_DEPTH = 368; // defaults to correct size for 1500 MTU
parameter NUM_PACKETS = 2;

localparam PKT_DEPTH_IN_BYTES = PKT_DEPTH * 4;

// Set memory to power of 2
localparam DEPTH = 2**$clog2((PKT_DEPTH+1)*NUM_PACKETS); // we need an extra word available per packet
localparam SZ_DATA = 32;
localparam SZ_ST_WIDTH = SZ_DATA + 2 + $clog2(SZ_DATA/8); // DATA + SOP,EOP + EMPTY
localparam SZ_DEPTH = $clog2(DEPTH);
localparam SZ_CNT = $clog2(DEPTH+1);
localparam SZ_PKTS = $clog2(NUM_PACKETS+1);

input wire clk;
input wire rst;
input wire in_sop;
input wire in_eop;
input wire [SZ_DATA-1:0] in_dat;
input wire in_val;
input wire [1:0] in_empty;
output reg in_rdy;

output reg out_val;
output wire out_sop;
output wire out_eop;
output wire [SZ_DATA-1:0] out_dat;
output wire [1:0] out_empty;
input wire out_rdy;

wire in_captured = in_val && in_rdy;
wire out_captured = out_val && out_rdy;
wire [15:0] in_count = 3'd4 - in_empty;
reg [SZ_ST_WIDTH-1:0] wr_dat;
wire [SZ_ST_WIDTH-1:0] rd_dat;
wire [SZ_DATA-1:0] in_data_masked;

reg wr_ena;
reg [SZ_DEPTH-1:0] wr_pos;
reg [SZ_DEPTH-1:0] rd_pos;
reg [SZ_DEPTH-1:0] wr_start;
reg [15:0] wr_cnt;
reg [15:0] prev_dat;
reg [15:0] first_dat;

reg [SZ_ST_WIDTH-1:0] r_data [DEPTH-1:0];

reg [SZ_PKTS-1:0] in_pkts;
reg [SZ_PKTS-1:0] out_pkts;

enum int unsigned { ST_IDLE = 0, ST_DATA = 2, ST_OVERFLOW = 4, ST_WR_LEN = 8 } in_state;

assign out_sop = rd_dat[35];
assign out_eop = rd_dat[34];
assign out_empty = rd_dat[33:32];
assign out_dat = rd_dat[31:0];

// mask off input data based on in_empty, to enforce zero padding
assign in_data_masked = 
	(in_empty == 2'b00 ) ? in_dat[31:0] : 
	(in_empty == 2'b01 ) ? {in_dat[31:8],   8'h0} : 
	(in_empty == 2'b10 ) ? {in_dat[31:16], 16'h0} : {in_dat[31:24], 24'h0};

// inferred ram block with pass through logic to match read-during-write behaviour
//assign rd_dat = (out_val) ? r_data[rd_pos] : 36'h0;
assign rd_dat = (out_val) ?  ((rd_pos == wr_pos) && wr_ena) ? wr_dat : r_data[rd_pos] : 36'h0;
always@(posedge clk) begin 
	if(wr_ena) 
		r_data[wr_pos] <= wr_dat;
end

// read side logic
always@(posedge rst, posedge clk) begin
	if(rst) begin
		out_pkts <= 0;
		rd_pos 	<= 0;
		out_val 	<= 1'b0;
	end else begin
		if(out_captured) begin 
			rd_pos 	<= rd_pos + 1'b1;
			out_pkts <= (out_eop) ? out_pkts + 1'b1 : out_pkts;
			out_val 	<= (out_eop) ? 1'b0 : 1'b1; // changed to go low for one clock cycle after EOP
		end else begin
			rd_pos 	<= rd_pos;
			out_val 	<= (in_pkts - out_pkts == 0) ? 1'b0 : 1'b1;
			out_pkts <= out_pkts;			
		end		
	end
end

always@(posedge rst, posedge clk) begin 
	if(rst) begin 
		in_rdy <= 1'b0;
		in_pkts <= 0;
		wr_ena <= 1'b0;
		wr_pos <= 0;
		wr_start <= 0;
		in_state <= ST_IDLE;
	end else begin
		case(in_state) 
		ST_IDLE: begin			
			prev_dat <= in_data_masked[15:0];
			first_dat <= in_data_masked[31:16];
			wr_pos <= wr_start;
			wr_cnt <= in_count;
			wr_dat <= (in_eop && (in_empty > 1)) ? {2'b11, 1'b0, in_empty[0], in_count, in_data_masked[31:16]} : {2'b10, 2'b00, 16'h0, in_data_masked[31:16]};
			
			// Did we capture data?
			if(in_sop && in_captured) begin				
				in_rdy 	<= (in_eop) ? (in_empty > 1) ? 1'b1 : 1'b0 : 1'b1;
				in_state <= (in_eop) ? (in_empty > 1) ? ST_IDLE : ST_OVERFLOW : ST_DATA;
				in_pkts  <= (in_eop && (in_empty > 1)) ? in_pkts + 1'b1 : in_pkts;
				wr_start <= (in_eop && (in_empty > 1)) ? wr_start + 1'b1 : wr_start;				
				wr_ena <= 1'b1;
			end else begin
				in_rdy <= (in_pkts - out_pkts) < NUM_PACKETS;
				in_state <= ST_IDLE;
				in_pkts <= in_pkts;
				wr_start <= wr_start;				
				wr_ena <= 1'b0;
			end
			
		end

		ST_DATA: begin
			first_dat <= first_dat;
			
			if(in_sop && in_captured) begin
				in_rdy 	<= (in_eop) ? (in_empty > 1) ? 1'b1 : 1'b0 : 1'b1;				
				in_pkts  <= (in_eop && (in_empty > 1)) ? in_pkts + 1'b1 : in_pkts;
				prev_dat <= in_data_masked[15:0];
				in_state <= (in_eop) ? (in_empty > 1) ? ST_IDLE : ST_OVERFLOW : ST_DATA;
				wr_start <= (in_eop && (in_empty > 1)) ? wr_start + 1'b1 : wr_start;
				wr_pos <= wr_start;
				wr_ena <= 1'b1;
				wr_cnt <= in_count;
				wr_dat <= (in_eop && (in_empty > 1)) ? {2'b11, 1'b0, in_empty[0], in_count, in_data_masked[31:16]} : {2'b10, 2'b00, 16'h0, in_data_masked[31:16]};
			end else if(in_eop && in_captured) begin
				in_pkts <= in_pkts;
				prev_dat <= in_data_masked[15:0];
				in_rdy 	<= 1'b0;
				in_state <= (in_empty > 1) ? ST_WR_LEN : ST_OVERFLOW;
				wr_start <= wr_start;
				wr_pos <= (in_empty > 1) ? wr_start : wr_pos + 1'b1;
				wr_ena <= 1'b1;
				wr_cnt <= wr_cnt + in_count;
				wr_dat <= (in_eop && (in_empty > 1)) ? {2'b01, 1'b0, wr_cnt[0], prev_dat, in_data_masked[31:16]} : {2'b00, 2'b00, prev_dat, in_data_masked[31:16]};				
			end else if(in_captured) begin
				in_pkts <= in_pkts;
				prev_dat <= in_data_masked[15:0];
				wr_start <= wr_start;
				wr_cnt <= wr_cnt + in_count;
				wr_dat <= {2'b00, 2'b00, prev_dat, in_data_masked[31:16]};
				// silently drop packet being created if it exceeds the maximum byte size allowed
				if(wr_cnt + in_count > PKT_DEPTH_IN_BYTES) begin 
					in_rdy <= (in_pkts - out_pkts) < NUM_PACKETS;
					in_state <= ST_IDLE;					
					wr_pos <= wr_start;
					wr_ena <= 1'b0;
				end else begin 
					in_rdy 	<= 1'b1;					
					in_state <= ST_DATA;					
					wr_pos <= wr_pos + 1'b1;
					wr_ena <= 1'b1;
				end
			end else begin
				// wait for data to continue
				in_rdy <= 1'b1;
				prev_dat <= prev_dat;
				in_state <= ST_DATA;
				wr_start <= wr_start;
				wr_pos <= wr_pos;
				wr_ena <= 1'b0;
				wr_cnt <= wr_cnt;
				wr_dat <= wr_dat;		
			end			
		end
		
		// This state should always transition 
		ST_OVERFLOW: begin
			in_pkts <= in_pkts;
			in_rdy <= 1'b0;
			in_state <= ST_WR_LEN;
			wr_start <= wr_start;
			wr_pos <= wr_pos + 1'b1;
			wr_ena <= 1'b1;
			wr_cnt <= wr_cnt;
			wr_dat <= {2'b01, 1'b1, wr_cnt[0], prev_dat, 16'h0};		
			first_dat <= first_dat;
			prev_dat <= prev_dat;
		end
		
		ST_WR_LEN: begin
			in_pkts <= in_pkts + 1'b1;
			in_rdy <= (in_pkts - out_pkts) < (NUM_PACKETS-1'b1);
			in_state <= ST_IDLE;
			wr_start <= wr_pos + 1'b1;
			wr_pos	<= wr_start;
			wr_ena 	<= 1'b1;
			wr_cnt	<= wr_cnt;
			wr_dat	<= {2'b10, 2'b00, wr_cnt, first_dat};
			first_dat <= first_dat;
			prev_dat <= prev_dat;
		end
				
		endcase
	
	end
end

endmodule
