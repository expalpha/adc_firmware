## Generated SDC file "alpha16.sdc"

## Copyright (C) 1991-2015 Altera Corporation. All rights reserved.
## Your use of Altera Corporation's design tools, logic functions 
## and other software and tools, and its AMPP partner logic 
## functions, and any output files from any of the foregoing 
## (including device programming or simulation files), and any 
## associated documentation or information are expressly subject 
## to the terms and conditions of the Altera Program License 
## Subscription Agreement, the Altera Quartus Prime License Agreement,
## the Altera MegaCore Function License Agreement, or other 
## applicable license agreement, including, without limitation, 
## that your use is for the sole purpose of programming logic 
## devices manufactured by Altera and sold by Altera or its 
## authorized distributors.  Please refer to the applicable 
## agreement for further details.


## VENDOR  "Altera"
## PROGRAM "Quartus Prime"
## VERSION "Version 15.1.0 Build 185 10/21/2015 SJ Standard Edition"

## DATE    "Sat May 14 17:51:59 2016"

##
## DEVICE  "5AGXFB3H4F35C4"
##


#**************************************************************
# Time Information
#**************************************************************

set_time_format -unit ns -decimal_places 3



#**************************************************************
# Create Clock
#**************************************************************



# For this experiment the eSATA Clock will be run at 125MHz
create_clock -name {NIM_CLK} 			-period 16.000 -waveform { 0.000 8.000 } [get_ports {NIM_CLK}]
create_clock -name {eSATA_CLK} 		-period 16.000 -waveform { 0.000 8.000 } [get_ports {eSATA_CLK}]
create_clock -name {CLK_125MHzT} 	-period 8.000 	-waveform { 0.000 4.000 } [get_ports {CLK_125MHzT}] 
create_clock -name {CLK_125MHzB} 	-period 8.000 	-waveform { 0.000 4.000 } [get_ports {CLK_125MHzB}]
create_clock -name {FPGA_ClnClk0} 	-period 8.000 	-waveform { 0.000 4.000 } [get_ports {FPGA_ClnClk[0]}] 
create_clock -name {FPGA_ClnClk1} 	-period 8.000 	-waveform { 0.000 4.000 } [get_ports {FPGA_ClnClk[1]}] 
create_clock -name {CLK_100MHz1} 	-period 10.000 -waveform { 0.000 5.000 } [get_ports {CLK_100MHz[1]}]
create_clock -name {CLK_100MHz2} 	-period 10.000 -waveform { 0.000 5.000 } [get_ports {CLK_100MHz[2]}]
create_clock -name {ADC_DCO_A} 		-period 2.500 	-waveform { 1.250 2.500 } [get_ports {ADC_DCO[0]}]
create_clock -name {ADC_DCO_B} 		-period 2.500 	-waveform { 1.250 2.500 } [get_ports {ADC_DCO[1]}]
create_clock -name {ADC_DCO_C} 		-period 2.500 	-waveform { 1.250 2.500 } [get_ports {ADC_DCO[2]}]
create_clock -name {ADC_DCO_D} 		-period 2.500 	-waveform { 1.250 2.500 } [get_ports {ADC_DCO[3]}]
create_clock -name {ADC32_DCO_A} 		-period "437.5 MHz"  [get_ports {FMC_ADC1_DCO1}]
create_clock -name {ADC32_DCO_B} 		-period "437.5 MHz" 	[get_ports {FMC_ADC1_DCO2}]
create_clock -name {ADC32_DCO_C} 		-period "437.5 MHz" 	[get_ports {FMC_ADC2_DCO1}]
create_clock -name {ADC32_DCO_D} 		-period "437.5 MHz" 	[get_ports {FMC_ADC2_DCO2}]

# create_clock -name {CLK_125_INTERNAL} -period 8.000 -waveform { 0.000 4.000 } [get_pins {glitchless_clkswitch|clkctrl|clockctrl_clkctrl_sub_component|sd1|outclk}]
#create_clock -name {CLK_125_INTERNAL} -period 8.000 -waveform { 0.000 4.000 } [get_pins {glitchless_clkswitch|outclk|combout}]

create_clock -name {ADC32_A_OUT}  -period 8.000 [get_pins { adc32_inst|gen_adc[0].serdes_inst|ALTLVDS_RX_component|auto_generated|pll_sclk~PLL_OUTPUT_COUNTER|divclk}]
create_clock -name {ADC32_B_OUT}  -period 8.000 [get_pins { adc32_inst|gen_adc[1].serdes_inst|ALTLVDS_RX_component|auto_generated|pll_sclk~PLL_OUTPUT_COUNTER|divclk}]
create_clock -name {ADC32_C_OUT}  -period 8.000 [get_pins { adc32_inst|gen_adc[2].serdes_inst|ALTLVDS_RX_component|auto_generated|pll_sclk~PLL_OUTPUT_COUNTER|divclk}]
create_clock -name {ADC32_D_OUT}  -period 8.000 [get_pins { adc32_inst|gen_adc[3].serdes_inst|ALTLVDS_RX_component|auto_generated|pll_sclk~PLL_OUTPUT_COUNTER|divclk}]

#create_generated_clock -name {DATA_PLL} -source {CLK_125_INTERNAL} -divide_by 10 -multiply_by 120 -duty_cycle 50.00 { pll_data|data_pll_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL|vcoph[0] }
#create_generated_clock -name {DATA_PLL} -master_clock {CLK_125_INTERNAL} -source [get_pins {glitchless_clkswitch|outclk~CLKENA0|outclk}] -divide_by 2 -multiply_by 8 { pll_data|data_pll_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL|vcoph[0] }
#create_generated_clock -name {  pll_data|data_pll_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL|vcoph[0] } -master_clock {CLK_125_INTERNAL} -source [get_pins {glitchless_clkswitch|outclk|combout}] -divide_by 2 -multiply_by 8 { pll_data|data_pll_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL|vcoph[0] }

# Automatically constrain PLL and other generated clocks
derive_pll_clocks -create_base_clocks

# Automatically calculate clock uncertainty to jitter and other effects.
derive_clock_uncertainty

#set_clock_groups -exclusive -group {CLK_125_INTERNAL} -group {FPGA_ClnClk0} -group {FPGA_ClnClk1} -group {CLK_125MHzT} -group {CLK_125MHzB}
set_clock_groups -asynchronous -group {ADC_DCO_A} -group {ADC_DCO_B} -group {ADC_DCO_C} -group {ADC_DCO_D}
set_clock_groups -asynchronous -group {ADC32_DCO_A} -group {ADC32_DCO_B} -group {ADC32_DCO_C} -group {ADC32_DCO_D}

# False path glitchless clock switch logic
#set_false_path -from {r_clkswitch[0]} -to {glitchless_clock_switch:glitchless_clkswitch|sync1_0}
#set_false_path -from {glitchless_clock_switch:glitchless_clkswitch|sync1_0} -to {glitchless_clock_switch:glitchless_clkswitch|sync1_1}
#set_false_path -from {glitchless_clock_switch:glitchless_clkswitch|sync0_1} -to {glitchless_clock_switch:glitchless_clkswitch|sync1_0}

# Cut reset logic
set_false_path -from [get_registers {delayer:initial_rst_delay|r_q}]
set_false_path -from [get_registers {delayer:pll_rst_delay|r_q}]
set_false_path -from [get_registers {*|r_sync_rst}]
set_false_path -from [get_registers {*|altera_reset_synchronizer:alt_rst_sync_uq1|altera_reset_synchronizer_int_chain_out}]
set_false_path -from [get_registers {management:mgmnt_qsys_inst|management_nios2_0:nios2_0|management_nios2_0_cpu:cpu|management_nios2_0_cpu_nios2_oci:the_management_nios2_0_cpu_nios2_oci|management_nios2_0_cpu_nios2_oci_debug:the_management_nios2_0_cpu_nios2_oci_debug|resetrequest}]
set_false_path -to 	[get_registers -no_case *sync_reg|r_q*]
set_false_path -from [get_registers -no_case *sync_reg|r_q*]



#set_false_path -from [get_registers {management:mgmnt_qsys_inst|board:board|freq_counter:freq_count_esata|rst_freq}]
#set_false_path -from [get_registers {management:mgmnt_qsys_inst|board:board|freq_counter:freq_count_nim|rst_freq}]
#set_false_path -from [get_registers {management:mgmnt_qsys_inst|board:board|freq_counter:freq_count_fpga_cln0|rst_freq}]
#set_false_path -from [get_registers {management:mgmnt_qsys_inst|board:board|freq_counter:freq_count_fpga_cln1|rst_freq}]
#set_false_path -from [get_registers {sp_top:signal_processing|r_int_rst}]

# Cut slow logic

set_false_path -to [get_ports {FMC_ADC_CS0n}]
set_false_path -to [get_ports {FMC_ADC_CS1n}]
set_false_path -to [get_ports {FMC_ADC_CS2n}]
set_false_path -to [get_ports {FMC_ADC_CS3n}]
set_false_path -to [get_ports {FMC_ADC_SCK}]
#set_false_path -to [get_ports {FMC_ADC_SDIO}]
#set_false_path -from [get_ports {FMC_ADC_SDIO}]
set_false_path -to [get_ports {FMC_ADC_SYNC}]
#set_false_path -to [get_ports {FMC_SPI_DAC_CSn}]
#set_false_path -from [get_ports {FMC_SPI_DAC_SDO}]

set_false_path -to [get_ports {ADC_CSn[0]}]
set_false_path -to [get_ports {ADC_CSn[1]}]
set_false_path -to [get_ports {ADC_CSn[2]}]
set_false_path -to [get_ports {ADC_CSn[3]}]
set_false_path -to [get_ports {ADC_SCK}]
set_false_path -to [get_ports {ADC_SDIO}]
set_false_path -to [get_ports {ADC_SYNC}]
set_false_path -to [get_ports {CLK_FPGA}]
set_false_path -to [get_ports {CLK_FPGA(n)}]
set_false_path -to [get_ports {DAC_SMB_SCL}]
set_false_path -to [get_ports {DAC_SMB_SDA}]
set_false_path -to [get_ports {DISP_SCL}]
set_false_path -to [get_ports {DISP_SDA}]
set_false_path -to [get_ports {FMC_SCL}]
set_false_path -to [get_ports {FMC_SDA}]
set_false_path -to [get_ports {FP_LED[0]}]
set_false_path -to [get_ports {FP_LED[1]}]
set_false_path -to [get_ports {FP_LED[2]}]
set_false_path -to [get_ports {FP_LED[3]}]
set_false_path -to [get_ports {LMK_CLKinSEL[0]}]
set_false_path -to [get_ports {LMK_CLKinSEL[1]}]
set_false_path -to [get_ports {LMK_CSn}]
set_false_path -to [get_ports {LMK_MOSI}]
set_false_path -to [get_ports {LMK_SCLK}]
set_false_path -to [get_ports {LMK_SYNC}]
set_false_path -to [get_ports {MAC_SCL}]
set_false_path -to [get_ports {MAC_SDA}]
set_false_path -to [get_ports {REFCLK_SEL[0]}]
set_false_path -to [get_ports {REFCLK_SEL[1]}]
set_false_path -to [get_ports {SD_CLK}]
set_false_path -to [get_ports {SD_CMD}]
set_false_path -to [get_ports {SD_DAT[0]}]
set_false_path -to [get_ports {SFP_SCL}]
set_false_path -to [get_ports {SFP_SDA}]
set_false_path -to [get_ports {SFP_TX_DISABLE}]
set_false_path -to [get_ports {TEMP_CSn}]
set_false_path -to [get_ports {TEMP_RSTn}]
set_false_path -to [get_ports {TEMP_SCK}]
set_false_path -to [get_ports {TEMP_SDI}]
set_false_path -to [get_ports {USB_CTSn}]
set_false_path -to [get_ports {USB_RXD}]
set_false_path -to [get_ports {altera_reserved_tdo}]
set_false_path -to [get_ports {altera_reserved_tdo}]
set_false_path -to [get_ports {SFP_RATESELECT}]

set_false_path -from [get_ports {FMC_PRSNTn}]
set_false_path -from [get_ports {SFP_MODDET}]
set_false_path -from [get_ports {NIM_TRIG}]
set_false_path -from [get_ports {eSATA_SYNC}]
set_false_path -from [get_ports {ADC_SDIO}]
set_false_path -from [get_ports {CPU_RESETn}]
set_false_path -from [get_ports {DAC_SMB_SCL}]
set_false_path -from [get_ports {DAC_SMB_SDA}]
set_false_path -from [get_ports {DISP_SCL}]
set_false_path -from [get_ports {DISP_SDA}]
set_false_path -from [get_ports {FMC_SCL}]
set_false_path -from [get_ports {FMC_SDA}]
set_false_path -from [get_ports {LMK_MISO}]
set_false_path -from [get_ports {LMK_StatusLD[1]}]
set_false_path -from [get_ports {LMK_StatusLD[2]}]
set_false_path -from [get_ports {MAC_SCL}]
set_false_path -from [get_ports {MAC_SDA}]
set_false_path -from [get_ports {SD_DETECT}]
set_false_path -from [get_ports {SD_CMD}]
set_false_path -from [get_ports {SD_DAT[0]}]
set_false_path -from [get_ports {SFP_LOS}]
set_false_path -from [get_ports {SFP_SCL}]
set_false_path -from [get_ports {SFP_SDA}]
set_false_path -from [get_ports {SFP_TX_FAULT}]
set_false_path -from [get_ports {TEMP_SDO}]
set_false_path -from [get_ports {USB_CBUS0}]
set_false_path -from [get_ports {USB_RTSn}]
set_false_path -from [get_ports {altera_reserved_tdi}]
set_false_path -from [get_ports {altera_reserved_tms}]

set_false_path -to {*|latched_bits[*]}

# The FP_LEDs are cut, but this snuck through... so cut path on LED blinker
set_false_path -from {management:mgmnt_qsys_inst|management_eth_tse:eth_tse|altera_eth_tse_pcs_pma_phyip:i_tse_pcs_0|altera_tse_top_1000_base_x_strx_gx:altera_tse_top_1000_base_x_strx_gx_inst|altera_tse_top_sgmii_strx_gx:U_SGMII|altera_tse_top_pcs_strx_gx:U_PCS|altera_tse_carrier_sense:U_SENS|gmii_crs} -to {oneshot_extend:extend_led_crs|oneshot:u0|d_prev}
set_false_path -from {management:mgmnt_qsys_inst|management_eth_tse:eth_tse|altera_eth_tse_pcs_pma_phyip:i_tse_pcs_0|altera_tse_top_1000_base_x_strx_gx:altera_tse_top_1000_base_x_strx_gx_inst|altera_tse_top_sgmii_strx_gx:U_SGMII|altera_tse_top_pcs_strx_gx:U_PCS|altera_tse_carrier_sense:U_SENS|gmii_crs} -to {oneshot_extend:extend_led_crs|oneshot:u0|q}

set_false_path -to {sld_signaltap:*|*}

#set_false_path -to [get_ports {FMC_SPI_DAC_CSn}]
#set_false_path -to [get_ports {FMC_SPI_DAC_DSO}]

set_false_path -to {*|sync_adc16_bits_ts|d*}
set_false_path -to {*|sync_adc32_bits_ts|d*}

set_false_path -to [get_ports {DAC_D*}]
set_false_path -to [get_ports {DAC_PD}]
set_false_path -to [get_ports {DAC_TORB}]
set_false_path -to [get_ports {DAC_SELIQ}]
set_false_path -to [get_ports {DAC_XOR}]
set_false_path -to [get_ports {DAC_SELIQ(n)}]
set_false_path -to [get_ports {DAC_XOR(n)}]

set_false_path -to [get_ports {FMC_ADC_DIR1}]
set_false_path -to [get_ports {FMC_ADC_DIR2}]
set_false_path -to [get_ports {FMC_ADC_SDIO1}]
set_false_path -to [get_ports {FMC_ADC_SDIO2}]
set_false_path -to [get_ports {FMC_SPI_DAC1_CSn}]
set_false_path -to [get_ports {FMC_SPI_DAC2_CSn}]
set_false_path -to [get_ports {FMC_SPI_DAC2_SCK}]
set_false_path -to [get_ports {FMC_SPI_DAC2_SDI}]
set_false_path -from [get_ports {FMC_ADC_SDIO1}]
set_false_path -from [get_ports {FMC_ADC_SDIO2}]
set_false_path -from [get_ports {FMC_SPI_DAC2_SDO}]

# false path code from Yair, via the PWB firmware project.

proc puts_and_post_message { x } {
    # set module [lindex $quartus(args) 0]                                                                                                                
    # if [string match "quartus_map" $module] {                                                                                                           
    # } else {                                                                                                                                            
    if { ([info commands post_message] eq "post_message") || ([info procs post_message] eq "post_message") } {
        puts $x
        post_message $x
    }
    # }                                                                                                                                                   
}

proc print_collection { col } {
    if {[catch {
        foreach_in_collection c $col  {
            puts_and_post_message "   [get_clock_info -name $c]";
        }
    } ] } {
        if {[catch {
            puts_and_post_message "[query_collection $col -all -report_format]"
        } ] } {
            puts_and_post_message "   $col"
        }
    }
}

proc timid_asynchronous { clock_name clockB { max_delay  50.0 } { min_delay  -50 } } {
    set_max_delay -from  $clock_name -to $clockB $max_delay
    set_max_delay -from $clockB -to  $clock_name $max_delay
    set_min_delay -from  $clock_name -to $clockB $min_delay
    set_min_delay -from $clockB -to  $clock_name $min_delay
    #puts_and_post_message "=========================================================================="
    #puts_and_post_message "timid_asynchronous $clock_name from/to $clockB, max_delay = $max_delay min_delay = $min_delay which means:"
    #print_collection $clock_name
    #puts_and_post_message "from/to: "
    #print_collection $clockB
    #puts_and_post_message "=========================================================================="
}

proc timid_asynchronous_group { clock_list { max_delay  50.0 } { min_delay  -50 } } {
    puts [concat "timid_asynchronous_group: " [llength $clock_list] ":" [join $clock_list]]
    for {set i 0 } { $i < [llength $clock_list] } { incr i } {
        for {set j [expr $i + 1] } { $j < [llength $clock_list] } { incr j }  {
            puts [concat "timid_asynchronous clocks " [lindex $clock_list $i] " and " [lindex $clock_list $j]" ]
            timid_asynchronous [lindex $clock_list $i] [lindex $clock_list $j] $max_delay $min_delay;
        }
    }
}

#timid_asynchronous_group {
#    OSC_100MHz
#    CC_125MHz
#    RD_CLK_MON
#    pll_main|main_pll_inst|altera_pll_i|general[0].gpll~PLL_OUTPUT_COUNTER|divclk
#    pll_main|main_pll_inst|altera_pll_i|general[1].gpll~PLL_OUTPUT_COUNTER|divclk
#    pll_main|main_pll_inst|altera_pll_i|general[2].gpll~PLL_OUTPUT_COUNTER|divclk
#    pll_main|main_pll_inst|altera_pll_i|general[3].gpll~PLL_OUTPUT_COUNTER|divclk
#    backup_link_phy|native_phy_inst|gen_native_inst.av_xcvr_native_insts[0].gen_bonded_group_native.av_xcvr_native_inst|inst_av_pcs|ch[0].inst_av_pcs_ch|inst_av_hssi_8g_tx_pcs|wys|txpmalocalclk
#    backup_link_phy|native_phy_inst|gen_native_inst.av_xcvr_native_insts[0].gen_bonded_group_native.av_xcvr_native_inst|inst_av_pcs|ch[0].inst_av_pcs_ch|inst_av_hssi_8g_rx_pcs|wys|rcvdclkpma
#}

timid_asynchronous_group {
        FPGA_ClnClk0
        FPGA_ClnClk1
        pll_data|data_pll_inst|altera_pll_i|general[0].gpll~PLL_OUTPUT_COUNTER|divclk
        pll_data|data_pll_inst|altera_pll_i|general[1].gpll~PLL_OUTPUT_COUNTER|divclk
        pll_data|data_pll_inst|altera_pll_i|general[2].gpll~PLL_OUTPUT_COUNTER|divclk
        pll_data|data_pll_inst|altera_pll_i|general[3].gpll~PLL_OUTPUT_COUNTER|divclk
        pll_data|data_pll_inst|altera_pll_i|general[4].gpll~PLL_OUTPUT_COUNTER|divclk
        ADC32_A_OUT
        ADC32_B_OUT
        ADC32_C_OUT
        ADC32_D_OUT
}

set_false_path -from {management:mgmnt_qsys_inst|ag:ag_0|avalon_ctrl_slave:ag_ctrl|register*}
set_false_path -to {ag_dac:ag_dac|trigger1}

# untangle ADC32_A_OUT, ADC32_B_OUT, ADC32_C_OUT, ADC32_D_OUT
#
#set_false_path -from {adc_data_aligner32:adc32_inst|adc_trainer:gen_adc[1].train_lo_inst|adc_slip:gen_slip[0].adc_slip_inst|aligned} -to {adc_data_aligner32:adc32_inst|gen_adc[0].gen_adc_ch[0].wr_active1}
#set_false_path -from {adc_data_aligner32:adc32_inst|adc_trainer:gen_adc[1].train_hi_inst|adc_slip:gen_slip[0].adc_slip_inst|aligned} -to {adc_data_aligner32:adc32_inst|gen_adc[0].gen_adc_ch[0].wr_active1}
#
#set_false_path -from {adc_data_aligner32:adc32_inst|adc_trainer:gen_adc[2].train_lo_inst|adc_slip:gen_slip[0].adc_slip_inst|aligned} -to {adc_data_aligner32:adc32_inst|gen_adc[0].gen_adc_ch[0].wr_active1}
#set_false_path -from {adc_data_aligner32:adc32_inst|adc_trainer:gen_adc[2].train_hi_inst|adc_slip:gen_slip[0].adc_slip_inst|aligned} -to {adc_data_aligner32:adc32_inst|gen_adc[0].gen_adc_ch[0].wr_active1}

#set_false_path -from {adc_data_aligner32:adc32_inst|adc_trainer:gen_adc[3].train_lo_inst|adc_slip:gen_slip[0].adc_slip_inst|aligned} -to {adc_data_aligner32:adc32_inst|gen_adc[0].gen_adc_ch[0].wr_active1}
#set_false_path -from {adc_data_aligner32:adc32_inst|adc_trainer:gen_adc[3].train_hi_inst|adc_slip:gen_slip[0].adc_slip_inst|aligned} -to {adc_data_aligner32:adc32_inst|gen_adc[0].gen_adc_ch[0].wr_active1}

set_false_path -from {alphag:ag|grand_or_clk625} -to {alphag:ag|gor625_clk_counter1}
set_false_path -from {alphag:ag|grand_or_clk100} -to {alphag:ag|gor100_clk_counter1}

set_false_path -to {adc_data_aligner16:adc16_inst|gen_adc[0].all_channels_aligned1}
set_false_path -to {adc_data_aligner16:adc16_inst|gen_adc[1].all_channels_aligned1}
set_false_path -to {adc_data_aligner16:adc16_inst|gen_adc[2].all_channels_aligned1}
set_false_path -to {adc_data_aligner16:adc16_inst|gen_adc[3].all_channels_aligned1}

set_false_path -to {adc_data_aligner32:adc32_inst|gen_adc[0].all_channels_aligned1}
set_false_path -to {adc_data_aligner32:adc32_inst|gen_adc[1].all_channels_aligned1}
set_false_path -to {adc_data_aligner32:adc32_inst|gen_adc[2].all_channels_aligned1}
set_false_path -to {adc_data_aligner32:adc32_inst|gen_adc[3].all_channels_aligned1}

#end
