#include <stdio.h>
#include "includes.h"
#include "main.h"
#include "task_esper.h"

/* Nichestack definitions */
#include "ipport.h"
#include "libport.h"
#include "osport.h"
#include "tcpport.h"
#include "net.h"

// we overwrite the altera jtag uart "write" function
// because there is no way to prevent the stock version
// from waiting forever if output fifo is full and
// nios2-terminal is not connected. K.O. Aug 2020.

#include "altera_avalon_jtag_uart_regs.h"
#include "altera_avalon_jtag_uart.h"

int altera_avalon_jtag_uart_write(altera_avalon_jtag_uart_state* sp, 
  const char * ptr, int count, int flags)
{
  unsigned int base = sp->base;

  const char * end = ptr + count;

  //while (ptr < end)
  //  if ((IORD_ALTERA_AVALON_JTAG_UART_CONTROL(base) & ALTERA_AVALON_JTAG_UART_CONTROL_WSPACE_MSK) != 0)
  //    IOWR_ALTERA_AVALON_JTAG_UART_DATA(base, *ptr++);

  while (ptr < end)
     IOWR_ALTERA_AVALON_JTAG_UART_DATA(base, *ptr++);

  return count;
}

/* Definition of Task Stacks */
OS_STK InitialTaskStk[TASK_STACKSIZE]; // Definition of Task Stacks for non-networking tasks

#include "dhcpclnt.h"

static void InitialTask(void* pdata) {
	// Initialize Altera NicheStack TCP/IP Stack - Nios II Edition specific code.
	alt_iniche_init();

	// Start the Iniche-specific network tasks and initialize the network devices
	netmain();

	printf("after netmain, waiting for iniche_net_ready!\n");
	printf("waiting for dhcp reply!\n");
	printf("\n");

	// Wait for the network stack to be ready before proceeding
	while (!iniche_net_ready) {
           static int t = 0;
           if (dhc_states[0].tries != t) {
              //printf("dhcp tries: %d -> %d\n", t, dhc_states[0].tries);
              t = dhc_states[0].tries;
              if (dhc_states[0].tries > 1) {
                 dhc_states[0].tries = 0; // see iniche/src/net/dhcpclnt.c
              }
           }

           //IOWR_ALTERA_RU_RESET_TIMER(REMOTE_UPDATE_BASE, 1);
           TK_SLEEP(1);
        }

	printf("have iniche_net_ready, dhc_alldone is %d, dhc_states[0].state is %d!\n", dhc_alldone(), dhc_states[0].state);
        //usleep(500000);

        // NB: DHCP timeout is 2 minutes, hardwired in the loop over (cticks - dhcp_started)
        // in dhc_setup() in iniche/src/misclib/dhcsetup.c

        if (dhc_states[0].state == 0) {
           printf("Error: cannot get an IP address, resetting the CPU!\n");
           usleep(500000);
           
           // https://www.intel.com/content/www/us/en/programmable/support/support-resources/knowledge-base/solutions/rd05062005_584.html
           NIOS2_WRITE_STATUS(0); 
           NIOS2_WRITE_IENABLE(0); 
           ((void (*) (void)) NIOS2_RESET_ADDR) ();

           // NB: tested. this reset works. K.O.
           // NB: this reset does not work in the eclipse debugger, shows stuck in alt_tick(). K.O.

           // CPU did not reset?!?
           while(1) { };
        }

        //while (1) {
        //   extern void dhc_setup(void); // not in any header file. see iniche/src/misclib/dhcsetup.c
        //   extern UDPCONN dhc_conn; // see iniche/src/net/dhcpclnt.c
        //   printf("dhc_conn %p, dhc_states[0].state %d, .ipaddr 0x%08lx\n", dhc_conn, dhc_states[0].state, (unsigned long)dhc_states[0].ipaddr);
        //   printf("calling dhc_setup()!\n");
        //   usleep(500000);
        //   if (dhc_conn) {
        //      udp_close(dhc_conn);
        //      dhc_conn = NULL;
        //   }
        //   dhc_setup();
        //}

	//printf("IP address 0x%08lx!\n", ((NET)(netlist.q_head))->n_ipaddr);


	// Wait for the network stack to be ready before proceeding
	//while (!iniche_net_ready) {	TK_SLEEP(1); }

	// Create HTML+JSON server
	CreateTaskESPER(TASK_ESPER_PRIO);

	// Done, delete the task
	OSTaskDel(OS_PRIO_SELF);

	while(1); /* Correct Program Flow never gets here. */
}

int main(void) {
	/* Clear the RTOS timer */
	OSTimeSet(0);

	// Create Initial Task
	OSTaskCreateExt(InitialTask,
			NULL,
			(void*)&InitialTaskStk[TASK_STACKSIZE-1],
			TASK_INIT_PRIO,
			TASK_INIT_PRIO,
			InitialTaskStk,
			TASK_STACKSIZE,
			NULL,
			0);

	OSStart();

	while(1); /* Correct Program Flow never gets here. */

	return -1;
}
