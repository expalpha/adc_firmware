#ifdef ALT_INICHE

/*
 * config_network.c
 *
 *  Created on: Mar 31, 2014
 *      Author: bryerton
 */

/* Nichestack definitions */
#include "ipport.h"
#include "libport.h"
#include "osport.h"
#include "tcpport.h"
#include "net.h"
#include <system.h>                // component names
#include <alt_iniche_dev.h>
#include <altera_avalon_tse.h>  // phy_cfg_fp, alt_tse_system_info, TSE_PHY_AUTO_ADDRESS
#include <altera_avalon_tse_system_info.h>  // TSE_SYSTEM_EXT_MEM_NO_SHARED_FIFO
#include <drivers/inc/24aa02e48.h>

alt_32 tse_phy_cfg(np_tse_mac *pmac) {
	return 0x3; // enable gigabit and full duplex bits
}

alt_tse_system_info tse_mac_device[MAXNETS] = {
	//Macro defined in altera_avalon_tse_system_info, should match TSE configuration
	TSE_SYSTEM_EXT_MEM_NO_SHARED_FIFO(
			ETH_TSE,                  //tse_name
			0,                        //offset
			SGDMA_TX,                 //sgdma_tx_name
			SGDMA_RX,                 //sgdma_rx_name
			TSE_PHY_AUTO_ADDRESS,     //phy_address
			tse_phy_cfg,				//phy_cfg_fp
			DESCRIPTOR_MEMORY)        //desc_mem_name
};


#define IP4_ADDR(ipaddr, a,b,c,d) ipaddr = htonl((((alt_u32)(a & 0xff) << 24) | ((alt_u32)(b & 0xff) << 16) | ((alt_u32)(c & 0xff) << 8) | (alt_u32)(d & 0xff)))

int get_ip_addr(alt_iniche_dev *p_dev,
                ip_addr* ipaddr,
                ip_addr* netmask,
                ip_addr* gw,
                int* use_dhcp)
{

#ifdef DHCP_CLIENT
	IP4_ADDR(*ipaddr, 	0, 	0, 	0, 	0);
	IP4_ADDR(*gw, 		0, 	0, 	0, 	0);
	IP4_ADDR(*netmask, 	0, 	0, 	0,  0);

	*use_dhcp = 1;
#else /* not DHCP_CLIENT */
    *use_dhcp = 0;

	IP4_ADDR(*ipaddr, 	192, 	168, 	1, 	100);
	IP4_ADDR(*gw, 		192, 	168, 	1, 	  1);
	IP4_ADDR(*netmask, 	255, 	255, 	255,  0);


    printf("Static IP Address is %d.%d.%d.%d\n",
        ip4_addr1(*ipaddr),
        ip4_addr2(*ipaddr),
        ip4_addr3(*ipaddr),
        ip4_addr4(*ipaddr));
#endif /* not DHCP_CLIENT */

    /* Non-standard API: return 1 for success */
    return 1;
}

int get_mac_addr(NET net, unsigned char mac_addr[6]) {
	EEPROM_24AA02E48_GetEUI48(I2C_MAC_BASE,(tEUI48*)mac_addr);

    printf("Ethernet MAC address: %02x:%02x:%02x:%02x:%02x:%02x\n",
            mac_addr[0],
            mac_addr[1],
            mac_addr[2],
            mac_addr[3],
            mac_addr[4],
            mac_addr[5]);

    return 0;
}


#endif // ALT_INICHE
