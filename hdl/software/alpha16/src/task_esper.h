/*
 * task_esper.h
 *
 *  Created on: Apr 11, 2014
 *      Author: bryerton
 */

#ifndef TASK_ESPER_H_
#define TASK_ESPER_H_

#include "includes.h"
#include <esper.h>

#define ESPER_TASK_STACKSIZE (65536*8)

#define ESPER_MAX_MODULES	256
#define ESPER_MAX_VARS		32768
#define ESPER_MAX_ATTRS		32768

#define NUM_ADC16 16
#define NUM_ADC32 32

INT8U CreateTaskESPER(INT8U priority);

#endif /* TASK_ESPER_H_ */
