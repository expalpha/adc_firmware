#ifndef BUILD_NUMBER_STR
#define BUILD_NUMBER_STR "405"
#endif

#ifndef BUILD_NUMBER
#define BUILD_NUMBER 405
#endif

#ifndef BUILD_TIMESTAMP
#define BUILD_TIMESTAMP 1604444433
#endif

#ifndef VERSION_MAJOR
#define VERSION_MAJOR 1
#endif

#ifndef VERSION_MINOR
#define VERSION_MINOR 0
#endif

#ifndef VERSION_STR
#define VERSION_STR "Ver 1.0  Build 405 - Tue Nov  3 15:00:33 PST 2020"
#endif

#ifndef VERSION_STR_SHORT
#define VERSION_STR_SHORT "1.0.405"
#endif

#ifndef GIT_HASH_STR
#define GIT_HASH_STR "39f4735e01a7687d071f6ec5260f212c6879ccb1"
#endif

#ifndef GIT_BRANCH_STR
#define GIT_BRANCH_STR "alphag"
#endif

#ifndef GIT_TAG_STR
#define GIT_TAG_STR ""
#endif

#ifndef BUILT_BY_USER_STR
#define BUILT_BY_USER_STR "First Last"
#endif

#ifndef BUILT_BY_EMAIL_STR
#define BUILT_BY_EMAIL_STR "noreply@example.com"
#endif

