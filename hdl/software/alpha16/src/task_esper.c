#include "task_esper.h"

#include <system.h>

/* Nichestack definitions */
#include "ipport.h"
#include "libport.h"
#include "osport.h"
#include "tcpport.h"
#include "net.h"

#include <drivers/inc/board_regs.h>
#include <drivers/inc/altera_avalon_spi.h>
#include <drivers/inc/altera_avalon_spi_regs.h>
#include <drivers/inc/altera_avalon_pio_regs.h>
#include <drivers/inc/24aa02e48.h>
#include <esper.h>
#include <esper_nios.h>
#include "modules/storage_epcq.h"
#include "modules/mod_remote_update.h"
#include "modules/mod_sfp.h"
#include "modules/mod_http.h"
#include "modules/mod_sp16.h"
#include "modules/mod_sp32.h"
#include "modules/mod_offload.h"
#include "modules/mod_board.h"
#include "modules/mod_ag.h"
#include "modules/mod_ethernet.h"

// INICHE Networking Task(s)
TK_OBJECT(to_espertask);
TK_ENTRY(task_esper);

static tESPERModule *g_modules;
static tESPERVar	*g_vars;
static tESPERAttr	*g_attrs;

// Hack to remove error, thanks altera
u_long inet_addr(char FAR * str);

static tESPERModuleUDPOffload mod_udp_ctx;
static tESPERModuleRemoteUpdate mod_remote_ctx;
static tESPERModuleHTTP mod_http_ctx;
static tESPERModuleSFP mod_sfp_ctx;
static tESPERStorageEPCQ storage_epcq;
static tESPERModuleBoard mod_board_ctx;
static tESPERModuleAG mod_ag_ctx;
static tESPERModuleSP16 mod_sp16_ctx;
static tESPERModuleSP32 mod_sp32_ctx;
static tESPERModuleEthernet mod_eth_ctx;

static uint8_t* GetMACAddr(void);

INT8U CreateTaskESPER(INT8U priority) {
	static struct inet_taskinfo esper_task;

	INT8U err;

	esper_task.entry = task_esper;
	esper_task.name = "esper";
	esper_task.priority = priority;
	esper_task.stacksize = ESPER_TASK_STACKSIZE;
	esper_task.tk_ptr = &to_espertask;

	// Create task
	err = TK_NEWTASK(&esper_task);

	return err;
}

void task_esper(void* pdata) {
	g_modules = malloc(ESPER_MAX_MODULES*sizeof(tESPERModule));
	g_vars = malloc(ESPER_MAX_VARS*sizeof(tESPERVar));
	g_attrs = malloc(ESPER_MAX_ATTRS*sizeof(tESPERAttr));

	ESPER_Init("ALPHAg ADC16", g_modules, g_vars, g_attrs,  ESPER_MAX_MODULES, ESPER_MAX_VARS, ESPER_MAX_ATTRS, EPCQStorage(EPCQ_CONFIG_AVL_MEM_NAME, 0, 0, &storage_epcq));

	ESPER_CreateModule("board",	"Board Settings", 		0, BoardModuleHandler, 			BoardModuleInit(GetMACAddr(), &mod_board_ctx));
	ESPER_CreateModule("update","Remote Update", 		0, RemoteUpdateModuleHandler, 	RemoteUpdateModuleInit( REMOTE_UPDATE_NAME, &mod_remote_ctx));

	ESPER_CreateModule("adc16",	"ADC16", 				0, ModuleSP16Handler, 			ModuleSP16Init(ALPHA_SIGPROC16_CONTROL_BASE, ALPHA_SIGPROC16_STATUS_BASE, 0, &mod_sp16_ctx));
	ESPER_CreateModule("fmc32",	"FMC ADC32",			0, ModuleSP32Handler, 			ModuleSP32Init(ALPHA_SIGPROC_FMC_ADC_CONTROL_BASE, ALPHA_SIGPROC_FMC_ADC_STATUS_BASE, 1, &mod_sp32_ctx));
        ESPER_CreateModule("ethernet",  "Ethernet Stats", 		0, ModuleEthernetHandler, 		ModuleEthernetInit(ETH_TSE_BASE, &mod_eth_ctx));
	ESPER_CreateModule("sfp", 	"SFP Details", 			0, ModuleSFPHandler, 			ModuleSFPInit(I2C_SFP_BASE, &mod_sfp_ctx));
	ESPER_CreateModule("udp",	"UDP Offloader", 		0, ModuleUDPOffloadHandler,		ModuleUDPOffloadInit(ADC_DATA_INSERTER_BASE, GetMACAddr(), inet_addr("192.168.1.46"), 50005, 1001, &mod_udp_ctx));
	ESPER_CreateModule("http", 	"Web Server", 			0, ModuleHTTPHandler, 			ModuleHTTPInit("ALPHA16", ALTERA_RO_ZIPFS_NAME, 80, &mod_http_ctx));
	ESPER_CreateModule("ag",	"Discriminator", 		0, ModuleAGHandler, 			ModuleAGInit(AG_0_CONTROL_BASE, AG_0_STATUS_BASE, &mod_ag_ctx));

	ESPER_Start();
	while(ESPER_Update() == ESPER_RESP_OK) {
		OSTimeDly(10);
	}
	ESPER_Stop();
}

static uint8_t* GetMACAddr(void) {
	static uint8_t mac_addr[6];
	static uint8_t init;
	if(!init) {
		init = 1;
		EEPROM_24AA02E48_GetEUI48(I2C_MAC_BASE, (tEUI48*)&mac_addr);
	}

	return mac_addr;
}

