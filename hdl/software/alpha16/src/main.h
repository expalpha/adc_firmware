/*
 * main.h
 *
 *  Created on: Nov 14, 2016
 *      Author: bryerton
 */

#ifndef MAIN_H_
#define MAIN_H_

/* Definition of Task Stacks */
#define TASK_STACKSIZE (8192)

/* Definition of Task Priorities */
#define TASK_INIT_PRIO      5
#define TASK_ESPER_PRIO		6

#endif /* MAIN_H_ */
