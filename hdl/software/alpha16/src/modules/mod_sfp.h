/*
 * sfp.h
 *
 *  Created on: Dec 9, 2016
 *      Author: admin
 */

#ifndef MOD_SFP_H_
#define MOD_SFP_H_

#include <esper.h>
#include <sfp.h>

typedef struct {
	uint32_t i2c_base;
	tSFPInfo sfp_info;
} tESPERModuleSFP;

tESPERModuleSFP* ModuleSFPInit(uint32_t i2c_base, tESPERModuleSFP* ctx);
eESPERResponse ModuleSFPHandler(tESPERMID mid, tESPERGID gid, eESPERState state, void* ctx);

#endif /* MOD_SFP_H_ */
