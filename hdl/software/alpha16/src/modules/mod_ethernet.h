/*
 * mod_ethernet.h
 *
 *  Created on: Dec 9, 2016
 *      Author: admin
 */

#ifndef MOD_ETHERNET_H_
#define MOD_ETHERNET_H_

#include <esper.h>

typedef struct {
	uint32_t aFramesTransmittedOK;
	uint32_t aFramesReceivedOK;
	uint32_t aFramesCheckSequenceErrors;
	uint32_t aAlignmentErrors;
	uint32_t aOctetsTransmittedOK;
	uint32_t aOctetsReceivedOK;
	uint32_t aTxPAUSEMACCtrlFrames;
	uint32_t aRxPAUSEMACCtrlFrames;
	uint32_t ifInErrors;
	uint32_t ifOutErrors;
	uint32_t ifInUcastPkts;
	uint32_t ifInMulticastPkts;
	uint32_t ifInBroadcastPkts;
	uint32_t ifOutDiscards;
	uint32_t ifOutUcastPkts;
	uint32_t ifOutMulticastPkts;
	uint32_t ifOutBroadcastPkts;
	uint32_t etherStatsDropEvent;
	uint32_t etherStatsOctets;
	uint32_t etherStatsPkts;
	uint32_t etherStatsUndersizePkts;
	uint32_t etherStatsOversizePkts;
	uint32_t etherStatsPkts64Octets;
	uint32_t etherStatsPkts65to127Octets;
	uint32_t etherStatsPkts128to255Octets;
	uint32_t etherStatsPkts256to511Octets;
	uint32_t etherStatsPkts512to1023Octets;
	uint32_t etherStatsPkts1024to1518Octets;
	uint32_t etherStatsPkts1519toXOctets;
	uint32_t etherStatsJabbers;
	uint32_t etherStatsFragments;
} tTSEStats;

typedef struct {
	uint32_t tse_base;
	tTSEStats tse_stats;
} tESPERModuleEthernet;

tESPERModuleEthernet* ModuleEthernetInit(uint32_t tse_base, tESPERModuleEthernet* ctx);
eESPERResponse ModuleEthernetHandler(tESPERMID mid, tESPERGID gid, eESPERState state, void* ctx);

#endif /* MOD_ETHERNET_H_ */
