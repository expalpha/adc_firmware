/*
 * mod_sp32.h
 *
 *  Created on: Nov 14, 2016
 *      Author: bryerton
 */

#ifndef MOD_SP32_H_
#define MOD_SP32_H_

#include <esper.h>

typedef struct {
	uint32_t sp_ctrl_base;
	uint32_t sp_stat_base;

	uint32_t num_channels;
	uint8_t default_type;

	uint64_t timestamp;
	uint8_t run_status;
	uint8_t int_trigger;

	uint8_t  adc_gain[4]; // global, only one per ADC
	int16_t dac_offset[32]; // globally set at the moment, normally every DAC channel maps to two ADC channels

	uint8_t adc_reset;
	uint8_t adc_init;

	uint8_t adc_testmode;
	uint8_t adc_testmodeU;
	uint8_t adc_testmode19;
	uint8_t adc_testmode1A;
	uint8_t adc_testmode1B;
	uint8_t adc_testmode1C;

	uint8_t  enable[32];
	uint8_t  type[32];
	uint16_t trig_delay[32];
	uint16_t trig_start[32];
	uint16_t trig_stop[32];

	uint32_t cnt_trig_in[32];
	uint32_t cnt_trig_accepted[32];
	uint32_t cnt_trig_dropped_busy[32];
	uint32_t cnt_trig_dropped_full[32];
	int8_t 	 adc_trim[32];
} tESPERModuleSP32;

tESPERModuleSP32* ModuleSP32Init(uint32_t sp_ctrl_base, uint32_t sp_stat_base, uint8_t default_type, tESPERModuleSP32* ctx);
eESPERResponse ModuleSP32Handler(tESPERMID mid, tESPERGID gid, eESPERState state, void* ctx);

#endif /* MOD_SP32_H_ */
