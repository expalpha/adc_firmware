/*
 * mod_board.c
 *
 *  Created on: Apr 2, 2017
 *      Author: admin
 */

#include <assert.h>
#include <system.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <esper_nios.h>
#include "mod_board.h"
#include <drivers/inc/board_regs.h>
#include <drivers/inc/board.h>
#include <drivers/inc/lmk48xx.h>
#include <altera_avalon_spi.h>
#include <altera_avalon_sysid_qsys.h>
#include <altera_avalon_sysid_qsys_regs.h>
#include <build_number.h>
#include <ltc2983/ltc2983.h>
#include <ltc2983/ltc2983_constants.h>

static eESPERResponse Init(tESPERMID mid, tESPERModuleBoard* data);
static eESPERResponse Start(tESPERMID mid, tESPERModuleBoard* data);
static eESPERResponse Update(tESPERMID mid, tESPERModuleBoard* data);

static void InitLTC2983(void);
static void lmk_spi_write(uint16_t addr, uint8_t cmd);
static alt_u8 lmk_spi_read(uint16_t addr);

tESPERModuleBoard* BoardModuleInit(uint8_t* board_mac, tESPERModuleBoard* ctx) {
	if(!ctx) return 0;

	strlcpy(ctx->elf_build_str, VERSION_STR, SYS_VAR_BUILD_STR_LEN);
	strlcpy(ctx->elf_build_user, BUILT_BY_USER_STR, SYS_VAR_BUILD_USER_LEN);
	strlcpy(ctx->elf_build_email, BUILT_BY_EMAIL_STR, SYS_VAR_BUILD_EMAIL_LEN);
	strlcpy(ctx->git_branch, GIT_BRANCH_STR, SYS_VAR_BUILD_GIT_BRANCH_LEN);
	strlcpy(ctx->git_hash, GIT_HASH_STR, SYS_VAR_BUILD_GIT_HASH_LEN);
	ctx->elf_buildnumber = BUILD_NUMBER;
	ctx->elf_buildtime = BUILD_TIMESTAMP;
	ctx->sw_qsys_sysid = SYSID_ID;
	ctx->sw_qsys_timestamp = SYSID_TIMESTAMP;
	ctx->hw_qsys_sysid = IORD_ALTERA_AVALON_SYSID_QSYS_ID(SYSID_BASE);
	ctx->hw_qsys_timestamp = IORD_ALTERA_AVALON_SYSID_QSYS_TIMESTAMP(SYSID_BASE);
	ctx->hw_sw_qsys_match = (alt_avalon_sysid_qsys_test() == 0) ? 1 : 0;

	memcpy(ctx->mac_addr, board_mac, sizeof(ctx->mac_addr));
	// Write board mac to registers so signal processing can include it in packets

	BOARD_Write(BOARD_CONTROL_BASE, BOARD_MAC_ADDR1,
				(ctx->mac_addr[0] << 8) |
				(ctx->mac_addr[1] << 0));

		BOARD_Write(BOARD_CONTROL_BASE, BOARD_MAC_ADDR0,
				(ctx->mac_addr[2] <<  24) |
				(ctx->mac_addr[3] <<  16) |
				(ctx->mac_addr[4] <<  8)  |
				(ctx->mac_addr[5] <<  0));

	return ctx;
}

eESPERResponse BoardModuleHandler(tESPERMID mid, tESPERGID gid, eESPERState state, void* ctx) {
	switch(state) {
	case ESPER_STATE_INIT:
		return Init(mid, (tESPERModuleBoard*)ctx);
	case ESPER_STATE_START:
		return Start(mid, (tESPERModuleBoard*)ctx);
	case ESPER_STATE_UPDATE:
		return Update(mid, (tESPERModuleBoard*)ctx);
	case ESPER_STATE_STOP:
		break;
	}

	return ESPER_RESP_OK;
}

static eESPERResponse Init(tESPERMID mid, tESPERModuleBoard* ctx) {
	tESPERVID vid;

	vid = ESPER_CreateVarUInt32(mid, "elf_buildtime", ESPER_OPTION_RD, 1, &ctx->elf_buildtime, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "ELF Build Timestamp");
	ESPER_CreateAttrNull(mid, vid, "format", "timestamp");

	vid = ESPER_CreateVarUInt32(mid, "fpga_build", 			ESPER_OPTION_RD, 1, &ctx->fpga_buildtime, (uint8_t*)GET_REG_OFFSET(BOARD_STATUS_BASE, BOARD_FPGA_TIMESTAMP), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "FPGA Build Timestamp");
	ESPER_CreateAttrNull(mid, vid, "format", "timestamp");

	vid = ESPER_CreateVarASCII(mid, "elf_build_str", ESPER_OPTION_RD, SYS_VAR_BUILD_STR_LEN, ctx->elf_build_str, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "ELF Build String");

	//vid = ESPER_CreateVarASCII(mid, "elf_build_user", ESPER_OPTION_RD, SYS_VAR_BUILD_USER_LEN, ctx->elf_build_user, 0, 0);
	//ESPER_CreateAttrNull(mid, vid, "name", "ELF Build User");

	//vid = ESPER_CreateVarASCII(mid, "elf_build_email", ESPER_OPTION_RD, SYS_VAR_BUILD_EMAIL_LEN, ctx->elf_build_email, 0, 0);
	//ESPER_CreateAttrNull(mid, vid, "name", "ELF Build Email");

	vid = ESPER_CreateVarUInt32(mid, "elf_buildnum", ESPER_OPTION_RD, 1, &ctx->elf_buildnumber, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "ELF Build Number");

	vid = ESPER_CreateVarASCII(mid, "git_branch", ESPER_OPTION_RD, SYS_VAR_BUILD_GIT_BRANCH_LEN, ctx->git_branch, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "GIT Branch");

	vid = ESPER_CreateVarASCII(mid, "git_hash", ESPER_OPTION_RD, SYS_VAR_BUILD_GIT_HASH_LEN, ctx->git_hash, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "GIT Commit Hash");

	vid = ESPER_CreateVarUInt32(mid, "sw_qsys_sysid", ESPER_OPTION_RD, 1, &ctx->sw_qsys_sysid, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "SW QSYS Sysid");
	ESPER_CreateAttrNull(mid, vid, "format", "hex");

	vid = ESPER_CreateVarUInt32(mid, "sw_qsys_ts", ESPER_OPTION_RD, 1, &ctx->sw_qsys_timestamp, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "SW QSYS Timestamp");
	ESPER_CreateAttrNull(mid, vid, "format", "timestamp");

	vid = ESPER_CreateVarUInt32(mid, "hw_qsys_sysid", ESPER_OPTION_RD, 1, &ctx->hw_qsys_sysid, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "HW QSYS Sysid");
	ESPER_CreateAttrNull(mid, vid, "format", "hex");

	vid = ESPER_CreateVarUInt32(mid, "hw_qsys_ts", ESPER_OPTION_RD, 1, &ctx->hw_qsys_timestamp, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "HW QSYS Timestamp");
	ESPER_CreateAttrNull(mid, vid, "format", "timestamp");

	vid = ESPER_CreateVarBool(mid, "hw_sw_match", ESPER_OPTION_RD, 1, &ctx->hw_sw_qsys_match, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "HW/SW Match");

	vid = ESPER_CreateVarUInt64(mid, "chip_id", 			ESPER_OPTION_RD, 1, &ctx->chipid, (uint8_t*)GET_REG_OFFSET(BOARD_STATUS_BASE, BOARD_CHIPID_LO), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "FPGA Chip ID");
	ESPER_CreateAttrNull(mid, vid, "format", "hex");

	vid = ESPER_CreateVarBool(mid, "force_run",		ESPER_OPTION_WR_RD, 1, &ctx->force_run, (uint8_t*)GET_REG_OFFSET(BOARD_CONTROL_BASE, BOARD_FORCE_RUN), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Force Run");

	vid = ESPER_CreateVarUInt8(mid, "module_id",		ESPER_OPTION_WR_RD, 1, &ctx->module_id, (uint8_t*)GET_REG_OFFSET(BOARD_CONTROL_BASE, BOARD_MODULE_ID), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Module ID");
	ESPER_CreateAttrNull(mid, vid, "format", "hex");

	// Removed, too dangerous!
	/*
	vid = ESPER_CreateVarBool(mid, "lmk_sync",		ESPER_OPTION_WR_RD, 1, &ctx->lmk_sync, (uint8_t*)GET_REG_OFFSET(BOARD_CONTROL_BASE, BOARD_LMK_SYNC), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "LMK Sync");
	 */

	vid = ESPER_CreateVarBool(mid, "adc_sync",		ESPER_OPTION_WR_RD, 1, &ctx->adc_sync, (uint8_t*)GET_REG_OFFSET(BOARD_CONTROL_BASE, BOARD_ADC_SYNC), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "ADC Sync");

	vid = ESPER_CreateVarBool(mid, "rst_serdes",		ESPER_OPTION_WR_RD, 1, &ctx->rst_serdes, (uint8_t*)GET_REG_OFFSET(BOARD_CONTROL_BASE, BOARD_RESET_SERDES), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Reset SERDES");

	vid = ESPER_CreateVarUInt8(mid, "clk_lmk", ESPER_OPTION_RD, 1, &ctx->clk_lmk, (uint8_t*)GET_REG_OFFSET(BOARD_STATUS_BASE, BOARD_LMK_CLKIN_SEL), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Clock Cleaner CLKinX");
	ESPER_CreateAttrNull(mid, vid, "format", "select");
	ESPER_CreateAttrUInt8(mid, vid, "option", "CLKin0 - Oscillator [100MHz]", 0);
	ESPER_CreateAttrUInt8(mid, vid, "option", "CLKin1 - eSATA [62.5MHz]", 1);
	ESPER_CreateAttrUInt8(mid, vid, "option", "CLKin2 - 4:1 Mux [12.5MHz]", 2);
	ESPER_CreateAttrUInt8(mid, vid, "option", "Holdover Mode", 3);

	vid = ESPER_CreateVarUInt8(mid, "clk_mux", ESPER_OPTION_RD, 1, &ctx->clk_mux, (uint8_t*)GET_REG_OFFSET(BOARD_STATUS_BASE, BOARD_REFCLK_SEL), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "4:1 Mux into CLKin2");
	ESPER_CreateAttrNull(mid, vid, "format", "select");
	ESPER_CreateAttrUInt8(mid, vid, "option", "FPGA", 0);
	ESPER_CreateAttrUInt8(mid, vid, "option", "125MHz1L", 1);
	ESPER_CreateAttrUInt8(mid, vid, "option", "M2C", 2);
	ESPER_CreateAttrUInt8(mid, vid, "option", "NIM", 3);

	vid = ESPER_CreateVarBool(mid, "lmk_status",		ESPER_OPTION_RD, 2, &ctx->lmk_status[0], (uint8_t*)GET_REG_OFFSET(BOARD_STATUS_BASE, BOARD_LMK_STATUSLD), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "LMK PLL1/PLL2 Status");

	vid = ESPER_CreateVarUInt8(mid, "lmk_device", ESPER_OPTION_RD, 1, &ctx->lmk_device, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "LMK Device");
	ESPER_CreateAttrNull(mid, vid, "format", "hex");

	vid = ESPER_CreateVarUInt16(mid, "lmk_product", ESPER_OPTION_RD, 1,&ctx->lmk_product, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "LMK Product");
	ESPER_CreateAttrNull(mid, vid, "format", "hex");

	vid = ESPER_CreateVarUInt16(mid, "lmk_vendor", ESPER_OPTION_RD, 1,&ctx->lmk_vendor, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "LMK Vendor");
	ESPER_CreateAttrNull(mid, vid, "format", "hex");


	vid = ESPER_CreateVarUInt8(mid, "lmk_revision", ESPER_OPTION_RD, 1,&ctx->lmk_maskrev, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "LMK Revision");
	ESPER_CreateAttrNull(mid, vid, "format", "select");
	ESPER_CreateAttrUInt8(mid, vid, "option", "LMK04821", 36);
	ESPER_CreateAttrUInt8(mid, vid, "option", "LMK04826", 37);
	ESPER_CreateAttrUInt8(mid, vid, "option", "LMK04828", 32);

	vid = ESPER_CreateVarUInt16(mid, "lmk_dac", ESPER_OPTION_RD, 1, &ctx->lmk_dac_val, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "LMK DAC");

	vid = ESPER_CreateVarBool(mid, "lmk_holdover", ESPER_OPTION_RD, 1, &ctx->lmk_stat_holdover, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "LMK Holdover Active");

	vid = ESPER_CreateVarBool(mid, "lmk_clk0_los", ESPER_OPTION_RD, 1, &ctx->lmk_stat_clkin0_los, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "LMK CLKin0 Lost");

	vid = ESPER_CreateVarBool(mid, "lmk_clk1_los", ESPER_OPTION_RD, 1, &ctx->lmk_stat_clkin1_los, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "LMK CLKin1 Lost");

	vid = ESPER_CreateVarBool(mid, "lmk_clk0_sel", ESPER_OPTION_RD, 1, &ctx->lmk_stat_clkin0_sel, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "LMK CLKin0 Selected");

	vid = ESPER_CreateVarBool(mid, "lmk_clk1_sel", ESPER_OPTION_RD, 1, &ctx->lmk_stat_clkin1_sel, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "LMK CLKin1 Selected");

	vid = ESPER_CreateVarBool(mid, "lmk_clk2_sel", ESPER_OPTION_RD, 1, &ctx->lmk_stat_clkin2_sel, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "LMK CLKin2 Selected");

	vid = ESPER_CreateVarBool(mid, "lmk_pll1_lock", ESPER_OPTION_RD, 1, &ctx->lmk_stat_pll1_ld_stat, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "LMK PLL1 Locked");

	vid = ESPER_CreateVarBool(mid, "lmk_pll2_lock", ESPER_OPTION_RD, 1, &ctx->lmk_stat_pll2_ld_stat, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "LMK PLL2 Locked");

	vid = ESPER_CreateVarUInt32(mid, "lmk_pll1_lcnt", ESPER_OPTION_RD, 1, &ctx->lmk_stat_pll1_ld_lost_cnt, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "LMK PLL1 Lock Lost Count");

	vid = ESPER_CreateVarUInt32(mid, "lmk_pll2_lcnt", ESPER_OPTION_RD, 1, &ctx->lmk_stat_pll2_ld_lost_cnt, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "LMK PLL2 Lock Lost Count");

	vid = ESPER_CreateVarBool(mid, "adc_locked",		ESPER_OPTION_RD, 4, &ctx->adc_locked[0], (uint8_t*)GET_REG_OFFSET(BOARD_STATUS_BASE, BOARD_ADC_LOCKED), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "ADC16 SERDES Locked");

	vid = ESPER_CreateVarUInt32(mid, "adc_locked_cnt",		ESPER_OPTION_RD, 4, &ctx->adc_locked_cnt[0], (uint8_t*)GET_REG_OFFSET(BOARD_STATUS_BASE, BOARD_ADC_LOCK_CNT0), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "ADC16 SERDES Locked Counter");

	vid = ESPER_CreateVarBool(mid, "adc_aligned",		ESPER_OPTION_RD, 4, &ctx->adc_aligned[0], (uint8_t*)GET_REG_OFFSET(BOARD_STATUS_BASE, BOARD_ADC_ALIGNED), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "ADC16 SERDES Aligned");

	vid = ESPER_CreateVarUInt32(mid, "adc_aligned_cnt",		ESPER_OPTION_RD, 4, &ctx->adc_aligned_cnt[0], (uint8_t*)GET_REG_OFFSET(BOARD_STATUS_BASE, BOARD_ADC_ALIGN_CNT0), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "ADC16 SERDES Aligned Counter");

	vid = ESPER_CreateVarBool(mid, "fmc_locked",		ESPER_OPTION_RD, 4, &ctx->fmc_locked[0], (uint8_t*)GET_REG_OFFSET(BOARD_STATUS_BASE, BOARD_FMC_ADC_LOCKED), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "FMC32 SERDES Locked");

	vid = ESPER_CreateVarUInt32(mid, "fmc_locked_cnt",		ESPER_OPTION_RD, 4, &ctx->fmc_locked_cnt[0], (uint8_t*)GET_REG_OFFSET(BOARD_STATUS_BASE, BOARD_FMC_ADC_LOCK_CNT0), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "FMC32 SERDES Locked Counter");

	vid = ESPER_CreateVarBool(mid, "fmc_aligned",		ESPER_OPTION_RD, 4, &ctx->fmc_aligned[0], (uint8_t*)GET_REG_OFFSET(BOARD_STATUS_BASE, BOARD_FMC_ADC_ALIGNED), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "FMC32 SERDES Aligned");

	vid = ESPER_CreateVarUInt32(mid, "fmc_aligned_cnt",		ESPER_OPTION_RD, 4, &ctx->fmc_aligned_cnt[0], (uint8_t*)GET_REG_OFFSET(BOARD_STATUS_BASE, BOARD_FMC_ADC_ALIGN_CNT0), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "FMC32 SERDES Aligned Counter");

	vid = ESPER_CreateVarUInt32(mid, "freq_nim",		ESPER_OPTION_RD, 1, &ctx->freq_nim, (uint8_t*)GET_REG_OFFSET(BOARD_STATUS_BASE, BOARD_FREQ_NIM_CNT), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "NIM Frequency");
	ESPER_CreateAttrNull(mid, vid, "format", "freq");

	vid = ESPER_CreateVarUInt32(mid, "freq_esata",			ESPER_OPTION_RD, 1, &ctx->freq_esata, (uint8_t*)GET_REG_OFFSET(BOARD_STATUS_BASE, BOARD_FREQ_ESATA_CNT), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "eSATA Frequency");
	ESPER_CreateAttrNull(mid, vid, "format", "freq");

	vid = ESPER_CreateVarUInt32(mid, "freq_clean0",			ESPER_OPTION_RD, 1, &ctx->freq_clean0, (uint8_t*)GET_REG_OFFSET(BOARD_STATUS_BASE, BOARD_FREQ_CLEAN0_CNT), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Clock Cleaner 0");
	ESPER_CreateAttrNull(mid, vid, "format", "freq");

	vid = ESPER_CreateVarUInt32(mid, "freq_clean1",			ESPER_OPTION_RD, 1, &ctx->freq_clean1, (uint8_t*)GET_REG_OFFSET(BOARD_STATUS_BASE, BOARD_FREQ_CLEAN1_CNT), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Clock Cleaner 1");
	ESPER_CreateAttrNull(mid, vid, "format", "freq");

	vid = ESPER_CreateVarUInt32(mid, "freq_adc",		ESPER_OPTION_RD, 1, &ctx->freq_adc, (uint8_t*)GET_REG_OFFSET(BOARD_STATUS_BASE, BOARD_FREQ_ADC_CNT), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "ADC16 Frequency");
	ESPER_CreateAttrNull(mid, vid, "format", "freq");

	vid = ESPER_CreateVarUInt32(mid, "freq_fmc",		ESPER_OPTION_RD, 1, &ctx->freq_fmc, (uint8_t*)GET_REG_OFFSET(BOARD_STATUS_BASE, BOARD_FREQ_FMC_CNT), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "FMC-ADC32 Frequency");
	ESPER_CreateAttrNull(mid, vid, "format", "freq");

	vid = ESPER_CreateVarUInt32(mid, "freq_eth",		ESPER_OPTION_RD, 1, &ctx->freq_eth, (uint8_t*)GET_REG_OFFSET(BOARD_STATUS_BASE, BOARD_FREQ_ETH_CNT), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Recovered Ethernet Frequency");
	ESPER_CreateAttrNull(mid, vid, "format", "freq");

	vid = ESPER_CreateVarBool(mid, "nim_ena", ESPER_OPTION_WR_RD, 1, &ctx->nim_ena, (uint8_t*)GET_REG_OFFSET(BOARD_CONTROL_BASE, BOARD_NIM_TRIG_ENA), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "NIM Trigger Enable");

	vid = ESPER_CreateVarBool(mid, "nim_inv", ESPER_OPTION_WR_RD, 1, &ctx->nim_inv, (uint8_t*)GET_REG_OFFSET(BOARD_CONTROL_BASE, BOARD_NIM_TRIG_INV), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "NIM Trigger Invert");

	vid = ESPER_CreateVarBool(mid, "esata_ena", ESPER_OPTION_WR_RD, 1, &ctx->esata_ena, (uint8_t*)GET_REG_OFFSET(BOARD_CONTROL_BASE, BOARD_ESATA_TRIG_ENA), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "eSATA Trigger Enable");

	vid = ESPER_CreateVarBool(mid, "esata_inv", ESPER_OPTION_WR_RD, 1, &ctx->esata_inv, (uint8_t*)GET_REG_OFFSET(BOARD_CONTROL_BASE, BOARD_ESATA_TRIG_INV), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "eSATA Trigger Invert");

	vid = ESPER_CreateVarBool(mid, "trig_nim_stat",		ESPER_OPTION_RD, 1, &ctx->trig_nim_stat, (uint8_t*)GET_REG_OFFSET(BOARD_STATUS_BASE, BOARD_NIM_TRIG_STAT), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "NIM Trigger Status");

	vid = ESPER_CreateVarUInt32(mid, "trig_nim_cnt",		ESPER_OPTION_RD, 1, &ctx->trig_nim_cnt, (uint8_t*)GET_REG_OFFSET(BOARD_STATUS_BASE, BOARD_NIM_TRIG_CNT), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "NIM Trigger Count");

	vid = ESPER_CreateVarBool(mid, "trig_esata_stat",		ESPER_OPTION_RD, 1, &ctx->trig_esata_stat, (uint8_t*)GET_REG_OFFSET(BOARD_STATUS_BASE, BOARD_ESATA_TRIG_STAT), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "eSATA Trigger Status");

	vid = ESPER_CreateVarUInt32(mid, "trig_esata_cnt",		ESPER_OPTION_RD, 1, &ctx->trig_esata_cnt, (uint8_t*)GET_REG_OFFSET(BOARD_STATUS_BASE, BOARD_ESATA_TRIG_CNT), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "eSATA Trigger Count");

	vid = ESPER_CreateVarUInt8(mid, "mac_addr", ESPER_OPTION_RD, 6, &ctx->mac_addr[0], 0,0);
	ESPER_CreateAttrNull(mid, vid, "name",	"Board MAC");
	ESPER_CreateAttrNull(mid, vid, "format", "mac");

	vid = ESPER_CreateVarFloat32(mid, "fpga_temp", ESPER_OPTION_RD, 1, &ctx->temp_fpga, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "FPGA Temperature");
	ESPER_CreateAttrNull(mid, vid, "unit", "C");
	ESPER_CreateAttrNull(mid, vid, "prefix", "");

	vid = ESPER_CreateVarFloat32(mid, "sensor_temp", ESPER_OPTION_RD, 9, ctx->temp_sensor, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "LTC2983 Temperature Sensor");
	ESPER_CreateAttrASCII(mid, vid, "si", "unit", "C");
	ESPER_CreateAttrSInt8(mid, vid, "si", "prefix", 0);
	ESPER_CreateAttrUInt8(mid, vid, "si", "precision", 2); // number of decimal places

	return ESPER_RESP_OK;
}

static eESPERResponse Start(tESPERMID mid, tESPERModuleBoard* ctx) {
	//InitLMK48xx(mid, ctx);
	InitLTC2983();

#if 0
        // temporary comment-out while debugging adc32 alignement
        // and sync problems. KO 2-NOV-2020.

	usleep(100000);

	// Toggle ADC Sync
	ESPER_WriteVarBool(mid, ESPER_GetVarIdByKey(mid, "adc_sync"), 0, 0);
	usleep(10000);
	ESPER_WriteVarBool(mid, ESPER_GetVarIdByKey(mid, "adc_sync"), 0, 1);
	usleep(10000);
	ESPER_WriteVarBool(mid, ESPER_GetVarIdByKey(mid, "adc_sync"), 0, 0);
	usleep(10000);

	usleep(100000);
	ESPER_WriteVarBool(mid, ESPER_GetVarIdByKey(mid, "rst_serdes"), 0, 0);
	usleep(10000);
	ESPER_WriteVarBool(mid, ESPER_GetVarIdByKey(mid, "rst_serdes"), 0, 1);
	usleep(10000);
	ESPER_WriteVarBool(mid, ESPER_GetVarIdByKey(mid, "rst_serdes"), 0, 0);
	usleep(10000);
#endif

	//usleep(100000);
	//usleep(100000);
	//usleep(100000);
	//usleep(100000);
	//usleep(100000);

	//ESPER_WriteVarBool(mid, ESPER_GetVarIdByKey(mid, "force_run"), 0, 1);

	return ESPER_RESP_OK;
}

static eESPERResponse Update(tESPERMID mid, tESPERModuleBoard* ctx) {
	tLMK48xxInfo info;

	// Update FPGA temp
	uint8_t temp_raw;

	temp_raw = *(uint8_t*)GET_REG_OFFSET(BOARD_STATUS_BASE, BOARD_FPGA_TEMP);
	if(temp_raw != ctx->temp_fpga_raw) {
		ctx->temp_fpga_raw = temp_raw;
		ctx->temp_fpga = (float)temp_raw - 128.0f;
		ESPER_TouchVar(mid, ESPER_GetVarIdByKey(mid, "fpga_temp"));
	}

	// Update these once a second
	if(!ltc2983_is_converting(SPI_TEMP_BASE, 0)) {
		ctx->temp_sensor[0] = ltc2983_ch_read_conversion(SPI_TEMP_BASE, 0, 4, TEMPERATURE);      // Ch 4: RTD PT-100
		ctx->temp_sensor[1] = ltc2983_ch_read_conversion(SPI_TEMP_BASE, 0, 6, TEMPERATURE);      // Ch 6: RTD PT-100
		ctx->temp_sensor[2] = ltc2983_ch_read_conversion(SPI_TEMP_BASE, 0, 8, TEMPERATURE);      // Ch 8: RTD PT-100
		ctx->temp_sensor[3] = ltc2983_ch_read_conversion(SPI_TEMP_BASE, 0, 10, TEMPERATURE);     // Ch 10: RTD PT-100
		ctx->temp_sensor[4] = ltc2983_ch_read_conversion(SPI_TEMP_BASE, 0, 12, TEMPERATURE);     // Ch 12: RTD PT-100
		ctx->temp_sensor[5] = ltc2983_ch_read_conversion(SPI_TEMP_BASE, 0, 14, TEMPERATURE);     // Ch 14: RTD PT-100
		ctx->temp_sensor[6] = ltc2983_ch_read_conversion(SPI_TEMP_BASE, 0, 16, TEMPERATURE);     // Ch 16: RTD PT-100
		ctx->temp_sensor[7] = ltc2983_ch_read_conversion(SPI_TEMP_BASE, 0, 18, TEMPERATURE);     // Ch 18: RTD PT-100
		ctx->temp_sensor[8] = ltc2983_ch_read_conversion(SPI_TEMP_BASE, 0, 20, TEMPERATURE);     // Ch 20: RTD PT-100
		ESPER_TouchVar(mid, ESPER_GetVarIdByKey(mid, "sensor_temp"));
		ltc2983_ch_start_conversion(SPI_TEMP_BASE, 0, 0); // all masked channels
	}

	LMK48xx_GetReadbackOnly(&info, lmk_spi_write, lmk_spi_read);

	if(ctx->lmk_dac_val != info.RB_DAC_VALUE) {
		ctx->lmk_dac_val = info.RB_DAC_VALUE;
		ESPER_TouchVarByKey(mid, "lmk_dac");
	}

	if(ctx->lmk_stat_holdover != info.RB_HOLDOVER) {
		ctx->lmk_stat_holdover = info.RB_HOLDOVER;
		ESPER_TouchVarByKey(mid, "lmk_holdover");
	}

	if(ctx->lmk_stat_clkin2_sel != info.RB_CLKin2_SEL) {
		ctx->lmk_stat_clkin2_sel = info.RB_CLKin2_SEL;
		ESPER_TouchVarByKey(mid, "lmk_clk2_sel");
	}

	if(ctx->lmk_stat_clkin1_sel != info.RB_CLKin1_SEL) {
		ctx->lmk_stat_clkin1_sel = info.RB_CLKin1_SEL;
		ESPER_TouchVarByKey(mid, "lmk_clk1_sel");
	}

	if(ctx->lmk_stat_clkin0_sel != info.RB_CLKin0_SEL) {
		ctx->lmk_stat_clkin0_sel = info.RB_CLKin0_SEL;
		ESPER_TouchVarByKey(mid, "lmk_clk0_sel");
	}

	if(ctx->lmk_stat_clkin1_los != info.RB_CLKin1_LOS) {
		ctx->lmk_stat_clkin1_los = info.RB_CLKin1_LOS;
		ESPER_TouchVarByKey(mid, "lmk_clk1_los");
	}

	if(ctx->lmk_stat_clkin0_los != info.RB_CLKin0_LOS) {
		ctx->lmk_stat_clkin0_los = info.RB_CLKin0_LOS;
		ESPER_TouchVarByKey(mid, "lmk_clk0_los");
	}

	if(info.RB_PLL1_LD_LOST) {
		ctx->lmk_stat_pll1_ld_lost_cnt++;
		ESPER_TouchVarByKey(mid, "lmk_pll1_lcnt");
	}

	if(info.RB_PLL2_LD_LOST) {
		ctx->lmk_stat_pll2_ld_lost_cnt++;
		ESPER_TouchVarByKey(mid, "lmk_pll2_lcnt");
	}

	if(ctx->lmk_stat_pll1_ld_stat != info.RB_PLL1_LD) {
		ctx->lmk_stat_pll1_ld_stat = info.RB_PLL1_LD;
		ESPER_TouchVarByKey(mid, "lmk_pll1_lock");
	}

	if(ctx->lmk_stat_pll2_ld_stat != info.RB_PLL2_LD) {
		ctx->lmk_stat_pll2_ld_stat = info.RB_PLL2_LD;
		ESPER_TouchVarByKey(mid, "lmk_pll2_lock");
	}

	return ESPER_RESP_OK;
}

static void InitLTC2983(void) {
	uint32_t channel_assignment_data;

	*(uint8_t*)GET_REG_OFFSET(BOARD_CONTROL_BASE, BOARD_TEMP_RST) = 0;
	usleep(200000);
	*(uint8_t*)GET_REG_OFFSET(BOARD_CONTROL_BASE, BOARD_TEMP_RST) = 1;
	usleep(200000); // 100ms startup time, we'll give it twice that

	// ----- Channel 2: Assign Sense Resistor -----
	channel_assignment_data =
			SENSOR_TYPE__SENSE_RESISTOR |
			(uint32_t) 0x1F4000 << SENSE_RESISTOR_VALUE_LSB;		// sense resistor - value: 2000.
	ltc2983_ch_assign(SPI_TEMP_BASE, 0, 2, channel_assignment_data);

		// ----- Channel 4: Assign RTD PT-100 -----
	channel_assignment_data =
			SENSOR_TYPE__RTD_PT_100 |
			RTD_RSENSE_CHANNEL__2 |
			RTD_NUM_WIRES__2_WIRE |
			RTD_EXCITATION_MODE__NO_ROTATION_SHARING |
			RTD_EXCITATION_CURRENT__500UA |
			RTD_STANDARD__AMERICAN;
	ltc2983_ch_assign(SPI_TEMP_BASE, 0, 4, channel_assignment_data);

	// ----- Channel 6: Assign RTD PT-100 -----
	channel_assignment_data =
			SENSOR_TYPE__RTD_PT_100 |
			RTD_RSENSE_CHANNEL__2 |
			RTD_NUM_WIRES__2_WIRE |
			RTD_EXCITATION_MODE__NO_ROTATION_SHARING |
			RTD_EXCITATION_CURRENT__500UA |
			RTD_STANDARD__AMERICAN;
	ltc2983_ch_assign(SPI_TEMP_BASE, 0, 6, channel_assignment_data);

	// ----- Channel 8: Assign RTD PT-100 -----
	channel_assignment_data =
			SENSOR_TYPE__RTD_PT_100 |
			RTD_RSENSE_CHANNEL__2 |
			RTD_NUM_WIRES__2_WIRE |
			RTD_EXCITATION_MODE__NO_ROTATION_SHARING |
			RTD_EXCITATION_CURRENT__500UA |
			RTD_STANDARD__AMERICAN;
	ltc2983_ch_assign(SPI_TEMP_BASE, 0, 8, channel_assignment_data);

	// ----- Channel 10: Assign RTD PT-100 -----
	channel_assignment_data =
			SENSOR_TYPE__RTD_PT_100 |
			RTD_RSENSE_CHANNEL__2 |
			RTD_NUM_WIRES__2_WIRE |
			RTD_EXCITATION_MODE__NO_ROTATION_SHARING |
			RTD_EXCITATION_CURRENT__500UA |
			RTD_STANDARD__AMERICAN;
	ltc2983_ch_assign(SPI_TEMP_BASE, 0, 10, channel_assignment_data);

	// ----- Channel 12: Assign RTD PT-100 -----
	channel_assignment_data =
			SENSOR_TYPE__RTD_PT_100 |
			RTD_RSENSE_CHANNEL__2 |
			RTD_NUM_WIRES__2_WIRE |
			RTD_EXCITATION_MODE__NO_ROTATION_SHARING |
			RTD_EXCITATION_CURRENT__500UA |
			RTD_STANDARD__AMERICAN;
	ltc2983_ch_assign(SPI_TEMP_BASE, 0, 12, channel_assignment_data);

	// ----- Channel 14: Assign RTD PT-100 -----
	channel_assignment_data =
			SENSOR_TYPE__RTD_PT_100 |
			RTD_RSENSE_CHANNEL__2 |
			RTD_NUM_WIRES__2_WIRE |
			RTD_EXCITATION_MODE__NO_ROTATION_SHARING |
			RTD_EXCITATION_CURRENT__500UA |
			RTD_STANDARD__AMERICAN;
	ltc2983_ch_assign(SPI_TEMP_BASE, 0, 14, channel_assignment_data);

	// ----- Channel 16: Assign RTD PT-100 -----
	channel_assignment_data =
			SENSOR_TYPE__RTD_PT_100 |
			RTD_RSENSE_CHANNEL__2 |
			RTD_NUM_WIRES__2_WIRE |
			RTD_EXCITATION_MODE__NO_ROTATION_SHARING |
			RTD_EXCITATION_CURRENT__500UA |
			RTD_STANDARD__AMERICAN;
	ltc2983_ch_assign(SPI_TEMP_BASE, 0, 16, channel_assignment_data);

	// ----- Channel 18: Assign RTD PT-100 -----
	channel_assignment_data =
			SENSOR_TYPE__RTD_PT_100 |
			RTD_RSENSE_CHANNEL__2 |
			RTD_NUM_WIRES__2_WIRE |
			RTD_EXCITATION_MODE__NO_ROTATION_SHARING |
			RTD_EXCITATION_CURRENT__500UA |
			RTD_STANDARD__AMERICAN;
	ltc2983_ch_assign(SPI_TEMP_BASE, 0, 18, channel_assignment_data);

	// ----- Channel 20: Assign RTD PT-100 -----
	channel_assignment_data =
			SENSOR_TYPE__RTD_PT_100 |
			RTD_RSENSE_CHANNEL__2 |
			RTD_NUM_WIRES__2_WIRE |
			RTD_EXCITATION_MODE__NO_ROTATION_SHARING |
			RTD_EXCITATION_CURRENT__500UA |
			RTD_STANDARD__AMERICAN;
	ltc2983_ch_assign(SPI_TEMP_BASE, 0, 20, channel_assignment_data);

	// Set global parameters
	ltc2983_transfer_byte(SPI_TEMP_BASE, 0, WRITE_TO_RAM, 0xF0, TEMP_UNIT__C | REJECTION__50_60_HZ);
	// Set any extra delay between conversions (in this case, 0*100us)
	ltc2983_transfer_byte(SPI_TEMP_BASE, 0, WRITE_TO_RAM, 0xFF, 0);
	// setup multiple channel masking for multiple read
	ltc2983_transfer_byte(SPI_TEMP_BASE, 0, WRITE_TO_RAM, 0xF5, 0x0A); // channel 20,18
	ltc2983_transfer_byte(SPI_TEMP_BASE, 0, WRITE_TO_RAM, 0xF6, 0xAA); // channel 16,14,12,10
	ltc2983_transfer_byte(SPI_TEMP_BASE, 0, WRITE_TO_RAM, 0xF7, 0xA8); // channel 8,6,4

	// start first conversion
	ltc2983_ch_start_conversion(SPI_TEMP_BASE, 0, 0); // all masked channels
}

static void lmk_spi_write(uint16_t addr, uint8_t cmd) {
	alt_u8 tx_data[3];

	tx_data[0] = (addr & 0x0F00) >> 8;
	tx_data[1] = (addr & 0x00FF);
	tx_data[2] = cmd;

	alt_avalon_spi_command(SPI_LMK48XX_BASE, 0, 3, (alt_u8*)&tx_data[0], 0, 0, 0);
}

static alt_u8 lmk_spi_read(uint16_t addr) {
	alt_u8 tx_data[3];
	alt_u8 rx_data;

	tx_data[0] = 0x80 | ((addr & 0x0F00) >> 8);
	tx_data[1] = (addr & 0x00FF);
	tx_data[2] = 0;

	alt_avalon_spi_command(SPI_LMK48XX_BASE, 0, 2, (alt_u8*)&tx_data[0], 1, (alt_u8*)&rx_data, 0);

	return rx_data;
}

