/*
 * mod_remote.h
 *
 *  Created on: Mar 27, 2017
 *      Author: admin
 */

// @NOTE: In order to use remote_update, the Altera Device must be configured to REMOTE mode inside Quartus Device Settings

#ifndef MOD_REMOTE_UPDATE_H_
#define MOD_REMOTE_UPDATE_H_

#include <drivers/inc/altera_remote_update_regs.h>
#include <drivers/inc/altera_remote_update.h>
#include <sys/alt_flash.h>
#include <esper/esper.h>

typedef struct {
	altera_remote_update_state* state;
	uint8_t image_selected;
	uint8_t wdtimer_source;
	uint8_t nconfig_source;
	uint8_t runconfig_source;
	uint8_t nstatus_source;
	uint8_t crcerror_source;
	uint8_t watchdog_enabled;
	uint32_t watchdog_timeout;
	uint32_t set_watchdog_timeout;
	uint8_t disable_watchdog_in_application_mode;
	uint8_t disable_watchdog_auto_reset;
	uint8_t reset_watchdog;
	uint32_t image_location;
	uint8_t config_mode;
	char dev_name[256];

	alt_flash_fd* flash_dev;

	uint8_t allow_write;
	uint8_t allow_write_to_factory;

	uint8_t* rpd_factory_chunk;
	uint8_t* rpd_file_chunk;
	uint8_t* temp_buffer;
} tESPERModuleRemoteUpdate;

eESPERResponse RemoteUpdateModuleHandler(tESPERMID mid, tESPERGID gid, eESPERState state, void* ctx);
tESPERModuleRemoteUpdate* RemoteUpdateModuleInit(char* dev_name, tESPERModuleRemoteUpdate* ctx);

#endif /* MOD_REMOTE_UPDATE_H_ */
