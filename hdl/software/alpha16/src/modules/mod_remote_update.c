/*
 * mod_remote.c
 *
 *  Created on: Mar 27, 2017
 *      Author: admin
 */

#include <stdio.h>
#include <unistd.h> // usleep()
#include <string.h>
#include <drivers/inc/altera_epcq_controller_regs.h>
#include <drivers/inc/altera_epcq_controller.h>
#include "osport.h" // TK_SLEEP()
#include "mod_remote_update.h"

// Divide the EPCQ256 in half
#define FACTORY_PAGE     0x0000000
#define APPLICATION_PAGE 0x1000000

// RPD is the entire upper-half of the EPCQ256, as it is a combination of all files
#define RPD_MAXSIZE		(APPLICATION_PAGE)

static void Init(tESPERMID mid, tESPERModuleRemoteUpdate* data);
static void Start(tESPERMID mid, tESPERModuleRemoteUpdate* data);
static void Update(tESPERMID mid, tESPERModuleRemoteUpdate* data);

static uint8_t Reconfig(tESPERMID mid, tESPERVID vid, tESPERVar* var, eESPERRequest request, uint32_t offset, uint32_t num_elements, void* ctx);

static uint8_t RPDApplicationHandler(tESPERMID mid, tESPERVID vid, tESPERVar* var, eESPERRequest request, uint32_t offset, uint32_t num_elements, void* ctx);
static uint8_t RPDFactoryHandler(tESPERMID mid, tESPERVID vid, tESPERVar* var, eESPERRequest request, uint32_t offset, uint32_t num_elements, void* ctx);
static uint8_t AllowFactoryHandler(tESPERMID mid, tESPERVID vid, tESPERVar* var, eESPERRequest request, uint32_t offset, uint32_t num_elements, void* ctx);
static uint8_t FileHandler(uint32_t file_offset, uint32_t file_maxsize, tESPERVar* var, eESPERRequest request, uint32_t offset, uint32_t num_elements, void* ctx);

eESPERResponse RemoteUpdateModuleHandler(tESPERMID mid, tESPERGID gid, eESPERState state, void* ctx) {
	switch(state) {
	case ESPER_STATE_INIT:
		Init(mid, (tESPERModuleRemoteUpdate*)ctx);
		break;
	case ESPER_STATE_START:
		Start(mid, (tESPERModuleRemoteUpdate*)ctx);
		break;
	case ESPER_STATE_UPDATE:
		Update(mid, (tESPERModuleRemoteUpdate*)ctx);
		break;
	case ESPER_STATE_STOP:
		break;
	}

	return ESPER_RESP_OK;
}

tESPERModuleRemoteUpdate* RemoteUpdateModuleInit(char* dev_name, tESPERModuleRemoteUpdate* ctx) {
	uint32_t reg;

	if(!ctx) return 0;

	esper_strcpy(ctx->dev_name, dev_name, 256);

	ctx->rpd_file_chunk = malloc(EPCQ_CONFIG_AVL_CSR_SECTOR_SIZE);
	ctx->rpd_factory_chunk = malloc(EPCQ_CONFIG_AVL_CSR_SECTOR_SIZE);
	ctx->temp_buffer = malloc(EPCQ_CONFIG_AVL_CSR_SECTOR_SIZE);

	ctx->state = altera_remote_update_open(ctx->dev_name);

	reg = IORD_ALTERA_RU_RECONFIG_TRIGGER_CONDITIONS(ctx->state->base);
	ctx->wdtimer_source 	= ((reg & (1 << 4)) != 0) ? 1 : 0;
	ctx->nconfig_source 	= ((reg & (1 << 3)) != 0) ? 1 : 0;
	ctx->runconfig_source 	= ((reg & (1 << 2)) != 0) ? 1 : 0;
	ctx->nstatus_source 	= ((reg & (1 << 1)) != 0) ? 1 : 0;
	ctx->crcerror_source 	= ((reg & (1 << 0)) != 0) ? 1 : 0;

	reg = IORD_ALTERA_RU_WATCHDOG_ENABLE(ctx->state->base);
	ctx->watchdog_enabled 	= ((reg & ALTERA_RU_WATCHDOG_ENABLE_MASK) != 0) ? 1 : 0;
	ctx->watchdog_timeout = IORD_ALTERA_RU_WATCHDOG_TIMEOUT(ctx->state->base);

	reg = IORD_ALTERA_RU_CONFIG_MODE(ctx->state->base);
	ctx->config_mode 		= reg & ALTERA_RU_RECONFIG_MODE_MASK;

	reg = IORD_ALTERA_RU_PAGE_SELECT(ctx->state->base);
	ctx->image_location 		= reg;

	if(ctx->image_location == 0) {
		ctx->image_selected = 0;
	} else {
		ctx->image_selected = 1; // user page booted
	}

	ctx->allow_write = 0;
	ctx->allow_write_to_factory = 0;

        //#define WATCHDOG_TIMEOUT_PERIOD ((12500000u * 30u) >> 17)
        //#define WATCHDOG_TIMEOUT_PERIOD ((12500000u * 30u) >> 17)
        //#define WATCHDOG_TIMEOUT_PERIOD (10)

	ctx->set_watchdog_timeout = ((12500000u * 30u) >> 17);
	ctx->disable_watchdog_in_application_mode = 0;
	ctx->disable_watchdog_auto_reset = 0;
	ctx->reset_watchdog = 0;

	return ctx;
}

static void Init(tESPERMID mid,  tESPERModuleRemoteUpdate* ctx) {
	tESPERVID vid;

	// attempt to load flash
	ctx->flash_dev = alt_flash_open_dev(EPCQ_CONFIG_AVL_MEM_NAME);

        // registers of remote update block

	vid = ESPER_CreateVarBool(mid, "wdtimer_src",	ESPER_OPTION_RD, 1, &ctx->wdtimer_source, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name",	"User Watchdog Timeout");

	vid = ESPER_CreateVarBool(mid, "nconfig_src",	ESPER_OPTION_RD, 1, &ctx->nconfig_source, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name",	"External nCONFIG Asserted");

	vid = ESPER_CreateVarBool(mid, "runconfig_src",ESPER_OPTION_RD, 1, &ctx->runconfig_source, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name",	"Reset Requested");

	vid = ESPER_CreateVarBool(mid, "nstatus_src",	ESPER_OPTION_RD, 1, &ctx->nstatus_source, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name",	"External nSTATUS Asserted");

	vid = ESPER_CreateVarBool(mid, "crcerror_src",	ESPER_OPTION_RD, 1, &ctx->crcerror_source, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name",	"User CRC Error");

	vid = ESPER_CreateVarBool(mid, "watchdog_ena",ESPER_OPTION_RD, 1, &ctx->watchdog_enabled, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name",	"Watchdog Enabled");

	vid = ESPER_CreateVarUInt32(mid, "watchdog_timeout",ESPER_OPTION_RD, 1, &ctx->watchdog_timeout, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name",	"Watchdog Timeout");

        // watchdog setup

	vid = ESPER_CreateVarBool(mid, "disable_watchdog", ESPER_OPTION_WR_RD, 1, &ctx->disable_watchdog_in_application_mode, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name",	"Disable watchdog when booted into User image");

	vid = ESPER_CreateVarUInt32(mid, "set_watchdog_timeout",ESPER_OPTION_WR_RD, 1, &ctx->set_watchdog_timeout, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name",	"Set Watchdog Timeout for next reboot");

	vid = ESPER_CreateVarBool(mid, "disable_watchdog_autoreset", ESPER_OPTION_WR_RD, 1, &ctx->disable_watchdog_auto_reset, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name",	"Disable watchdog automatic reset");

	vid = ESPER_CreateVarBool(mid, "reset_watchdog", ESPER_OPTION_WR_RD, 1, &ctx->reset_watchdog, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name",	"Manually reset the watchdog");

        // page selection, etc...

	vid = ESPER_CreateVarBool(mid, "application", 	ESPER_OPTION_RD, 1, &ctx->config_mode, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name",	"User Image Loaded");

	vid = ESPER_CreateVarUInt32(mid, "image_location", ESPER_OPTION_RD, 1, &ctx->image_location, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name",	"Loaded Image Location");
	ESPER_CreateAttrNull(mid, vid, "format", "hex");

	vid = ESPER_CreateVarUInt8(mid, "image_selected", ESPER_OPTION_WR_RD, 1, &ctx->image_selected, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name",	"Image Select");
	ESPER_CreateAttrNull(mid, vid, "format", "select");
	ESPER_CreateAttrUInt8(mid, vid, "option", "Factory", 0);
	ESPER_CreateAttrUInt8(mid, vid, "option", "User", 1);

	vid = ESPER_CreateVarNull(mid, "reconfigure", ESPER_OPTION_WR, 1, Reconfig);
	ESPER_CreateAttrNull(mid, vid, "name",	"Load Selected Image");

	vid = ESPER_CreateVarBool(mid, "allow_write", ESPER_OPTION_WR_RD, 1, &ctx->allow_write, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name",	"Allow Write To Flash Memory");

	vid = ESPER_CreateVarRaw(mid, "file_rpd", ESPER_OPTION_WR_RD | ESPER_OPTION_WINDOW, EPCQ_CONFIG_AVL_CSR_SECTOR_SIZE, ctx->rpd_file_chunk, 0, RPDApplicationHandler);
	ESPER_CreateAttrNull(mid, vid, "name", "Application RPD");
	ESPER_CreateAttrNull(mid, vid, "format", "file");
	ESPER_CreateAttrASCII(mid, vid, "fileopt", "accept", ".rpd");
	ESPER_CreateAttrUInt32(mid, vid, "fileopt", "maxsize", RPD_MAXSIZE);

	vid = ESPER_CreateVarRaw(mid, "factory_rpd", ESPER_OPTION_WR_RD | ESPER_OPTION_WINDOW /*| ESPER_OPTION_LOCKABLE*/, EPCQ_CONFIG_AVL_CSR_SECTOR_SIZE, ctx->rpd_factory_chunk, 0, RPDFactoryHandler);
	ESPER_CreateAttrNull(mid, vid, "name", "Factory RPD");
	ESPER_CreateAttrNull(mid, vid, "format", "file");
	ESPER_CreateAttrASCII(mid, vid, "fileopt", "accept", ".rpd");
	ESPER_CreateAttrUInt32(mid, vid, "fileopt", "maxsize", RPD_MAXSIZE);

	vid = ESPER_CreateVarBool(mid, "allow_factory_write", ESPER_OPTION_WR_RD, 1, &ctx->allow_write_to_factory, 0, AllowFactoryHandler);
	ESPER_CreateAttrNull(mid, vid, "name",	"Allow Write To Factory Page");
}

static void Start(tESPERMID mid, tESPERModuleRemoteUpdate* ctx) {
	if(ctx->allow_write_to_factory) {
           //ESPER_UnlockVar(mid, ESPER_GetVarIdByKey(mid, "factory_rpd"));
	} else {
           //ESPER_LockVar(mid, ESPER_GetVarIdByKey(mid, "factory_rpd"));
	}
}

static void Update(tESPERMID mid, tESPERModuleRemoteUpdate* ctx) {
	// If the watchdog was enabled by the remote upgrade ip, toggle it!
	if(ctx->watchdog_enabled) {
           if (ctx->disable_watchdog_auto_reset) {
              if (ctx->reset_watchdog) {
                 //printf("manual reset watchdog!\n");
                 ctx->reset_watchdog = 0;
                 IOWR_ALTERA_RU_RESET_TIMER(ctx->state->base, 1);
              }
           } else {
		IOWR_ALTERA_RU_RESET_TIMER(ctx->state->base, 1);
           }
	}
}

static uint8_t Reconfig(tESPERMID mid, tESPERVID vid, tESPERVar* var, eESPERRequest request, uint32_t offset, uint32_t num_elements, void* ctx) {
	tESPERModuleRemoteUpdate* remote_ctx = (tESPERModuleRemoteUpdate*)ctx;

	switch(request) {
	case ESPER_REQUEST_WRITE_POST:
		if(remote_ctx->image_selected == 0) {
			alt_epcq_controller_lock(remote_ctx->flash_dev, 0x1F); // lock out entire EPCQ256
                        printf("Rebooting to Factory Image...\n");
                        usleep(500000);
			altera_remote_update_trigger_reconfig(remote_ctx->state, ALTERA_RU_RECONFIG_MODE_FACTORY, FACTORY_PAGE, 0);
		} else {
			//alt_epcq_controller_lock(remote_ctx->flash_dev, 0x19); // lock out the lower half of the EPCQ, sectors 0-255
			alt_epcq_controller_lock(remote_ctx->flash_dev, 0x1F); // lock out entire EPCQ256

                        alt_u32 watch_dog_timeout = remote_ctx->set_watchdog_timeout;

                        if (!remote_ctx->disable_watchdog_in_application_mode) {
                           printf("Setting Watchdog Timeout 0%08lx\n", (long unsigned int)watch_dog_timeout);
                           IOWR_ALTERA_RU_WATCHDOG_TIMEOUT(REMOTE_UPDATE_BASE, watch_dog_timeout);
                           printf("Enabling Watchdog!\n");
                           IOWR_ALTERA_RU_WATCHDOG_ENABLE(REMOTE_UPDATE_BASE, ALTERA_RU_WATCHDOG_ENABLE);
                           
                           printf("Watchdog enable: 0x%08x\n", IORD_ALTERA_RU_WATCHDOG_ENABLE(REMOTE_UPDATE_BASE));
                           printf("Watchdog timeout: 0x%08x\n", IORD_ALTERA_RU_WATCHDOG_TIMEOUT(REMOTE_UPDATE_BASE));
                        } else {
                           printf("Disabling Watchdog!\n");
                           IOWR_ALTERA_RU_WATCHDOG_TIMEOUT(REMOTE_UPDATE_BASE, 0);
                           IOWR_ALTERA_RU_WATCHDOG_ENABLE(REMOTE_UPDATE_BASE, 0);
                        }

                        printf("Rebooting to User Image...\n");
                        for (int i=0; i<5; i++) {
                           usleep(500000);
                        }
			altera_remote_update_trigger_reconfig(remote_ctx->state, ALTERA_RU_RECONFIG_MODE_APP, APPLICATION_PAGE, watch_dog_timeout);
		}
		break;
	default:
		break;
	}

	return 1;
}

static uint8_t RPDApplicationHandler(tESPERMID mid, tESPERVID vid, tESPERVar* var, eESPERRequest request, uint32_t offset, uint32_t num_elements, void* ctx) {
	// Hack to fix RPD download
	var->info.num_elements = RPD_MAXSIZE;
	return FileHandler(APPLICATION_PAGE, RPD_MAXSIZE, var, request, offset, num_elements, ctx);
}

static uint8_t RPDFactoryHandler(tESPERMID mid, tESPERVID vid, tESPERVar* var, eESPERRequest request, uint32_t offset, uint32_t num_elements, void* ctx) {
	// Hack to fix RPD download
	var->info.num_elements = RPD_MAXSIZE;
	return FileHandler(FACTORY_PAGE, RPD_MAXSIZE, var, request, offset, num_elements, ctx);
}

static uint8_t AllowFactoryHandler(tESPERMID mid, tESPERVID vid, tESPERVar* var, eESPERRequest request, uint32_t offset, uint32_t num_elements, void* ctx) {
	tESPERModuleRemoteUpdate* context;

	context = (tESPERModuleRemoteUpdate*)ctx;

	switch(request) {
		case ESPER_REQUEST_INIT:
		case ESPER_REQUEST_REFRESH:
			break;
		case ESPER_REQUEST_WRITE_PRE:
			break;
		 case ESPER_REQUEST_WRITE_POST:
			if(context->allow_write_to_factory) {
                           //ESPER_UnlockVar(mid, ESPER_GetVarIdByKey(mid, "factory_rpd"));
			} else {
                           //ESPER_LockVar(mid, ESPER_GetVarIdByKey(mid, "factory_rpd"));
			}
			break;
		case ESPER_REQUEST_READ_PRE:
			break;
	}

	return 1;
}


static uint8_t FileHandler(uint32_t file_offset, uint32_t file_maxsize, tESPERVar* var, eESPERRequest request, uint32_t offset, uint32_t num_elements, void* ctx) {
	tESPERModuleRemoteUpdate* context;

    if(!ctx) {
        ESPER_LOG(ESPER_DEBUG_LEVEL_CRIT, "Null ctx passed");
        return 0;
    }

    if(!num_elements) {
        ESPER_LOG(ESPER_DEBUG_LEVEL_CRIT, "Null num_elements passed");
        return 0;
    }

    context = (tESPERModuleRemoteUpdate*)ctx;

    if(!context->flash_dev) {
    	ESPER_LOG(ESPER_DEBUG_LEVEL_CRIT, "Flash device (EPCQ) not opened");
    	return 0;
    }

    // Request exceeds size for SOF
    if((offset + num_elements) > file_maxsize) {
    	ESPER_LOG(ESPER_DEBUG_LEVEL_WARN, "Request exceeds maximum file size");
    	return 0;
    }

    switch(request) {
    case ESPER_REQUEST_INIT:
    case ESPER_REQUEST_REFRESH:
        break;
    case ESPER_REQUEST_WRITE_PRE:
    	if((offset % var->info.max_elements_per_request) != 0) {
    		// Bail write isn't block aligned
    		ESPER_LOG(ESPER_DEBUG_LEVEL_WARN, "Bad block offset: %u, expected something modulus by %u", offset, var->info.max_elements_per_request);
    		return 0;
    	}
        break;
     case ESPER_REQUEST_WRITE_POST:
        // Check buffer against what is already in flash, skip the write sequence, otherwise erase then write
    	alt_read_flash(context->flash_dev, file_offset + offset, context->temp_buffer, num_elements);
    	if(memcmp(var->data, context->temp_buffer, num_elements) != 0) {

           if (!context->allow_write) {
              // Don't allow writing to flash memory
              return 0;
           }

           if((file_offset == FACTORY_PAGE) && (context->allow_write_to_factory == 0)) {
              // Don't allow flashing to factory unless the option has been set
              return 0;
           }

    		if(file_offset == FACTORY_PAGE) {
    			alt_epcq_controller_lock(context->flash_dev, 0x00); // unlock all of the EPCQ
    		} else {
    			alt_epcq_controller_lock(context->flash_dev, 0x19); // lock out the lower half of the EPCQ, sectors 0-255
    		}
                TK_SLEEP(1);
    		alt_erase_flash_block(context->flash_dev, file_offset + offset, var->info.max_elements_per_request);	// Erase whole block
                TK_SLEEP(1);
    		alt_write_flash(context->flash_dev, file_offset + offset, var->data, num_elements);	// Write in new data
                TK_SLEEP(1);
    		alt_epcq_controller_lock(context->flash_dev, 0x1F); // lock out entire EPCQ256
                TK_SLEEP(1);

                // verify flash contents
                alt_read_flash(context->flash_dev, file_offset + offset, context->temp_buffer, num_elements);
                TK_SLEEP(1);
                if(memcmp(var->data, context->temp_buffer, num_elements) != 0) {
                   ESPER_LOG(ESPER_DEBUG_LEVEL_WARN, "Flash memory miscompare at offset %u", offset);
                   return 0;
                }
    	}
    	break;
    case ESPER_REQUEST_READ_PRE:
    	// load into var->data
    	alt_read_flash(context->flash_dev, file_offset + offset, var->data, num_elements);
        break;
    }

    return 1;
}
