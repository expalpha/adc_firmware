/*
 * mod_udp.c
 *
 *  Created on: Dec 9, 2016
 *      Author: admin
 */

#include <assert.h>
#include <string.h>
#include <i2c_opencores_regs.h>
#include <i2c_opencores.h>
#include <alt_iniche_dev.h>
#include <iniche/src/h/icmp.h>
#include <iniche/src/h/arp.h>
#include "mod_offload.h"

static eESPERResponse Init(tESPERMID mid, tESPERModuleUDPOffload* ctx);
static eESPERResponse Start(tESPERMID mid, tESPERModuleUDPOffload* ctx);
static eESPERResponse Update(tESPERMID mid, tESPERModuleUDPOffload* ctx);

static uint8_t UDP_Reset(tESPERMID mid, tESPERVID vid, tESPERVar* var, eESPERRequest request, uint32_t offset, uint32_t num_elements, void* ctx);

static struct arptabent *find_arp_entry(ip_addr dest_ip);

tESPERModuleUDPOffload* ModuleUDPOffloadInit(uint32_t udp_base, uint8_t* src_mac, uint32_t dstIP, uint16_t dstPort, uint16_t sendPort, tESPERModuleUDPOffload* ctx) {
	if(!ctx) return 0;

	ctx->udp_base = udp_base;
	memcpy(ctx->src_mac, src_mac, sizeof(ctx->src_mac));
	ctx->dst_ip = htonl(dstIP);
	ctx->dst_port = dstPort;
	ctx->src_port = sendPort;

	return ctx;
}

eESPERResponse ModuleUDPOffloadHandler(tESPERMID mid, tESPERGID gid, eESPERState state, void* ctx) {
	switch(state) {
	case ESPER_STATE_INIT:
		return Init(mid, (tESPERModuleUDPOffload*)ctx);
	case ESPER_STATE_START:
		return Start(mid, (tESPERModuleUDPOffload*)ctx);
	case ESPER_STATE_UPDATE:
		return Update(mid, (tESPERModuleUDPOffload*)ctx);
	case ESPER_STATE_STOP:
		break;
	}

	return ESPER_RESP_OK;
}

static eESPERResponse Init(tESPERMID mid, tESPERModuleUDPOffload* ctx){
	tESPERVID vid;

	vid = ESPER_CreateVarUInt32(mid, "dst_ip", 	ESPER_OPTION_WR_RD, 1, &ctx->dst_ip, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name",	"Destination IP");
	ESPER_CreateAttrNull(mid, vid, "format",	"ip");

	vid = ESPER_CreateVarUInt16(mid, "dst_port", 	ESPER_OPTION_WR_RD, 1, &ctx->dst_port, 0,0);
	ESPER_CreateAttrNull(mid, vid, "name",	"Destination Port");

	vid = ESPER_CreateVarUInt8(mid, "dst_mac", 	ESPER_OPTION_RD, 6, &ctx->dst_mac[0], 0,0);
	ESPER_CreateAttrNull(mid, vid, "name",	"Destination MAC");
	ESPER_CreateAttrNull(mid, vid, "format",	"mac");

	vid = ESPER_CreateVarUInt32(mid, "src_ip", 	ESPER_OPTION_RD, 1, &ctx->src_ip, 0,0);
	ESPER_CreateAttrNull(mid, vid, "name",	"Source IP");
	ESPER_CreateAttrNull(mid, vid, "format",	"ip");

	vid = ESPER_CreateVarUInt16(mid, "src_port", 	ESPER_OPTION_RD, 1, &ctx->src_port, 0,0);
	ESPER_CreateAttrNull(mid, vid, "name",	"Source Port");

	vid = ESPER_CreateVarUInt32(mid, "src_mask", 	ESPER_OPTION_RD, 1, &ctx->src_mask, 0,0);
	ESPER_CreateAttrNull(mid, vid, "name",	"Source Netmask");
	ESPER_CreateAttrNull(mid, vid, "format",	"ip");

	vid = ESPER_CreateVarUInt32(mid, "src_gw", 	ESPER_OPTION_RD, 1, &ctx->src_gw, 0,0);
	ESPER_CreateAttrNull(mid, vid, "name",	"Source Gateway");
	ESPER_CreateAttrNull(mid, vid, "format",	"ip");

	vid = ESPER_CreateVarUInt8(mid, "src_mac", 	ESPER_OPTION_RD, 6, &ctx->src_mac[0], 0,0);
	ESPER_CreateAttrNull(mid, vid, "name",	"Source MAC");
	ESPER_CreateAttrNull(mid, vid, "format",	"mac");

	vid = ESPER_CreateVarBool(mid, "enable", 		ESPER_OPTION_WR_RD, 1, &ctx->enable, 0,0);
	ESPER_CreateAttrNull(mid, vid, "name",	"Enable UDP Transmission");

	vid = ESPER_CreateVarNull(mid, "reset", 		ESPER_OPTION_WR, 1, UDP_Reset);
	ESPER_CreateAttrNull(mid, vid, "name",	"Reset UDP Offloader");

	vid = ESPER_CreateVarUInt32(mid, "status", 	ESPER_OPTION_RD, 1, &ctx->status, (void*)&ctx->stats.csr_state, 0);
	ESPER_CreateAttrNull(mid, vid, "name",	"UDP Offloader Status");
	ESPER_CreateAttrNull(mid, vid, "format", "select");
	ESPER_CreateAttrUInt32(mid, vid, "option", "Off", 0);
	ESPER_CreateAttrUInt32(mid, vid, "option", "Off, Error", FABRIC_UDP_STREAM_CSR_ERROR_BIT_MASK);
	ESPER_CreateAttrUInt32(mid, vid, "option", "On", FABRIC_UDP_STREAM_CSR_GO_BIT_MASK);
	ESPER_CreateAttrUInt32(mid, vid, "option", "On, Running", FABRIC_UDP_STREAM_CSR_GO_BIT_MASK | FABRIC_UDP_STREAM_CSR_RUNNING_BIT_MASK);
	ESPER_CreateAttrUInt32(mid, vid, "option", "On, Error", FABRIC_UDP_STREAM_CSR_GO_BIT_MASK | FABRIC_UDP_STREAM_CSR_ERROR_BIT_MASK);
	ESPER_CreateAttrUInt32(mid, vid, "option", "On, Running, Error", FABRIC_UDP_STREAM_CSR_GO_BIT_MASK | FABRIC_UDP_STREAM_CSR_ERROR_BIT_MASK | FABRIC_UDP_STREAM_CSR_RUNNING_BIT_MASK);

	vid = ESPER_CreateVarUInt32(mid, "tx_cnt", 	ESPER_OPTION_RD, 1, &ctx->packet_count, &ctx->stats.packet_count, 0);
	ESPER_CreateAttrNull(mid, vid, "name",	"Packets Sent");

	StopFabricUDPStream(ctx->udp_base);
	FABRIC_UDP_STREAM_CLEAR_PACKET_COUNTER(ctx->udp_base);


	return ESPER_RESP_OK;
}

static eESPERResponse Start(tESPERMID mid, tESPERModuleUDPOffload* ctx) {
	return ESPER_RESP_OK;
}

static eESPERResponse Update(tESPERMID mid, tESPERModuleUDPOffload* ctx){
	uint32_t n;
	static uint32_t seqnum;
	static char echoData[8];
	struct arptabent *tp;

	// Get latest IP address information
	ctx->src_ip = 	htonl(nets[0]->n_ipaddr);
	ctx->src_mask = htonl(nets[0]->snmask);
	ctx->src_gw = htonl(nets[0]->n_defgw);

	if(ctx->enable) {
		if((ctx->stats.ip_dst != ctx->dst_ip) || (ctx->stats.udp_dst != ctx->dst_port) || (ctx->stats.udp_src != ctx->src_port)) {
			n = 0;
			icmpEcho(htonl(ctx->dst_ip), echoData, 8, seqnum++);
			n = 0;
			do {
				usleep(10000);
				tp = find_arp_entry(ctx->dst_ip);
				if(tp) {
					ctx->dst_mac[0] = tp->t_phy_addr[0];
					ctx->dst_mac[1] = tp->t_phy_addr[1];
					ctx->dst_mac[2] = tp->t_phy_addr[2];
					ctx->dst_mac[3] = tp->t_phy_addr[3];
					ctx->dst_mac[4] = tp->t_phy_addr[4];
					ctx->dst_mac[5] = tp->t_phy_addr[5];
					ESPER_TouchVar(mid, ESPER_GetVarIdByKey(mid, "dst_mac"));
				}
				n++;
			} while(( n < 10) && (tp == 0));

			if(tp != 0) {
				ctx->stats.ip_dst = ctx->dst_ip;
				ctx->stats.ip_src = ctx->src_ip;
				ctx->stats.mac_dst_hi = (ctx->dst_mac[0] << 24 ) | (ctx->dst_mac[1] << 16 ) | (ctx->dst_mac[2] << 8 ) | (ctx->dst_mac[3]);
				ctx->stats.mac_dst_lo = (ctx->dst_mac[4] <<  8 ) | (ctx->dst_mac[5]);
				ctx->stats.mac_src_hi = (ctx->src_mac[0] << 24 ) | (ctx->src_mac[1] << 16 ) | (ctx->src_mac[2] << 8 ) | (ctx->src_mac[3]);
				ctx->stats.mac_src_lo = (ctx->src_mac[4] <<  8 ) | (ctx->src_mac[5]);
				ctx->stats.udp_dst = ctx->dst_port;
				ctx->stats.udp_src = ctx->src_port;
			} else {
				// failed to start, don't keep trying on failure
				ESPER_WriteVarBool(mid, ESPER_GetVarIdByKey(mid, "enable"), 0, 0);
			}
		}
		if(ctx->enable) {
			StartFabricUDPStream(ctx->udp_base, (tFabricUDPStreamStats*)&ctx->stats);
		}
	} else {
		StopFabricUDPStream(ctx->udp_base);
	}

	GetFabricUDPStreamStats(ctx->udp_base, (tFabricUDPStreamStats*)&ctx->stats);

	return ESPER_RESP_OK;
}

static uint8_t UDP_Reset(tESPERMID mid, tESPERVID vid, tESPERVar* var, eESPERRequest request, uint32_t offset, uint32_t num_elements, void* ctx) {
	tESPERModuleUDPOffload* udp_ctx = (tESPERModuleUDPOffload*)ctx;

	switch(request) {
	case ESPER_REQUEST_WRITE_POST:
		StopFabricUDPStream(udp_ctx->udp_base);
		if(udp_ctx->enable) {
			StartFabricUDPStream(udp_ctx->udp_base, (tFabricUDPStreamStats*)&udp_ctx->stats);
		}
		break;
	default:
		break;
	}

	return 1;
}

static struct arptabent *find_arp_entry(ip_addr dest_ip) {
	uint32_t n;

	for (n=0; n < MAXARPS; n++) {
		if (arp_table[n].t_pro_addr == htonl(dest_ip)) {
			return &arp_table[n];
		}
	}

	return 0;
}
