/*
 * mod_sfp.c
 *
 *  Created on: Dec 9, 2016
 *      Author: admin
 */

#include <assert.h>
#include <i2c_opencores_regs.h>
#include <i2c_opencores.h>
#include "mod_sfp.h"


static eESPERResponse Init(tESPERMID mid, tESPERModuleSFP* ctx);
static eESPERResponse Start(tESPERMID mid, tESPERModuleSFP* ctx);
static eESPERResponse Update(tESPERMID mid, tESPERModuleSFP* ctx);

tESPERModuleSFP* ModuleSFPInit(uint32_t i2c_base, tESPERModuleSFP* ctx) {
	if(!ctx) return 0;

	ctx->i2c_base = i2c_base;

	return ctx;
}

eESPERResponse ModuleSFPHandler(tESPERMID mid, tESPERGID gid, eESPERState state, void* ctx) {
	switch(state) {
	case ESPER_STATE_INIT:
		return Init(mid, (tESPERModuleSFP*)ctx);
	case ESPER_STATE_START:
		return Start(mid, (tESPERModuleSFP*)ctx);
	case ESPER_STATE_UPDATE:
		return Update(mid, (tESPERModuleSFP*)ctx);
	case ESPER_STATE_STOP:
		break;
	}

	return ESPER_RESP_OK;
}

static eESPERResponse Init(tESPERMID mid, tESPERModuleSFP* ctx){
	tESPERVID vid;

	vid = ESPER_CreateVarUInt8(mid, "serial_xcvr", 	ESPER_OPTION_RD, 1, &ctx->sfp_info.transceiver_type, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Identifier");
	ESPER_CreateAttrNull(mid, vid, "format", "select");
	ESPER_CreateAttrUInt8(mid, vid, "option", "Unknown", 0x00);
	ESPER_CreateAttrUInt8(mid, vid, "option", "GBIC", 0x01);
	ESPER_CreateAttrUInt8(mid, vid, "option", "Built-in", 0x02);
	ESPER_CreateAttrUInt8(mid, vid, "option", "SFP/SFP+", 0x03);
	ESPER_CreateAttrUInt8(mid, vid, "option", "300 pin XBI", 0x04);
	ESPER_CreateAttrUInt8(mid, vid, "option", "XENPACK", 0x05);
	ESPER_CreateAttrUInt8(mid, vid, "option", "XFP", 0x06);
	ESPER_CreateAttrUInt8(mid, vid, "option", "XFF", 0x07);
	ESPER_CreateAttrUInt8(mid, vid, "option", "XFP-E", 0x08);
	ESPER_CreateAttrUInt8(mid, vid, "option", "XPAK", 0x09);
	ESPER_CreateAttrUInt8(mid, vid, "option", "X2", 0x0A);
	ESPER_CreateAttrUInt8(mid, vid, "option", "DWDM-SFP", 0x0B);
	ESPER_CreateAttrUInt8(mid, vid, "option", "QSFP", 0x0C);
	ESPER_CreateAttrUInt8(mid, vid, "option", "QSFP+", 0x0D);
	ESPER_CreateAttrUInt8(mid, vid, "option", "CXP", 0x0E);

	vid = ESPER_CreateVarUInt8(mid, "connector", 		ESPER_OPTION_RD, 1, &ctx->sfp_info.connector, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Identifier");
	ESPER_CreateAttrNull(mid, vid, "format", "select");
	ESPER_CreateAttrUInt8(mid, vid, "option", "Unknown", 0x00);
	ESPER_CreateAttrUInt8(mid, vid, "option", "SC", 0x01);
	ESPER_CreateAttrUInt8(mid, vid, "option", "Fibre Channel 1", 0x02);
	ESPER_CreateAttrUInt8(mid, vid, "option", "Fibre Channel 2", 0x03);
	ESPER_CreateAttrUInt8(mid, vid, "option", "BNC/TNC", 0x04);
	ESPER_CreateAttrUInt8(mid, vid, "option", "Fibre Channel Coaxial", 0x05);
	ESPER_CreateAttrUInt8(mid, vid, "option", "FiberJack", 0x06);
	ESPER_CreateAttrUInt8(mid, vid, "option", "LC", 0x07);
	ESPER_CreateAttrUInt8(mid, vid, "option", "MT-RJ", 0x08);
	ESPER_CreateAttrUInt8(mid, vid, "option", "MU", 0x09);
	ESPER_CreateAttrUInt8(mid, vid, "option", "SG", 0x0A);
	ESPER_CreateAttrUInt8(mid, vid, "option", "Optical Pigtail", 0x0B);
	ESPER_CreateAttrUInt8(mid, vid, "option", "MPO Parallel Optic", 0x0C);
	ESPER_CreateAttrUInt8(mid, vid, "option", "HSSDC II", 0x20);
	ESPER_CreateAttrUInt8(mid, vid, "option", "Copper Pigtail", 0x21);
	ESPER_CreateAttrUInt8(mid, vid, "option", "RJ45", 0x22);

	vid = ESPER_CreateVarUInt8(mid, "encoding", 		ESPER_OPTION_RD, 1, &ctx->sfp_info.encoding, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Encoding");
	ESPER_CreateAttrNull(mid, vid, "format", "select");
	ESPER_CreateAttrUInt8(mid, vid, "option", "Unspecified", 0x00);
	ESPER_CreateAttrUInt8(mid, vid, "option", "8B10B", 0x01);
	ESPER_CreateAttrUInt8(mid, vid, "option", "4B5B", 0x02);
	ESPER_CreateAttrUInt8(mid, vid, "option", "NRZ", 0x03);
	ESPER_CreateAttrUInt8(mid, vid, "option", "Manchester", 0x04);
	ESPER_CreateAttrUInt8(mid, vid, "option", "SONET Scrambled", 0x05);
	ESPER_CreateAttrUInt8(mid, vid, "option", "64B/66B", 0x06);

	vid = ESPER_CreateVarUInt32(mid, "br_nominal", 		ESPER_OPTION_RD, 1, &ctx->sfp_info.br_nominal, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Nominal Bitrate");

	vid = ESPER_CreateVarUInt8(mid, "rate_id", 		ESPER_OPTION_RD, 1, &ctx->sfp_info.rate_id, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Rate Identifier");
	ESPER_CreateAttrNull(mid, vid, "format", "select");
	ESPER_CreateAttrUInt8(mid, vid, "option", "Unspecified", 0x00);
	ESPER_CreateAttrUInt8(mid, vid, "option", "SFF-8079 (4/2/1G)", 0x01);
	ESPER_CreateAttrUInt8(mid, vid, "option", "SFF-8431 (8/4/2G RX)", 0x02);
	ESPER_CreateAttrUInt8(mid, vid, "option", "Unspecified", 0x03);
	ESPER_CreateAttrUInt8(mid, vid, "option", "SFF-8431 (8/4/2G TX)", 0x04);
	ESPER_CreateAttrUInt8(mid, vid, "option", "Unspecified", 0x05);
	ESPER_CreateAttrUInt8(mid, vid, "option", "SFF-8431 (8/4/2G TX/RX)", 0x06);
	ESPER_CreateAttrUInt8(mid, vid, "option", "Unspecified", 0x07);
	ESPER_CreateAttrUInt8(mid, vid, "option", "FC-PI-5 (16/8/4G RX)", 0x08);
	ESPER_CreateAttrUInt8(mid, vid, "option", "Unspecified", 0x09);
	ESPER_CreateAttrUInt8(mid, vid, "option", "FC-PI-5 (16/8/4G TX/RX)", 0x0A);

	vid = ESPER_CreateVarASCII(mid, "vendor_name", ESPER_OPTION_RD, sizeof(ctx->sfp_info.vendor_name), ctx->sfp_info.vendor_name, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Vendor Name");

	vid = ESPER_CreateVarUInt8(mid, "vendor_oui", 		ESPER_OPTION_RD, sizeof(ctx->sfp_info.vendor_oui), ctx->sfp_info.vendor_oui, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Vendor OUI");
	ESPER_CreateAttrNull(mid, vid, "format", "hex");

	vid = ESPER_CreateVarASCII(mid, "vendor_rev", 		ESPER_OPTION_RD, sizeof(ctx->sfp_info.vendor_rev), ctx->sfp_info.vendor_rev, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Vendor Part Revision");

	vid = ESPER_CreateVarASCII(mid, "vendor_sn", 		ESPER_OPTION_RD, sizeof(ctx->sfp_info.vendor_sn), ctx->sfp_info.vendor_sn, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Vendor Serial Number");

	vid = ESPER_CreateVarUInt16(mid, "wavelength", 		ESPER_OPTION_RD, 1, &ctx->sfp_info.wavelength, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Wavelength");

	vid = ESPER_CreateVarASCII(mid, "vendor_pn", 		ESPER_OPTION_RD, sizeof(ctx->sfp_info.vendor_pn), ctx->sfp_info.vendor_pn, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Vendor Part Number");

	vid = ESPER_CreateVarASCII(mid, "datecode", 		ESPER_OPTION_RD, sizeof(ctx->sfp_info.datecode), ctx->sfp_info.datecode, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Date Code (YYMMDD Lot)");

	vid = ESPER_CreateVarBool(mid, "dmm_ena", 		ESPER_OPTION_RD | ESPER_OPTION_HIDDEN, 1, &ctx->sfp_info.dmm_ena, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Digital Monitoring");

	vid = ESPER_CreateVarBool(mid, "int_cal", 		ESPER_OPTION_RD | ESPER_OPTION_HIDDEN, 1, &ctx->sfp_info.int_cal, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Internally Calibrated");

	vid = ESPER_CreateVarBool(mid, "ext_cal", 		ESPER_OPTION_RD | ESPER_OPTION_HIDDEN, 1, &ctx->sfp_info.ext_cal, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Externally Calibrated");

	vid = ESPER_CreateVarUInt8(mid, "meas_type", 		ESPER_OPTION_RD, 1, &ctx->sfp_info.meas_type, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Received Power Type");
	ESPER_CreateAttrNull(mid, vid, "format", "select");
	ESPER_CreateAttrUInt8(mid, vid, "option", "Optical Modulation Amplitude", 0x00);
	ESPER_CreateAttrUInt8(mid, vid, "option", "Average Power", 0x01);

	vid = ESPER_CreateVarBool(mid, "addr_mode", 		ESPER_OPTION_RD | ESPER_OPTION_HIDDEN, 1, &ctx->sfp_info.addr_mode, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Special Address Mode");

	vid = ESPER_CreateVarFloat32(mid, "cal_rx_pwr", 	ESPER_OPTION_RD | ESPER_OPTION_HIDDEN, 5, ctx->sfp_info.cal_rx_pwr, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Calibration RX Power");

	vid = ESPER_CreateVarUInt16(mid, "cal_tx_i_slope", 	ESPER_OPTION_RD | ESPER_OPTION_HIDDEN, 1, &ctx->sfp_info.cal_tx_i_slope, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Calibration TX I Slope");

	vid = ESPER_CreateVarUInt16(mid, "cal_tx_p_slope", 	ESPER_OPTION_RD | ESPER_OPTION_HIDDEN, 1, &ctx->sfp_info.cal_tx_p_slope, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Calibration TX P Slope");

	vid = ESPER_CreateVarUInt16(mid, "cal_temp_slope", 	ESPER_OPTION_RD | ESPER_OPTION_HIDDEN, 1, &ctx->sfp_info.cal_temp_slope, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Calibration Temp Slope");

	vid = ESPER_CreateVarUInt16(mid, "cal_volt_slope", 	ESPER_OPTION_RD | ESPER_OPTION_HIDDEN, 1, &ctx->sfp_info.cal_volt_slope, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Calibration Volt Slope");

	vid = ESPER_CreateVarSInt16(mid, "cal_tx_i_offset", 	ESPER_OPTION_RD | ESPER_OPTION_HIDDEN, 1, &ctx->sfp_info.cal_tx_i_offset, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Calibration TX I Offset");

	vid = ESPER_CreateVarSInt16(mid, "cal_tx_p_offset", 	ESPER_OPTION_RD | ESPER_OPTION_HIDDEN, 1, &ctx->sfp_info.cal_tx_p_offset, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Calibration TX P Offset");

	vid = ESPER_CreateVarSInt16(mid, "cal_temp_offset", 	ESPER_OPTION_RD | ESPER_OPTION_HIDDEN, 1, &ctx->sfp_info.cal_temp_offset, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Calibration Temp Offset");

	vid = ESPER_CreateVarSInt16(mid, "cal_volt_offset", 	ESPER_OPTION_RD | ESPER_OPTION_HIDDEN, 1, &ctx->sfp_info.cal_volt_offset, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Calibration Volt Offset");

	vid = ESPER_CreateVarFloat32(mid, "temp", 	ESPER_OPTION_RD, 1, &ctx->sfp_info.temp, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Temperature");
	ESPER_CreateAttrASCII(mid, vid, "si", "unit", "C");
	ESPER_CreateAttrSInt8(mid, vid, "si", "prefix", 0);
	ESPER_CreateAttrUInt8(mid, vid, "si", "precision", 2); // number of decimal places

	vid = ESPER_CreateVarFloat32(mid, "vcc", 	ESPER_OPTION_RD, 1, &ctx->sfp_info.vcc, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Vcc");
	ESPER_CreateAttrASCII(mid, vid, "si", "unit", "V");
	ESPER_CreateAttrSInt8(mid, vid, "si", "prefix", 0);
	ESPER_CreateAttrUInt8(mid, vid, "si", "precision", 2); // number of decimal places

	vid = ESPER_CreateVarFloat32(mid, "tx_bias", 	ESPER_OPTION_RD, 1, &ctx->sfp_info.tx_bias, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "TX Bias");
	ESPER_CreateAttrASCII(mid, vid, "si", "unit", "uA");
	ESPER_CreateAttrSInt8(mid, vid, "si", "prefix", 0);
	ESPER_CreateAttrUInt8(mid, vid, "si", "precision", 2); // number of decimal places

	vid = ESPER_CreateVarFloat32(mid, "tx_power", 	ESPER_OPTION_RD, 1, &ctx->sfp_info.tx_power, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "TX Power");
	ESPER_CreateAttrASCII(mid, vid, "si", "unit", "uW");
	ESPER_CreateAttrSInt8(mid, vid, "si", "prefix", 0);
	ESPER_CreateAttrUInt8(mid, vid, "si", "precision", 2); // number of decimal places

	vid = ESPER_CreateVarFloat32(mid, "rx_power", 	ESPER_OPTION_RD, 1, &ctx->sfp_info.rx_power, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "RX Power");
	ESPER_CreateAttrASCII(mid, vid, "si", "unit", "uW");
	ESPER_CreateAttrSInt8(mid, vid, "si", "prefix", 0);
	ESPER_CreateAttrUInt8(mid, vid, "si", "precision", 2); // number of decimal places

	return ESPER_RESP_OK;
}

static eESPERResponse Start(tESPERMID mid, tESPERModuleSFP* ctx) {

	ctx->sfp_info.br_nominal *= 100; // translate into mbit/sec

	return ESPER_RESP_OK;
}

static eESPERResponse Update(tESPERMID mid, tESPERModuleSFP* ctx){
	SFP_GetInfo(ctx->i2c_base, &ctx->sfp_info);
	ESPER_TouchModule(mid);

	return ESPER_RESP_OK;
}

