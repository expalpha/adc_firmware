/*
 * mod_sp16.h
 *
 *  Created on: Nov 14, 2016
 *      Author: bryerton
 */

#ifndef MOD_SP16_H_
#define MOD_SP16_H_

#include <esper.h>

typedef struct {
	uint32_t sp_ctrl_base;
	uint32_t sp_stat_base;

	uint32_t num_channels;
	uint8_t default_type;

	uint64_t timestamp;
	uint8_t run_status;
	uint8_t int_trigger;

	uint8_t  adc_gain[4]; // global, only one per ADC
	int8_t 	 adc_trim[16];
	int16_t dac_offset[16];

        uint8_t adc_reset;
        uint8_t adc_init;

	uint8_t adc_testmode;
	uint8_t adc_testmodeU;
	uint8_t adc_testmode19;
	uint8_t adc_testmode1A;
	uint8_t adc_testmode1B;
	uint8_t adc_testmode1C;

	uint8_t  enable[16];
	uint8_t  type[16];
	uint16_t trig_delay[16];
	uint16_t trig_start[16];
	uint16_t trig_stop[16];

	uint32_t cnt_trig_in[16];
	uint32_t cnt_trig_accepted[16];
	uint32_t cnt_trig_dropped_busy[16];
	uint32_t cnt_trig_dropped_full[16];
} tESPERModuleSP16;

tESPERModuleSP16* ModuleSP16Init(uint32_t sp_ctrl_base, uint32_t sp_stat_base, uint8_t default_type, tESPERModuleSP16* ctx);
eESPERResponse ModuleSP16Handler(tESPERMID mid, tESPERGID gid, eESPERState state, void* ctx);

#endif /* MOD_SP16_H_ */
