/*
 * mod_board.h
 *
 *  Created on: Apr 2, 2017
 *      Author: admin
 */

#ifndef MOD_BOARD_H_
#define MOD_BOARD_H_

#include <unistd.h>
#include <esper.h>

#define SYS_VAR_NAME_LEN 32
#define SYS_VAR_BUILD_USER_LEN 64
#define SYS_VAR_BUILD_EMAIL_LEN 64
#define SYS_VAR_BUILD_STR_LEN 64
#define SYS_VAR_BUILD_GIT_BRANCH_LEN 256
#define SYS_VAR_BUILD_GIT_HASH_LEN 64

typedef struct {
	uint32_t elf_buildtime;
	uint32_t elf_buildnumber;
	char elf_build_str[SYS_VAR_BUILD_STR_LEN];
	char elf_build_user[SYS_VAR_BUILD_USER_LEN];
	char elf_build_email[SYS_VAR_BUILD_EMAIL_LEN];
	char git_branch[SYS_VAR_BUILD_GIT_BRANCH_LEN];
	char git_hash[SYS_VAR_BUILD_GIT_HASH_LEN];
	uint32_t sw_qsys_sysid;
	uint32_t sw_qsys_timestamp;
	uint32_t hw_qsys_sysid;
	uint32_t hw_qsys_timestamp;
	uint8_t	 hw_sw_qsys_match;
	uint64_t chipid;
	uint32_t fpga_buildtime;
	uint8_t mac_addr[6];

	uint8_t module_id;

	uint8_t clk_lmk;
	uint8_t clk_mux;
	uint8_t clk_switch;
	uint8_t pll_ethernet_locked;
	uint8_t pll_cleaner_locked;
	uint8_t trig_nim_stat;
	uint8_t trig_esata_stat;
	uint32_t trig_nim_cnt;
	uint32_t trig_esata_cnt;

	float temp_fpga;
	float temp_sensor[9];
	uint8_t temp_fpga_raw; // use this for internal comparisons
	uint8_t lmk_sync;
	uint8_t adc_sync;
	uint8_t rst_serdes;
	uint8_t force_run;
	uint8_t nim_ena;
	uint8_t nim_inv;
	uint8_t esata_ena;
	uint8_t esata_inv;

	uint8_t lmk_device;
	uint16_t lmk_product;
	uint8_t lmk_maskrev;
	uint16_t lmk_vendor;

	uint16_t lmk_dac_val;
	uint8_t lmk_stat_holdover;

	uint8_t lmk_stat_clkin2_sel;
	uint8_t lmk_stat_clkin1_sel;
	uint8_t lmk_stat_clkin0_sel;

	uint8_t lmk_stat_clkin1_los;
	uint8_t lmk_stat_clkin0_los;

	uint32_t lmk_stat_pll1_ld_lost_cnt;
	uint8_t lmk_stat_pll1_ld_stat;

	uint32_t lmk_stat_pll2_ld_lost_cnt;
	uint8_t lmk_stat_pll2_ld_stat;

	uint8_t lmk_status[2];
	uint32_t freq_clean0;
	uint32_t freq_clean1;
	uint32_t freq_adc;
	uint32_t freq_fmc;
	uint32_t freq_esata;
	uint32_t freq_nim;
	uint32_t freq_eth;
	uint8_t adc_locked[4];
	uint8_t adc_aligned[4];
	uint32_t adc_locked_cnt[4];
	uint32_t adc_aligned_cnt[4];
	uint8_t fmc_locked[4];
	uint8_t fmc_aligned[4];
	uint32_t fmc_locked_cnt[4];
	uint32_t fmc_aligned_cnt[4];
} tESPERModuleBoard;

eESPERResponse BoardModuleHandler(tESPERMID mid, tESPERGID gid, eESPERState state, void* ctx);
tESPERModuleBoard* BoardModuleInit(uint8_t* board_mac, tESPERModuleBoard* ctx);

#endif /* MOD_BOARD_H_ */
