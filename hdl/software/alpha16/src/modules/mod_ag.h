/*
 * ag.h
 *
 *  Created on: Dec 9, 2016
 *      Author: admin
 */

#ifndef MOD_AG_H_
#define MOD_AG_H_

#include <esper.h>

typedef struct {
	uint32_t ctrl_base;
	uint32_t stat_base;
	int16_t adc16_threshold;
	int16_t adc32_threshold;
	uint16_t adc16_bits;
	uint32_t adc32_bits;
	uint32_t adc16_counter;
	uint32_t adc32_counter;
	uint32_t dac_data;
	uint32_t dac_ctrl;
	uint32_t dac_ctrl_a;
	uint32_t dac_ctrl_b;
	uint32_t dac_ctrl_c;
	uint32_t dac_ctrl_d;
	int16_t adc16_sthreshold;
	int16_t adc32_sthreshold;
	uint32_t ctrl_a;
	uint32_t ctrl_b;
	uint32_t ctrl_c;
	uint32_t ctrl_d;
	uint32_t ctrl_e;
	uint32_t ctrl_f;
	uint32_t stat_a;
	uint32_t stat_b;
	uint32_t stat_c;
	uint32_t stat_d;
} tESPERModuleAG;

tESPERModuleAG* ModuleAGInit(uint32_t ctrl_base, uint32_t stat_base, tESPERModuleAG* ctx);
eESPERResponse ModuleAGHandler(tESPERMID mid, tESPERGID gid, eESPERState state, void* ctx);

#endif /* MOD_SFP_H_ */
