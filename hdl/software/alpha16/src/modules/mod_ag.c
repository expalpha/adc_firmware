/*
 * mod_ag.c
 *
 *  Created on: Dec 9, 2016
 *      Author: admin
 */

#include <stdio.h>
#include <assert.h>
#include "mod_ag.h"
#include <drivers/inc/ag_regs.h>
#include "esper_nios.h"

static eESPERResponse Init(tESPERMID mid, tESPERModuleAG* ctx);
static eESPERResponse Start(tESPERMID mid, tESPERModuleAG* ctx);
static eESPERResponse Update(tESPERMID mid, tESPERModuleAG* ctx);

tESPERModuleAG* ModuleAGInit(uint32_t ctrl_base, uint32_t stat_base, tESPERModuleAG* ctx) {
	if(!ctx) return 0;

	//printf("ModuleAGInit: ctrl_base 0x%08x, stat_base 0x%08x\n", ctrl_base, stat_base);

	ctx->ctrl_base = ctrl_base;
	ctx->stat_base = stat_base;

	return ctx;
}

eESPERResponse ModuleAGHandler(tESPERMID mid, tESPERGID gid, eESPERState state, void* ctx) {
	switch(state) {
	case ESPER_STATE_INIT:
		return Init(mid, (tESPERModuleAG*)ctx);
	case ESPER_STATE_START:
		return Start(mid, (tESPERModuleAG*)ctx);
	case ESPER_STATE_UPDATE:
		return Update(mid, (tESPERModuleAG*)ctx);
	case ESPER_STATE_STOP:
		break;
	}

	return ESPER_RESP_OK;
}

static eESPERResponse Init(tESPERMID mid, tESPERModuleAG* ctx){
	tESPERVID vid;

	vid = ESPER_CreateVarSInt16(mid, "adc16_threshold", 	ESPER_OPTION_RD|ESPER_OPTION_WR, 1, &ctx->adc16_threshold, (int16_t*)GET_REG_OFFSET(ctx->ctrl_base, AG_ADC16_THRESHOLD), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "adc16 discriminator threshold");

	vid = ESPER_CreateVarSInt16(mid, "adc32_threshold", 	ESPER_OPTION_RD|ESPER_OPTION_WR, 1, &ctx->adc32_threshold, (int16_t*)GET_REG_OFFSET(ctx->ctrl_base, AG_ADC32_THRESHOLD), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "adc32 discriminator threshold");

	vid = ESPER_CreateVarUInt16(mid, "adc16_bits", 	ESPER_OPTION_RD, 1, &ctx->adc16_bits, (uint16_t*)GET_REG_OFFSET(ctx->stat_base, AG_ADC16_BITS), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "adc16 discriminator outputs");
        ESPER_CreateAttrNull(mid, vid, "format", "hex");

	vid = ESPER_CreateVarUInt32(mid, "adc32_bits", 	ESPER_OPTION_RD, 1, &ctx->adc32_bits, (uint32_t*)GET_REG_OFFSET(ctx->stat_base, AG_ADC32_BITS), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "adc32 discriminator outputs");
        ESPER_CreateAttrNull(mid, vid, "format", "hex");

	vid = ESPER_CreateVarUInt32(mid, "adc16_counter", 	ESPER_OPTION_RD, 1, &ctx->adc16_counter, (uint32_t*)GET_REG_OFFSET(ctx->stat_base, AG_ADC16_COUNTER), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "adc16 discriminator hit counter");

	vid = ESPER_CreateVarUInt32(mid, "adc32_counter", 	ESPER_OPTION_RD, 1, &ctx->adc32_counter, (uint32_t*)GET_REG_OFFSET(ctx->stat_base, AG_ADC32_COUNTER), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "adc32 discriminator hit counter");

	vid = ESPER_CreateVarUInt32(mid, "dac_data", 	ESPER_OPTION_RD|ESPER_OPTION_WR, 1, &ctx->dac_data, (uint32_t*)GET_REG_OFFSET(ctx->ctrl_base, AG_DAC_DATA), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "dac data");
        ESPER_CreateAttrNull(mid, vid, "format", "hex");

	vid = ESPER_CreateVarUInt32(mid, "dac_ctrl", 	ESPER_OPTION_RD|ESPER_OPTION_WR, 1, &ctx->dac_ctrl, (uint32_t*)GET_REG_OFFSET(ctx->ctrl_base, AG_DAC_CTRL), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "dac control");
        ESPER_CreateAttrNull(mid, vid, "format", "hex");

	vid = ESPER_CreateVarUInt32(mid, "dac_ctrl_a", 	ESPER_OPTION_RD|ESPER_OPTION_WR, 1, &ctx->dac_ctrl_a, (uint32_t*)GET_REG_OFFSET(ctx->ctrl_base, AG_DAC_CTRL_A), 0);
        ESPER_CreateAttrNull(mid, vid, "format", "hex");

	vid = ESPER_CreateVarUInt32(mid, "dac_ctrl_b", 	ESPER_OPTION_RD|ESPER_OPTION_WR, 1, &ctx->dac_ctrl_b, (uint32_t*)GET_REG_OFFSET(ctx->ctrl_base, AG_DAC_CTRL_B), 0);
        ESPER_CreateAttrNull(mid, vid, "format", "hex");

	vid = ESPER_CreateVarUInt32(mid, "dac_ctrl_c", 	ESPER_OPTION_RD|ESPER_OPTION_WR, 1, &ctx->dac_ctrl_c, (uint32_t*)GET_REG_OFFSET(ctx->ctrl_base, AG_DAC_CTRL_C), 0);
        ESPER_CreateAttrNull(mid, vid, "format", "hex");

	vid = ESPER_CreateVarUInt32(mid, "dac_ctrl_d", 	ESPER_OPTION_RD|ESPER_OPTION_WR, 1, &ctx->dac_ctrl_d, (uint32_t*)GET_REG_OFFSET(ctx->ctrl_base, AG_DAC_CTRL_D), 0);
        ESPER_CreateAttrNull(mid, vid, "format", "hex");

	vid = ESPER_CreateVarSInt16(mid, "adc16_sthreshold", 	ESPER_OPTION_RD|ESPER_OPTION_WR, 1, &ctx->adc16_sthreshold, (int16_t*)GET_REG_OFFSET(ctx->ctrl_base, AG_ADC16_STHRESHOLD), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "adc16 suppression threshold");

	vid = ESPER_CreateVarSInt16(mid, "adc32_sthreshold", 	ESPER_OPTION_RD|ESPER_OPTION_WR, 1, &ctx->adc32_sthreshold, (int16_t*)GET_REG_OFFSET(ctx->ctrl_base, AG_ADC32_STHRESHOLD), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "adc32 suppression threshold");

	vid = ESPER_CreateVarUInt32(mid, "ctrl_a", 	ESPER_OPTION_RD|ESPER_OPTION_WR, 1, &ctx->ctrl_a, (uint32_t*)GET_REG_OFFSET(ctx->ctrl_base, AG_CTRL_A), 0);
        ESPER_CreateAttrNull(mid, vid, "format", "hex");

	vid = ESPER_CreateVarUInt32(mid, "ctrl_b", 	ESPER_OPTION_RD|ESPER_OPTION_WR, 1, &ctx->ctrl_b, (uint32_t*)GET_REG_OFFSET(ctx->ctrl_base, AG_CTRL_B), 0);
        ESPER_CreateAttrNull(mid, vid, "format", "hex");

	vid = ESPER_CreateVarUInt32(mid, "ctrl_c", 	ESPER_OPTION_RD|ESPER_OPTION_WR, 1, &ctx->ctrl_c, (uint32_t*)GET_REG_OFFSET(ctx->ctrl_base, AG_CTRL_C), 0);
        ESPER_CreateAttrNull(mid, vid, "format", "hex");

	vid = ESPER_CreateVarUInt32(mid, "ctrl_d", 	ESPER_OPTION_RD|ESPER_OPTION_WR, 1, &ctx->ctrl_d, (uint32_t*)GET_REG_OFFSET(ctx->ctrl_base, AG_CTRL_D), 0);
        ESPER_CreateAttrNull(mid, vid, "format", "hex");

	vid = ESPER_CreateVarUInt32(mid, "ctrl_e", 	ESPER_OPTION_RD|ESPER_OPTION_WR, 1, &ctx->ctrl_e, (uint32_t*)GET_REG_OFFSET(ctx->ctrl_base, AG_CTRL_E), 0);
        ESPER_CreateAttrNull(mid, vid, "format", "hex");

	vid = ESPER_CreateVarUInt32(mid, "ctrl_f", 	ESPER_OPTION_RD|ESPER_OPTION_WR, 1, &ctx->ctrl_f, (uint32_t*)GET_REG_OFFSET(ctx->ctrl_base, AG_CTRL_F), 0);
        ESPER_CreateAttrNull(mid, vid, "format", "hex");

	vid = ESPER_CreateVarUInt32(mid, "stat_a", 	ESPER_OPTION_RD, 1, &ctx->stat_a, (uint32_t*)GET_REG_OFFSET(ctx->stat_base, AG_STAT_A), 0);
        ESPER_CreateAttrNull(mid, vid, "format", "hex");

	vid = ESPER_CreateVarUInt32(mid, "stat_b", 	ESPER_OPTION_RD, 1, &ctx->stat_b, (uint32_t*)GET_REG_OFFSET(ctx->stat_base, AG_STAT_B), 0);
        ESPER_CreateAttrNull(mid, vid, "format", "hex");

	vid = ESPER_CreateVarUInt32(mid, "stat_c", 	ESPER_OPTION_RD, 1, &ctx->stat_c, (uint32_t*)GET_REG_OFFSET(ctx->stat_base, AG_STAT_C), 0);
        ESPER_CreateAttrNull(mid, vid, "format", "hex");

	vid = ESPER_CreateVarUInt32(mid, "stat_d", 	ESPER_OPTION_RD, 1, &ctx->stat_d, (uint32_t*)GET_REG_OFFSET(ctx->stat_base, AG_STAT_D), 0);
        ESPER_CreateAttrNull(mid, vid, "format", "hex");

	return ESPER_RESP_OK;
}

static eESPERResponse Start(tESPERMID mid, tESPERModuleAG* ctx) {

	int default_threshold = 0x7000;
	int default_sthreshold = 0x0000;

	ESPER_WriteVarSInt16(mid, ESPER_GetVarIdByKey(mid, "adc16_threshold"), 0, default_threshold);
	ESPER_WriteVarSInt16(mid, ESPER_GetVarIdByKey(mid, "adc32_threshold"), 0, default_threshold);

	ESPER_WriteVarUInt32(mid, ESPER_GetVarIdByKey(mid, "dac_data"), 0, 0);
	ESPER_WriteVarUInt32(mid, ESPER_GetVarIdByKey(mid, "dac_ctrl"), 0, 0);

	ESPER_WriteVarUInt32(mid, ESPER_GetVarIdByKey(mid, "dac_ctrl_a"), 0, 0);
	ESPER_WriteVarUInt32(mid, ESPER_GetVarIdByKey(mid, "dac_ctrl_b"), 0, 0);
	ESPER_WriteVarUInt32(mid, ESPER_GetVarIdByKey(mid, "dac_ctrl_c"), 0, 0);
	ESPER_WriteVarUInt32(mid, ESPER_GetVarIdByKey(mid, "dac_ctrl_d"), 0, 0);

	ESPER_WriteVarSInt16(mid, ESPER_GetVarIdByKey(mid, "adc16_sthreshold"), 0, default_sthreshold);
	ESPER_WriteVarSInt16(mid, ESPER_GetVarIdByKey(mid, "adc32_sthreshold"), 0, default_sthreshold);

	ESPER_WriteVarUInt32(mid, ESPER_GetVarIdByKey(mid, "ctrl_a"), 0, 0);
	ESPER_WriteVarUInt32(mid, ESPER_GetVarIdByKey(mid, "ctrl_b"), 0, 0);
	ESPER_WriteVarUInt32(mid, ESPER_GetVarIdByKey(mid, "ctrl_c"), 0, 0);
	ESPER_WriteVarUInt32(mid, ESPER_GetVarIdByKey(mid, "ctrl_d"), 0, 0);
	ESPER_WriteVarUInt32(mid, ESPER_GetVarIdByKey(mid, "ctrl_e"), 0, 0);
	ESPER_WriteVarUInt32(mid, ESPER_GetVarIdByKey(mid, "ctrl_f"), 0, 0);

	return ESPER_RESP_OK;
}

static eESPERResponse Update(tESPERMID mid, tESPERModuleAG* ctx){
	//SFP_GetInfo(ctx->i2c_base, &ctx->sfp_info);
	//ESPER_TouchModule(mid);

	return ESPER_RESP_OK;
}

