/*
 * mod_sp.c
 *
 *  Created on: Nov 14, 2016
 *      Author: bryerton
 */

#include <sys/types.h>
#include <alt_iniche_dev.h>
#include <io.h>
#include <assert.h>
#include <system.h>
#include <unistd.h>
#include <string.h>
#include <drivers/inc/altera_avalon_pio_regs.h>
#include <drivers/inc/sigproc_regs.h>
#include <drivers/inc/ad9253.h>
#include <drivers/inc/ltc2655.h>
#include <esper_nios.h>
#include <stdlib.h>
#include "mod_sp16.h"


#define GET_SP_CTRL_REG_CH_OFFSET(base, ch, reg) \
	(uint8_t*)(base + ((SIGPROC_NUM_CTRL_BASE_REGS + ((ch)*SIGPROC_NUM_CTRL_CH_REGS) + (reg)) * 4))

#define GET_SP_STAT_REG_CH_OFFSET(base, ch, reg) \
	(uint8_t*)(base + ((SIGPROC_NUM_STAT_BASE_REGS + ((ch)*SIGPROC_NUM_STAT_CH_REGS) + (reg)) * 4))

static uint8_t GetI2CAddr[] = {
	// Offset 1
	0x10,
	0x10,
	0x10,
	0x10,
	// Offset 2
	0x12,
	0x12,
	0x12,
	0x12,
	// Offset 3
	0x22,
	0x22,
	0x22,
	0x22,
	// Offset 4
	0x30,
	0x30,
	0x30,
	0x30,
};

static uint8_t GetDACAddr[] = {
	LTC2655_ADDR_DAC_A,
	LTC2655_ADDR_DAC_B,
	LTC2655_ADDR_DAC_C,
	LTC2655_ADDR_DAC_D,

	LTC2655_ADDR_DAC_A,
	LTC2655_ADDR_DAC_B,
	LTC2655_ADDR_DAC_C,
	LTC2655_ADDR_DAC_D,

	LTC2655_ADDR_DAC_A,
	LTC2655_ADDR_DAC_B,
	LTC2655_ADDR_DAC_C,
	LTC2655_ADDR_DAC_D,

	LTC2655_ADDR_DAC_A,
	LTC2655_ADDR_DAC_B,
	LTC2655_ADDR_DAC_C,
	LTC2655_ADDR_DAC_D
};

static uint8_t ChannelVarHandler(tESPERMID mid, tESPERVID vid, tESPERVar* var, eESPERRequest request, uint32_t offset, uint32_t num_elements, void* module_ctx);
static uint8_t DACHandler(tESPERMID mid, tESPERVID vid, tESPERVar* var, eESPERRequest request, uint32_t offset, uint32_t num_elements, void* module_ctx);
static uint8_t ADCTrimHandler(tESPERMID mid, tESPERVID vid, tESPERVar* var, eESPERRequest request, uint32_t offset, uint32_t num_elements, void* module_ctx);
static uint8_t ADCGainHandler(tESPERMID mid, tESPERVID vid, tESPERVar* var, eESPERRequest request, uint32_t offset, uint32_t num_elements, void* module_ctx);
static uint8_t ADCInitHandler(tESPERMID mid, tESPERVID vid, tESPERVar* var, eESPERRequest request, uint32_t offset, uint32_t num_elements, void* module_ctx);
static uint8_t ADCResetHandler(tESPERMID mid, tESPERVID vid, tESPERVar* var, eESPERRequest request, uint32_t offset, uint32_t num_elements, void* module_ctx);
static uint8_t ADCTestModeHandler(tESPERMID mid, tESPERVID vid, tESPERVar* var, eESPERRequest request, uint32_t offset, uint32_t num_elements, void* module_ctx);

static eESPERResponse Init(tESPERMID mid, tESPERModuleSP16* ctx);
static eESPERResponse Start(tESPERMID mid, tESPERModuleSP16* ctx);
static eESPERResponse Update(tESPERMID mid, tESPERModuleSP16* ctx);

static void adc_reset(tESPERModuleSP16* ctx)
{
   printf("mod_sp16: adc_reset %d!\n", ctx->adc_reset);

   if (ctx->adc_reset)
      AD9253_Write(PIO_ADC_OUT_BASE, 1, SPI_ADC_BASE, 4, 0x08, 0x03);
   else
      AD9253_Write(PIO_ADC_OUT_BASE, 1, SPI_ADC_BASE, 4, 0x08, 0x00);
}

static void adc_init(tESPERModuleSP16* ctx)
{
   printf("mod_sp16: adc_init!\n");
   
   // Perform ADC reset
   AD9253_Write(PIO_ADC_OUT_BASE, 1, SPI_ADC_BASE, 4, 0x00, 0x20);
   
   // Into Digital Reset
   AD9253_Write(PIO_ADC_OUT_BASE, 1, SPI_ADC_BASE, 4, 0x08, 0x03);
   usleep(100000);
   AD9253_Write(PIO_ADC_OUT_BASE, 1, SPI_ADC_BASE, 4, 0x08, 0x00);
   usleep(100000);
   
   usleep(100000);
   usleep(100000);
   usleep(100000);
   usleep(100000);
   usleep(100000);
   usleep(100000);
   usleep(100000);
   usleep(100000);
   
   // Read ADC device chipid
   //for(i = 0; i < 4; i++) {
   //   //g_data.adc_chipid[i] = AD9253_Read(PIO_ADC_OUT_BASE, 1, SPI_ADC_BASE, i, 0x01);
   //   //g_data.adc_grade[i]  = (AD9253_Read(PIO_ADC_OUT_BASE, 1, SPI_ADC_BASE, i, 0x02) >> 4) & 0x7;
   //}
   
   // Enable SYNC pin
   AD9253_Write(PIO_ADC_OUT_BASE, 1, SPI_ADC_BASE, 4, 0x109, 0x01);
}

static void set_test_mode(tESPERModuleSP16* ctx)
{
   printf("mod_sp16: set_test_mode %d!\n", ctx->adc_testmode);

   uint8_t reg0D = 0;
   reg0D |= (ctx->adc_testmode & 0xF);
   reg0D |= ((ctx->adc_testmodeU & 0x3) << 6);
   AD9253_Write(PIO_ADC_OUT_BASE, 1, SPI_ADC_BASE, 4, 0x0D, reg0D);
   AD9253_Write(PIO_ADC_OUT_BASE, 1, SPI_ADC_BASE, 4, 0x19, ctx->adc_testmode19);
   AD9253_Write(PIO_ADC_OUT_BASE, 1, SPI_ADC_BASE, 4, 0x1A, ctx->adc_testmode1A);
   AD9253_Write(PIO_ADC_OUT_BASE, 1, SPI_ADC_BASE, 4, 0x1B, ctx->adc_testmode1B);
   AD9253_Write(PIO_ADC_OUT_BASE, 1, SPI_ADC_BASE, 4, 0x1C, ctx->adc_testmode1C);
}

tESPERModuleSP16* ModuleSP16Init(uint32_t sp_ctrl_base, uint32_t sp_stat_base, uint8_t default_type, tESPERModuleSP16* ctx) {
	if(!ctx) return 0;

	ctx->sp_ctrl_base = sp_ctrl_base;
	ctx->sp_stat_base = sp_stat_base;

	ctx->num_channels = 16;
	ctx->default_type = default_type;

        ctx->adc_reset = 0;
        ctx->adc_init  = 0;

        ctx->adc_testmode = 4;
        ctx->adc_testmodeU = 0;
        ctx->adc_testmode19 = 0;
        ctx->adc_testmode1A = 0;
        ctx->adc_testmode1B = 0;
        ctx->adc_testmode1C = 0;

	return ctx;
}

eESPERResponse ModuleSP16Handler(tESPERMID mid, tESPERGID gid, eESPERState state, void* ctx) {
	switch(state) {
	case ESPER_STATE_INIT:
		return Init(mid, (tESPERModuleSP16*)ctx);
	case ESPER_STATE_START:
		return Start(mid, (tESPERModuleSP16*)ctx);
	case ESPER_STATE_UPDATE:
		return Update(mid, (tESPERModuleSP16*)ctx);
	case ESPER_STATE_STOP:
		break;
	}

	return ESPER_RESP_OK;
}

static eESPERResponse Init(tESPERMID mid, tESPERModuleSP16* ctx) {
	tESPERVID vid;
	uint16_t num_ch = ctx->num_channels;

        adc_init(ctx);
        set_test_mode(ctx);

	vid = ESPER_CreateVarBool(mid, "int_trigger",	ESPER_OPTION_WR_RD, 1, &ctx->int_trigger, (uint8_t*)GET_REG_OFFSET(ctx->sp_ctrl_base, SIGPROC_REG_CTRL_INT_TRIGGER), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Internal Trigger");

	vid = ESPER_CreateVarBool(mid, "run_status",	ESPER_OPTION_RD, 1, &ctx->run_status, (uint8_t*)GET_REG_OFFSET(ctx->sp_stat_base, SIGPROC_REG_STAT_RUN), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Run Status");

	vid = ESPER_CreateVarUInt64(mid, "timestamp",	ESPER_OPTION_RD, 1, &ctx->timestamp, (uint8_t*)GET_REG_OFFSET(ctx->sp_stat_base, SIGPROC_REG_STAT_TIMESTAMP_LO), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Timestamp");
	ESPER_CreateAttrNull(mid, vid, "format", "hex");

	// Control Registers
	vid = ESPER_CreateVarBool(mid, "enable", ESPER_OPTION_WR_RD, num_ch, ctx->enable, (void*)SIGPROC_REG_CTRL_CH_ENA, ChannelVarHandler);
	ESPER_CreateAttrNull(mid, vid, "name", "Channel Enable");

	vid = ESPER_CreateVarUInt8(mid, "type",	ESPER_OPTION_WR_RD, num_ch, ctx->type, (void*)SIGPROC_REG_CTRL_CH_TYPE, ChannelVarHandler);
	ESPER_CreateAttrNull(mid, vid, "name", "Channel Type");
	ESPER_CreateAttrNull(mid, vid, "format", "select");
	ESPER_CreateAttrUInt8(mid, vid, "option", "Barrel Scintillator", 0);
	ESPER_CreateAttrUInt8(mid, vid, "option", "Anode Wire", 1);

	vid = ESPER_CreateVarSInt8(mid, "adc_trim",	ESPER_OPTION_WR_RD, num_ch, ctx->adc_trim, 0, ADCTrimHandler);
	ESPER_CreateAttrNull(mid, vid, "name", "ADC Internal Trim");
    ESPER_CreateAttrSInt8(mid, vid, "limit", "min", -128);
    ESPER_CreateAttrSInt8(mid, vid, "limit", "max", 127);

	vid = ESPER_CreateVarUInt8(mid, "adc_gain",	ESPER_OPTION_WR_RD, 4, ctx->adc_gain, 0, ADCGainHandler);
	ESPER_CreateAttrNull(mid, vid, "format", "select");
	ESPER_CreateAttrUInt8(mid, vid, "option", "1.0 Vp-p", 0);
	ESPER_CreateAttrUInt8(mid, vid, "option", "1.14 Vp-p", 1);
	ESPER_CreateAttrUInt8(mid, vid, "option", "1.33 Vp-p", 2);
	ESPER_CreateAttrUInt8(mid, vid, "option", "1.6 Vp-p", 3);
	ESPER_CreateAttrUInt8(mid, vid, "option", "2.0 Vp-p", 4);

	vid = ESPER_CreateVarSInt16(mid, "dac_offset",	ESPER_OPTION_WR_RD, num_ch, ctx->dac_offset, 0, DACHandler);
	ESPER_CreateAttrNull(mid, vid, "name", "DAC Gain");
    ESPER_CreateAttrSInt16(mid, vid, "limit", "min", -2048);
    ESPER_CreateAttrSInt16(mid, vid, "limit", "max", 2047);

	vid = ESPER_CreateVarBool(mid, "adc_init",	ESPER_OPTION_WR_RD, 1, &ctx->adc_init, 0, ADCInitHandler);
	ESPER_CreateAttrNull(mid, vid, "name", "ADC initialize");

	vid = ESPER_CreateVarBool(mid, "adc_reset",	ESPER_OPTION_WR_RD, 1, &ctx->adc_reset, 0, ADCResetHandler);
	ESPER_CreateAttrNull(mid, vid, "name", "ADC reset");

	vid = ESPER_CreateVarUInt8(mid, "adc_testmode",	ESPER_OPTION_WR_RD, 1, &ctx->adc_testmode, 0, ADCTestModeHandler);
	ESPER_CreateAttrNull(mid, vid, "format", "select");
	ESPER_CreateAttrUInt8(mid, vid, "option", "Off", 0);
	ESPER_CreateAttrUInt8(mid, vid, "option", "Midscale Short", 1);
	ESPER_CreateAttrUInt8(mid, vid, "option", "Positive FS", 2);
	ESPER_CreateAttrUInt8(mid, vid, "option", "Negative FS", 3);
	ESPER_CreateAttrUInt8(mid, vid, "option", "Alt Checkerboard", 4);
	ESPER_CreateAttrUInt8(mid, vid, "option", "PN23 Seq", 5);
	ESPER_CreateAttrUInt8(mid, vid, "option", "PN9 Seq", 6);
	ESPER_CreateAttrUInt8(mid, vid, "option", "one/zero word toggle", 7);
	ESPER_CreateAttrUInt8(mid, vid, "option", "User", 8);
	ESPER_CreateAttrUInt8(mid, vid, "option", "one/zero bit toggle", 9);
	ESPER_CreateAttrUInt8(mid, vid, "option", "1x sync", 0xA);
	ESPER_CreateAttrUInt8(mid, vid, "option", "1 bit high", 0xB);
	ESPER_CreateAttrUInt8(mid, vid, "option", "mixed bit freq", 0xC);

	vid = ESPER_CreateVarUInt8(mid, "adc_testmode_user", ESPER_OPTION_WR_RD, 1, &ctx->adc_testmodeU, 0, ADCTestModeHandler);
	ESPER_CreateAttrNull(mid, vid, "format", "hex");

	vid = ESPER_CreateVarUInt8(mid, "adc_testmode_19", ESPER_OPTION_WR_RD, 1, &ctx->adc_testmode19, 0, ADCTestModeHandler);
	ESPER_CreateAttrNull(mid, vid, "format", "hex");

	vid = ESPER_CreateVarUInt8(mid, "adc_testmode_1A", ESPER_OPTION_WR_RD, 1, &ctx->adc_testmode1A, 0, ADCTestModeHandler);
	ESPER_CreateAttrNull(mid, vid, "format", "hex");

	vid = ESPER_CreateVarUInt8(mid, "adc_testmode_1B", ESPER_OPTION_WR_RD, 1, &ctx->adc_testmode1B, 0, ADCTestModeHandler);
	ESPER_CreateAttrNull(mid, vid, "format", "hex");

	vid = ESPER_CreateVarUInt8(mid, "adc_testmode_1C", ESPER_OPTION_WR_RD, 1, &ctx->adc_testmode1C, 0, ADCTestModeHandler);
	ESPER_CreateAttrNull(mid, vid, "format", "hex");

	vid = ESPER_CreateVarUInt16(mid, "trig_delay",	ESPER_OPTION_WR_RD, num_ch, ctx->trig_delay, (void*)SIGPROC_REG_CTRL_CH_TRIG_DELAY, ChannelVarHandler);
	ESPER_CreateAttrNull(mid, vid, "name", "Trigger Delay");
    ESPER_CreateAttrUInt16(mid, vid, "limit", "min", 0);
    ESPER_CreateAttrUInt16(mid, vid, "limit", "max", 511);

	vid = ESPER_CreateVarUInt16(mid, "trig_start",	ESPER_OPTION_WR_RD, num_ch, ctx->trig_start, (void*)SIGPROC_REG_CTRL_CH_TRIG_POINT, ChannelVarHandler);
	ESPER_CreateAttrNull(mid, vid, "name", "Trigger Start");
    ESPER_CreateAttrUInt16(mid, vid, "limit", "min", 0);
    ESPER_CreateAttrUInt16(mid, vid, "limit", "max", 511);

	vid = ESPER_CreateVarUInt16(mid, "trig_stop",	ESPER_OPTION_WR_RD, num_ch, ctx->trig_stop, (void*)SIGPROC_REG_CTRL_CH_STOP_POINT, ChannelVarHandler);
	ESPER_CreateAttrNull(mid, vid, "name", "Trigger Stop");
    ESPER_CreateAttrUInt16(mid, vid, "limit", "min", 0);
    ESPER_CreateAttrUInt16(mid, vid, "limit", "max", 699);

	// Status Registers
	vid = ESPER_CreateVarUInt32(mid, "cnt_trig_in",	ESPER_OPTION_RD, num_ch, ctx->cnt_trig_in, (void*)SIGPROC_REG_STAT_CH_TRIG_IN, ChannelVarHandler);
	ESPER_CreateAttrNull(mid, vid, "name", "Attempted Trigger Count");

	vid = ESPER_CreateVarUInt32(mid, "cnt_trig_acpt",	ESPER_OPTION_RD, num_ch, ctx->cnt_trig_accepted, (void*)SIGPROC_REG_STAT_CH_TRIG_ACPTD, ChannelVarHandler);
	ESPER_CreateAttrNull(mid, vid, "name", "Accepted Trigger Count");

	vid = ESPER_CreateVarUInt32(mid, "cnt_trig_dbusy",	ESPER_OPTION_RD, num_ch, ctx->cnt_trig_dropped_busy, (void*)SIGPROC_REG_STAT_CH_TRIG_DROP_BUSY, ChannelVarHandler);
	ESPER_CreateAttrNull(mid, vid, "name", "Dropped Triggers Due to Busy");

	vid = ESPER_CreateVarUInt32(mid, "cnt_trig_dfull",	ESPER_OPTION_RD, num_ch, ctx->cnt_trig_dropped_full, (void*)SIGPROC_REG_STAT_CH_TRIG_DROP_FULL, ChannelVarHandler);
	ESPER_CreateAttrNull(mid, vid, "name", "Dropped Triggers Due to Full");

	return ESPER_RESP_OK;
}

static eESPERResponse Start(tESPERMID mid, tESPERModuleSP16* ctx) {
	uint32_t n;

	for(n=0; n < ctx->num_channels; n++ ) {
		ESPER_WriteVarBool(mid, ESPER_GetVarIdByKey(mid, "enable"), n, 1);
		ESPER_WriteVarUInt8(mid, ESPER_GetVarIdByKey(mid, "type"), n, ctx->default_type);

		ESPER_WriteVarUInt16(mid, ESPER_GetVarIdByKey(mid, "trig_delay"), n, 0);
		ESPER_WriteVarUInt16(mid, ESPER_GetVarIdByKey(mid, "trig_start"), n, 32);
		ESPER_WriteVarUInt16(mid, ESPER_GetVarIdByKey(mid, "trig_stop"), n, 511);
	}

	return ESPER_RESP_OK;
}

static eESPERResponse Update(tESPERMID mid, tESPERModuleSP16* ctx) {

	return ESPER_RESP_OK;
}

static uint8_t ChannelVarHandler(tESPERMID mid, tESPERVID vid, tESPERVar* var, eESPERRequest request, uint32_t offset, uint32_t num_elements, void* module_ctx) {
    uint32_t reg;
    uint32_t n;
    tESPERModuleSP16* ctx;

    ctx = (tESPERModuleSP16*)module_ctx;

    // we've hidden the register as the io pointer
    reg = (uint32_t)var->io;

    switch(request) {
    case ESPER_REQUEST_INIT:
         // If we have IO, load it into data by default
        // Loading from DISK/FLASH/NETWORK will occur later, as the Handler may require other variables to exist, but this is safe to do
        for(n=offset; n<(offset+num_elements); n++) {
        	if((var->info.options & ESPER_OPTION_WR_RD) == ESPER_OPTION_WR_RD) {
        		memcpy(var->data + (n * ESPER_GetTypeSize(var->info.type)), GET_SP_CTRL_REG_CH_OFFSET(ctx->sp_ctrl_base, n, reg), ESPER_GetTypeSize(var->info.type));
        	} else {
        		memcpy(var->data + (n * ESPER_GetTypeSize(var->info.type)), GET_SP_STAT_REG_CH_OFFSET(ctx->sp_stat_base, n, reg), ESPER_GetTypeSize(var->info.type));
        	}
        }
        break;

    case ESPER_REQUEST_READ_PRE:
	case ESPER_REQUEST_REFRESH:
	   for(n=offset; n<(offset+num_elements); n++) {
			if((var->info.options & ESPER_OPTION_WR_RD) == ESPER_OPTION_WR_RD) {
				if (memcmp(var->data + (n * ESPER_GetTypeSize(var->info.type)), GET_SP_CTRL_REG_CH_OFFSET(ctx->sp_ctrl_base, n, reg), ESPER_GetTypeSize(var->info.type)) != 0) {
					memcpy(var->data + (n * ESPER_GetTypeSize(var->info.type)), GET_SP_CTRL_REG_CH_OFFSET(ctx->sp_ctrl_base, n, reg), ESPER_GetTypeSize(var->info.type));
					ESPER_TouchVar(mid, vid);
				}
			} else {
				if (memcmp(var->data + (n * ESPER_GetTypeSize(var->info.type)), GET_SP_STAT_REG_CH_OFFSET(ctx->sp_stat_base, n, reg), ESPER_GetTypeSize(var->info.type)) != 0) {
					memcpy(var->data + (n * ESPER_GetTypeSize(var->info.type)), GET_SP_STAT_REG_CH_OFFSET(ctx->sp_stat_base, n, reg), ESPER_GetTypeSize(var->info.type));
					ESPER_TouchVar(mid, vid);
				}
			}
		}
        break;

    case ESPER_REQUEST_WRITE_PRE:
        break;

    case ESPER_REQUEST_WRITE_POST:
 	   for(n=offset; n<(offset+num_elements); n++) {
 			if((var->info.options & ESPER_OPTION_WR_RD) == ESPER_OPTION_WR_RD) {
 				memcpy(GET_SP_CTRL_REG_CH_OFFSET(ctx->sp_ctrl_base, n, reg), var->data + (n * ESPER_GetTypeSize(var->info.type)), ESPER_GetTypeSize(var->info.type));
 			} else {
 				memcpy(GET_SP_STAT_REG_CH_OFFSET(ctx->sp_stat_base, n, reg), var->data + (n * ESPER_GetTypeSize(var->info.type)), ESPER_GetTypeSize(var->info.type));
 			}
 		}
        break;
    }

    return 1;
}

static uint8_t DACHandler(tESPERMID mid, tESPERVID vid, tESPERVar* var, eESPERRequest request, uint32_t offset, uint32_t num_elements, void* module_ctx) {
	uint8_t n;
	uint16_t dac_offset;

	switch(request) {
	case ESPER_REQUEST_INIT:
		// Get initial value of ADC trim
		for(n=offset; n<(offset+num_elements); n++) {
			dac_offset = ((*(int16_t*)(var->data + (n * sizeof(int16_t)))) * -1) + 2048;
			LTC2655_Write(I2C_DAC2655_BASE, GetI2CAddr[n], LTC2655_CMD_WRITE_UPDATE_n, GetDACAddr[n], dac_offset << 4);
		}
		break;

	case ESPER_REQUEST_REFRESH:
	case ESPER_REQUEST_READ_PRE:
		// We don't want to call the SPI on every read request, accessing the ADC over SPI can insert digital noise
		// just return the current value we read on startup
		break;

	case ESPER_REQUEST_WRITE_PRE:
		break;

	case ESPER_REQUEST_WRITE_POST:
		// allow setting of all gains at once
		for(n=offset; n<(offset+num_elements); n++) {
			dac_offset = ((*(int16_t*)(var->data + (n * sizeof(int16_t)))) * -1) + 2048;
			LTC2655_Write(I2C_DAC2655_BASE, GetI2CAddr[n], LTC2655_CMD_WRITE_UPDATE_n, GetDACAddr[n], dac_offset << 4);
		}
		break;
	}
	return 1;
}

static uint8_t ADCTrimHandler(tESPERMID mid, tESPERVID vid, tESPERVar* var, eESPERRequest request, uint32_t offset, uint32_t num_elements, void* module_ctx) {
	uint8_t n;

	switch(request) {
	case ESPER_REQUEST_INIT:
		// Get initial value of ADC trim
    	for(n=offset; n<(offset+num_elements); n++) {
			// Select Channel to Use
			AD9253_Write(PIO_ADC_OUT_BASE, 1, SPI_ADC_BASE, (n / 4), 0x05, 1 << (n % 4));
			// -127 to +128 Channel offset trim
			*(int8_t*)(var->data + (n * sizeof(int8_t))) = AD9253_Read(PIO_ADC_OUT_BASE, 1, SPI_ADC_BASE, (n / 4), 0x10);
			// Reset to 'all channel' commands
			AD9253_Write(PIO_ADC_OUT_BASE, 1, SPI_ADC_BASE, (n / 4), 0x05, 0x3F);
    	}
		break;

	case ESPER_REQUEST_REFRESH:
	case ESPER_REQUEST_READ_PRE:
		// We don't want to call the SPI on every read request, accessing the ADC over SPI can insert digital noise
		// just return the current value we read on startup
		break;

    case ESPER_REQUEST_WRITE_PRE:
        break;

    case ESPER_REQUEST_WRITE_POST:
    	// allow setting of all gains at once
    	for(n=offset; n<(offset+num_elements); n++) {
			// Select Channel to Use
			AD9253_Write(PIO_ADC_OUT_BASE, 1, SPI_ADC_BASE, (n / 4), 0x05, 1 << (n % 4));
			// -127 to +128 Channel offset trim
			AD9253_Write(PIO_ADC_OUT_BASE, 1, SPI_ADC_BASE, (n / 4), 0x10, *(int8_t*)(var->data + (n * sizeof(int8_t))));
			// Reset to 'all channel' commands
			AD9253_Write(PIO_ADC_OUT_BASE, 1, SPI_ADC_BASE, (n / 4), 0x05, 0x3F);
    	}
		break;
    }
	return 1;
}

static uint8_t ADCGainHandler(tESPERMID mid, tESPERVID vid, tESPERVar* var, eESPERRequest request, uint32_t offset, uint32_t num_elements, void* module_ctx) {
	uint8_t n;

	switch(request) {
	case ESPER_REQUEST_INIT:
		// Get initial value of ADC trim
    	for(n=offset; n<(offset+num_elements); n++) {
			// Select Channel to Use
			AD9253_Write(PIO_ADC_OUT_BASE, 1, SPI_ADC_BASE, n, 0x05, 1 << n);
			// ADC Gain Select
			*(uint8_t*)(var->data + (n * sizeof(uint8_t))) = AD9253_Read(PIO_ADC_OUT_BASE, 1, SPI_ADC_BASE, n, 0x18);
			// Reset to 'all channel' commands
			AD9253_Write(PIO_ADC_OUT_BASE, 1, SPI_ADC_BASE, n, 0x05, 0x3F);
    	}
		break;

	case ESPER_REQUEST_REFRESH:
	case ESPER_REQUEST_READ_PRE:
		// We don't want to call the SPI on every read request, accessing the ADC over SPI can insert digital noise
		// just return the current value we read on startup
		break;

    case ESPER_REQUEST_WRITE_PRE:
        break;

    case ESPER_REQUEST_WRITE_POST:
    	// allow setting of all gains at once
    	for(n=offset; n<(offset+num_elements); n++) {
    		// Select Channel to Use
    		AD9253_Write(PIO_ADC_OUT_BASE, 1, SPI_ADC_BASE, n, 0x05, 1 << n);
    		// ADC Gain Select
    		AD9253_Write(PIO_ADC_OUT_BASE, 1, SPI_ADC_BASE, n, 0x18, *(uint8_t*)(var->data + (n * sizeof(uint8_t))));
    		// Reset to 'all channel' commands
    		AD9253_Write(PIO_ADC_OUT_BASE, 1, SPI_ADC_BASE, n, 0x05, 0x3F);
    	}
		break;
    }
	return 1;
}

static uint8_t ADCInitHandler(tESPERMID mid, tESPERVID vid, tESPERVar* var, eESPERRequest request, uint32_t offset, uint32_t num_elements, void* module_ctx) {
   
   tESPERModuleSP16* ctx = (tESPERModuleSP16*)module_ctx;
   
   switch(request) {
   case ESPER_REQUEST_INIT:
      break;
      
   case ESPER_REQUEST_REFRESH:
   case ESPER_REQUEST_READ_PRE:
      // We don't want to call the SPI on every read request, accessing the ADC over SPI can insert digital noise
      // just return the current value we read on startup
      break;
      
   case ESPER_REQUEST_WRITE_PRE:
      break;
      
   case ESPER_REQUEST_WRITE_POST: {
      if (ctx->adc_init) {
         adc_init(ctx);
      }
      break;
   }
   }
   return 1;
}

static uint8_t ADCResetHandler(tESPERMID mid, tESPERVID vid, tESPERVar* var, eESPERRequest request, uint32_t offset, uint32_t num_elements, void* module_ctx) {
   
   tESPERModuleSP16* ctx = (tESPERModuleSP16*)module_ctx;
   
   switch(request) {
   case ESPER_REQUEST_INIT:
      break;
      
   case ESPER_REQUEST_REFRESH:
   case ESPER_REQUEST_READ_PRE:
      // We don't want to call the SPI on every read request, accessing the ADC over SPI can insert digital noise
      // just return the current value we read on startup
      break;
      
   case ESPER_REQUEST_WRITE_PRE:
      break;
      
   case ESPER_REQUEST_WRITE_POST: {
      adc_reset(ctx);
      break;
   }
   }
   return 1;
}

static uint8_t ADCTestModeHandler(tESPERMID mid, tESPERVID vid, tESPERVar* var, eESPERRequest request, uint32_t offset, uint32_t num_elements, void* module_ctx) {
   
   tESPERModuleSP16* ctx = (tESPERModuleSP16*)module_ctx;
   
   switch(request) {
   case ESPER_REQUEST_INIT:
      break;
      
   case ESPER_REQUEST_REFRESH:
   case ESPER_REQUEST_READ_PRE:
      // We don't want to call the SPI on every read request, accessing the ADC over SPI can insert digital noise
      // just return the current value we read on startup
      break;
      
   case ESPER_REQUEST_WRITE_PRE:
      break;
      
   case ESPER_REQUEST_WRITE_POST: {
      set_test_mode(ctx);
      break;
   }
   }
   return 1;
}

//end
