/*
 * mod_sp.c
 *
 *  Created on: Nov 14, 2016
 *      Author: bryerton
 */

#include <sys/types.h>
#include <alt_iniche_dev.h>
#include "io.h"
#include <assert.h>
#include <system.h>
#include <unistd.h>
#include <string.h>
#include <drivers/inc/altera_avalon_pio_regs.h>
#include <drivers/inc/sigproc_regs.h>
#include <drivers/inc/ad9249.h>
#include "mod_sp32.h"
#include <drivers/inc/ltc2668.h>
#include <esper_nios.h>
#include <stdlib.h>

#define GET_SP_CTRL_REG_CH_OFFSET(base, ch, reg) \
	(uint8_t*)(base + ((SIGPROC_NUM_CTRL_BASE_REGS + ((ch)*SIGPROC_NUM_CTRL_CH_REGS) + (reg)) * 4))

#define GET_SP_STAT_REG_CH_OFFSET(base, ch, reg) \
	(uint8_t*)(base + ((SIGPROC_NUM_STAT_BASE_REGS + ((ch)*SIGPROC_NUM_STAT_CH_REGS) + (reg)) * 4))

static eESPERResponse Init(tESPERMID mid, tESPERModuleSP32* ctx);
static eESPERResponse Start(tESPERMID mid, tESPERModuleSP32* ctx);
static eESPERResponse Update(tESPERMID mid, tESPERModuleSP32* ctx);

static uint8_t ChannelVarHandler(tESPERMID mid, tESPERVID vid, tESPERVar* var, eESPERRequest request, uint32_t offset, uint32_t num_elements, void* module_ctx);
static uint8_t DACHandler(tESPERMID mid, tESPERVID vid, tESPERVar* var, eESPERRequest request, uint32_t offset, uint32_t num_elements, void* module_ctx);
static uint8_t ADCTrimHandler(tESPERMID mid, tESPERVID vid, tESPERVar* var, eESPERRequest request, uint32_t offset, uint32_t num_elements, void* module_ctx);
static uint8_t ADCGainHandler(tESPERMID mid, tESPERVID vid, tESPERVar* var, eESPERRequest request, uint32_t offset, uint32_t num_elements, void* module_ctx);
static uint8_t ADCInitHandler(tESPERMID mid, tESPERVID vid, tESPERVar* var, eESPERRequest request, uint32_t offset, uint32_t num_elements, void* module_ctx);
static uint8_t ADCResetHandler(tESPERMID mid, tESPERVID vid, tESPERVar* var, eESPERRequest request, uint32_t offset, uint32_t num_elements, void* module_ctx);
static uint8_t ADCTestModeHandler(tESPERMID mid, tESPERVID vid, tESPERVar* var, eESPERRequest request, uint32_t offset, uint32_t num_elements, void* module_ctx);

//                               0   1   2  3  4  5  6  7  8  9 10 11 12  13  14 15 16 17 18  19 20 21  22  23  24  25  26 27  28  29 30, 31
static uint8_t dac_cs_remap[] = {0,  1,  1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 1,  0,  0, 1, 1, 0, 0,  1, 1, 0, 0,  1,   1,  0, 0, 1,  0,  0, 1,  1 };
static uint8_t dac_ch_remap[] = {15, 14, 0, 3, 1, 1, 9, 9, 4, 2, 8, 8, 15, 14, 4, 5, 2, 0, 10, 6, 7, 7, 13, 13, 11, 11, 6, 10, 5, 12, 12, 3 };

static uint8_t adc_cs_remap[] = {0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3 };
static uint8_t adc_ch_remap[] = {0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7 };

static void adc_reset(tESPERModuleSP32* ctx)
{
   printf("mod_sp32: adc_reset %d!\n", ctx->adc_reset);

   if (ctx->adc_reset)
      AD9249_Write(PIO_FMC_ADC_OUT_BASE, 1, SPI_FMC_ADC_BASE, 4, 0x08, 0x03);
   else
      AD9249_Write(PIO_FMC_ADC_OUT_BASE, 1, SPI_FMC_ADC_BASE, 4, 0x08, 0x00);
}

static void adc_init(tESPERModuleSP32* ctx)
{
   printf("mod_sp32: adc_init!\n");

   AD9249_Write(PIO_FMC_ADC_OUT_BASE, 1, SPI_FMC_ADC_BASE, 4, 0x00, 0x20);
   
   // Set ADC to divide by 2
   AD9249_Write(PIO_FMC_ADC_OUT_BASE, 1, SPI_FMC_ADC_BASE, 4, 0x0B, 0x1);
   usleep(100000);
   
   // Into Digital Reset
   AD9249_Write(PIO_FMC_ADC_OUT_BASE, 1, SPI_FMC_ADC_BASE, 4, 0x08, 0x03);
   usleep(100000);
   AD9249_Write(PIO_FMC_ADC_OUT_BASE, 1, SPI_FMC_ADC_BASE, 4, 0x08, 0x00);
   usleep(100000);
   
   // Read ADC device chipid
   //for(i = 0; i < 4; i++) {
   //	g_data.fmc_adc_chipid[i] = AD9249_Read(PIO_FMC_ADC_OUT_BASE, 1, SPI_FMC_ADC_BASE, i, 0x01);
   //	g_data.fmc_adc_grade[i]  = (AD9249_Read(PIO_FMC_ADC_OUT_BASE, 1, SPI_FMC_ADC_BASE, i, 0x02) >> 4) & 0x7;
   //}
   
   // Enable SYNC pin
   AD9249_Write(PIO_FMC_ADC_OUT_BASE, 1, SPI_FMC_ADC_BASE, 4, 0x109, 0x01);
}

static void set_test_mode(tESPERModuleSP32* ctx)
{
   printf("mod_sp32: set_test_mode %d!\n", ctx->adc_testmode);

   uint8_t reg0D = 0;
   reg0D |= (ctx->adc_testmode & 0xF);
   reg0D |= ((ctx->adc_testmodeU & 0x3) << 6);
   AD9249_Write(PIO_FMC_ADC_OUT_BASE, 1, SPI_FMC_ADC_BASE, 4, 0x0D, reg0D);
   AD9249_Write(PIO_FMC_ADC_OUT_BASE, 1, SPI_FMC_ADC_BASE, 4, 0x19, ctx->adc_testmode19);
   AD9249_Write(PIO_FMC_ADC_OUT_BASE, 1, SPI_FMC_ADC_BASE, 4, 0x1A, ctx->adc_testmode1A);
   AD9249_Write(PIO_FMC_ADC_OUT_BASE, 1, SPI_FMC_ADC_BASE, 4, 0x1B, ctx->adc_testmode1B);
   AD9249_Write(PIO_FMC_ADC_OUT_BASE, 1, SPI_FMC_ADC_BASE, 4, 0x1C, ctx->adc_testmode1C);
}

tESPERModuleSP32* ModuleSP32Init(uint32_t sp_ctrl_base, uint32_t sp_stat_base, uint8_t default_type, tESPERModuleSP32* ctx) {
	if(!ctx) return 0;

	ctx->sp_ctrl_base = sp_ctrl_base;
	ctx->sp_stat_base = sp_stat_base;

	ctx->num_channels = 32;
	ctx->default_type = default_type;

        ctx->adc_reset = 0;
        ctx->adc_init  = 0;

        ctx->adc_testmode = 4;
        ctx->adc_testmodeU = 0;
        ctx->adc_testmode19 = 0;
        ctx->adc_testmode1A = 0;
        ctx->adc_testmode1B = 0;
        ctx->adc_testmode1C = 0;

	return ctx;
}

eESPERResponse ModuleSP32Handler(tESPERMID mid, tESPERGID gid, eESPERState state, void* ctx) {
	switch(state) {
	case ESPER_STATE_INIT:
		return Init(mid, (tESPERModuleSP32*)ctx);
	case ESPER_STATE_START:
		return Start(mid, (tESPERModuleSP32*)ctx);
	case ESPER_STATE_UPDATE:
		return Update(mid, (tESPERModuleSP32*)ctx);
	case ESPER_STATE_STOP:
		break;
	}

	return ESPER_RESP_OK;
}

static eESPERResponse Init(tESPERMID mid, tESPERModuleSP32* ctx) {
	tESPERVID vid;
	uint16_t num_ch = ctx->num_channels;

        adc_init(ctx);
        set_test_mode(ctx);

	vid = ESPER_CreateVarBool(mid, "int_trigger",	ESPER_OPTION_WR_RD, 1, &ctx->int_trigger, (uint8_t*)GET_REG_OFFSET(ctx->sp_ctrl_base, SIGPROC_REG_CTRL_INT_TRIGGER), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Internal Trigger");

	vid = ESPER_CreateVarBool(mid, "run_status",	ESPER_OPTION_RD, 1, &ctx->run_status, (uint8_t*)GET_REG_OFFSET(ctx->sp_stat_base, SIGPROC_REG_STAT_RUN), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Run Status");

	vid = ESPER_CreateVarUInt64(mid, "timestamp",	ESPER_OPTION_RD, 1, &ctx->timestamp, (uint8_t*)GET_REG_OFFSET(ctx->sp_stat_base, SIGPROC_REG_STAT_TIMESTAMP_LO), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Timestamp");
	ESPER_CreateAttrNull(mid, vid, "format", "hex");

	// Control Registers
	vid = ESPER_CreateVarBool(mid, "enable", ESPER_OPTION_WR_RD, num_ch, ctx->enable, (void*)SIGPROC_REG_CTRL_CH_ENA, ChannelVarHandler);
	ESPER_CreateAttrNull(mid, vid, "name", "Channel Enable");

	vid = ESPER_CreateVarUInt8(mid, "type",	ESPER_OPTION_WR_RD, num_ch, ctx->type, (void*)SIGPROC_REG_CTRL_CH_TYPE, ChannelVarHandler);
	ESPER_CreateAttrNull(mid, vid, "name", "Channel Type");
	ESPER_CreateAttrNull(mid, vid, "format", "select");
	ESPER_CreateAttrUInt8(mid, vid, "option", "Barrel Scintillator", 0);
	ESPER_CreateAttrUInt8(mid, vid, "option", "Anode Wire", 1);

	vid = ESPER_CreateVarSInt16(mid, "dac_offset",	ESPER_OPTION_WR_RD, 32, ctx->dac_offset, 0, DACHandler);
	ESPER_CreateAttrNull(mid, vid, "name", "DAC Offset");
    ESPER_CreateAttrSInt16(mid, vid, "limit", "min", -2048);
    ESPER_CreateAttrSInt16(mid, vid, "limit", "max", 2047);

	vid = ESPER_CreateVarSInt8(mid, "adc_trim",	ESPER_OPTION_WR_RD, 32, ctx->adc_trim, 0, ADCTrimHandler);
	ESPER_CreateAttrNull(mid, vid, "name", "ADC Internal Trim");
    ESPER_CreateAttrSInt8(mid, vid, "limit", "min", -128);
    ESPER_CreateAttrSInt8(mid, vid, "limit", "max", 127);

	vid = ESPER_CreateVarUInt8(mid, "adc_gain",	ESPER_OPTION_WR_RD, 4, ctx->adc_gain, 0, ADCGainHandler);
	ESPER_CreateAttrNull(mid, vid, "format", "select");
	ESPER_CreateAttrUInt8(mid, vid, "option", "1.0 Vp-p", 0);
	ESPER_CreateAttrUInt8(mid, vid, "option", "1.14 Vp-p", 1);
	ESPER_CreateAttrUInt8(mid, vid, "option", "1.33 Vp-p", 2);
	ESPER_CreateAttrUInt8(mid, vid, "option", "1.6 Vp-p", 3);
	ESPER_CreateAttrUInt8(mid, vid, "option", "2.0 Vp-p", 4);

	vid = ESPER_CreateVarBool(mid, "adc_init",	ESPER_OPTION_WR_RD, 1, &ctx->adc_init, 0, ADCInitHandler);
	ESPER_CreateAttrNull(mid, vid, "name", "ADC initialize");

	vid = ESPER_CreateVarBool(mid, "adc_reset",	ESPER_OPTION_WR_RD, 1, &ctx->adc_reset, 0, ADCResetHandler);
	ESPER_CreateAttrNull(mid, vid, "name", "ADC reset");

	vid = ESPER_CreateVarUInt8(mid, "adc_testmode",	ESPER_OPTION_WR_RD, 1, &ctx->adc_testmode, 0, ADCTestModeHandler);
	ESPER_CreateAttrNull(mid, vid, "format", "select");
	ESPER_CreateAttrUInt8(mid, vid, "option", "Off", 0);
	ESPER_CreateAttrUInt8(mid, vid, "option", "Midscale Short", 1);
	ESPER_CreateAttrUInt8(mid, vid, "option", "Positive FS", 2);
	ESPER_CreateAttrUInt8(mid, vid, "option", "Negative FS", 3);
	ESPER_CreateAttrUInt8(mid, vid, "option", "Alt Checkerboard", 4);
	ESPER_CreateAttrUInt8(mid, vid, "option", "PN23 Seq", 5);
	ESPER_CreateAttrUInt8(mid, vid, "option", "PN9 Seq", 6);
	ESPER_CreateAttrUInt8(mid, vid, "option", "one/zero word toggle", 7);
	ESPER_CreateAttrUInt8(mid, vid, "option", "User", 8);
	ESPER_CreateAttrUInt8(mid, vid, "option", "one/zero bit toggle", 9);
	ESPER_CreateAttrUInt8(mid, vid, "option", "1x sync", 0xA);
	ESPER_CreateAttrUInt8(mid, vid, "option", "1 bit high", 0xB);
	ESPER_CreateAttrUInt8(mid, vid, "option", "mixed bit freq", 0xC);

	vid = ESPER_CreateVarUInt8(mid, "adc_testmode_user", ESPER_OPTION_WR_RD, 1, &ctx->adc_testmodeU, 0, ADCTestModeHandler);
	ESPER_CreateAttrNull(mid, vid, "format", "hex");

	vid = ESPER_CreateVarUInt8(mid, "adc_testmode_19", ESPER_OPTION_WR_RD, 1, &ctx->adc_testmode19, 0, ADCTestModeHandler);
	ESPER_CreateAttrNull(mid, vid, "format", "hex");

	vid = ESPER_CreateVarUInt8(mid, "adc_testmode_1A", ESPER_OPTION_WR_RD, 1, &ctx->adc_testmode1A, 0, ADCTestModeHandler);
	ESPER_CreateAttrNull(mid, vid, "format", "hex");

	vid = ESPER_CreateVarUInt8(mid, "adc_testmode_1B", ESPER_OPTION_WR_RD, 1, &ctx->adc_testmode1B, 0, ADCTestModeHandler);
	ESPER_CreateAttrNull(mid, vid, "format", "hex");

	vid = ESPER_CreateVarUInt8(mid, "adc_testmode_1C", ESPER_OPTION_WR_RD, 1, &ctx->adc_testmode1C, 0, ADCTestModeHandler);
	ESPER_CreateAttrNull(mid, vid, "format", "hex");

#if 0
	S2 S1 S0
	OUTPUT RANGE
	INTERNAL REFERENCE EXTERNAL REFERENCE
	0 0 0 0V TO 5V 0V to 2VREF
	0 0 1 0V to 10V 0V to 4VREF
	0 1 0 �5V �2VREF
	0 1 1 �10V �4VREF
	1 0 0 �2.5V �VREF

	vid = ESPER_CreateVarUInt8(mid, "dac_span",	ESPER_OPTION_WR_RD, 1, &ctx->dac_span, 0, DACSpanHandler);
	ESPER_CreateAttrNull(mid, vid, "name", "DAC Span");
	ESPER_CreateAttrNull(mid, vid, "format", "select");
	ESPER_CreateAttrUInt8(mid, vid, "option", "0 - 5V", 0);
	ESPER_CreateAttrUInt8(mid, vid, "option", "0 - 10V", 1);
	ESPER_CreateAttrUInt8(mid, vid, "option", "+/- 5V", 2);
	ESPER_CreateAttrUInt8(mid, vid, "option", "+/- 10V", 3);
	ESPER_CreateAttrUInt8(mid, vid, "option", "+/- 2.5V", 4);
#endif

    vid = ESPER_CreateVarUInt16(mid, "trig_delay",	ESPER_OPTION_WR_RD, num_ch, ctx->trig_delay, (void*)SIGPROC_REG_CTRL_CH_TRIG_DELAY, ChannelVarHandler);
	ESPER_CreateAttrNull(mid, vid, "name", "Trigger Delay");
    ESPER_CreateAttrUInt16(mid, vid, "limit", "min", 0);
    ESPER_CreateAttrUInt16(mid, vid, "limit", "max", 511);

	vid = ESPER_CreateVarUInt16(mid, "trig_start",	ESPER_OPTION_WR_RD, num_ch, ctx->trig_start, (void*)SIGPROC_REG_CTRL_CH_TRIG_POINT, ChannelVarHandler);
	ESPER_CreateAttrNull(mid, vid, "name", "Trigger Start");
    ESPER_CreateAttrUInt16(mid, vid, "limit", "min", 0);
    ESPER_CreateAttrUInt16(mid, vid, "limit", "max", 511);

	vid = ESPER_CreateVarUInt16(mid, "trig_stop",	ESPER_OPTION_WR_RD, num_ch, ctx->trig_stop, (void*)SIGPROC_REG_CTRL_CH_STOP_POINT, ChannelVarHandler);
	ESPER_CreateAttrNull(mid, vid, "name", "Trigger Stop");
    ESPER_CreateAttrUInt16(mid, vid, "limit", "min", 0);
    ESPER_CreateAttrUInt16(mid, vid, "limit", "max", 511);

	// Status Registers
	vid = ESPER_CreateVarUInt32(mid, "cnt_trig_in",	ESPER_OPTION_RD, num_ch, ctx->cnt_trig_in, (void*)SIGPROC_REG_STAT_CH_TRIG_IN, ChannelVarHandler);
	ESPER_CreateAttrNull(mid, vid, "name", "Attempted Trigger Count");

	vid = ESPER_CreateVarUInt32(mid, "cnt_trig_acpt",	ESPER_OPTION_RD, num_ch, ctx->cnt_trig_accepted, (void*)SIGPROC_REG_STAT_CH_TRIG_ACPTD, ChannelVarHandler);
	ESPER_CreateAttrNull(mid, vid, "name", "Accepted Trigger Count");

	vid = ESPER_CreateVarUInt32(mid, "cnt_trig_dbusy",	ESPER_OPTION_RD, num_ch, ctx->cnt_trig_dropped_busy, (void*)SIGPROC_REG_STAT_CH_TRIG_DROP_BUSY, ChannelVarHandler);
	ESPER_CreateAttrNull(mid, vid, "name", "Dropped Triggers Due to Busy");

	vid = ESPER_CreateVarUInt32(mid, "cnt_trig_dfull",	ESPER_OPTION_RD, num_ch, ctx->cnt_trig_dropped_full, (void*)SIGPROC_REG_STAT_CH_TRIG_DROP_FULL, ChannelVarHandler);
	ESPER_CreateAttrNull(mid, vid, "name", "Dropped Triggers Due to Full");

	return ESPER_RESP_OK;
}

static eESPERResponse Start(tESPERMID mid, tESPERModuleSP32* ctx) {
	uint32_t n;

	for(n=0; n < ctx->num_channels; n++ ) {
		ESPER_WriteVarBool(mid, ESPER_GetVarIdByKey(mid, "enable"), n, 1);
		ESPER_WriteVarUInt8(mid, ESPER_GetVarIdByKey(mid, "type"), n, ctx->default_type);

		ESPER_WriteVarUInt16(mid, ESPER_GetVarIdByKey(mid, "trig_delay"), n, 0);
		ESPER_WriteVarUInt16(mid, ESPER_GetVarIdByKey(mid, "trig_start"), n, 32);
		ESPER_WriteVarUInt16(mid, ESPER_GetVarIdByKey(mid, "trig_stop"), n, 511);

		ESPER_WriteVarSInt16(mid, ESPER_GetVarIdByKey(mid, "dac_offset"), n, 2000);
}

	//ESPER_WriteVarUInt8(mid, ESPER_GetVarIdByKey(mid, "dac_span"), 0, 4);

	return ESPER_RESP_OK;
}

static eESPERResponse Update(tESPERMID mid, tESPERModuleSP32* ctx) {
	/*

	static int16_t val;
	uint32_t n;

	val+=100;
	if(val > 2000) val = -2000;
	for(n=0; n<31; n++) {
		LTC2668_Write(PIO_FMC_ADC_OUT_BASE, 1, SPI_FMC_DAC2_BASE, dac_cs_remap[n], LTC2668_CMD_WRITE_N_UPDATE_N, dac_ch_remap[n], val << 4);
	}
	*/

	return ESPER_RESP_OK;
}

static uint8_t ChannelVarHandler(tESPERMID mid, tESPERVID vid, tESPERVar* var, eESPERRequest request, uint32_t offset, uint32_t num_elements, void* module_ctx) {
    uint32_t reg;
    uint32_t n;
    tESPERModuleSP32* ctx;

    ctx = (tESPERModuleSP32*)module_ctx;

    // we've hidden the register as the io pointer
    reg = (uint32_t)var->io;

    switch(request) {
    case ESPER_REQUEST_INIT:
         // If we have IO, load it into data by default
        // Loading from DISK/FLASH/NETWORK will occur later, as the Handler may require other variables to exist, but this is safe to do
        for(n=offset; n<(offset+num_elements); n++) {
        	if((var->info.options & ESPER_OPTION_WR_RD) == ESPER_OPTION_WR_RD) {
        		memcpy(var->data + (n * ESPER_GetTypeSize(var->info.type)), GET_SP_CTRL_REG_CH_OFFSET(ctx->sp_ctrl_base, n, reg), ESPER_GetTypeSize(var->info.type));
        	} else {
        		memcpy(var->data + (n * ESPER_GetTypeSize(var->info.type)), GET_SP_STAT_REG_CH_OFFSET(ctx->sp_stat_base, n, reg), ESPER_GetTypeSize(var->info.type));
        	}
        }
        break;

	case ESPER_REQUEST_REFRESH:
    case ESPER_REQUEST_READ_PRE:
	   for(n=offset; n<(offset+num_elements); n++) {
			if((var->info.options & ESPER_OPTION_WR_RD) == ESPER_OPTION_WR_RD) {
				if (memcmp(var->data + (n * ESPER_GetTypeSize(var->info.type)), GET_SP_CTRL_REG_CH_OFFSET(ctx->sp_ctrl_base, n, reg), ESPER_GetTypeSize(var->info.type)) != 0) {
					memcpy(var->data + (n * ESPER_GetTypeSize(var->info.type)), GET_SP_CTRL_REG_CH_OFFSET(ctx->sp_ctrl_base, n, reg), ESPER_GetTypeSize(var->info.type));
					ESPER_TouchVar(mid, vid);
				}
			} else {
				if (memcmp(var->data + (n * ESPER_GetTypeSize(var->info.type)), GET_SP_STAT_REG_CH_OFFSET(ctx->sp_stat_base, n, reg), ESPER_GetTypeSize(var->info.type)) != 0) {
					memcpy(var->data + (n * ESPER_GetTypeSize(var->info.type)), GET_SP_STAT_REG_CH_OFFSET(ctx->sp_stat_base, n, reg), ESPER_GetTypeSize(var->info.type));
					ESPER_TouchVar(mid, vid);
				}
			}
		}
        break;

    case ESPER_REQUEST_WRITE_PRE:
        break;

    case ESPER_REQUEST_WRITE_POST:
 	   for(n=offset; n<(offset+num_elements); n++) {
 			if((var->info.options & ESPER_OPTION_WR_RD) == ESPER_OPTION_WR_RD) {
 				memcpy(GET_SP_CTRL_REG_CH_OFFSET(ctx->sp_ctrl_base, n, reg), var->data + (n * ESPER_GetTypeSize(var->info.type)), ESPER_GetTypeSize(var->info.type));
 			} else {
 				memcpy(GET_SP_STAT_REG_CH_OFFSET(ctx->sp_stat_base, n, reg), var->data + (n * ESPER_GetTypeSize(var->info.type)), ESPER_GetTypeSize(var->info.type));
 			}
 		}
        break;
    }

    return 1;
}

static uint8_t DACHandler(tESPERMID mid, tESPERVID vid, tESPERVar* var, eESPERRequest request, uint32_t offset, uint32_t num_elements, void* module_ctx) {
	uint32_t n;
	uint16_t dac_offset;

	tESPERModuleSP32* ctx = (tESPERModuleSP32*)module_ctx;

	switch(request) {
	case ESPER_REQUEST_INIT:

		// Set initial span values
		for(n=offset; n<(offset+num_elements); n++) {
			dac_offset = (uint16_t)(ctx->dac_offset[n] + 2048);
			LTC2668_Write(PIO_FMC_ADC_OUT_BASE, 1, SPI_FMC_DAC2_BASE, dac_cs_remap[n], LTC2668_CMD_WRITE_N_UPDATE_N, dac_ch_remap[n], dac_offset << 4);
		}

		break;

	case ESPER_REQUEST_REFRESH:
	case ESPER_REQUEST_READ_PRE:
		// We don't want to call the SPI on every read request, accessing the ADC over SPI can insert digital noise
		// just return the current value we read on startup
		break;

	case ESPER_REQUEST_WRITE_PRE:
		break;

	case ESPER_REQUEST_WRITE_POST:
		for(n=offset; n<(offset+num_elements); n++) {
			dac_offset = (uint16_t)(ctx->dac_offset[n] + 2048);
			LTC2668_Write(PIO_FMC_ADC_OUT_BASE, 1, SPI_FMC_DAC2_BASE, dac_cs_remap[n], LTC2668_CMD_WRITE_N_UPDATE_N, dac_ch_remap[n], dac_offset << 4);
		}
		break;
	}
	return 1;
}

static uint8_t ADCTrimHandler(tESPERMID mid, tESPERVID vid, tESPERVar* var, eESPERRequest request, uint32_t offset, uint32_t num_elements, void* module_ctx) {
	uint8_t n;

	tESPERModuleSP32* ctx = (tESPERModuleSP32*)module_ctx;

	switch(request) {
	case ESPER_REQUEST_INIT:
		// Get initial value of ADC trim
    	for(n=offset; n<(offset+num_elements); n++) {
			// Select Channel to Use
			AD9249_Write(PIO_FMC_ADC_OUT_BASE, 1, SPI_FMC_ADC_BASE, adc_cs_remap[n], 0x04, (adc_ch_remap[n] >> 4) & 0xF);
			AD9249_Write(PIO_FMC_ADC_OUT_BASE, 1, SPI_FMC_ADC_BASE, adc_cs_remap[n], 0x05, adc_ch_remap[n] & 0xF);
			// -127 to +128 Channel offset trim
			ctx->adc_trim[n] = AD9249_Read(PIO_FMC_ADC_OUT_BASE, 1, SPI_FMC_ADC_BASE, adc_cs_remap[n], 0x10);
			// Reset to 'all channel' commands
			AD9249_Write(PIO_FMC_ADC_OUT_BASE, 1, SPI_FMC_ADC_BASE, adc_cs_remap[n], 0x04, 0x0F);
			AD9249_Write(PIO_FMC_ADC_OUT_BASE, 1, SPI_FMC_ADC_BASE, adc_cs_remap[n], 0x05, 0x3F);
    	}
		break;

	case ESPER_REQUEST_REFRESH:
	case ESPER_REQUEST_READ_PRE:
		// We don't want to call the SPI on every read request, accessing the ADC over SPI can insert digital noise
		// just return the current value we read on startup
		break;

    case ESPER_REQUEST_WRITE_PRE:
        break;

    case ESPER_REQUEST_WRITE_POST:
    	// allow setting of all gains at once
    	for(n=offset; n<(offset+num_elements); n++) {
			// Select Channel to Use
			AD9249_Write(PIO_FMC_ADC_OUT_BASE, 1, SPI_FMC_ADC_BASE, adc_cs_remap[n], 0x04, (adc_ch_remap[n] >> 4) & 0xF);
			AD9249_Write(PIO_FMC_ADC_OUT_BASE, 1, SPI_FMC_ADC_BASE, adc_cs_remap[n], 0x05, adc_ch_remap[n] & 0xF);
			// -127 to +128 Channel offset trim
			AD9249_Write(PIO_FMC_ADC_OUT_BASE, 1, SPI_FMC_ADC_BASE, adc_cs_remap[n], 0x10, ctx->adc_trim[n]);
			// Reset to 'all channel' commands
			AD9249_Write(PIO_FMC_ADC_OUT_BASE, 1, SPI_FMC_ADC_BASE, adc_cs_remap[n], 0x04, 0x0F);
			AD9249_Write(PIO_FMC_ADC_OUT_BASE, 1, SPI_FMC_ADC_BASE, adc_cs_remap[n], 0x05, 0x3F);
    	}
		break;
    }
	return 1;
}

static uint8_t ADCGainHandler(tESPERMID mid, tESPERVID vid, tESPERVar* var, eESPERRequest request, uint32_t offset, uint32_t num_elements, void* module_ctx) {
	uint8_t n;

	tESPERModuleSP32* ctx = (tESPERModuleSP32*)module_ctx;

	switch(request) {
	case ESPER_REQUEST_INIT:
		// Get initial value of ADC trim
    	for(n=offset; n<(offset+num_elements); n++) {
			// Select Channel to Use
			AD9249_Write(PIO_FMC_ADC_OUT_BASE, 1, SPI_FMC_ADC_BASE, adc_cs_remap[n], 0x04, (adc_ch_remap[n] >> 4) & 0xF);
			AD9249_Write(PIO_FMC_ADC_OUT_BASE, 1, SPI_FMC_ADC_BASE, adc_cs_remap[n], 0x05, adc_ch_remap[n] & 0xF);
			// ADC Gain Select
			ctx->adc_gain[n] = AD9249_Read(PIO_FMC_ADC_OUT_BASE, 1, SPI_FMC_ADC_BASE, adc_cs_remap[n], 0x18);
			// Reset to 'all channel' commands
			AD9249_Write(PIO_FMC_ADC_OUT_BASE, 1, SPI_FMC_ADC_BASE, adc_cs_remap[n], 0x04, 0x0F);
			AD9249_Write(PIO_FMC_ADC_OUT_BASE, 1, SPI_FMC_ADC_BASE, adc_cs_remap[n], 0x05, 0x3F);
    	}
		break;

	case ESPER_REQUEST_REFRESH:
	case ESPER_REQUEST_READ_PRE:
		// We don't want to call the SPI on every read request, accessing the ADC over SPI can insert digital noise
		// just return the current value we read on startup
		break;

    case ESPER_REQUEST_WRITE_PRE:
        break;

    case ESPER_REQUEST_WRITE_POST:
    	// allow setting of all gains at once
    	for(n=offset; n<(offset+num_elements); n++) {
			// Select Channel to Use
			AD9249_Write(PIO_FMC_ADC_OUT_BASE, 1, SPI_FMC_ADC_BASE, adc_cs_remap[n], 0x04, (adc_ch_remap[n] >> 4) & 0xF);
			AD9249_Write(PIO_FMC_ADC_OUT_BASE, 1, SPI_FMC_ADC_BASE, adc_cs_remap[n], 0x05, adc_ch_remap[n] & 0xF);
			// ADC Gain Select
			AD9249_Write(PIO_FMC_ADC_OUT_BASE, 1, SPI_FMC_ADC_BASE, adc_cs_remap[n], 0x18, ctx->adc_gain[n]);
			// Reset to 'all channel' commands
			AD9249_Write(PIO_FMC_ADC_OUT_BASE, 1, SPI_FMC_ADC_BASE, adc_cs_remap[n], 0x04, 0x0F);
			AD9249_Write(PIO_FMC_ADC_OUT_BASE, 1, SPI_FMC_ADC_BASE, adc_cs_remap[n], 0x05, 0x3F);
    	}
		break;
    }
	return 1;
}

static uint8_t ADCInitHandler(tESPERMID mid, tESPERVID vid, tESPERVar* var, eESPERRequest request, uint32_t offset, uint32_t num_elements, void* module_ctx) {
   
   tESPERModuleSP32* ctx = (tESPERModuleSP32*)module_ctx;
   
   switch(request) {
   case ESPER_REQUEST_INIT:
      break;
      
   case ESPER_REQUEST_REFRESH:
   case ESPER_REQUEST_READ_PRE:
      // We don't want to call the SPI on every read request, accessing the ADC over SPI can insert digital noise
      // just return the current value we read on startup
      break;
      
   case ESPER_REQUEST_WRITE_PRE:
      break;
      
   case ESPER_REQUEST_WRITE_POST: {
      if (ctx->adc_init) {
         adc_init(ctx);
      }
      break;
   }
   }
   return 1;
}

static uint8_t ADCResetHandler(tESPERMID mid, tESPERVID vid, tESPERVar* var, eESPERRequest request, uint32_t offset, uint32_t num_elements, void* module_ctx) {
   
   tESPERModuleSP32* ctx = (tESPERModuleSP32*)module_ctx;
   
   switch(request) {
   case ESPER_REQUEST_INIT:
      break;
      
   case ESPER_REQUEST_REFRESH:
   case ESPER_REQUEST_READ_PRE:
      // We don't want to call the SPI on every read request, accessing the ADC over SPI can insert digital noise
      // just return the current value we read on startup
      break;
      
   case ESPER_REQUEST_WRITE_PRE:
      break;
      
   case ESPER_REQUEST_WRITE_POST: {
      adc_reset(ctx);
      break;
   }
   }
   return 1;
}

static uint8_t ADCTestModeHandler(tESPERMID mid, tESPERVID vid, tESPERVar* var, eESPERRequest request, uint32_t offset, uint32_t num_elements, void* module_ctx) {
   
   tESPERModuleSP32* ctx = (tESPERModuleSP32*)module_ctx;
   
   switch(request) {
   case ESPER_REQUEST_INIT:
      break;
      
   case ESPER_REQUEST_REFRESH:
   case ESPER_REQUEST_READ_PRE:
      // We don't want to call the SPI on every read request, accessing the ADC over SPI can insert digital noise
      // just return the current value we read on startup
      break;
      
   case ESPER_REQUEST_WRITE_PRE:
      break;
      
   case ESPER_REQUEST_WRITE_POST: {
      set_test_mode(ctx);
      break;
   }
   }
   return 1;
}

//end
