/*!



LTC2983_support_functions.cpp:
This file contains all the support functions used in the main program.


http://www.linear.com/product/LTC2983

http://www.linear.com/product/LTC2983#demoboards

$Revision: 1.3.4 $
$Date: October 5, 2016 $
Copyright (c) 2014, Linear Technology Corp.(LTC)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of Linear Technology Corp.

The Linear Technology Linduino is not affiliated with the official Arduino team.
However, the Linduino is only possible because of the Arduino team's commitment
to the open-source community.  Please, visit http://www.arduino.cc and
http://store.arduino.cc , and consider a purchase that will help fund their
ongoing work.
*/

#include "ltc2983_constants.h"
#include "ltc2983.h"
#include <altera_avalon_spi.h>
#include <unistd.h>

static float read_conversion_result(uint32_t raw_conversion_result, uint8_t channel_output);
static uint16_t get_start_address(uint16_t base_address, uint8_t channel_number);

// ***********************
// Program the part
// ***********************
void ltc2983_ch_assign(uint32_t spi_base, uint32_t cs, uint8_t channel_number, uint32_t channel_assignment_data) {
	uint16_t start_address;

	start_address = get_start_address(CH_ADDRESS_BASE, channel_number);
	ltc2983_transfer_four_bytes(spi_base, cs, WRITE_TO_RAM, start_address, channel_assignment_data);
}

// *****************
// Measure channel
// *****************
float ltc2983_ch_measure(uint32_t spi_base, uint32_t cs, uint8_t channel_number, uint8_t channel_output) {
	ltc2983_ch_start_conversion(spi_base, cs, channel_number);

	while(ltc2983_is_converting(spi_base, cs)) {
		usleep(1);
	}

    return ltc2983_ch_read_conversion(spi_base, cs, channel_number, channel_output);
}

void ltc2983_ch_start_conversion(uint32_t spi_base, uint32_t cs, uint8_t channel_number) {
	ltc2983_transfer_byte(spi_base, cs, WRITE_TO_RAM, COMMAND_STATUS_REGISTER, CONVERSION_CONTROL_BYTE | channel_number);
}

float ltc2983_ch_read_conversion(uint32_t spi_base, uint32_t cs, uint8_t channel_number, uint8_t channel_output) {
	uint32_t raw_data;
	uint16_t start_address = get_start_address(CONVERSION_RESULT_MEMORY_BASE, channel_number);
	uint32_t raw_conversion_result;
	//uint8_t fault_data;

	raw_data = ltc2983_transfer_four_bytes(spi_base, cs, READ_FROM_RAM, start_address, 0);

	// 24 LSB's are conversion result
	raw_conversion_result = raw_data & 0xFFFFFF;

	return read_conversion_result(raw_conversion_result, channel_output);
}

uint8_t ltc2983_is_converting(uint32_t spi_base, uint8_t cs) {
	uint8_t process_finished;
	uint8_t data;

	data = ltc2983_transfer_byte(spi_base, cs, READ_FROM_RAM, COMMAND_STATUS_REGISTER, 0);
	process_finished  = data & 0x40;

	return (process_finished) ? 0 : 1;
}


static float read_conversion_result(uint32_t raw_conversion_result, uint8_t channel_output) {
	int32_t signed_data = raw_conversion_result;

	// Convert the 24 LSB's into a signed 32-bit integer
	if(signed_data & 0x800000) {
		signed_data = signed_data | 0xFF000000;
	}

	// Translate and print result
	if (channel_output == TEMPERATURE) {
		return (float)(signed_data) / 1024;
	} else if (channel_output == VOLTAGE) {
		return (float)(signed_data) / 2097152;
	}
  
	return 0.0f;
}

// ******************************
// Misc support functions
// ******************************
static uint16_t get_start_address(uint16_t base_address, uint8_t channel_number) {
	return base_address + 4 * (channel_number-1);
}

uint32_t ltc2983_transfer_four_bytes(uint32_t spi_base, uint32_t cs, uint8_t ram_read_or_write, uint16_t start_address, uint32_t input_data) {
	uint32_t output_data;
	uint8_t tx[7];
	uint8_t rx[4];

	tx[0] = ram_read_or_write;
	tx[1] = (start_address >> 8) & 0xFF;
	tx[2] = (start_address) & 0xFF;
	tx[3] = (uint8_t)(input_data >> 24);
	tx[4] = (uint8_t)(input_data >> 16);
	tx[5] = (uint8_t)(input_data >> 8);
	tx[6] = (uint8_t) input_data;

	if(ram_read_or_write == READ_FROM_RAM) {
		alt_avalon_spi_command(spi_base, cs, 3, tx, 4, rx, 0);
		output_data = 	(uint32_t) rx[0] << 24 |
						(uint32_t) rx[1] << 16 |
						(uint32_t) rx[2] << 8  |
						(uint32_t) rx[3];
	} else {
		alt_avalon_spi_command(spi_base, cs, 7, tx, 0, 0, 0);
		output_data = 0;
	}

	return output_data;
}

uint8_t ltc2983_transfer_byte(uint32_t spi_base, uint32_t cs, uint8_t ram_read_or_write, uint16_t start_address, uint8_t input_data) {
	uint8_t tx[4];
	uint8_t rx[1];

	tx[0] = ram_read_or_write;
	tx[1] = (start_address >> 8) & 0xFF;
	tx[2] = start_address & 0xFF;
	tx[3] = input_data;

	if(ram_read_or_write == READ_FROM_RAM) {
		alt_avalon_spi_command(spi_base, cs, 3, tx, 1, rx, 0);
		return rx[0];
	} else {
		alt_avalon_spi_command(spi_base, cs, 4, tx, 0, rx, 0);
		return 0;
	}
}
