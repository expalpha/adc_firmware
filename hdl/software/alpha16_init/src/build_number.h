#ifndef BUILD_NUMBER_STR
#define BUILD_NUMBER_STR "24"
#endif

#ifndef BUILD_NUMBER
#define BUILD_NUMBER 24
#endif

#ifndef BUILD_TIMESTAMP
#define BUILD_TIMESTAMP 1603332452
#endif

#ifndef VERSION_MAJOR
#define VERSION_MAJOR 1
#endif

#ifndef VERSION_MINOR
#define VERSION_MINOR 0
#endif

#ifndef VERSION_STR
#define VERSION_STR "Ver 1.0  Build 24 - Wed Oct 21 19:07:32 PDT 2020"
#endif

#ifndef VERSION_STR_SHORT
#define VERSION_STR_SHORT "1.0.24"
#endif

#ifndef GIT_HASH_STR
#define GIT_HASH_STR "10c6b5bf0d75f0072fd0aa99111242f7e2139ccb"
#endif

#ifndef GIT_BRANCH_STR
#define GIT_BRANCH_STR "alphag"
#endif

#ifndef GIT_TAG_STR
#define GIT_TAG_STR ""
#endif

#ifndef BUILT_BY_USER_STR
#define BUILT_BY_USER_STR "First Last"
#endif

#ifndef BUILT_BY_EMAIL_STR
#define BUILT_BY_EMAIL_STR "noreply@example.com"
#endif

