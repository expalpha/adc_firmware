#include <drivers/inc/lmk48xx.h>
#include <drivers/inc/altera_avalon_pio_regs.h>
#include <drivers/inc/altera_avalon_spi_regs.h>
#include <drivers/inc/altera_avalon_spi.h>
#include <stdint.h>
#include <unistd.h>
#include <system.h>

#include <sys/alt_stdio.h>
#include "build_number.h"

#define PIN_LMK_SYNC 	0x01
#define PIN_LMK_CLKin0 	0x02
#define PIN_LMK_CLKin1 	0x04
#define PIN_REF_CLK0 	0x08
#define PIN_REF_CLK1 	0x10
#define PIN_MAIN_RST_N	0x20

#define PIN_LMK_STATUS0	0x01
#define PIN_LMK_STATUS1	0x02

#define LMK_PLL1_LOCKED 0x01
#define LMK_PLL2_LOCKED 0x02

#define LMK_CLK_INTERNAL_OSC	0x00
#define LMK_CLK_ESATA			0x01
#define LMK_CLK_MUX_CLK			0x02
#define LMK_CLK_HOLDOVER		0x03

#define MUX_CLK_FPGA			0x00
#define MUX_CLK_INVALID			0x01
#define MUX_CLK_FMC				0x02
#define MUX_CLK_NIM				0x03


static void InitLMK48xx(uint8_t lmk_clkin, uint8_t mux_ref_clk);
static void lmk_spi_write(uint16_t addr, uint8_t cmd);
static alt_u8 lmk_spi_read(uint16_t addr);

int main() {
  //alt_putstr("\n\n\nALPHA16 Init\n");
  //alt_putstr(VERSION_STR);
  //alt_putstr("\n");

	InitLMK48xx(LMK_CLK_ESATA, MUX_CLK_FPGA);

	while(1); // never end the program

	return 0;
}

static void InitLMK48xx(uint8_t lmk_clkin, uint8_t mux_ref_clk) {
	tLMK48xx settings;
	tLMK48xxInfo info;
	uint8_t n;

	uint8_t output_pins;
	uint8_t lmk_status;

	output_pins = PIN_LMK_SYNC;
	output_pins |= lmk_clkin << 1;
	output_pins |= mux_ref_clk << 3;

	// Setup clock cleaner input clock, and turn 'off' sync
	IOWR_ALTERA_AVALON_PIO_DATA(PIO_INIT_OUTPUT_BASE, output_pins);

	LMK48xx_SetDefaults(&settings);

	settings.SPI_3WIRE_DIS 	= 1;	// Go to 4-wire mode
	settings.RESET_MUX    	= 6;    // SPI Readback
	settings.RESET_TYPE 	= 3;    // Output Push/Pull
	settings.OSCin_FREQ 	= 1;	// 63-127 MHz
	settings.OSCout_FMT 	= 0;	// Disable OSCout / Enable CLKin2
	settings.SYNC_POL   	= 1;
	settings.VCO_MUX    	= 1;	// 0 - ~2400MHz VCO, 1 - ~3000MHz VCO
	settings.SYSREF_GBL_PD 	= 1;	// Not using SYSREF
	settings.HOLDOVER_EN 	= 1;

	settings.CLKin_SEL_MODE = 3; // Pin select mode
	settings.CLKin0_R = 8; 	// Always 100MHz on Alpha16
	settings.CLKin1_R = 5; 	// eSATA clock, 62.5 MHz
	settings.CLKin2_R = 1; // 125MHz = 10, 12.5MHz = 1
	settings.PLL1_N = 8; 	// This makes the above "R" settings work!

	settings.PLL2_R = 2;
	settings.PLL2_P = 2;
	settings.PLL2_N = 15;
	settings.PLL2_N_CAL = 15;

	settings.PLL1_CP_POL 	= 1;
	settings.PLL1_CP_GAIN	= 4;
	settings.PLL1_LD_MUX 	= 1; // PLL1 DLD
	settings.PLL1_LD_TYPE 	= 3; // Push/Pull output
	settings.PLL1_NCLK_MUX  = 1; // Feedback MUX

	settings.PLL2_LD_MUX 	= 2; // PLL2 DLD
	settings.PLL2_LD_TYPE 	= 3; // Push/Pull output
	settings.PLL2_NCLK_MUX  = 0; // PLL Prescaler
	settings.FB_MUX_EN 		= 1;
	//settings.FB_MUX			= 0; // DCLKout6
	settings.FB_MUX			= 1; // DCLKout8
	settings.OSCin_PD		= 0;
	settings.CLKin0_OUT_MUX	= 2;
	settings.CLKin1_OUT_MUX	= 2;
	settings.CLKin_SEL0_TYPE = 0;
	settings.CLKin_SEL1_TYPE = 0;

	settings.CLKin0_EN = 1;
	settings.CLKin1_EN = 1;
	settings.CLKin2_EN = 1;

	// Clean Clocks to FPGA
	settings.ch[0].DCLKoutX_DIV 		= 24;   // 125MHz
	settings.ch[0].DCLKoutX_DDLY_PD 	= 1;    // Disable delay
	settings.ch[0].CLKoutX_Y_PD 		= 0;    // Enable
	settings.ch[0].SDCLKoutY_PD 		= 0;    // Enable
	settings.ch[0].DCLKoutX_FMT 		= 1;    // LVDS
	settings.ch[0].SDCLKoutY_FMT 		= 1;    // LVDS
	settings.ch[0].DCLKoutX_ADLYg_PD 	= 1;
	settings.ch[0].DCLKoutX_ADLY_PD 	= 1;
	settings.ch[0].DCLKoutX_MUX 		= 0;
	settings.ch[0].DCLKoutX_ADLY 		= 0;

	settings.ch[1].DCLKoutX_DIV 		= 24;   // 125MHz
	settings.ch[1].DCLKoutX_DDLY_PD 	= 1;    // Disable delay
	settings.ch[1].CLKoutX_Y_PD 		= 0;    // Enable
	settings.ch[1].SDCLKoutY_PD 		= 0;    // Enable
	settings.ch[1].DCLKoutX_FMT 		= 1;    // LVDS
	settings.ch[1].SDCLKoutY_FMT 		= 1;    // LVDS
	settings.ch[1].DCLKoutX_ADLYg_PD 	= 1;
	settings.ch[1].DCLKoutX_ADLY_PD 	= 1;
	settings.ch[1].DCLKoutX_MUX 		= 0;
	settings.ch[1].DCLKoutX_ADLY 		= 0;

	settings.ch[2].DCLKoutX_DIV 		= 24;   // 125MHz
	settings.ch[2].DCLKoutX_DDLY_PD 	= 1;    // Disable delay
	settings.ch[2].CLKoutX_Y_PD 		= 0;    // Enable
	settings.ch[2].SDCLKoutY_PD 		= 0;    // Enable
	settings.ch[2].DCLKoutX_FMT 		= 1;    // LVDS
	settings.ch[2].SDCLKoutY_FMT 		= 1;    // LVDS
	settings.ch[2].DCLKoutX_ADLYg_PD 	= 1;
	settings.ch[2].DCLKoutX_ADLY_PD 	= 1;
	settings.ch[2].DCLKoutX_MUX 		= 0;
	settings.ch[2].DCLKoutX_ADLY 		= 0;

	// DAC and CleanClock2 to FPGA NEVER CHANGE THESE FROM 100! Used for Cascaded 0-phase feedback, see "settings.FB_MUX"
	//settings.ch[3].DCLKoutX_DIV 		= 30;   // 30=100MHz
	settings.ch[3].DCLKoutX_DIV 		= 24;   // 24=125MHz
	settings.ch[3].DCLKoutX_DDLY_PD 	= 1;    // Disable delay
	settings.ch[3].CLKoutX_Y_PD 		= 0;    // Enable
	settings.ch[3].SDCLKoutY_PD 		= 0;    // Enable
	settings.ch[3].DCLKoutX_FMT 		= 1;    // LVDS
	settings.ch[3].SDCLKoutY_FMT 		= 1;    // LVDS
	settings.ch[3].DCLKoutX_ADLYg_PD 	= 1;
	settings.ch[3].DCLKoutX_ADLY_PD 	= 1;
	settings.ch[3].DCLKoutX_MUX 		= 0;
	settings.ch[3].DCLKoutX_ADLY 		= 0;

	// The 100 MHz clock settings.ch[4] is used for Cascaded 0-phase feedback, must be 100MHz, see "settings.FB_MUX"
	// ADC0-4 Clocks
	for(n=4; n<6; n++) {
		settings.ch[n].DCLKoutX_DIV 		= 30;   // 100MHz
		settings.ch[n].DCLKoutX_DDLY_PD 	= 1;    // Disable delay
		settings.ch[n].CLKoutX_Y_PD 		= 0;    // Enable
		settings.ch[n].SDCLKoutY_PD 		= 0;    // Enable
		settings.ch[n].DCLKoutX_FMT 		= 1;    // LVDS
		settings.ch[n].SDCLKoutY_FMT 		= 1;    // LVDS
		settings.ch[n].DCLKoutX_ADLYg_PD 	= 1;
		settings.ch[n].DCLKoutX_ADLY_PD 	= 1;
		settings.ch[n].DCLKoutX_MUX 		= 0;
		settings.ch[n].DCLKoutX_ADLY 		= 0;
	}

	// FMC ADC32 is run at 125MHz then using an internal div2
	settings.ch[6].DCLKoutX_DIV 		= 24;   // 24=125MHz. Divided by two by the ADC
	settings.ch[6].DCLKoutX_DDLY_PD 	= 1;    // Disable delay
	settings.ch[6].CLKoutX_Y_PD 		= 0;    // Enable
	settings.ch[6].SDCLKoutY_PD 		= 0;    // Enable
	settings.ch[6].DCLKoutX_FMT 		= 1;    // LVDS
	settings.ch[6].SDCLKoutY_FMT 		= 1;    // LVDS
	settings.ch[6].DCLKoutX_ADLYg_PD 	= 1;
	settings.ch[6].DCLKoutX_ADLY_PD 	= 1;
	settings.ch[6].DCLKoutX_MUX 		= 0;
	settings.ch[6].DCLKoutX_ADLY 		= 0;

	LMK48xx_GetID(&settings, &info, lmk_spi_write, lmk_spi_read);
	/*
	ctx->lmk_device =  info.ID_DEVICE_TYPE;
	ctx->lmk_product = info.ID_PROD;
	ctx->lmk_maskrev = info.ID_MASKREV;
	ctx->lmk_vendor =  info.ID_VNDR;
	*/

	LMK48xx_Program(&settings, info.ID_MASKREV, lmk_spi_write);

	do {
		// Get the two LMK status pins
		lmk_status = IORD_ALTERA_AVALON_PIO_DATA(PIO_INIT_INPUT_BASE);
		// Wait for the PLL1 and PLL2 to lock, or timeout
		usleep(1000);
	} while((lmk_status & LMK_PLL2_LOCKED) != LMK_PLL2_LOCKED);

	// Down the LMK_SYNC pin, forcing the LMK to reset it's output counters and align the clocks
	usleep(1000);
	IOWR_ALTERA_AVALON_PIO_CLEAR_BITS(PIO_INIT_OUTPUT_BASE, PIN_LMK_SYNC);
	usleep(1000);
	IOWR_ALTERA_AVALON_PIO_SET_BITS(PIO_INIT_OUTPUT_BASE, PIN_LMK_SYNC);

	do {
		// Get the two LMK status pins
		lmk_status = IORD_ALTERA_AVALON_PIO_DATA(PIO_INIT_INPUT_BASE);
		// Wait for the PLL1 and PLL2 to lock, or timeout
		usleep(1000);
	} while((lmk_status & LMK_PLL2_LOCKED) != LMK_PLL2_LOCKED);

	// Release the main NIOS from reset
	usleep(1000000);

	IOWR_ALTERA_AVALON_PIO_SET_BITS(PIO_INIT_OUTPUT_BASE, PIN_MAIN_RST_N);
}

static void lmk_spi_write(uint16_t addr, uint8_t cmd) {
	alt_u8 tx_data[3];

	tx_data[0] = (addr & 0x0F00) >> 8;
	tx_data[1] = (addr & 0x00FF);
	tx_data[2] = cmd;

	alt_avalon_spi_command(SPI_CLOCKCLEANER_BASE, 0, 3, (alt_u8*)&tx_data[0], 0, 0, 0);
}

static alt_u8 lmk_spi_read(uint16_t addr) {
	alt_u8 tx_data[3];
	alt_u8 rx_data;

	tx_data[0] = 0x80 | ((addr & 0x0F00) >> 8);
	tx_data[1] = (addr & 0x00FF);
	tx_data[2] = 0;

	alt_avalon_spi_command(SPI_CLOCKCLEANER_BASE, 0, 2, (alt_u8*)&tx_data[0], 1, (alt_u8*)&rx_data, 0);

	return rx_data;
}
