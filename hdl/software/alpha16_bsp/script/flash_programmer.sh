#!/bin/sh
#
# This file was automatically generated.
#
# It can be overwritten by nios2-flash-programmer-generate or nios2-flash-programmer-gui.
#

#
# Converting ELF File: E:\Dropbox\work\alphag\alpha16_rev1\trunk\tsb\ip\rtl\software\ALPHA16\ALPHA16.elf to: "..\flash/ALPHA16_epcq_config_avl_mem.flash"
#
elf2flash --input="E:/Dropbox/work/alphag/alpha16_rev1/trunk/tsb/ip/rtl/software/ALPHA16/ALPHA16.elf" --output="../flash/ALPHA16_epcq_config_avl_mem.flash" --boot="$SOPC_KIT_NIOS2/components/altera_nios2/boot_loader_cfi.srec" --base=0 --end=0 --reset=0x21000000 --verbose --epcs 

#
# Programming File: "..\flash/ALPHA16_epcq_config_avl_mem.flash" To Device: epcq_config_avl_mem
#
nios2-flash-programmer "../flash/ALPHA16_epcq_config_avl_mem.flash" --base=0x20000000 --sidp=0x2300B2C0 --id=0x414C3136 --timestamp=1464937681 --device=1 --instance=0 '--cable=USB-Blaster on localhost [USB-0]' --program --verbose 

