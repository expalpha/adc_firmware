#ifndef __AG_REGS_H__
#define __AG_REGS_H__

// Control Registers
#define AG_ADC16_THRESHOLD     0
#define AG_ADC32_THRESHOLD     1
#define AG_DAC_DATA            2
#define AG_DAC_CTRL            3
#define AG_DAC_CTRL_A          4
#define AG_DAC_CTRL_B          5
#define AG_DAC_CTRL_C          6
#define AG_DAC_CTRL_D          7
#define AG_ADC16_STHRESHOLD    8
#define AG_ADC32_STHRESHOLD    9
#define AG_CTRL_A             10
#define AG_CTRL_B             11
#define AG_CTRL_C             12
#define AG_CTRL_D             13
#define AG_CTRL_E             14
#define AG_CTRL_F             15

// Status Registers
#define AG_ADC16_BITS          0
#define AG_ADC32_BITS          1
#define AG_ADC16_COUNTER       2
#define AG_ADC32_COUNTER       3
#define AG_STAT_A              4
#define AG_STAT_B              5
#define AG_STAT_C              6
#define AG_STAT_D              7

#endif /* __AG_REGS_H__ */
