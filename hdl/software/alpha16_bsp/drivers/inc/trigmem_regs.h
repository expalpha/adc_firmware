#ifndef __TRIGMEM_REGS_H__
#define __TRIGMEM_REGS_H__

#define TRIGMEM_REG_CTRL_FORCE_TRIG      ( 0 )
#define TRIGMEM_NUM_CTRL_BASE_REGS 			( 1 )

#define TRIGMEM_REG_CTRL_CH_TRIG_POINT 			( 0 )
#define TRIGMEM_REG_CTRL_CH_STOP_POINT			( 1 )
#define TRIGMEM_REG_CTRL_CH_FORCE_TRIG			( 2 )
#define TRIGMEM_REG_CTRL_CH_SOFT_RESET			( 3 )
#define TRIGMEM_NUM_CTRL_CH_REGS 						( 4 )

// Status Registers
#define TRIGMEM_REG_STAT_DEPTH						( 0 )
#define TRIGMEM_REG_STAT_NUM_BUFF				( 1 )
#define TRIGMEM_REG_STAT_NUM_CH					( 2 )
#define TRIGMEM_REG_STAT_WIDTH						( 3 )
#define TRIGMEM_NUM_STAT_BASE_REGS 			( 4 )

#define TRIGMEM_REG_STAT_CH_TRIGERRABLE	( 0 )
#define TRIGMEM_REG_STAT_CH_TRIG_STATUS	( 1 )
#define TRIGMEM_REG_STAT_CH_FILL_STATUS	( 2 )
#define TRIGMEM_REG_STAT_CH_FILL_COUNT	( 3 )
#define TRIGMEM_NUM_STAT_CH_REGS					( 4 )

#endif /* __TRIGMEM_REGS_H__ */
