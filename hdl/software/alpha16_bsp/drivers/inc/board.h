#ifndef __BOARD_H__
#define __BOARD_H__

#include "alt_types.h"

#ifdef __cplusplus
extern "C"
{
#endif /* __cplusplus */

alt_u32 BOARD_Read(alt_u32 base, alt_u32 id);
void BOARD_Write(alt_u32 base, alt_u32 id, alt_u32 data);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __BOARD_H__ */
