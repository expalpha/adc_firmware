/*
 * Copyright (c) 2014, TRIUMF
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * 	  this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of TRIUMF.
 *
 * Created by: Bryerton Shaw
 * Created on: March 03, 2014
 */

#ifndef LTC2655_H_
#define LTC2655_H_

#include <alt_types.h>

#ifdef __cplusplus
extern "C"
{
#endif /* __cplusplus */

#define LTC2655_INSTANCE(name, dev) extern int alt_no_storage
#define LTC2655_INIT(name, dev) while (0)

#define LTC2655_CMD_WRITE_n 			0x00
#define LTC2655_CMD_UPDATE_n			0x01
#define LTC2655_CMD_WRITE_UPDATE_ALL	0x02
#define LTC2655_CMD_WRITE_UPDATE_n		0x03
#define LTC2655_CMD_POWER_DOWN_n		0x04
#define LTC2655_CMD_POWER_DOWN_CHIP		0x05
#define LTC2655_CMD_SEL_INT_REF			0x06
#define LTC2655_CMD_SEL_EXT_REF			0x07

#define LTC2655_ADDR_DAC_A				0x00
#define LTC2655_ADDR_DAC_B				0x01
#define LTC2655_ADDR_DAC_C				0x02
#define LTC2655_ADDR_DAC_D				0x03
#define LTC2655_ADDR_DAC_ALL			0x0F

/*!
 Sends a command to the LTC2655 DAC
 @param[in]  i2c_base	The avalon address of the i2c_opencore component used to communicate with the DAC
 @param[in]  addr		The i2c address of the DAC chip
 @param[in]  cmd		The command to send to the DAC
 @param[in]  dac		The DAC being addressed (A,B,C,D,ALL)
 @param[in]	 value		The value being assigned to the DAC
 */
void LTC2655_Write(alt_u32 i2c_base, alt_u8 addr,  alt_u8 cmd, alt_u8 dac, alt_u16 value);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* LTC2655_H_ */
