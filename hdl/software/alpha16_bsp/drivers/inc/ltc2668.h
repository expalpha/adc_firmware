/*
 * Copyright (c) 2017, TRIUMF
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * 	  this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of TRIUMF.
 *
 * Created by: Bryerton Shaw
 * Created on: June 14, 2017
 */

#ifndef LTC2668_H_
#define LTC2668_H_

#include <alt_types.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C"
{
#endif /* __cplusplus */

#define LTC2668_INSTANCE(name, dev) extern int alt_no_storage
#define LTC2668_INIT(name, dev) while (0)

#define LTC2668_CMD_WRITE_CODE_N 		    0x0
#define LTC2668_CMD_WRITE_CODE_ALL          0x8
#define LTC2668_CMD_WRITE_SPAN_N            0x6
#define LTC2668_CMD_WRITE_SPAN_ALL          0xE
#define LTC2668_CMD_UPDATE_N                0x1
#define LTC2668_CMD_UPDATE_ALL              0x9
#define LTC2668_CMD_WRITE_N_UPDATE_N        0x3
#define LTC2668_CMD_WRITE_N_UPDATE_ALL      0x2
#define LTC2668_CMD_WRITE_ALL_UPDATE_ALL    0xA
#define LTC2668_CMD_POWER_DOWN_N            0x4
#define LTC2668_CMD_POWER_DOWN_CHIP         0x5
#define LTC2668_CMD_MONITOR_MUX             0xB
#define LTC2668_CMD_TOGGLE_SELECT           0xC
#define LTC2668_CMD_CONFIG                  0x7
#define LTC2668_CMD_NO_OP                   0xF

#define LTC2668_ADDR_DAC_0				    0x0
#define LTC2668_ADDR_DAC_1				    0x1
#define LTC2668_ADDR_DAC_2				    0x2
#define LTC2668_ADDR_DAC_3				    0x3
#define LTC2668_ADDR_DAC_4	    		    0x4
#define LTC2668_ADDR_DAC_5				    0x5
#define LTC2668_ADDR_DAC_6				    0x6
#define LTC2668_ADDR_DAC_7				    0x7
#define LTC2668_ADDR_DAC_8	    		    0x8
#define LTC2668_ADDR_DAC_9				    0x9
#define LTC2668_ADDR_DAC_10				    0xA
#define LTC2668_ADDR_DAC_11				    0xB
#define LTC2668_ADDR_DAC_12	    		    0xC
#define LTC2668_ADDR_DAC_13				    0xD
#define LTC2668_ADDR_DAC_14				    0xE
#define LTC2668_ADDR_DAC_15				    0xF

/*!
 Sends a command to the LTC2655 DAC
 @param[in]  spi_base	The avalon address of the spi component used to communicate with the DAC
 @param[in]  cs         chipselect of DAC
 @param[in]  cmd		The command to send to the DAC
 @param[in]  addr		The dac to address on the DAC chip
 @param[in]	 value		The value being assigned to the DAC
 */
void LTC2668_Write(alt_u32 io_base, alt_u32 io_tri,uint32_t spi_base, uint32_t cs, uint8_t cmd, uint8_t addr, uint16_t value);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* LTC2668_H_ */
