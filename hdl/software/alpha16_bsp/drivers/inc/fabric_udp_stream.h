#ifndef FABRIC_UDP_STREAM_H
#define FABRIC_UDP_STREAM_H

#include "alt_types.h"

// the entries below marked with "*" are required inputs for start inserter routine
// all entries are returned from the inserter stat routine
typedef struct {
    alt_u32 csr_state;      // out only - csr value
    alt_u32 mac_dst_hi;     // * hi 32 bits of the 48 bit mac destination address
    alt_u16 mac_dst_lo;     // * lo 16 bits of the 48 bit mac destination address
    alt_u32 mac_src_hi;     // * hi 32 bits of the 48 bit mac source address
    alt_u16 mac_src_lo;     // * lo 16 bits of the 48 bit mac source address
    alt_u32 ip_src;         // * IP address of source
    alt_u32 ip_dst;         // * IP address of destination
    alt_u16 udp_src;        // * UDP source port
    alt_u16 udp_dst;        // * UDP destination port
    alt_u32 packet_count;   // packet counter value
} tFabricUDPStreamStats;

int StartFabricUDPStream(unsigned int base, tFabricUDPStreamStats *stats);
int StopFabricUDPStream(unsigned int base);
int IsFabricUDPStreamRunning(unsigned int base);
int GetFabricUDPStreamStats(unsigned int base, tFabricUDPStreamStats *stats);

#endif /* FABRIC_UDP_STREAM_H */
