#include <alt_types.h>
#include <altera_avalon_pio_regs.h>
#include <board_regs.h>
#include <board.h>

alt_u32 BOARD_Read(alt_u32 base, alt_u32 id) {
	return IORD(base, id);
}

void BOARD_Write(alt_u32 base, alt_u32 id, alt_u32 data) {
	IOWR(base, id, data);
}
