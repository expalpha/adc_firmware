/*
 * Copyright (c) 2016, TRIUMF
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * 	  this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of TRIUMF.
 *
 * Created by: Bryerton Shaw
 * Created on: May 16, 2016
 */

#include <unistd.h>
#include <drivers/inc/24aa02e48.h>
#include <drivers/inc/i2c_opencores_regs.h>
#include <drivers/inc/i2c_opencores.h>

/*!
 Write a byte to the EEPROM
 @param[in]  i2c_base	The avalon address of the i2c_opencore component
 @param[in]  addr		The address to write the data to
 @param[in]  data		The data to write to the EEPROM
 */
void EEPROM_24AA02E48_WriteByte(alt_u32 i2c_base, alt_u8 addr,  alt_u8 data) {
	if(I2C_start(i2c_base, EEPROM_24AA02E48_CTRL_CODE, I2C_OPENCORES_TXR_WR_MSK) == I2C_ACK) {
		I2C_write(i2c_base,  addr, 0);
		I2C_write(i2c_base, data, 1);

		// ACK Poll
		do {
				usleep(100);
		} while(I2C_start(i2c_base, EEPROM_24AA02E48_CTRL_CODE, I2C_OPENCORES_TXR_WR_MSK) == I2C_NOACK);
	}
}

/*!
 Write a page (8 bytes) to the EEPROM
 @param[in]  i2c_base	The avalon address of the i2c_opencore component
 @param[in]  addr		The address to write the data to, must be page aligned (0,8,16,etc)
 @param[in]  data		The array of data to write to the EEPROM, must be 8 or less bytes, or the write will wrap on the page
 */
void EEPROM_24AA02E48_WritePage(alt_u32 i2c_base, alt_u8 addr,  alt_u8* data, alt_u8 data_len) {
	alt_u8 n;
	
	if(I2C_start(i2c_base, EEPROM_24AA02E48_CTRL_CODE, I2C_OPENCORES_TXR_WR_MSK) == I2C_ACK) {
		I2C_write(i2c_base,  addr, 0);		
		for(n=0; n<data_len; n++) {
			I2C_write(i2c_base, data[n], (n == (data_len - 1)) ? 1 : 0);
		}

		// ACK Poll
		do {
				usleep(100);
		} while(I2C_start(i2c_base, EEPROM_24AA02E48_CTRL_CODE, I2C_OPENCORES_TXR_WR_MSK) == I2C_NOACK);
	}
}

alt_u8 EEPROM_24AA02E48_ReadByte(alt_u32 i2c_base, alt_u8 addr) {
	alt_u8 data;
	
	data = 0;
	if(I2C_start(i2c_base, EEPROM_24AA02E48_CTRL_CODE, I2C_OPENCORES_TXR_WR_MSK) == I2C_ACK) {		
		I2C_write(i2c_base, addr, 0);
		I2C_start(i2c_base, EEPROM_24AA02E48_CTRL_CODE, I2C_OPENCORES_TXR_RD_MSK);
		data = I2C_read(i2c_base, 1);
	}

	return data;
}

void EEPROM_24AA02E48_ReadPage(alt_u32 i2c_base, alt_u8 addr, alt_u8* data, alt_u8 data_len) {
	alt_u8 n;

	if(I2C_start(i2c_base, EEPROM_24AA02E48_CTRL_CODE, I2C_OPENCORES_TXR_WR_MSK) == I2C_ACK) {		
		I2C_write(i2c_base, addr, 0);
		I2C_start(i2c_base, EEPROM_24AA02E48_CTRL_CODE, I2C_OPENCORES_TXR_RD_MSK);
		
		for(n=0; n<data_len; n++) {
			usleep(100);
			data[n] = I2C_read(i2c_base, (n == (data_len - 1)) ? 1 : 0);
		}
	}
}

void EEPROM_24AA02E48_GetEUI48(alt_u32 i2c_base, tEUI48* eui) {
	EEPROM_24AA02E48_ReadPage(i2c_base, EEPROM_24AA02E48_ADDR_EUI48, eui->field, 6);
}

void EEPROM_24AA02E48_GetEUI64(alt_u32 i2c_base, tEUI64* eui) {
	alt_u8 field[6];
	
	EEPROM_24AA02E48_ReadPage(i2c_base, EEPROM_24AA02E48_ADDR_EUI48, field, 6);
	
	eui->field[0] = field[0]; // Should be 0x00 
	eui->field[1] = field[1]; // Should be 0x04
	eui->field[2] = field[2]; // Should be 0xA3
	
	eui->field[3] = 0xFF; // Defined by datasheet for EEPROM
	eui->field[4] = 0xFE;
	
	eui->field[5] = field[5];
	eui->field[6] = field[6];
	eui->field[7] = field[7];
}
