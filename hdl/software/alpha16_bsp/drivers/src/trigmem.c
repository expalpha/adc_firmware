#include <alt_types.h>
#include <unistd.h>
#include <string.h>
#include <trigmem_regs.h>
#include <trigmem.h>

static unsigned int bits_to_represent(unsigned int x) {
	int result = 0;
	while (x >>= 1) {
		result += 1;
	}
	return result;
}

void TRIGMEM_SetChannel(tTriggerMem* tmem, alt_u16 ch, alt_u16 trig_point, alt_u16 stop_point) {
	if(ch >= tmem->num_ch) { return; }

	if(trig_point > tmem->depth) trig_point = tmem->depth-1;
	if(stop_point > tmem->depth) stop_point = tmem->depth-1;

	// Hold Channel in reset while we modify it
	TRIGMEM_WRITE_REG_CTRL_CH(tmem->ctrl_base, ch, TRIGMEM_REG_CTRL_CH_SOFT_RESET, 1);
	TRIGMEM_WRITE_REG_CTRL_CH(tmem->ctrl_base, ch, TRIGMEM_REG_CTRL_CH_TRIG_POINT, trig_point);
	TRIGMEM_WRITE_REG_CTRL_CH(tmem->ctrl_base, ch, TRIGMEM_REG_CTRL_CH_STOP_POINT, stop_point);
	TRIGMEM_WRITE_REG_CTRL_CH(tmem->ctrl_base, ch, TRIGMEM_REG_CTRL_CH_FORCE_TRIG, 0);
	TRIGMEM_WRITE_REG_CTRL_CH(tmem->ctrl_base, ch, TRIGMEM_REG_CTRL_CH_SOFT_RESET, 0);
}

void TRIGMEM_Init(tTriggerMem* tmem, alt_u32 ctrl_base, alt_u32 stat_base, alt_u32 data_base) {
	tmem->ctrl_base = ctrl_base;
	tmem->stat_base = stat_base;
	tmem->data_base = data_base;

	TRIGMEM_GetInfo(tmem);
}

void TRIGMEM_ForceGlobalTrigger(tTriggerMem* tmem) {
   TRIGMEM_WRITE_REG_CTRL(tmem->ctrl_base, TRIGMEM_REG_CTRL_FORCE_TRIG, 1);
   TRIGMEM_WRITE_REG_CTRL(tmem->ctrl_base, TRIGMEM_REG_CTRL_FORCE_TRIG, 0);
}

void TRIGMEM_GetInfo(tTriggerMem* tmem) {
	tmem->num_ch 		= TRIGMEM_READ_REG_STAT(tmem->stat_base,TRIGMEM_REG_STAT_NUM_CH);
	tmem->num_buffer 	= TRIGMEM_READ_REG_STAT(tmem->stat_base,TRIGMEM_REG_STAT_NUM_BUFF);
	tmem->depth 		= TRIGMEM_READ_REG_STAT(tmem->stat_base,TRIGMEM_REG_STAT_DEPTH);
	tmem->width 		= TRIGMEM_READ_REG_STAT(tmem->stat_base,TRIGMEM_REG_STAT_WIDTH);
	tmem->width_in_bytes = tmem->width >> 3;
	tmem->data_offset = bits_to_represent(tmem->depth*tmem->width_in_bytes);
	tmem->buff_offset = bits_to_represent(tmem->num_buffer);
}

void TRIGMEM_GetChannelInfo(tTriggerMem* tmem, alt_u16 ch, tTriggerStatus* status) {
	if(ch >= tmem->num_ch) { return; }

	status->trig_point = TRIGMEM_READ_REG_CTRL_CH(tmem->ctrl_base, ch, TRIGMEM_REG_CTRL_CH_TRIG_POINT);
	status->stop_point = TRIGMEM_READ_REG_CTRL_CH(tmem->ctrl_base, ch, TRIGMEM_REG_CTRL_CH_STOP_POINT);
	status->samples_to_read = status->stop_point + 1;

	status->triggerable	= TRIGMEM_READ_REG_STAT_CH(tmem->stat_base, ch, TRIGMEM_REG_STAT_CH_TRIGERRABLE);
	status->triggered 	= TRIGMEM_READ_REG_STAT_CH(tmem->stat_base, ch, TRIGMEM_REG_STAT_CH_TRIG_STATUS);
	status->filled 		= TRIGMEM_READ_REG_STAT_CH(tmem->stat_base, ch, TRIGMEM_REG_STAT_CH_FILL_STATUS);
	status->fill_count	= TRIGMEM_READ_REG_STAT_CH(tmem->stat_base, ch, TRIGMEM_REG_STAT_CH_FILL_COUNT);
}

alt_u32 TRIGMEM_IsChannelFilled(tTriggerMem* tmem, alt_u16 ch) {
	if(ch >= tmem->num_ch) { return 0; }

	return TRIGMEM_READ_REG_STAT_CH(tmem->stat_base, ch, TRIGMEM_REG_STAT_CH_FILL_STATUS);
}

alt_u32 TRIGMEM_IsChannelTriggered(tTriggerMem* tmem, alt_u16 ch) {
	if(ch >= tmem->num_ch) { return 0; }

	return TRIGMEM_READ_REG_STAT_CH(tmem->stat_base, ch, TRIGMEM_REG_STAT_CH_TRIG_STATUS);
}


void TRIGMEM_TriggerChannel(tTriggerMem* tmem, alt_u16 ch) {
	if(ch >= tmem->num_ch) { return; }

	TRIGMEM_WRITE_REG_CTRL_CH(tmem->ctrl_base, ch, TRIGMEM_REG_CTRL_CH_FORCE_TRIG, 1);

}

void TRIGMEM_ResetChannel(tTriggerMem* tmem, alt_u16 ch) {
	if(ch >= tmem->num_ch) { return; }

	TRIGMEM_WRITE_REG_CTRL_CH(tmem->ctrl_base, ch, TRIGMEM_REG_CTRL_CH_FORCE_TRIG, 0);
	TRIGMEM_WRITE_REG_CTRL_CH(tmem->ctrl_base, ch, TRIGMEM_REG_CTRL_CH_SOFT_RESET, 1);
	TRIGMEM_WRITE_REG_CTRL_CH(tmem->ctrl_base, ch, TRIGMEM_REG_CTRL_CH_SOFT_RESET, 0);
}

volatile void* TRIGMEM_GetChannelLocation(tTriggerMem* tmem, alt_u16 ch, alt_u16 buffer) {
	return (void*) (tmem->data_base | 0x80000000 | ((((ch << tmem->buff_offset) | buffer) << tmem->data_offset)));
}

alt_u16 TRIGMEM_GetChannelBuffer(tTriggerMem* tmem, alt_u16 ch, alt_u16 buffer,  alt_u8* data, alt_u16 len, alt_u32 self_trig) {
	tTriggerStatus tstatus;
	//alt_u16 ch_offset = ch << 11;

	if(ch >= tmem->num_ch) { return 0; }

	
	// If self_trig requested, trigger then wait, otherwise check if a trigger has occurred and bail if not!
	if(self_trig) {
		TRIGMEM_TriggerChannel(tmem, ch);
	} else {
		// Exit immediately if channel is not triggered, don't want to get stuck, poll elsewhere
		if(TRIGMEM_IsChannelTriggered(tmem, ch) == 0) return 0;		
	}
	
	// Wait for channel to be filled
	do {
		usleep(1);
	} while(TRIGMEM_IsChannelFilled(tmem, ch) == 0);
	
	TRIGMEM_GetChannelInfo(tmem, ch, &tstatus);

	if(len > (tstatus.fill_count * tmem->width_in_bytes)) len = tstatus.fill_count * tmem->width_in_bytes;
	memcpy((alt_u8*) data,
		(alt_u8*)(tmem->data_base | 0x80000000 | ((((ch << tmem->buff_offset) | buffer) << tmem->data_offset))),
		len // number of bytes to copy
	);

	TRIGMEM_ResetChannel(tmem, ch);

	return len;
}
