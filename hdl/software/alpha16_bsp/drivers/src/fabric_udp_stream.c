#include "fabric_udp_stream_regs.h"
#include "fabric_udp_stream.h"

//
// Fabric UDP Stream utility routines
//
int StartFabricUDPStream(unsigned int base, tFabricUDPStreamStats *stats) {
    alt_u32 current_csr;

    // is the packet generator already running?
    current_csr = FABRIC_UDP_STREAM_RD_CSR(base);
    if(current_csr & FABRIC_UDP_STREAM_CSR_GO_BIT_MASK) {
        return 1;
    }
    if(current_csr & FABRIC_UDP_STREAM_CSR_RUNNING_BIT_MASK) {
        return 2;
    }
    
    // clear the counter    
    FABRIC_UDP_STREAM_CLEAR_PACKET_COUNTER(base);
    
    // write the parameter registers
    FABRIC_UDP_STREAM_WR_MAC_DST_HI  (base, stats->mac_dst_hi);
    FABRIC_UDP_STREAM_WR_MAC_DST_LO  (base, stats->mac_dst_lo);
    FABRIC_UDP_STREAM_WR_MAC_SRC_HI  (base, stats->mac_src_hi);
    FABRIC_UDP_STREAM_WR_MAC_SRC_LO  (base, stats->mac_src_lo);
    FABRIC_UDP_STREAM_WR_IP_SRC      (base, stats->ip_src);
    FABRIC_UDP_STREAM_WR_IP_DST      (base, stats->ip_dst);
    FABRIC_UDP_STREAM_WR_UDP_PORTS   (base, (alt_u32)(stats->udp_src << 16) | (alt_u32)(stats->udp_dst));

    // and set the go bit
    FABRIC_UDP_STREAM_WR_CSR(base, FABRIC_UDP_STREAM_CSR_GO_BIT_MASK);
    
    return 0;
}

int StopFabricUDPStream(unsigned int base) {
    // is the peripheral already stopped?
    if(!(FABRIC_UDP_STREAM_RD_CSR(base) & FABRIC_UDP_STREAM_CSR_GO_BIT_MASK)) {
        return 1;
    }

    // clear the go bit
    FABRIC_UDP_STREAM_WR_CSR(base, 0);
    
    return 0;
}

int IsFabricUDPStreamRunning(unsigned int base) {
    // is the peripheral running?
    if((FABRIC_UDP_STREAM_RD_CSR(base) & FABRIC_UDP_STREAM_CSR_RUNNING_BIT_MASK)) {
        return 1;
    }

    return 0;
}

int GetFabricUDPStreamStats(unsigned int base, tFabricUDPStreamStats *stats) {
    stats->csr_state    = FABRIC_UDP_STREAM_RD_CSR(base);
    stats->mac_dst_hi   = FABRIC_UDP_STREAM_RD_MAC_DST_HI(base);
    stats->mac_dst_lo   = FABRIC_UDP_STREAM_RD_MAC_DST_LO(base);
    stats->mac_src_hi   = FABRIC_UDP_STREAM_RD_MAC_SRC_HI(base);
    stats->mac_src_lo   = FABRIC_UDP_STREAM_RD_MAC_SRC_LO(base);
    stats->ip_src       = FABRIC_UDP_STREAM_RD_IP_SRC(base);
    stats->ip_dst       = FABRIC_UDP_STREAM_RD_IP_DST(base);
    stats->udp_src      = (FABRIC_UDP_STREAM_RD_UDP_PORTS(base) & FABRIC_UDP_STREAM_UDP_SRC_MASK) >> FABRIC_UDP_STREAM_UDP_SRC_OFST;
    stats->udp_dst      = (FABRIC_UDP_STREAM_RD_UDP_PORTS(base) & FABRIC_UDP_STREAM_UDP_DST_MASK) >> FABRIC_UDP_STREAM_UDP_DST_OFST;
    stats->packet_count = FABRIC_UDP_STREAM_RD_PACKET_COUNTER(base);
    
    return 0;
}
