#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <system.h>
#include <sys/alt_stdio.h>
#include <alt_types.h>
#include <sys/alt_alarm.h>
#include <sys/alt_cache.h>
#include <sys/alt_dev.h>
#include <sys/alt_irq.h>
#include <sys/alt_sys_init.h>
#include <sys/alt_flash.h>
#include <priv/alt_file.h>
#include <drivers/inc/altera_remote_update_regs.h>
#include <drivers/inc/altera_remote_update.h>
#include <drivers/inc/altera_epcq_controller_regs.h>
#include <drivers/inc/altera_epcq_controller.h>
#include "bootloader.h"
#include "memtest.h"
#include "build_number.h"

// Watchdog Timeout Period, the clock is 12MHz, but it only uses the upper 12 bits of the 29 bit counter
// NOTE: If this is changed, please update the alt_printf() text for the watchdog timeout, as the alt_printf() doesn't support decimal printing, so the value is hard-coded
#define WATCHDOG_TIMEOUT_PERIOD (12500000u * 5u) >> 17

#define WATCHDOG_RESET(dev) if(dev) { IOWR_ALTERA_RU_RESET_TIMER(dev->base, 1); }

/*
 * Flash device name to use
 */
#define FLASH_DEV_NAME EPCQ_CONFIG_AVL_MEM_NAME

/*
 *  Edit this define to control the boot method.
 *  Options are:
 *        BOOT_FROM_CFI_FLASH
 *        BOOT_CFI_FROM_ONCHIP_ROM
 *        BOOT_EPCS_FROM_ONCHIP_ROM
 */
#define BOOT_METHOD BOOT_EPCS_FROM_ONCHIP_ROM

/*
 * These defines locate our two possible application images at specifically
 * these two offsets in the flash memory.  If you edit these defines, ensure
 * when programming the images to flash that you program them to the new
 * locations you define here.
 */
#if BOOT_METHOD == BOOT_FROM_CFI_FLASH

#	define BOOT_IMAGE_1_OFFSET  ( 0x00240000 )
#	define BOOT_IMAGE_2_OFFSET  ( 0x00440000 )
#	define BOOT_IMAGE_1_ADDR  ( EXT_FLASH_BASE + BOOT_IMAGE_1_OFFSET )
#	define BOOT_IMAGE_2_ADDR  ( EXT_FLASH_BASE + BOOT_IMAGE_2_OFFSET )

#elif BOOT_METHOD == BOOT_CFI_FROM_ONCHIP_ROM

#	define BOOT_IMAGE_1_OFFSET  ( 0x00240000 )
#	define BOOT_IMAGE_2_OFFSET  ( 0x00440000 )
#	define BOOT_IMAGE_1_ADDR  ( EXT_FLASH_BASE + BOOT_IMAGE_1_OFFSET )
#	define BOOT_IMAGE_2_ADDR  ( EXT_FLASH_BASE + BOOT_IMAGE_2_OFFSET )

#elif BOOT_METHOD == BOOT_EPCS_FROM_ONCHIP_ROM

// In an EPCS, the data is not present in the CPU's memory map, so we just
// specify the image addresses as offsets within the EPCS.  The CopyFromFlash
// routine will work out the details later.
// It is important to carefully choose the offsets at which to place boot images
// in EPCS devices, ensuring they do not overlap FPGA configuration data. These
// offsets should be sufficient for a Cyclone II 2C35 development board.
#	define BOOT_IMAGE  ( 0x00C00000 )

#endif // BOOT_METHOD
/*
 * Don't edit these defines
 */
#if (BOOT_METHOD == BOOT_FROM_CFI_FLASH || BOOT_METHOD == BOOT_CFI_FROM_ONCHIP_ROM)
# define FLASH_TYPE CFI
#elif (BOOT_METHOD == BOOT_EPCS_FROM_ONCHIP_ROM)
# define FLASH_TYPE EPCS
#endif // BOOT_METHOD

// we overwrite the altera jtag uart "write" function
// because there is no way to prevent the stock version
// from waiting forever if output fifo is full and
// nios2-terminal is not connected. K.O. Aug 2020.

#include "altera_avalon_jtag_uart_regs.h"
#include "altera_avalon_jtag_uart.h"

int altera_avalon_jtag_uart_write(altera_avalon_jtag_uart_state* sp, 
  const char * ptr, int count, int flags)
{
  unsigned int base = sp->base;

  const char * end = ptr + count;

  //while (ptr < end)
  //  if ((IORD_ALTERA_AVALON_JTAG_UART_CONTROL(base) & ALTERA_AVALON_JTAG_UART_CONTROL_WSPACE_MSK) != 0)
  //    IOWR_ALTERA_AVALON_JTAG_UART_DATA(base, *ptr++);

  while (ptr < end)
     IOWR_ALTERA_AVALON_JTAG_UART_DATA(base, *ptr++);

  return count;
}

/*
 * The following statement defines "main()" so that when the Nios II SBT4E
 * debugger is set to break at "main()", it will break at the appropriate
 * place in this program, which does not contain a function called "main()".
 */
int main(void) __attribute__ ((weak, alias ("alt_main")));

/*****************************************************************************
 *  Function: alt_main
 *
 *  Purpose: This is our boot copier's entry point. We are implementing
 *  this as an alt_main() instead of a main(), so that we can better control
 *  the drivers that load and the system resources that are enabled.  Since
 *  code size may be a consideration, this method allows us to keep the
 *  memory requirements small.
 *
 *****************************************************************************/
int alt_main(void) {
	alt_u32 entry_point;
	alt_u32 current_page;
	alt_u32 image_location;
	alt_flash_fd* fd;
	altera_remote_update_state* rem_update_dev;
	int result;

	// Initialize relevant drivers

	// In order to allow interrupts to occur while the boot copier executes we initialize the main irq handler.
	alt_irq_init(ALT_IRQ_BASE);

	// Now we initialize the drivers that we require.
	alt_sys_init();

	// Any further initialization
	alt_io_redirect (ALT_STDOUT, ALT_STDIN, ALT_STDERR);

	/*
	 * Pick a flash image to load.  The criteria for picking an image are
	 * discussed the text of the application note, and also in the code comments
	 * preceding the function "PickFlashImage()" found in this file.
	 */

	/*
	 * Now we're going to try to load the application into memory, this will
	 * likely overwrite our current exception handler, so before we do that
	 * we'll disable interrupts and not turn them back on again.
	 *
	 * It's also important minimize your reliance on the ".rwdata", ".bss" and
	 * stack sections.  Since all of these sections can exist in the exception
	 * memory they are all subject to being overwritten.  You can inspect how
	 * much of the ".rwdata" and ".bss" sections the bootcopier uses by
	 * looking at the disassembly for the bootcopier.  The disassembly can be
	 * generated by running "nios2-elf-objdump.exe" with the -d option from a
	 * Nios II Command Shell.
	 */
	alt_irq_disable_all();

	alt_printf("\n\n\nALPHA16 Revision 1 Boot Loader\n");
	alt_printf("%s\n", VERSION_STR);
	alt_printf("Git Hash: %s\n", GIT_HASH_STR);
	alt_printf("Git Branch (Tag): %s (%s)\n", GIT_BRANCH_STR, GIT_TAG_STR);
	alt_printf("Built by: %s (%s)\n", BUILT_BY_USER_STR, BUILT_BY_EMAIL_STR);

	// Lock the EPCQ immediately!
	alt_printf("Opening Flash Device %s... ", FLASH_DEV_NAME);
	fd = alt_flash_open_dev(FLASH_DEV_NAME);
	if(fd) {
		alt_printf("Success\n");
	} else {
		alt_printf("Failed\n");
	}

	alt_printf("Locking All Sectors on EPCQ256... ");

	result = alt_epcq_controller_lock(fd, 0x1F); // lock out entire EPCQ256
	if(result == 0) {
		alt_printf("Success\n");
	} else if (result == -ETIME) {
		alt_printf("Timed out\n");
	} else {
		alt_printf("Lock Failed\n");
	}


	// Get Current page_select
	current_page = 0;
	alt_printf("Opening Remote Update Module %s... ", REMOTE_UPDATE_NAME);
	rem_update_dev = altera_remote_update_open(REMOTE_UPDATE_NAME);
	if(rem_update_dev) {
		alt_printf("Success\n");
		current_page = IORD_ALTERA_RU_PAGE_SELECT(rem_update_dev->base);

		// Enable Watchdog if a user page is loaded and remote configuration mode is set
		if((current_page != 0x0) && (current_page != 0xffffff00)) {
			alt_printf("User Image Selected at 0x%x\n", current_page);

			alt_printf("Setting Watchdog Timeout Period to 5s\n");
			IOWR_ALTERA_RU_WATCHDOG_TIMEOUT(rem_update_dev->base, WATCHDOG_TIMEOUT_PERIOD);

			alt_printf("Enabling Watchdog\n");
			IOWR_ALTERA_RU_WATCHDOG_ENABLE(rem_update_dev->base, ALTERA_RU_WATCHDOG_ENABLE);
		} else {
			if(current_page == 0xffffff00) {
				alt_printf("!!FPGA is not in Remote Configuration mode!!\nPlease configure the project in Quartus to use Remote Configuration\n");
				current_page = 0;
			}

			alt_printf("Factory Image Selected\n");

			// Disable Watchdog
			alt_printf("Disabling Watchdog\n");
			IOWR_ALTERA_RU_WATCHDOG_ENABLE(rem_update_dev->base, 0);
		}
	} else {
		alt_printf("Failed\n");
		current_page = 0;
	}

	image_location = current_page + BOOT_IMAGE;
	alt_printf("Image Location In Flash Set To 0x%x\n", image_location);

	alt_printf("Checking Flash Image... ");
	WATCHDOG_RESET(rem_update_dev);

	result = ValidateFlashImage(image_location);
	if (result == CRC_VALID) {
		WATCHDOG_RESET(rem_update_dev);
		alt_printf("Passed\n");

                if (current_page == 0) {
		// Run Mem Test
		result = memtest();
		if(result != 0) {
			alt_printf("Memory Check Failed\n");

			// Go back to the first page in the EPCQ, if we aren't already there
			if(current_page != 0) {
				alt_printf("Triggering Reconfig To Factory Image\n\n");

				altera_remote_update_trigger_reconfig(rem_update_dev, ALTERA_RU_RECONFIG_MODE_FACTORY, 0, 0);
			} else {
				alt_printf("Jumping To Reset\n\n");

				JumpFromBootCopier((void (*)(void)) (NIOS2_RESET_ADDR));
			}
		} else {
			alt_printf("Memory Check Passed\n");
		}
                } else {
			alt_printf("User Image Selected: Memory Check Skipped\n");
                }

		alt_printf("Loading Image From Flash... ");

		WATCHDOG_RESET(rem_update_dev);
		entry_point = LoadFlashImage(image_location);

		WATCHDOG_RESET(rem_update_dev);
		alt_printf("Done\n", entry_point);

		// Validate Image now that it's in DDR and make sure the entry point is found
		alt_printf("Comparing Flash Image to Loaded Image in DDR... ");

		WATCHDOG_RESET(rem_update_dev);
		if(ValidateDDRImage(image_location) == MEM_FLASH_MATCH) {
			WATCHDOG_RESET(rem_update_dev);
			alt_printf("Match\n");

			if(entry_point >= 0) {  // load the image
				alt_printf("Jumping to Entry Point Located at 0x%x\n\n", entry_point);

				// Jump to the entry point of the application
				JumpFromBootCopier((void (*)(void)) (entry_point));
			} else {
				alt_printf("No Entry Point Found\nJumping to Reset\n\n");

				usleep(500);
				// If the entry point is not found, then we should jump back to the
				// reset vector.
				JumpFromBootCopier((void (*)(void)) (NIOS2_RESET_ADDR));
			}
		// Bad DDR, no point continuing
		} else {
			alt_printf("Mismatch\nJumping To Reset\n\n");

			usleep(500);
			// If the entry point is not found, then we should jump back to the
			// reset vector.
			JumpFromBootCopier((void (*)(void)) (NIOS2_RESET_ADDR));
		}
	} else {

		if(result == SIGNATURE_INVALID) { alt_printf("Signature Not Found\n"); }
		if(result == HEADER_CRC_INVALID){ alt_printf("Header CRC Invalid\n"); }
		if(result == DATA_CRC_INVALID)	{ alt_printf("Data CRC Invalid\n"); }

		alt_printf("CRC Check Failed\n");

		// Go back to the first page in the EPCQ, if we aren't already there
		if(current_page != 0) {
			alt_printf("Triggering Reconfig To Factory Image\n\n");
			altera_remote_update_trigger_reconfig(rem_update_dev, ALTERA_RU_RECONFIG_MODE_FACTORY, 0, 0);
		} else {
			alt_printf("Factory Image is Corrupt or Missing\nJumping To Reset\n\n");
			JumpFromBootCopier((void (*)(void)) (NIOS2_RESET_ADDR));
		}
	}

	// We should never get here
	exit(0);
}

/*****************************************************************************
 *  Function: JumpFromBootCopier
 *
 *  Purpose: This routine shuts down the boot copier and jumps somewhere else.
 *  The place to jump is passed in as a function pointer named "target".
 *
 *****************************************************************************/
void JumpFromBootCopier(void target(void)) {
	/*
	 * If you have any outstanding I/O or system resources that needed to be
	 * cleanly disabled before leaving the boot copier program, then this is
	 * the place to do that.
	 *
	 * In this example we only need to ensure the state of the Nios II cpu is
	 * equivalent to reset.  If we disable interrupts, and flush the caches,
	 * then the program we jump to should receive the cpu just as it would
	 * coming out of a hardware reset.
	 */
	alt_irq_disable_all();
	alt_dcache_flush_all();
	alt_icache_flush_all();

	/*
	 * The cpu state is as close to reset as we can get it, so we jump to the new
	 * application.
	 */
	target();

	/*
	 * In the odd event that the program we jump to decides to return, we should
	 * probably just jump back to the reset vector. We pass in the reset address
	 * as a function pointer.
	 */

	// Wait 0.5 seconds
	usleep(500000);

	// Jump back to the reset address
	JumpFromBootCopier((void (*)(void)) (NIOS2_RESET_ADDR));
}

/*****************************************************************************
 *  Function: CopyFromFlash
 *
 *  Purpose:  This subroutine copies data from a flash memory to a buffer
 *  The function uses the appropriate copy routine for the flash that is
 *  defined by FLASH_TYPE.  EPCS devices can't simply be read from using
 *  memcpy().
 *
 *****************************************************************************/
void* CopyFromFlash(alt_u32 src, void * dest, size_t num) {
	alt_flash_fd* fd;

	fd = alt_flash_open_dev(FLASH_DEV_NAME);

# if( FLASH_TYPE == CFI )

	memcpy( dest, src, num );

# elif( FLASH_TYPE == EPCS )

	// If we're dealing with EPCS, "src" has already been defined for us as
	// an offset into the EPCS, not an absolute address.
	alt_read_flash(fd, src, dest, num);
# endif //FLASH_TYPE
	return (dest);
}

/*****************************************************************************
 *  Function: CompareFromFlash
 *
 *  Purpose:  This subroutine copies data from a flash memory to a buffer
 *  The function uses the appropriate copy routine for the flash that is
 *  defined by FLASH_TYPE.  EPCS devices can't simply be read from using
 *  memcpy().
 *
 *****************************************************************************/
int CompareFromFlash(alt_u32 src, void * dest, size_t num) {
	alt_flash_fd* fd;
	size_t bytes_to_check;
	char mem_buff[FLASH_BUFFER_LENGTH];

	fd = alt_flash_open_dev(FLASH_DEV_NAME);

	// If we're dealing with EPCS, "src" has already been defined for us as
	// an offset into the EPCS, not an absolute address.

	while(num > 0) {
		bytes_to_check = (FLASH_BUFFER_LENGTH < num) ? FLASH_BUFFER_LENGTH : num;

		// Load mem_buff from flash
		# if( FLASH_TYPE == CFI )
			memcpy( mem_buff, src, bytes_to_check );
		# elif( FLASH_TYPE == EPCS )
			alt_read_flash(fd, src, mem_buff, bytes_to_check);
		#endif

		// Compare mem_buff with contents of DDR
		if(memcmp(mem_buff, dest, bytes_to_check) != 0) {
			return -1;
		}

		dest += bytes_to_check; // move pointer in DDR along
		num -= bytes_to_check; // note that we've processed a number of bytes
		src += bytes_to_check; // move pointer in Flash along
	}

	return 0;
}

/*****************************************************************************
 *  Function: LoadFlashImage
 *
 *  Purpose:  This subroutine loads an image from flash into the Nios II
 *  memory map.  It decodes boot records in the format produced from the
 *  elf2flash utility, and loads the image as directed by those records.
 *  The format of the boot record is described in the text of the application
 *  note.
 *
 *  The input operand, "image" is expected to be the image selector indicating
 *  which flash image, 1 or 2, should be loaded.
 *
 *****************************************************************************/
alt_u32 LoadFlashImage(alt_u32 image_addr) {
	alt_u32 next_flash_byte;
	alt_u32 length;
	alt_u32 address;

	/*
	 * Load the image pointer based on the value of "image"
	 * The boot image header is 32 bytes long, so we add an offset of 32.
	 */
	next_flash_byte = image_addr + 32;

	/*
	 * Flash images are not guaranteed to be word-aligned within the flash
	 * memory, so a word-by-word copy loop should not be used.
	 *
	 * The "memcpy()" function works well to copy non-word-aligned data, and
	 * it is relatively small, so that's what we'll use.
	 */

	// Get the first 4 bytes of the boot record, which should be a length record
	CopyFromFlash(next_flash_byte, (void*) (&length), (size_t) (4));
	next_flash_byte += 4;

	// Now loop until we get jump record, or a halt record
	while ((length != 0) && (length != 0xffffffff)) {
		// Get the next 4 bytes of the boot record, which should be an address
		// record
		CopyFromFlash(next_flash_byte, (void*) (&address), (size_t) (4));
		next_flash_byte += 4;

		// Copy the next "length" bytes to "address"
		CopyFromFlash(next_flash_byte, (void*) (address), (size_t) (length));
		next_flash_byte += length;

		// Get the next 4 bytes of the boot record, which now should be another
		// length record
		CopyFromFlash(next_flash_byte, (void*) (&length), (size_t) (4));
		next_flash_byte += 4;
	}

	// "length" was read as either 0x0 or 0xffffffff, which means we are done
	// copying.
	if (length == 0xffffffff) {
		// We read a HALT record, so return a -1
		return -1;
	} else // length == 0x0
	{
		// We got a jump record, so read the next 4 bytes for the entry address
		CopyFromFlash(next_flash_byte, (void*) (&address), (size_t) (4));
		next_flash_byte += 4;

		// Return the entry point address
		return address;
	}
}

/*****************************************************************************
 *  Function: ValidateFlashImage
 *
 *  Purpose:  This routine validates a flash image based upon three criteria:
 *            1.) It contains the correct flash image signature
 *            2.) A CRC check of the image header
 *            3.) A CRC check of the image data (payload)
 *
 *  Since it's inefficient to read individual bytes from EPCS, and since
 *  we don't really want to expend RAM to buffer the entire image, we compromise
 *  in the case of EPCS, and create a medium-size buffer, who's size is
 *  adjustable by the user.
 *
 *****************************************************************************/
int ValidateFlashImage(alt_u32 image_addr) {
	my_flash_header_type temp_header __attribute__((aligned(4)));

	/*
	 * Again, we don't assume the image is word aligned, so we copy the header
	 * from flash to a word aligned buffer.
	 */
	CopyFromFlash(image_addr, &temp_header, 32);

	// Check the signature first
	if (temp_header.signature == 0xa5a5a5a5) {
		// Signature is good, validate the header crc
		if (temp_header.header_crc != FlashCalcCRC32(image_addr, 28)) {
			// Header crc is not valid
			return HEADER_CRC_INVALID;
		} else {
			// header crc is valid, now validate the data crc
			if (temp_header.data_crc == FlashCalcCRC32(image_addr + 32, temp_header.data_length)) {
				// data crc validates, the image is good
				return CRC_VALID;
			} else {
				// data crc is not valid
				return DATA_CRC_INVALID;
			}
		}
	} else {
		// bad signature, return 1
		return SIGNATURE_INVALID;
	}
}


/*****************************************************************************
 *  Function: ValidateDDRImage
 *
 *  Purpose:  This routine validates a DDR image based upon three criteria:
 *            1.) It contains the correct flash image signature
 *            2.) A CRC check of the image header
 *            3.) A CRC check of the image data (payload)
 *
 *  Since it's inefficient to read individual bytes from EPCS, and since
 *  we don't really want to expend RAM to buffer the entire image, we compromise
 *  in the case of EPCS, and create a medium-size buffer, who's size is
 *  adjustable by the user.
 *
 *****************************************************************************/
int ValidateDDRImage(alt_u32 image_addr) {
	alt_u32 next_flash_byte;
	alt_u32 length;
	alt_u32 address;
	int is_valid;

	/*
	 * Load the image pointer based on the value of "image"
	 * The boot image header is 32 bytes long, so we add an offset of 32.
	 */
	next_flash_byte = image_addr + 32;

	/*
	 * Flash images are not guaranteed to be word-aligned within the flash
	 * memory, so a word-by-word copy loop should not be used.
	 *
	 * The "memcpy()" function works well to copy non-word-aligned data, and
	 * it is relatively small, so that's what we'll use.
	 */

	// Get the first 4 bytes of the boot record, which should be a length record
	CopyFromFlash(next_flash_byte, (void*) (&length), (size_t) (4));
	next_flash_byte += 4;

	is_valid = MEM_FLASH_MATCH;

	// Now loop until we get jump record, or a halt record
	while ((length != 0) && (length != 0xffffffff)) {
		// Get the next 4 bytes of the boot record, which should be an address
		// record
		CopyFromFlash(next_flash_byte, (void*) (&address), (size_t) (4));
		next_flash_byte += 4;

		// Compare the next "length" bytes to what's stored at "address"
		//CopyFromFlash(next_flash_byte, (void*) (address), (size_t) (length));
		if(CompareFromFlash(next_flash_byte, (void*)(address), (size_t)(length)) != 0) {
			is_valid = MEM_FLASH_MISMATCH;
		}
		next_flash_byte += length;

		// Get the next 4 bytes of the boot record, which now should be another
		// length record
		CopyFromFlash(next_flash_byte, (void*) (&length), (size_t) (4));
		next_flash_byte += 4;
	}

	return is_valid;
}

/*****************************************************************************
 *  Function: FlashCalcCRC32
 *
 *  Purpose:  This subroutine calcuates a reflected CRC32 on data located
 *  flash.  The routine buffers flash contents locally in order
 *  to support EPCS flash as well as CFI
 *
 *****************************************************************************/
alt_u32 FlashCalcCRC32(alt_u32 flash_addr, int bytes) {
	alt_u32 crcval = 0xffffffff;
	int i, buf_index, copy_length;
	alt_u8 cval;
	char flash_buffer[FLASH_BUFFER_LENGTH];

	while (bytes != 0) {
		copy_length = (FLASH_BUFFER_LENGTH < bytes) ? FLASH_BUFFER_LENGTH : bytes;
		CopyFromFlash(flash_addr, flash_buffer, copy_length);
		for (buf_index = 0; buf_index < copy_length; buf_index++) {
			cval = flash_buffer[buf_index];
			crcval ^= cval;
			for (i = 8; i > 0; i--) {
				crcval = (crcval & 0x00000001) ? ((crcval >> 1) ^ 0xEDB88320) : (crcval >> 1);
			}
			bytes--;
		}
		flash_addr += FLASH_BUFFER_LENGTH;
	}
	return crcval;
}


void print_uint(unsigned int n) {
    if (n / 10 != 0) { print_uint(n / 10); }
    alt_putchar((n % 10) + '0');
}

void print_int(int n) {
    if (n < 0) {
        alt_putchar('-');
        n = -n;
    }
    print_uint((unsigned int) n);
}
