#ifndef BUILD_NUMBER_STR
#define BUILD_NUMBER_STR "34"
#endif

#ifndef BUILD_NUMBER
#define BUILD_NUMBER 34
#endif

#ifndef BUILD_TIMESTAMP
#define BUILD_TIMESTAMP 1603849925
#endif

#ifndef VERSION_MAJOR
#define VERSION_MAJOR 1
#endif

#ifndef VERSION_MINOR
#define VERSION_MINOR 0
#endif

#ifndef VERSION_STR
#define VERSION_STR "Ver 1.0  Build 34 - Tue Oct 27 18:52:05 PDT 2020"
#endif

#ifndef VERSION_STR_SHORT
#define VERSION_STR_SHORT "1.0.34"
#endif

#ifndef GIT_HASH_STR
#define GIT_HASH_STR "87960d578290588221697eaa7463bf05685d5eda"
#endif

#ifndef GIT_BRANCH_STR
#define GIT_BRANCH_STR "alphag"
#endif

#ifndef GIT_TAG_STR
#define GIT_TAG_STR ""
#endif

#ifndef BUILT_BY_USER_STR
#define BUILT_BY_USER_STR "First Last"
#endif

#ifndef BUILT_BY_EMAIL_STR
#define BUILT_BY_EMAIL_STR "noreply@example.com"
#endif

