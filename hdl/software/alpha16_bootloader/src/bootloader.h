#ifndef BOOTLOADER_H_
#define BOOTLOADER_H_

#include "alt_types.h"

/*
 * Define whether we are booting from flash or from on-chip RAM
 */
// Do not edit these defines
#define BOOT_FROM_CFI_FLASH -1
#define BOOT_CFI_FROM_ONCHIP_ROM -2
#define BOOT_EPCS_FROM_ONCHIP_ROM -3
#define CFI -10
#define EPCS -11

/*
 * Some CRC error codes for readability.
 */
#define CRC_VALID 0
#define SIGNATURE_INVALID 1
#define HEADER_CRC_INVALID 2
#define DATA_CRC_INVALID 3

#define MEM_FLASH_MATCH 0
#define MEM_FLASH_MISMATCH 1

/*
 * Size of buffer for processing flash contents
 */
#define FLASH_BUFFER_LENGTH 256


/*
 * The boot images stored in flash memory, have a specific header
 * attached to them.  This is the structure of that header.  The
 * perl script "make_header.pl", included with this example is
 *  used to add this header to your boot image.
 */
typedef struct {
  alt_u32 signature;
  alt_u32 version;
  alt_u32 timestamp;
  alt_u32 data_length;
  alt_u32 data_crc;
  alt_u32 res1;
  alt_u32 res2;
  alt_u32 header_crc;
} my_flash_header_type;

void* CopyFromFlash( alt_u32 src, void * dest, size_t num );
int CompareFromFlash( alt_u32 src, void * dest, size_t num );
alt_u32 LoadFlashImage (alt_u32 image_addr);
alt_u32 FlashCalcCRC32(alt_u32 flash_addr, int bytes);
int ValidateFlashImage(alt_u32 image_addr);
int ValidateDDRImage(alt_u32 image_addr);
void JumpFromBootCopier(void target(void));
void print_uint(unsigned int n);
void print_int(int n);

#endif /* BOOTLOADER_H_ */
