/*
 * system.h - SOPC Builder system and BSP software package information
 *
 * Machine generated for CPU 'nios2_init' in SOPC Builder design 'management'
 * SOPC Builder design path: ../../management.sopcinfo
 *
 * Generated: Wed Oct 21 19:07:30 PDT 2020
 */

/*
 * DO NOT MODIFY THIS FILE
 *
 * Changing this file will have subtle consequences
 * which will almost certainly lead to a nonfunctioning
 * system. If you do modify this file, be aware that your
 * changes will be overwritten and lost when this file
 * is generated again.
 *
 * DO NOT MODIFY THIS FILE
 */

/*
 * License Agreement
 *
 * Copyright (c) 2008
 * Altera Corporation, San Jose, California, USA.
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * This agreement shall be governed in all respects by the laws of the State
 * of California and by the laws of the United States of America.
 */

#ifndef __SYSTEM_H_
#define __SYSTEM_H_

/* Include definitions from linker script generator */
#include "linker.h"


/*
 * CPU configuration
 *
 */

#define ALT_CPU_ARCHITECTURE "altera_nios2_gen2"
#define ALT_CPU_BIG_ENDIAN 0
#define ALT_CPU_BREAK_ADDR 0x00000020
#define ALT_CPU_CPU_ARCH_NIOS2_R1
#define ALT_CPU_CPU_FREQ 125000000u
#define ALT_CPU_CPU_ID_SIZE 1
#define ALT_CPU_CPU_ID_VALUE 0x00000000
#define ALT_CPU_CPU_IMPLEMENTATION "tiny"
#define ALT_CPU_DATA_ADDR_WIDTH 0xf
#define ALT_CPU_DCACHE_LINE_SIZE 0
#define ALT_CPU_DCACHE_LINE_SIZE_LOG2 0
#define ALT_CPU_DCACHE_SIZE 0
#define ALT_CPU_EXCEPTION_ADDR 0x00000020
#define ALT_CPU_FLASH_ACCELERATOR_LINES 0
#define ALT_CPU_FLASH_ACCELERATOR_LINE_SIZE 0
#define ALT_CPU_FLUSHDA_SUPPORTED
#define ALT_CPU_FREQ 125000000
#define ALT_CPU_HARDWARE_DIVIDE_PRESENT 0
#define ALT_CPU_HARDWARE_MULTIPLY_PRESENT 0
#define ALT_CPU_HARDWARE_MULX_PRESENT 0
#define ALT_CPU_HAS_DEBUG_CORE 0
#define ALT_CPU_HAS_DEBUG_STUB
#define ALT_CPU_HAS_ILLEGAL_INSTRUCTION_EXCEPTION
#define ALT_CPU_HAS_JMPI_INSTRUCTION
#define ALT_CPU_ICACHE_LINE_SIZE 0
#define ALT_CPU_ICACHE_LINE_SIZE_LOG2 0
#define ALT_CPU_ICACHE_SIZE 0
#define ALT_CPU_INST_ADDR_WIDTH 0xe
#define ALT_CPU_NAME "nios2_init"
#define ALT_CPU_OCI_VERSION 1
#define ALT_CPU_RESET_ADDR 0x00000000


/*
 * CPU configuration (with legacy prefix - don't use these anymore)
 *
 */

#define NIOS2_BIG_ENDIAN 0
#define NIOS2_BREAK_ADDR 0x00000020
#define NIOS2_CPU_ARCH_NIOS2_R1
#define NIOS2_CPU_FREQ 125000000u
#define NIOS2_CPU_ID_SIZE 1
#define NIOS2_CPU_ID_VALUE 0x00000000
#define NIOS2_CPU_IMPLEMENTATION "tiny"
#define NIOS2_DATA_ADDR_WIDTH 0xf
#define NIOS2_DCACHE_LINE_SIZE 0
#define NIOS2_DCACHE_LINE_SIZE_LOG2 0
#define NIOS2_DCACHE_SIZE 0
#define NIOS2_EXCEPTION_ADDR 0x00000020
#define NIOS2_FLASH_ACCELERATOR_LINES 0
#define NIOS2_FLASH_ACCELERATOR_LINE_SIZE 0
#define NIOS2_FLUSHDA_SUPPORTED
#define NIOS2_HARDWARE_DIVIDE_PRESENT 0
#define NIOS2_HARDWARE_MULTIPLY_PRESENT 0
#define NIOS2_HARDWARE_MULX_PRESENT 0
#define NIOS2_HAS_DEBUG_CORE 0
#define NIOS2_HAS_DEBUG_STUB
#define NIOS2_HAS_ILLEGAL_INSTRUCTION_EXCEPTION
#define NIOS2_HAS_JMPI_INSTRUCTION
#define NIOS2_ICACHE_LINE_SIZE 0
#define NIOS2_ICACHE_LINE_SIZE_LOG2 0
#define NIOS2_ICACHE_SIZE 0
#define NIOS2_INST_ADDR_WIDTH 0xe
#define NIOS2_OCI_VERSION 1
#define NIOS2_RESET_ADDR 0x00000000


/*
 * Define for each module class mastered by the CPU
 *
 */

#define __ALTERA_AVALON_ONCHIP_MEMORY2
#define __ALTERA_AVALON_PIO
#define __ALTERA_AVALON_SPI
#define __ALTERA_AVALON_SYSID_QSYS
#define __ALTERA_AVALON_TIMER
#define __ALTERA_NIOS2_GEN2


/*
 * System configuration
 *
 */

#define ALT_DEVICE_FAMILY "Arria V"
#define ALT_ENHANCED_INTERRUPT_API_PRESENT
#define ALT_IRQ_BASE NULL
#define ALT_LOG_PORT "/dev/null"
#define ALT_LOG_PORT_BASE 0x0
#define ALT_LOG_PORT_DEV null
#define ALT_LOG_PORT_TYPE ""
#define ALT_NUM_EXTERNAL_INTERRUPT_CONTROLLERS 0
#define ALT_NUM_INTERNAL_INTERRUPT_CONTROLLERS 1
#define ALT_NUM_INTERRUPT_CONTROLLERS 1
#define ALT_STDERR "/dev/null"
#define ALT_STDERR_BASE 0x0
#define ALT_STDERR_DEV null
#define ALT_STDERR_TYPE ""
#define ALT_STDIN "/dev/null"
#define ALT_STDIN_BASE 0x0
#define ALT_STDIN_DEV null
#define ALT_STDIN_TYPE ""
#define ALT_STDOUT "/dev/null"
#define ALT_STDOUT_BASE 0x0
#define ALT_STDOUT_DEV null
#define ALT_STDOUT_TYPE ""
#define ALT_SYSTEM_NAME "management"


/*
 * hal configuration
 *
 */

#define ALT_MAX_FD 4
#define ALT_SYS_CLK SYSTIME_INIT
#define ALT_TIMESTAMP_CLK none


/*
 * pio_init_input configuration
 *
 */

#define ALT_MODULE_CLASS_pio_init_input altera_avalon_pio
#define PIO_INIT_INPUT_BASE 0x4060
#define PIO_INIT_INPUT_BIT_CLEARING_EDGE_REGISTER 0
#define PIO_INIT_INPUT_BIT_MODIFYING_OUTPUT_REGISTER 0
#define PIO_INIT_INPUT_CAPTURE 0
#define PIO_INIT_INPUT_DATA_WIDTH 2
#define PIO_INIT_INPUT_DO_TEST_BENCH_WIRING 1
#define PIO_INIT_INPUT_DRIVEN_SIM_VALUE 0
#define PIO_INIT_INPUT_EDGE_TYPE "NONE"
#define PIO_INIT_INPUT_FREQ 125000000
#define PIO_INIT_INPUT_HAS_IN 1
#define PIO_INIT_INPUT_HAS_OUT 0
#define PIO_INIT_INPUT_HAS_TRI 0
#define PIO_INIT_INPUT_IRQ -1
#define PIO_INIT_INPUT_IRQ_INTERRUPT_CONTROLLER_ID -1
#define PIO_INIT_INPUT_IRQ_TYPE "NONE"
#define PIO_INIT_INPUT_NAME "/dev/pio_init_input"
#define PIO_INIT_INPUT_RESET_VALUE 0
#define PIO_INIT_INPUT_SPAN 16
#define PIO_INIT_INPUT_TYPE "altera_avalon_pio"


/*
 * pio_init_output configuration
 *
 */

#define ALT_MODULE_CLASS_pio_init_output altera_avalon_pio
#define PIO_INIT_OUTPUT_BASE 0x4040
#define PIO_INIT_OUTPUT_BIT_CLEARING_EDGE_REGISTER 0
#define PIO_INIT_OUTPUT_BIT_MODIFYING_OUTPUT_REGISTER 1
#define PIO_INIT_OUTPUT_CAPTURE 0
#define PIO_INIT_OUTPUT_DATA_WIDTH 6
#define PIO_INIT_OUTPUT_DO_TEST_BENCH_WIRING 0
#define PIO_INIT_OUTPUT_DRIVEN_SIM_VALUE 0
#define PIO_INIT_OUTPUT_EDGE_TYPE "NONE"
#define PIO_INIT_OUTPUT_FREQ 125000000
#define PIO_INIT_OUTPUT_HAS_IN 0
#define PIO_INIT_OUTPUT_HAS_OUT 1
#define PIO_INIT_OUTPUT_HAS_TRI 0
#define PIO_INIT_OUTPUT_IRQ -1
#define PIO_INIT_OUTPUT_IRQ_INTERRUPT_CONTROLLER_ID -1
#define PIO_INIT_OUTPUT_IRQ_TYPE "NONE"
#define PIO_INIT_OUTPUT_NAME "/dev/pio_init_output"
#define PIO_INIT_OUTPUT_RESET_VALUE 0
#define PIO_INIT_OUTPUT_SPAN 32
#define PIO_INIT_OUTPUT_TYPE "altera_avalon_pio"


/*
 * ram_nios_init configuration
 *
 */

#define ALT_MODULE_CLASS_ram_nios_init altera_avalon_onchip_memory2
#define RAM_NIOS_INIT_ALLOW_IN_SYSTEM_MEMORY_CONTENT_EDITOR 0
#define RAM_NIOS_INIT_ALLOW_MRAM_SIM_CONTENTS_ONLY_FILE 0
#define RAM_NIOS_INIT_BASE 0x0
#define RAM_NIOS_INIT_CONTENTS_INFO ""
#define RAM_NIOS_INIT_DUAL_PORT 0
#define RAM_NIOS_INIT_GUI_RAM_BLOCK_TYPE "AUTO"
#define RAM_NIOS_INIT_INIT_CONTENTS_FILE "management_ram_nios_init"
#define RAM_NIOS_INIT_INIT_MEM_CONTENT 1
#define RAM_NIOS_INIT_INSTANCE_ID "NONE"
#define RAM_NIOS_INIT_IRQ -1
#define RAM_NIOS_INIT_IRQ_INTERRUPT_CONTROLLER_ID -1
#define RAM_NIOS_INIT_NAME "/dev/ram_nios_init"
#define RAM_NIOS_INIT_NON_DEFAULT_INIT_FILE_ENABLED 0
#define RAM_NIOS_INIT_RAM_BLOCK_TYPE "AUTO"
#define RAM_NIOS_INIT_READ_DURING_WRITE_MODE "DONT_CARE"
#define RAM_NIOS_INIT_SINGLE_CLOCK_OP 0
#define RAM_NIOS_INIT_SIZE_MULTIPLE 1
#define RAM_NIOS_INIT_SIZE_VALUE 16384
#define RAM_NIOS_INIT_SPAN 16384
#define RAM_NIOS_INIT_TYPE "altera_avalon_onchip_memory2"
#define RAM_NIOS_INIT_WRITABLE 1


/*
 * spi_clockcleaner configuration
 *
 */

#define ALT_MODULE_CLASS_spi_clockcleaner altera_avalon_spi
#define SPI_CLOCKCLEANER_BASE 0x4000
#define SPI_CLOCKCLEANER_CLOCKMULT 1
#define SPI_CLOCKCLEANER_CLOCKPHASE 0
#define SPI_CLOCKCLEANER_CLOCKPOLARITY 0
#define SPI_CLOCKCLEANER_CLOCKUNITS "Hz"
#define SPI_CLOCKCLEANER_DATABITS 8
#define SPI_CLOCKCLEANER_DATAWIDTH 16
#define SPI_CLOCKCLEANER_DELAYMULT "1.0E-9"
#define SPI_CLOCKCLEANER_DELAYUNITS "ns"
#define SPI_CLOCKCLEANER_EXTRADELAY 0
#define SPI_CLOCKCLEANER_INSERT_SYNC 0
#define SPI_CLOCKCLEANER_IRQ 0
#define SPI_CLOCKCLEANER_IRQ_INTERRUPT_CONTROLLER_ID 0
#define SPI_CLOCKCLEANER_ISMASTER 1
#define SPI_CLOCKCLEANER_LSBFIRST 0
#define SPI_CLOCKCLEANER_NAME "/dev/spi_clockcleaner"
#define SPI_CLOCKCLEANER_NUMSLAVES 1
#define SPI_CLOCKCLEANER_PREFIX "spi_"
#define SPI_CLOCKCLEANER_SPAN 32
#define SPI_CLOCKCLEANER_SYNC_REG_DEPTH 2
#define SPI_CLOCKCLEANER_TARGETCLOCK 1250000u
#define SPI_CLOCKCLEANER_TARGETSSDELAY "0.0"
#define SPI_CLOCKCLEANER_TYPE "altera_avalon_spi"


/*
 * sysid_init configuration
 *
 */

#define ALT_MODULE_CLASS_sysid_init altera_avalon_sysid_qsys
#define SYSID_INIT_BASE 0x4070
#define SYSID_INIT_ID 1095512374
#define SYSID_INIT_IRQ -1
#define SYSID_INIT_IRQ_INTERRUPT_CONTROLLER_ID -1
#define SYSID_INIT_NAME "/dev/sysid_init"
#define SYSID_INIT_SPAN 8
#define SYSID_INIT_TIMESTAMP 1603214124
#define SYSID_INIT_TYPE "altera_avalon_sysid_qsys"


/*
 * systime_init configuration
 *
 */

#define ALT_MODULE_CLASS_systime_init altera_avalon_timer
#define SYSTIME_INIT_ALWAYS_RUN 0
#define SYSTIME_INIT_BASE 0x4020
#define SYSTIME_INIT_COUNTER_SIZE 32
#define SYSTIME_INIT_FIXED_PERIOD 0
#define SYSTIME_INIT_FREQ 125000000
#define SYSTIME_INIT_IRQ 1
#define SYSTIME_INIT_IRQ_INTERRUPT_CONTROLLER_ID 0
#define SYSTIME_INIT_LOAD_VALUE 124999
#define SYSTIME_INIT_MULT 0.001
#define SYSTIME_INIT_NAME "/dev/systime_init"
#define SYSTIME_INIT_PERIOD 1
#define SYSTIME_INIT_PERIOD_UNITS "ms"
#define SYSTIME_INIT_RESET_OUTPUT 0
#define SYSTIME_INIT_SNAPSHOT 1
#define SYSTIME_INIT_SPAN 32
#define SYSTIME_INIT_TICKS_PER_SEC 1000
#define SYSTIME_INIT_TIMEOUT_PULSE_OUTPUT 0
#define SYSTIME_INIT_TYPE "altera_avalon_timer"

#endif /* __SYSTEM_H_ */
