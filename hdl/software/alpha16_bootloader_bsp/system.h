/*
 * system.h - SOPC Builder system and BSP software package information
 *
 * Machine generated for CPU 'nios2_0' in SOPC Builder design 'management'
 * SOPC Builder design path: ../../management.sopcinfo
 *
 * Generated: Wed Oct 21 19:07:22 PDT 2020
 */

/*
 * DO NOT MODIFY THIS FILE
 *
 * Changing this file will have subtle consequences
 * which will almost certainly lead to a nonfunctioning
 * system. If you do modify this file, be aware that your
 * changes will be overwritten and lost when this file
 * is generated again.
 *
 * DO NOT MODIFY THIS FILE
 */

/*
 * License Agreement
 *
 * Copyright (c) 2008
 * Altera Corporation, San Jose, California, USA.
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * This agreement shall be governed in all respects by the laws of the State
 * of California and by the laws of the United States of America.
 */

#ifndef __SYSTEM_H_
#define __SYSTEM_H_

/* Include definitions from linker script generator */
#include "linker.h"


/*
 * CPU configuration
 *
 */

#define ALT_CPU_ARCHITECTURE "altera_nios2_gen2"
#define ALT_CPU_BIG_ENDIAN 0
#define ALT_CPU_BREAK_ADDR 0x20013820
#define ALT_CPU_CPU_ARCH_NIOS2_R1
#define ALT_CPU_CPU_FREQ 125000000u
#define ALT_CPU_CPU_ID_SIZE 1
#define ALT_CPU_CPU_ID_VALUE 0x00000001
#define ALT_CPU_CPU_IMPLEMENTATION "fast"
#define ALT_CPU_DATA_ADDR_WIDTH 0x1f
#define ALT_CPU_DCACHE_BYPASS_MASK 0x80000000
#define ALT_CPU_DCACHE_LINE_SIZE 32
#define ALT_CPU_DCACHE_LINE_SIZE_LOG2 5
#define ALT_CPU_DCACHE_SIZE 32768
#define ALT_CPU_EXCEPTION_ADDR 0x00000020
#define ALT_CPU_FLASH_ACCELERATOR_LINES 0
#define ALT_CPU_FLASH_ACCELERATOR_LINE_SIZE 0
#define ALT_CPU_FLUSHDA_SUPPORTED
#define ALT_CPU_FREQ 125000000
#define ALT_CPU_HARDWARE_DIVIDE_PRESENT 1
#define ALT_CPU_HARDWARE_MULTIPLY_PRESENT 1
#define ALT_CPU_HARDWARE_MULX_PRESENT 0
#define ALT_CPU_HAS_DEBUG_CORE 1
#define ALT_CPU_HAS_DEBUG_STUB
#define ALT_CPU_HAS_DIVISION_ERROR_EXCEPTION
#define ALT_CPU_HAS_EXTRA_EXCEPTION_INFO
#define ALT_CPU_HAS_ILLEGAL_INSTRUCTION_EXCEPTION
#define ALT_CPU_HAS_JMPI_INSTRUCTION
#define ALT_CPU_ICACHE_LINE_SIZE 32
#define ALT_CPU_ICACHE_LINE_SIZE_LOG2 5
#define ALT_CPU_ICACHE_SIZE 32768
#define ALT_CPU_INITDA_SUPPORTED
#define ALT_CPU_INST_ADDR_WIDTH 0x1e
#define ALT_CPU_NAME "nios2_0"
#define ALT_CPU_NUM_OF_SHADOW_REG_SETS 0
#define ALT_CPU_OCI_VERSION 1
#define ALT_CPU_PERIPHERAL_REGION_BASE 0x40000000
#define ALT_CPU_PERIPHERAL_REGION_PRESENT
#define ALT_CPU_PERIPHERAL_REGION_SIZE 0x40000000
#define ALT_CPU_RESET_ADDR 0x20008000


/*
 * CPU configuration (with legacy prefix - don't use these anymore)
 *
 */

#define NIOS2_BIG_ENDIAN 0
#define NIOS2_BREAK_ADDR 0x20013820
#define NIOS2_CPU_ARCH_NIOS2_R1
#define NIOS2_CPU_FREQ 125000000u
#define NIOS2_CPU_ID_SIZE 1
#define NIOS2_CPU_ID_VALUE 0x00000001
#define NIOS2_CPU_IMPLEMENTATION "fast"
#define NIOS2_DATA_ADDR_WIDTH 0x1f
#define NIOS2_DCACHE_BYPASS_MASK 0x80000000
#define NIOS2_DCACHE_LINE_SIZE 32
#define NIOS2_DCACHE_LINE_SIZE_LOG2 5
#define NIOS2_DCACHE_SIZE 32768
#define NIOS2_EXCEPTION_ADDR 0x00000020
#define NIOS2_FLASH_ACCELERATOR_LINES 0
#define NIOS2_FLASH_ACCELERATOR_LINE_SIZE 0
#define NIOS2_FLUSHDA_SUPPORTED
#define NIOS2_HARDWARE_DIVIDE_PRESENT 1
#define NIOS2_HARDWARE_MULTIPLY_PRESENT 1
#define NIOS2_HARDWARE_MULX_PRESENT 0
#define NIOS2_HAS_DEBUG_CORE 1
#define NIOS2_HAS_DEBUG_STUB
#define NIOS2_HAS_DIVISION_ERROR_EXCEPTION
#define NIOS2_HAS_EXTRA_EXCEPTION_INFO
#define NIOS2_HAS_ILLEGAL_INSTRUCTION_EXCEPTION
#define NIOS2_HAS_JMPI_INSTRUCTION
#define NIOS2_ICACHE_LINE_SIZE 32
#define NIOS2_ICACHE_LINE_SIZE_LOG2 5
#define NIOS2_ICACHE_SIZE 32768
#define NIOS2_INITDA_SUPPORTED
#define NIOS2_INST_ADDR_WIDTH 0x1e
#define NIOS2_NUM_OF_SHADOW_REG_SETS 0
#define NIOS2_OCI_VERSION 1
#define NIOS2_PERIPHERAL_REGION_BASE 0x40000000
#define NIOS2_PERIPHERAL_REGION_PRESENT
#define NIOS2_PERIPHERAL_REGION_SIZE 0x40000000
#define NIOS2_RESET_ADDR 0x20008000


/*
 * Define for each module class mastered by the CPU
 *
 */

#define __AG
#define __ALTERA_AVALON_JTAG_UART
#define __ALTERA_AVALON_ONCHIP_MEMORY2
#define __ALTERA_AVALON_PIO
#define __ALTERA_AVALON_SGDMA
#define __ALTERA_AVALON_SPI
#define __ALTERA_AVALON_SYSID_QSYS
#define __ALTERA_AVALON_TIMER
#define __ALTERA_AVALON_UART
#define __ALTERA_EPCQ_CONTROLLER
#define __ALTERA_ETH_TSE
#define __ALTERA_MEM_IF_DDR3_EMIF
#define __ALTERA_NIOS2_GEN2
#define __ALTERA_REMOTE_UPDATE
#define __ALTERA_UP_SD_CARD_AVALON_INTERFACE
#define __ALT_XCVR_RECONFIG
#define __BOARD
#define __CUSTOM_PATTERN_CHECKER
#define __CUSTOM_PATTERN_GENERATOR
#define __FABRIC_UDP_STREAM
#define __I2C_OPENCORES
#define __ONE_TO_TWO_ST_DEMUX
#define __PRBS_PATTERN_CHECKER
#define __PRBS_PATTERN_GENERATOR
#define __RAM_TEST_CONTROLLER
#define __SIGPROC
#define __TWO_TO_ONE_ST_MUX


/*
 * System configuration
 *
 */

#define ALT_DEVICE_FAMILY "Arria V"
#define ALT_ENHANCED_INTERRUPT_API_PRESENT
#define ALT_IRQ_BASE NULL
#define ALT_LOG_PORT "/dev/null"
#define ALT_LOG_PORT_BASE 0x0
#define ALT_LOG_PORT_DEV null
#define ALT_LOG_PORT_TYPE ""
#define ALT_NUM_EXTERNAL_INTERRUPT_CONTROLLERS 0
#define ALT_NUM_INTERNAL_INTERRUPT_CONTROLLERS 1
#define ALT_NUM_INTERRUPT_CONTROLLERS 1
#define ALT_STDERR "/dev/jtaguart_0"
#define ALT_STDERR_BASE 0x20014b08
#define ALT_STDERR_DEV jtaguart_0
#define ALT_STDERR_IS_JTAG_UART
#define ALT_STDERR_PRESENT
#define ALT_STDERR_TYPE "altera_avalon_jtag_uart"
#define ALT_STDIN "/dev/jtaguart_0"
#define ALT_STDIN_BASE 0x20014b08
#define ALT_STDIN_DEV jtaguart_0
#define ALT_STDIN_IS_JTAG_UART
#define ALT_STDIN_PRESENT
#define ALT_STDIN_TYPE "altera_avalon_jtag_uart"
#define ALT_STDOUT "/dev/jtaguart_0"
#define ALT_STDOUT_BASE 0x20014b08
#define ALT_STDOUT_DEV jtaguart_0
#define ALT_STDOUT_IS_JTAG_UART
#define ALT_STDOUT_PRESENT
#define ALT_STDOUT_TYPE "altera_avalon_jtag_uart"
#define ALT_SYSTEM_NAME "management"


/*
 * adc_data_inserter configuration
 *
 */

#define ADC_DATA_INSERTER_BASE 0x20014a00
#define ADC_DATA_INSERTER_IRQ -1
#define ADC_DATA_INSERTER_IRQ_INTERRUPT_CONTROLLER_ID -1
#define ADC_DATA_INSERTER_NAME "/dev/adc_data_inserter"
#define ADC_DATA_INSERTER_SPAN 64
#define ADC_DATA_INSERTER_TYPE "fabric_udp_stream"
#define ALT_MODULE_CLASS_adc_data_inserter fabric_udp_stream


/*
 * ag_0_control configuration
 *
 */

#define AG_0_CONTROL_BASE 0x72000400
#define AG_0_CONTROL_IRQ -1
#define AG_0_CONTROL_IRQ_INTERRUPT_CONTROLLER_ID -1
#define AG_0_CONTROL_NAME "/dev/ag_0_control"
#define AG_0_CONTROL_SPAN 64
#define AG_0_CONTROL_TYPE "ag"
#define ALT_MODULE_CLASS_ag_0_control ag


/*
 * ag_0_status configuration
 *
 */

#define AG_0_STATUS_BASE 0x72000500
#define AG_0_STATUS_IRQ -1
#define AG_0_STATUS_IRQ_INTERRUPT_CONTROLLER_ID -1
#define AG_0_STATUS_NAME "/dev/ag_0_status"
#define AG_0_STATUS_SPAN 32
#define AG_0_STATUS_TYPE "ag"
#define ALT_MODULE_CLASS_ag_0_status ag


/*
 * alpha_sigproc16_control configuration
 *
 */

#define ALPHA_SIGPROC16_CONTROL_BASE 0x60000200
#define ALPHA_SIGPROC16_CONTROL_IRQ -1
#define ALPHA_SIGPROC16_CONTROL_IRQ_INTERRUPT_CONTROLLER_ID -1
#define ALPHA_SIGPROC16_CONTROL_NAME "/dev/alpha_sigproc16_control"
#define ALPHA_SIGPROC16_CONTROL_SPAN 512
#define ALPHA_SIGPROC16_CONTROL_TYPE "sigproc"
#define ALT_MODULE_CLASS_alpha_sigproc16_control sigproc


/*
 * alpha_sigproc16_status configuration
 *
 */

#define ALPHA_SIGPROC16_STATUS_BASE 0x60000000
#define ALPHA_SIGPROC16_STATUS_IRQ -1
#define ALPHA_SIGPROC16_STATUS_IRQ_INTERRUPT_CONTROLLER_ID -1
#define ALPHA_SIGPROC16_STATUS_NAME "/dev/alpha_sigproc16_status"
#define ALPHA_SIGPROC16_STATUS_SPAN 512
#define ALPHA_SIGPROC16_STATUS_TYPE "sigproc"
#define ALT_MODULE_CLASS_alpha_sigproc16_status sigproc


/*
 * alpha_sigproc_fmc_adc_control configuration
 *
 */

#define ALPHA_SIGPROC_FMC_ADC_CONTROL_BASE 0x40000400
#define ALPHA_SIGPROC_FMC_ADC_CONTROL_IRQ -1
#define ALPHA_SIGPROC_FMC_ADC_CONTROL_IRQ_INTERRUPT_CONTROLLER_ID -1
#define ALPHA_SIGPROC_FMC_ADC_CONTROL_NAME "/dev/alpha_sigproc_fmc_adc_control"
#define ALPHA_SIGPROC_FMC_ADC_CONTROL_SPAN 1024
#define ALPHA_SIGPROC_FMC_ADC_CONTROL_TYPE "sigproc"
#define ALT_MODULE_CLASS_alpha_sigproc_fmc_adc_control sigproc


/*
 * alpha_sigproc_fmc_adc_status configuration
 *
 */

#define ALPHA_SIGPROC_FMC_ADC_STATUS_BASE 0x40000000
#define ALPHA_SIGPROC_FMC_ADC_STATUS_IRQ -1
#define ALPHA_SIGPROC_FMC_ADC_STATUS_IRQ_INTERRUPT_CONTROLLER_ID -1
#define ALPHA_SIGPROC_FMC_ADC_STATUS_NAME "/dev/alpha_sigproc_fmc_adc_status"
#define ALPHA_SIGPROC_FMC_ADC_STATUS_SPAN 1024
#define ALPHA_SIGPROC_FMC_ADC_STATUS_TYPE "sigproc"
#define ALT_MODULE_CLASS_alpha_sigproc_fmc_adc_status sigproc


/*
 * board_control configuration
 *
 */

#define ALT_MODULE_CLASS_board_control board
#define BOARD_CONTROL_BASE 0x72000100
#define BOARD_CONTROL_IRQ -1
#define BOARD_CONTROL_IRQ_INTERRUPT_CONTROLLER_ID -1
#define BOARD_CONTROL_NAME "/dev/board_control"
#define BOARD_CONTROL_SPAN 64
#define BOARD_CONTROL_TYPE "board"


/*
 * board_status configuration
 *
 */

#define ALT_MODULE_CLASS_board_status board
#define BOARD_STATUS_BASE 0x72000000
#define BOARD_STATUS_IRQ -1
#define BOARD_STATUS_IRQ_INTERRUPT_CONTROLLER_ID -1
#define BOARD_STATUS_NAME "/dev/board_status"
#define BOARD_STATUS_SPAN 256
#define BOARD_STATUS_TYPE "board"


/*
 * boot_ram configuration
 *
 */

#define ALT_MODULE_CLASS_boot_ram altera_avalon_onchip_memory2
#define BOOT_RAM_ALLOW_IN_SYSTEM_MEMORY_CONTENT_EDITOR 0
#define BOOT_RAM_ALLOW_MRAM_SIM_CONTENTS_ONLY_FILE 0
#define BOOT_RAM_BASE 0x20008000
#define BOOT_RAM_CONTENTS_INFO ""
#define BOOT_RAM_DUAL_PORT 0
#define BOOT_RAM_GUI_RAM_BLOCK_TYPE "AUTO"
#define BOOT_RAM_INIT_CONTENTS_FILE "management_boot_ram"
#define BOOT_RAM_INIT_MEM_CONTENT 1
#define BOOT_RAM_INSTANCE_ID "NONE"
#define BOOT_RAM_IRQ -1
#define BOOT_RAM_IRQ_INTERRUPT_CONTROLLER_ID -1
#define BOOT_RAM_NAME "/dev/boot_ram"
#define BOOT_RAM_NON_DEFAULT_INIT_FILE_ENABLED 0
#define BOOT_RAM_RAM_BLOCK_TYPE "AUTO"
#define BOOT_RAM_READ_DURING_WRITE_MODE "DONT_CARE"
#define BOOT_RAM_SINGLE_CLOCK_OP 0
#define BOOT_RAM_SIZE_MULTIPLE 1
#define BOOT_RAM_SIZE_VALUE 32768
#define BOOT_RAM_SPAN 32768
#define BOOT_RAM_TYPE "altera_avalon_onchip_memory2"
#define BOOT_RAM_WRITABLE 1


/*
 * descriptor_memory configuration
 *
 */

#define ALT_MODULE_CLASS_descriptor_memory altera_avalon_onchip_memory2
#define DESCRIPTOR_MEMORY_ALLOW_IN_SYSTEM_MEMORY_CONTENT_EDITOR 0
#define DESCRIPTOR_MEMORY_ALLOW_MRAM_SIM_CONTENTS_ONLY_FILE 0
#define DESCRIPTOR_MEMORY_BASE 0x20012000
#define DESCRIPTOR_MEMORY_CONTENTS_INFO ""
#define DESCRIPTOR_MEMORY_DUAL_PORT 1
#define DESCRIPTOR_MEMORY_GUI_RAM_BLOCK_TYPE "AUTO"
#define DESCRIPTOR_MEMORY_INIT_CONTENTS_FILE "management_descriptor_memory"
#define DESCRIPTOR_MEMORY_INIT_MEM_CONTENT 1
#define DESCRIPTOR_MEMORY_INSTANCE_ID "NONE"
#define DESCRIPTOR_MEMORY_IRQ -1
#define DESCRIPTOR_MEMORY_IRQ_INTERRUPT_CONTROLLER_ID -1
#define DESCRIPTOR_MEMORY_NAME "/dev/descriptor_memory"
#define DESCRIPTOR_MEMORY_NON_DEFAULT_INIT_FILE_ENABLED 0
#define DESCRIPTOR_MEMORY_RAM_BLOCK_TYPE "AUTO"
#define DESCRIPTOR_MEMORY_READ_DURING_WRITE_MODE "DONT_CARE"
#define DESCRIPTOR_MEMORY_SINGLE_CLOCK_OP 1
#define DESCRIPTOR_MEMORY_SIZE_MULTIPLE 1
#define DESCRIPTOR_MEMORY_SIZE_VALUE 4096
#define DESCRIPTOR_MEMORY_SPAN 4096
#define DESCRIPTOR_MEMORY_TYPE "altera_avalon_onchip_memory2"
#define DESCRIPTOR_MEMORY_WRITABLE 1


/*
 * epcq_config_avl_csr configuration
 *
 */

#define ALT_MODULE_CLASS_epcq_config_avl_csr altera_epcq_controller
#define EPCQ_CONFIG_AVL_CSR_BASE 0x72000260
#define EPCQ_CONFIG_AVL_CSR_FLASH_TYPE "EPCQ256"
#define EPCQ_CONFIG_AVL_CSR_IRQ 5
#define EPCQ_CONFIG_AVL_CSR_IRQ_INTERRUPT_CONTROLLER_ID 0
#define EPCQ_CONFIG_AVL_CSR_IS_EPCS 0
#define EPCQ_CONFIG_AVL_CSR_NAME "/dev/epcq_config_avl_csr"
#define EPCQ_CONFIG_AVL_CSR_NUMBER_OF_SECTORS 512
#define EPCQ_CONFIG_AVL_CSR_PAGE_SIZE 256
#define EPCQ_CONFIG_AVL_CSR_SECTOR_SIZE 65536
#define EPCQ_CONFIG_AVL_CSR_SPAN 32
#define EPCQ_CONFIG_AVL_CSR_SUBSECTOR_SIZE 4096
#define EPCQ_CONFIG_AVL_CSR_TYPE "altera_epcq_controller"


/*
 * epcq_config_avl_mem configuration
 *
 */

#define ALT_MODULE_CLASS_epcq_config_avl_mem altera_epcq_controller
#define EPCQ_CONFIG_AVL_MEM_BASE 0x70000000
#define EPCQ_CONFIG_AVL_MEM_FLASH_TYPE "EPCQ256"
#define EPCQ_CONFIG_AVL_MEM_IRQ -1
#define EPCQ_CONFIG_AVL_MEM_IRQ_INTERRUPT_CONTROLLER_ID -1
#define EPCQ_CONFIG_AVL_MEM_IS_EPCS 0
#define EPCQ_CONFIG_AVL_MEM_NAME "/dev/epcq_config_avl_mem"
#define EPCQ_CONFIG_AVL_MEM_NUMBER_OF_SECTORS 512
#define EPCQ_CONFIG_AVL_MEM_PAGE_SIZE 256
#define EPCQ_CONFIG_AVL_MEM_SECTOR_SIZE 65536
#define EPCQ_CONFIG_AVL_MEM_SPAN 33554432
#define EPCQ_CONFIG_AVL_MEM_SUBSECTOR_SIZE 4096
#define EPCQ_CONFIG_AVL_MEM_TYPE "altera_epcq_controller"


/*
 * eth_tse configuration
 *
 */

#define ALT_MODULE_CLASS_eth_tse altera_eth_tse
#define ETH_TSE_BASE 0x20014000
#define ETH_TSE_ENABLE_MACLITE 0
#define ETH_TSE_FIFO_WIDTH 32
#define ETH_TSE_IRQ -1
#define ETH_TSE_IRQ_INTERRUPT_CONTROLLER_ID -1
#define ETH_TSE_IS_MULTICHANNEL_MAC 0
#define ETH_TSE_MACLITE_GIGE 0
#define ETH_TSE_MDIO_SHARED 0
#define ETH_TSE_NAME "/dev/eth_tse"
#define ETH_TSE_NUMBER_OF_CHANNEL 1
#define ETH_TSE_NUMBER_OF_MAC_MDIO_SHARED 1
#define ETH_TSE_PCS 1
#define ETH_TSE_PCS_ID 0
#define ETH_TSE_PCS_SGMII 1
#define ETH_TSE_RECEIVE_FIFO_DEPTH 16384
#define ETH_TSE_REGISTER_SHARED 0
#define ETH_TSE_RGMII 0
#define ETH_TSE_SPAN 1024
#define ETH_TSE_TRANSMIT_FIFO_DEPTH 16384
#define ETH_TSE_TYPE "altera_eth_tse"
#define ETH_TSE_USE_MDIO 0


/*
 * hal configuration
 *
 */

#define ALT_MAX_FD 32
#define ALT_SYS_CLK SYSTIME
#define ALT_TIMESTAMP_CLK SYSTIMESTAMP


/*
 * i2c_dac2655 configuration
 *
 */

#define ALT_MODULE_CLASS_i2c_dac2655 i2c_opencores
#define I2C_DAC2655_BASE 0x720002a0
#define I2C_DAC2655_BUS_FREQ 100000u
#define I2C_DAC2655_CLOCK_FREQ 12500000u
#define I2C_DAC2655_IRQ 8
#define I2C_DAC2655_IRQ_INTERRUPT_CONTROLLER_ID 0
#define I2C_DAC2655_NAME "/dev/i2c_dac2655"
#define I2C_DAC2655_SPAN 32
#define I2C_DAC2655_TYPE "i2c_opencores"


/*
 * i2c_disp configuration
 *
 */

#define ALT_MODULE_CLASS_i2c_disp i2c_opencores
#define I2C_DISP_BASE 0x720002e0
#define I2C_DISP_BUS_FREQ 100000u
#define I2C_DISP_CLOCK_FREQ 12500000u
#define I2C_DISP_IRQ 10
#define I2C_DISP_IRQ_INTERRUPT_CONTROLLER_ID 0
#define I2C_DISP_NAME "/dev/i2c_disp"
#define I2C_DISP_SPAN 32
#define I2C_DISP_TYPE "i2c_opencores"


/*
 * i2c_fmc configuration
 *
 */

#define ALT_MODULE_CLASS_i2c_fmc i2c_opencores
#define I2C_FMC_BASE 0x720002c0
#define I2C_FMC_BUS_FREQ 100000u
#define I2C_FMC_CLOCK_FREQ 12500000u
#define I2C_FMC_IRQ 9
#define I2C_FMC_IRQ_INTERRUPT_CONTROLLER_ID 0
#define I2C_FMC_NAME "/dev/i2c_fmc"
#define I2C_FMC_SPAN 32
#define I2C_FMC_TYPE "i2c_opencores"


/*
 * i2c_mac configuration
 *
 */

#define ALT_MODULE_CLASS_i2c_mac i2c_opencores
#define I2C_MAC_BASE 0x72000280
#define I2C_MAC_BUS_FREQ 100000u
#define I2C_MAC_CLOCK_FREQ 12500000u
#define I2C_MAC_IRQ 14
#define I2C_MAC_IRQ_INTERRUPT_CONTROLLER_ID 0
#define I2C_MAC_NAME "/dev/i2c_mac"
#define I2C_MAC_SPAN 32
#define I2C_MAC_TYPE "i2c_opencores"


/*
 * i2c_sfp configuration
 *
 */

#define ALT_MODULE_CLASS_i2c_sfp i2c_opencores
#define I2C_SFP_BASE 0x72000300
#define I2C_SFP_BUS_FREQ 100000u
#define I2C_SFP_CLOCK_FREQ 12500000u
#define I2C_SFP_IRQ 11
#define I2C_SFP_IRQ_INTERRUPT_CONTROLLER_ID 0
#define I2C_SFP_NAME "/dev/i2c_sfp"
#define I2C_SFP_SPAN 32
#define I2C_SFP_TYPE "i2c_opencores"


/*
 * jtaguart_0 configuration
 *
 */

#define ALT_MODULE_CLASS_jtaguart_0 altera_avalon_jtag_uart
#define JTAGUART_0_BASE 0x20014b08
#define JTAGUART_0_IRQ 2
#define JTAGUART_0_IRQ_INTERRUPT_CONTROLLER_ID 0
#define JTAGUART_0_NAME "/dev/jtaguart_0"
#define JTAGUART_0_READ_DEPTH 16384
#define JTAGUART_0_READ_THRESHOLD 8
#define JTAGUART_0_SPAN 8
#define JTAGUART_0_TYPE "altera_avalon_jtag_uart"
#define JTAGUART_0_WRITE_DEPTH 2048
#define JTAGUART_0_WRITE_THRESHOLD 8


/*
 * mem_ddr configuration
 *
 */

#define ALT_MODULE_CLASS_mem_ddr altera_mem_if_ddr3_emif
#define MEM_DDR_BASE 0x0
#define MEM_DDR_IRQ -1
#define MEM_DDR_IRQ_INTERRUPT_CONTROLLER_ID -1
#define MEM_DDR_NAME "/dev/mem_ddr"
#define MEM_DDR_SPAN 536870912
#define MEM_DDR_TYPE "altera_mem_if_ddr3_emif"


/*
 * mem_ddr configuration as viewed by sgdma_rx_m_write
 *
 */

#define SGDMA_RX_M_WRITE_MEM_DDR_BASE 0x0
#define SGDMA_RX_M_WRITE_MEM_DDR_IRQ -1
#define SGDMA_RX_M_WRITE_MEM_DDR_IRQ_INTERRUPT_CONTROLLER_ID -1
#define SGDMA_RX_M_WRITE_MEM_DDR_NAME "/dev/mem_ddr"
#define SGDMA_RX_M_WRITE_MEM_DDR_SPAN 536870912
#define SGDMA_RX_M_WRITE_MEM_DDR_TYPE "altera_mem_if_ddr3_emif"


/*
 * mem_ddr configuration as viewed by sgdma_tx_m_read
 *
 */

#define SGDMA_TX_M_READ_MEM_DDR_BASE 0x0
#define SGDMA_TX_M_READ_MEM_DDR_IRQ -1
#define SGDMA_TX_M_READ_MEM_DDR_IRQ_INTERRUPT_CONTROLLER_ID -1
#define SGDMA_TX_M_READ_MEM_DDR_NAME "/dev/mem_ddr"
#define SGDMA_TX_M_READ_MEM_DDR_SPAN 536870912
#define SGDMA_TX_M_READ_MEM_DDR_TYPE "altera_mem_if_ddr3_emif"


/*
 * memory_checker_pattern_checker_subsystem_custom_pattern_checker_csr configuration
 *
 */

#define ALT_MODULE_CLASS_memory_checker_pattern_checker_subsystem_custom_pattern_checker_csr custom_pattern_checker
#define MEMORY_CHECKER_PATTERN_CHECKER_SUBSYSTEM_CUSTOM_PATTERN_CHECKER_CSR_BASE 0x20011420
#define MEMORY_CHECKER_PATTERN_CHECKER_SUBSYSTEM_CUSTOM_PATTERN_CHECKER_CSR_IRQ -1
#define MEMORY_CHECKER_PATTERN_CHECKER_SUBSYSTEM_CUSTOM_PATTERN_CHECKER_CSR_IRQ_INTERRUPT_CONTROLLER_ID -1
#define MEMORY_CHECKER_PATTERN_CHECKER_SUBSYSTEM_CUSTOM_PATTERN_CHECKER_CSR_NAME "/dev/memory_checker_pattern_checker_subsystem_custom_pattern_checker_csr"
#define MEMORY_CHECKER_PATTERN_CHECKER_SUBSYSTEM_CUSTOM_PATTERN_CHECKER_CSR_SPAN 16
#define MEMORY_CHECKER_PATTERN_CHECKER_SUBSYSTEM_CUSTOM_PATTERN_CHECKER_CSR_TYPE "custom_pattern_checker"


/*
 * memory_checker_pattern_checker_subsystem_custom_pattern_checker_pattern_access configuration
 *
 */

#define ALT_MODULE_CLASS_memory_checker_pattern_checker_subsystem_custom_pattern_checker_pattern_access custom_pattern_checker
#define MEMORY_CHECKER_PATTERN_CHECKER_SUBSYSTEM_CUSTOM_PATTERN_CHECKER_PATTERN_ACCESS_BASE 0x20011000
#define MEMORY_CHECKER_PATTERN_CHECKER_SUBSYSTEM_CUSTOM_PATTERN_CHECKER_PATTERN_ACCESS_IRQ -1
#define MEMORY_CHECKER_PATTERN_CHECKER_SUBSYSTEM_CUSTOM_PATTERN_CHECKER_PATTERN_ACCESS_IRQ_INTERRUPT_CONTROLLER_ID -1
#define MEMORY_CHECKER_PATTERN_CHECKER_SUBSYSTEM_CUSTOM_PATTERN_CHECKER_PATTERN_ACCESS_NAME "/dev/memory_checker_pattern_checker_subsystem_custom_pattern_checker_pattern_access"
#define MEMORY_CHECKER_PATTERN_CHECKER_SUBSYSTEM_CUSTOM_PATTERN_CHECKER_PATTERN_ACCESS_SPAN 1024
#define MEMORY_CHECKER_PATTERN_CHECKER_SUBSYSTEM_CUSTOM_PATTERN_CHECKER_PATTERN_ACCESS_TYPE "custom_pattern_checker"


/*
 * memory_checker_pattern_checker_subsystem_one_to_two_st_demux configuration
 *
 */

#define ALT_MODULE_CLASS_memory_checker_pattern_checker_subsystem_one_to_two_st_demux one_to_two_st_demux
#define MEMORY_CHECKER_PATTERN_CHECKER_SUBSYSTEM_ONE_TO_TWO_ST_DEMUX_BASE 0x20011400
#define MEMORY_CHECKER_PATTERN_CHECKER_SUBSYSTEM_ONE_TO_TWO_ST_DEMUX_IRQ -1
#define MEMORY_CHECKER_PATTERN_CHECKER_SUBSYSTEM_ONE_TO_TWO_ST_DEMUX_IRQ_INTERRUPT_CONTROLLER_ID -1
#define MEMORY_CHECKER_PATTERN_CHECKER_SUBSYSTEM_ONE_TO_TWO_ST_DEMUX_NAME "/dev/memory_checker_pattern_checker_subsystem_one_to_two_st_demux"
#define MEMORY_CHECKER_PATTERN_CHECKER_SUBSYSTEM_ONE_TO_TWO_ST_DEMUX_SPAN 8
#define MEMORY_CHECKER_PATTERN_CHECKER_SUBSYSTEM_ONE_TO_TWO_ST_DEMUX_TYPE "one_to_two_st_demux"


/*
 * memory_checker_pattern_checker_subsystem_prbs_pattern_checker configuration
 *
 */

#define ALT_MODULE_CLASS_memory_checker_pattern_checker_subsystem_prbs_pattern_checker prbs_pattern_checker
#define MEMORY_CHECKER_PATTERN_CHECKER_SUBSYSTEM_PRBS_PATTERN_CHECKER_BASE 0x20011440
#define MEMORY_CHECKER_PATTERN_CHECKER_SUBSYSTEM_PRBS_PATTERN_CHECKER_IRQ -1
#define MEMORY_CHECKER_PATTERN_CHECKER_SUBSYSTEM_PRBS_PATTERN_CHECKER_IRQ_INTERRUPT_CONTROLLER_ID -1
#define MEMORY_CHECKER_PATTERN_CHECKER_SUBSYSTEM_PRBS_PATTERN_CHECKER_NAME "/dev/memory_checker_pattern_checker_subsystem_prbs_pattern_checker"
#define MEMORY_CHECKER_PATTERN_CHECKER_SUBSYSTEM_PRBS_PATTERN_CHECKER_SPAN 32
#define MEMORY_CHECKER_PATTERN_CHECKER_SUBSYSTEM_PRBS_PATTERN_CHECKER_TYPE "prbs_pattern_checker"


/*
 * memory_checker_pattern_generator_subsystem_custom_pattern_generator_csr configuration
 *
 */

#define ALT_MODULE_CLASS_memory_checker_pattern_generator_subsystem_custom_pattern_generator_csr custom_pattern_generator
#define MEMORY_CHECKER_PATTERN_GENERATOR_SUBSYSTEM_CUSTOM_PATTERN_GENERATOR_CSR_BASE 0x20010400
#define MEMORY_CHECKER_PATTERN_GENERATOR_SUBSYSTEM_CUSTOM_PATTERN_GENERATOR_CSR_IRQ -1
#define MEMORY_CHECKER_PATTERN_GENERATOR_SUBSYSTEM_CUSTOM_PATTERN_GENERATOR_CSR_IRQ_INTERRUPT_CONTROLLER_ID -1
#define MEMORY_CHECKER_PATTERN_GENERATOR_SUBSYSTEM_CUSTOM_PATTERN_GENERATOR_CSR_NAME "/dev/memory_checker_pattern_generator_subsystem_custom_pattern_generator_csr"
#define MEMORY_CHECKER_PATTERN_GENERATOR_SUBSYSTEM_CUSTOM_PATTERN_GENERATOR_CSR_SPAN 16
#define MEMORY_CHECKER_PATTERN_GENERATOR_SUBSYSTEM_CUSTOM_PATTERN_GENERATOR_CSR_TYPE "custom_pattern_generator"


/*
 * memory_checker_pattern_generator_subsystem_custom_pattern_generator_pattern_access configuration
 *
 */

#define ALT_MODULE_CLASS_memory_checker_pattern_generator_subsystem_custom_pattern_generator_pattern_access custom_pattern_generator
#define MEMORY_CHECKER_PATTERN_GENERATOR_SUBSYSTEM_CUSTOM_PATTERN_GENERATOR_PATTERN_ACCESS_BASE 0x20010000
#define MEMORY_CHECKER_PATTERN_GENERATOR_SUBSYSTEM_CUSTOM_PATTERN_GENERATOR_PATTERN_ACCESS_IRQ -1
#define MEMORY_CHECKER_PATTERN_GENERATOR_SUBSYSTEM_CUSTOM_PATTERN_GENERATOR_PATTERN_ACCESS_IRQ_INTERRUPT_CONTROLLER_ID -1
#define MEMORY_CHECKER_PATTERN_GENERATOR_SUBSYSTEM_CUSTOM_PATTERN_GENERATOR_PATTERN_ACCESS_NAME "/dev/memory_checker_pattern_generator_subsystem_custom_pattern_generator_pattern_access"
#define MEMORY_CHECKER_PATTERN_GENERATOR_SUBSYSTEM_CUSTOM_PATTERN_GENERATOR_PATTERN_ACCESS_SPAN 1024
#define MEMORY_CHECKER_PATTERN_GENERATOR_SUBSYSTEM_CUSTOM_PATTERN_GENERATOR_PATTERN_ACCESS_TYPE "custom_pattern_generator"


/*
 * memory_checker_pattern_generator_subsystem_prbs_pattern_generator configuration
 *
 */

#define ALT_MODULE_CLASS_memory_checker_pattern_generator_subsystem_prbs_pattern_generator prbs_pattern_generator
#define MEMORY_CHECKER_PATTERN_GENERATOR_SUBSYSTEM_PRBS_PATTERN_GENERATOR_BASE 0x20010420
#define MEMORY_CHECKER_PATTERN_GENERATOR_SUBSYSTEM_PRBS_PATTERN_GENERATOR_IRQ -1
#define MEMORY_CHECKER_PATTERN_GENERATOR_SUBSYSTEM_PRBS_PATTERN_GENERATOR_IRQ_INTERRUPT_CONTROLLER_ID -1
#define MEMORY_CHECKER_PATTERN_GENERATOR_SUBSYSTEM_PRBS_PATTERN_GENERATOR_NAME "/dev/memory_checker_pattern_generator_subsystem_prbs_pattern_generator"
#define MEMORY_CHECKER_PATTERN_GENERATOR_SUBSYSTEM_PRBS_PATTERN_GENERATOR_SPAN 32
#define MEMORY_CHECKER_PATTERN_GENERATOR_SUBSYSTEM_PRBS_PATTERN_GENERATOR_TYPE "prbs_pattern_generator"


/*
 * memory_checker_pattern_generator_subsystem_two_to_one_st_mux configuration
 *
 */

#define ALT_MODULE_CLASS_memory_checker_pattern_generator_subsystem_two_to_one_st_mux two_to_one_st_mux
#define MEMORY_CHECKER_PATTERN_GENERATOR_SUBSYSTEM_TWO_TO_ONE_ST_MUX_BASE 0x20010440
#define MEMORY_CHECKER_PATTERN_GENERATOR_SUBSYSTEM_TWO_TO_ONE_ST_MUX_IRQ -1
#define MEMORY_CHECKER_PATTERN_GENERATOR_SUBSYSTEM_TWO_TO_ONE_ST_MUX_IRQ_INTERRUPT_CONTROLLER_ID -1
#define MEMORY_CHECKER_PATTERN_GENERATOR_SUBSYSTEM_TWO_TO_ONE_ST_MUX_NAME "/dev/memory_checker_pattern_generator_subsystem_two_to_one_st_mux"
#define MEMORY_CHECKER_PATTERN_GENERATOR_SUBSYSTEM_TWO_TO_ONE_ST_MUX_SPAN 8
#define MEMORY_CHECKER_PATTERN_GENERATOR_SUBSYSTEM_TWO_TO_ONE_ST_MUX_TYPE "two_to_one_st_mux"


/*
 * memory_checker_ram_test_controller configuration
 *
 */

#define ALT_MODULE_CLASS_memory_checker_ram_test_controller ram_test_controller
#define MEMORY_CHECKER_RAM_TEST_CONTROLLER_BASE 0x20010800
#define MEMORY_CHECKER_RAM_TEST_CONTROLLER_CLOCK_FREQUENCY_IN_HZ 125000000
#define MEMORY_CHECKER_RAM_TEST_CONTROLLER_DEFAULT_BLOCK_SIZE 1024
#define MEMORY_CHECKER_RAM_TEST_CONTROLLER_DEFAULT_TIMER_RESOLUTION 10
#define MEMORY_CHECKER_RAM_TEST_CONTROLLER_DEFAULT_TRAIL_DISTANCE 1
#define MEMORY_CHECKER_RAM_TEST_CONTROLLER_IRQ -1
#define MEMORY_CHECKER_RAM_TEST_CONTROLLER_IRQ_INTERRUPT_CONTROLLER_ID -1
#define MEMORY_CHECKER_RAM_TEST_CONTROLLER_NAME "/dev/memory_checker_ram_test_controller"
#define MEMORY_CHECKER_RAM_TEST_CONTROLLER_SPAN 32
#define MEMORY_CHECKER_RAM_TEST_CONTROLLER_TYPE "ram_test_controller"


/*
 * pio_adc_out configuration
 *
 */

#define ALT_MODULE_CLASS_pio_adc_out altera_avalon_pio
#define PIO_ADC_OUT_BASE 0x72000220
#define PIO_ADC_OUT_BIT_CLEARING_EDGE_REGISTER 0
#define PIO_ADC_OUT_BIT_MODIFYING_OUTPUT_REGISTER 1
#define PIO_ADC_OUT_CAPTURE 0
#define PIO_ADC_OUT_DATA_WIDTH 1
#define PIO_ADC_OUT_DO_TEST_BENCH_WIRING 0
#define PIO_ADC_OUT_DRIVEN_SIM_VALUE 0
#define PIO_ADC_OUT_EDGE_TYPE "NONE"
#define PIO_ADC_OUT_FREQ 12500000
#define PIO_ADC_OUT_HAS_IN 0
#define PIO_ADC_OUT_HAS_OUT 1
#define PIO_ADC_OUT_HAS_TRI 0
#define PIO_ADC_OUT_IRQ -1
#define PIO_ADC_OUT_IRQ_INTERRUPT_CONTROLLER_ID -1
#define PIO_ADC_OUT_IRQ_TYPE "NONE"
#define PIO_ADC_OUT_NAME "/dev/pio_adc_out"
#define PIO_ADC_OUT_RESET_VALUE 0
#define PIO_ADC_OUT_SPAN 32
#define PIO_ADC_OUT_TYPE "altera_avalon_pio"


/*
 * pio_fmc_adc_out configuration
 *
 */

#define ALT_MODULE_CLASS_pio_fmc_adc_out altera_avalon_pio
#define PIO_FMC_ADC_OUT_BASE 0x720001e0
#define PIO_FMC_ADC_OUT_BIT_CLEARING_EDGE_REGISTER 0
#define PIO_FMC_ADC_OUT_BIT_MODIFYING_OUTPUT_REGISTER 1
#define PIO_FMC_ADC_OUT_CAPTURE 0
#define PIO_FMC_ADC_OUT_DATA_WIDTH 1
#define PIO_FMC_ADC_OUT_DO_TEST_BENCH_WIRING 0
#define PIO_FMC_ADC_OUT_DRIVEN_SIM_VALUE 0
#define PIO_FMC_ADC_OUT_EDGE_TYPE "NONE"
#define PIO_FMC_ADC_OUT_FREQ 12500000
#define PIO_FMC_ADC_OUT_HAS_IN 0
#define PIO_FMC_ADC_OUT_HAS_OUT 1
#define PIO_FMC_ADC_OUT_HAS_TRI 0
#define PIO_FMC_ADC_OUT_IRQ -1
#define PIO_FMC_ADC_OUT_IRQ_INTERRUPT_CONTROLLER_ID -1
#define PIO_FMC_ADC_OUT_IRQ_TYPE "NONE"
#define PIO_FMC_ADC_OUT_NAME "/dev/pio_fmc_adc_out"
#define PIO_FMC_ADC_OUT_RESET_VALUE 0
#define PIO_FMC_ADC_OUT_SPAN 32
#define PIO_FMC_ADC_OUT_TYPE "altera_avalon_pio"


/*
 * pio_fpLED configuration
 *
 */

#define ALT_MODULE_CLASS_pio_fpLED altera_avalon_pio
#define PIO_FPLED_BASE 0x72000320
#define PIO_FPLED_BIT_CLEARING_EDGE_REGISTER 0
#define PIO_FPLED_BIT_MODIFYING_OUTPUT_REGISTER 0
#define PIO_FPLED_CAPTURE 0
#define PIO_FPLED_DATA_WIDTH 1
#define PIO_FPLED_DO_TEST_BENCH_WIRING 0
#define PIO_FPLED_DRIVEN_SIM_VALUE 0
#define PIO_FPLED_EDGE_TYPE "NONE"
#define PIO_FPLED_FREQ 12500000
#define PIO_FPLED_HAS_IN 0
#define PIO_FPLED_HAS_OUT 1
#define PIO_FPLED_HAS_TRI 0
#define PIO_FPLED_IRQ -1
#define PIO_FPLED_IRQ_INTERRUPT_CONTROLLER_ID -1
#define PIO_FPLED_IRQ_TYPE "NONE"
#define PIO_FPLED_NAME "/dev/pio_fpLED"
#define PIO_FPLED_RESET_VALUE 0
#define PIO_FPLED_SPAN 16
#define PIO_FPLED_TYPE "altera_avalon_pio"


/*
 * remote_update configuration
 *
 */

#define ALT_MODULE_CLASS_remote_update altera_remote_update
#define REMOTE_UPDATE_BASE 0x72000240
#define REMOTE_UPDATE_IRQ -1
#define REMOTE_UPDATE_IRQ_INTERRUPT_CONTROLLER_ID -1
#define REMOTE_UPDATE_NAME "/dev/remote_update"
#define REMOTE_UPDATE_SPAN 32
#define REMOTE_UPDATE_TYPE "altera_remote_update"


/*
 * sdcard configuration
 *
 */

#define ALT_MODULE_CLASS_sdcard Altera_UP_SD_Card_Avalon_Interface
#define SDCARD_BASE 0x20014400
#define SDCARD_IRQ -1
#define SDCARD_IRQ_INTERRUPT_CONTROLLER_ID -1
#define SDCARD_NAME "/dev/sdcard"
#define SDCARD_SPAN 1024
#define SDCARD_TYPE "Altera_UP_SD_Card_Avalon_Interface"


/*
 * sgdma_rx configuration
 *
 */

#define ALT_MODULE_CLASS_sgdma_rx altera_avalon_sgdma
#define SGDMA_RX_ADDRESS_WIDTH 32
#define SGDMA_RX_ALWAYS_DO_MAX_BURST 1
#define SGDMA_RX_ATLANTIC_CHANNEL_DATA_WIDTH 4
#define SGDMA_RX_AVALON_MM_BYTE_REORDER_MODE 0
#define SGDMA_RX_BASE 0x20014a40
#define SGDMA_RX_BURST_DATA_WIDTH 8
#define SGDMA_RX_BURST_TRANSFER 0
#define SGDMA_RX_BYTES_TO_TRANSFER_DATA_WIDTH 16
#define SGDMA_RX_CHAIN_WRITEBACK_DATA_WIDTH 32
#define SGDMA_RX_COMMAND_FIFO_DATA_WIDTH 104
#define SGDMA_RX_CONTROL_DATA_WIDTH 8
#define SGDMA_RX_CONTROL_SLAVE_ADDRESS_WIDTH 0x4
#define SGDMA_RX_CONTROL_SLAVE_DATA_WIDTH 32
#define SGDMA_RX_DESCRIPTOR_READ_BURST 0
#define SGDMA_RX_DESC_DATA_WIDTH 32
#define SGDMA_RX_HAS_READ_BLOCK 0
#define SGDMA_RX_HAS_WRITE_BLOCK 1
#define SGDMA_RX_IN_ERROR_WIDTH 6
#define SGDMA_RX_IRQ 3
#define SGDMA_RX_IRQ_INTERRUPT_CONTROLLER_ID 0
#define SGDMA_RX_NAME "/dev/sgdma_rx"
#define SGDMA_RX_OUT_ERROR_WIDTH 0
#define SGDMA_RX_READ_BLOCK_DATA_WIDTH 32
#define SGDMA_RX_READ_BURSTCOUNT_WIDTH 4
#define SGDMA_RX_SPAN 64
#define SGDMA_RX_STATUS_TOKEN_DATA_WIDTH 24
#define SGDMA_RX_STREAM_DATA_WIDTH 32
#define SGDMA_RX_SYMBOLS_PER_BEAT 4
#define SGDMA_RX_TYPE "altera_avalon_sgdma"
#define SGDMA_RX_UNALIGNED_TRANSFER 0
#define SGDMA_RX_WRITE_BLOCK_DATA_WIDTH 32
#define SGDMA_RX_WRITE_BURSTCOUNT_WIDTH 4


/*
 * sgdma_tx configuration
 *
 */

#define ALT_MODULE_CLASS_sgdma_tx altera_avalon_sgdma
#define SGDMA_TX_ADDRESS_WIDTH 32
#define SGDMA_TX_ALWAYS_DO_MAX_BURST 1
#define SGDMA_TX_ATLANTIC_CHANNEL_DATA_WIDTH 4
#define SGDMA_TX_AVALON_MM_BYTE_REORDER_MODE 0
#define SGDMA_TX_BASE 0x20014a80
#define SGDMA_TX_BURST_DATA_WIDTH 8
#define SGDMA_TX_BURST_TRANSFER 0
#define SGDMA_TX_BYTES_TO_TRANSFER_DATA_WIDTH 16
#define SGDMA_TX_CHAIN_WRITEBACK_DATA_WIDTH 32
#define SGDMA_TX_COMMAND_FIFO_DATA_WIDTH 104
#define SGDMA_TX_CONTROL_DATA_WIDTH 8
#define SGDMA_TX_CONTROL_SLAVE_ADDRESS_WIDTH 0x4
#define SGDMA_TX_CONTROL_SLAVE_DATA_WIDTH 32
#define SGDMA_TX_DESCRIPTOR_READ_BURST 0
#define SGDMA_TX_DESC_DATA_WIDTH 32
#define SGDMA_TX_HAS_READ_BLOCK 1
#define SGDMA_TX_HAS_WRITE_BLOCK 0
#define SGDMA_TX_IN_ERROR_WIDTH 0
#define SGDMA_TX_IRQ 4
#define SGDMA_TX_IRQ_INTERRUPT_CONTROLLER_ID 0
#define SGDMA_TX_NAME "/dev/sgdma_tx"
#define SGDMA_TX_OUT_ERROR_WIDTH 1
#define SGDMA_TX_READ_BLOCK_DATA_WIDTH 32
#define SGDMA_TX_READ_BURSTCOUNT_WIDTH 4
#define SGDMA_TX_SPAN 64
#define SGDMA_TX_STATUS_TOKEN_DATA_WIDTH 24
#define SGDMA_TX_STREAM_DATA_WIDTH 32
#define SGDMA_TX_SYMBOLS_PER_BEAT 4
#define SGDMA_TX_TYPE "altera_avalon_sgdma"
#define SGDMA_TX_UNALIGNED_TRANSFER 0
#define SGDMA_TX_WRITE_BLOCK_DATA_WIDTH 32
#define SGDMA_TX_WRITE_BURSTCOUNT_WIDTH 4


/*
 * spi_adc configuration
 *
 */

#define ALT_MODULE_CLASS_spi_adc altera_avalon_spi
#define SPI_ADC_BASE 0x720001c0
#define SPI_ADC_CLOCKMULT 1
#define SPI_ADC_CLOCKPHASE 0
#define SPI_ADC_CLOCKPOLARITY 0
#define SPI_ADC_CLOCKUNITS "Hz"
#define SPI_ADC_DATABITS 8
#define SPI_ADC_DATAWIDTH 16
#define SPI_ADC_DELAYMULT "1.0E-9"
#define SPI_ADC_DELAYUNITS "ns"
#define SPI_ADC_EXTRADELAY 0
#define SPI_ADC_INSERT_SYNC 0
#define SPI_ADC_IRQ 7
#define SPI_ADC_IRQ_INTERRUPT_CONTROLLER_ID 0
#define SPI_ADC_ISMASTER 1
#define SPI_ADC_LSBFIRST 0
#define SPI_ADC_NAME "/dev/spi_adc"
#define SPI_ADC_NUMSLAVES 5
#define SPI_ADC_PREFIX "spi_"
#define SPI_ADC_SPAN 32
#define SPI_ADC_SYNC_REG_DEPTH 2
#define SPI_ADC_TARGETCLOCK 1250000u
#define SPI_ADC_TARGETSSDELAY "0.0"
#define SPI_ADC_TYPE "altera_avalon_spi"


/*
 * spi_fmc_adc configuration
 *
 */

#define ALT_MODULE_CLASS_spi_fmc_adc altera_avalon_spi
#define SPI_FMC_ADC_BASE 0x72000180
#define SPI_FMC_ADC_CLOCKMULT 1
#define SPI_FMC_ADC_CLOCKPHASE 0
#define SPI_FMC_ADC_CLOCKPOLARITY 0
#define SPI_FMC_ADC_CLOCKUNITS "Hz"
#define SPI_FMC_ADC_DATABITS 8
#define SPI_FMC_ADC_DATAWIDTH 16
#define SPI_FMC_ADC_DELAYMULT "1.0E-9"
#define SPI_FMC_ADC_DELAYUNITS "ns"
#define SPI_FMC_ADC_EXTRADELAY 0
#define SPI_FMC_ADC_INSERT_SYNC 0
#define SPI_FMC_ADC_IRQ 15
#define SPI_FMC_ADC_IRQ_INTERRUPT_CONTROLLER_ID 0
#define SPI_FMC_ADC_ISMASTER 1
#define SPI_FMC_ADC_LSBFIRST 0
#define SPI_FMC_ADC_NAME "/dev/spi_fmc_adc"
#define SPI_FMC_ADC_NUMSLAVES 5
#define SPI_FMC_ADC_PREFIX "spi_"
#define SPI_FMC_ADC_SPAN 32
#define SPI_FMC_ADC_SYNC_REG_DEPTH 2
#define SPI_FMC_ADC_TARGETCLOCK 1250000u
#define SPI_FMC_ADC_TARGETSSDELAY "0.0"
#define SPI_FMC_ADC_TYPE "altera_avalon_spi"


/*
 * spi_fmc_dac2 configuration
 *
 */

#define ALT_MODULE_CLASS_spi_fmc_dac2 altera_avalon_spi
#define SPI_FMC_DAC2_BASE 0x72000140
#define SPI_FMC_DAC2_CLOCKMULT 1
#define SPI_FMC_DAC2_CLOCKPHASE 0
#define SPI_FMC_DAC2_CLOCKPOLARITY 0
#define SPI_FMC_DAC2_CLOCKUNITS "Hz"
#define SPI_FMC_DAC2_DATABITS 8
#define SPI_FMC_DAC2_DATAWIDTH 16
#define SPI_FMC_DAC2_DELAYMULT "1.0E-9"
#define SPI_FMC_DAC2_DELAYUNITS "ns"
#define SPI_FMC_DAC2_EXTRADELAY 0
#define SPI_FMC_DAC2_INSERT_SYNC 0
#define SPI_FMC_DAC2_IRQ 17
#define SPI_FMC_DAC2_IRQ_INTERRUPT_CONTROLLER_ID 0
#define SPI_FMC_DAC2_ISMASTER 1
#define SPI_FMC_DAC2_LSBFIRST 0
#define SPI_FMC_DAC2_NAME "/dev/spi_fmc_dac2"
#define SPI_FMC_DAC2_NUMSLAVES 2
#define SPI_FMC_DAC2_PREFIX "spi_"
#define SPI_FMC_DAC2_SPAN 32
#define SPI_FMC_DAC2_SYNC_REG_DEPTH 2
#define SPI_FMC_DAC2_TARGETCLOCK 1250000u
#define SPI_FMC_DAC2_TARGETSSDELAY "0.0"
#define SPI_FMC_DAC2_TYPE "altera_avalon_spi"


/*
 * spi_lmk48xx configuration
 *
 */

#define ALT_MODULE_CLASS_spi_lmk48xx altera_avalon_spi
#define SPI_LMK48XX_BASE 0x72000160
#define SPI_LMK48XX_CLOCKMULT 1
#define SPI_LMK48XX_CLOCKPHASE 0
#define SPI_LMK48XX_CLOCKPOLARITY 0
#define SPI_LMK48XX_CLOCKUNITS "Hz"
#define SPI_LMK48XX_DATABITS 8
#define SPI_LMK48XX_DATAWIDTH 16
#define SPI_LMK48XX_DELAYMULT "1.0E-9"
#define SPI_LMK48XX_DELAYUNITS "ns"
#define SPI_LMK48XX_EXTRADELAY 0
#define SPI_LMK48XX_INSERT_SYNC 0
#define SPI_LMK48XX_IRQ 6
#define SPI_LMK48XX_IRQ_INTERRUPT_CONTROLLER_ID 0
#define SPI_LMK48XX_ISMASTER 1
#define SPI_LMK48XX_LSBFIRST 0
#define SPI_LMK48XX_NAME "/dev/spi_lmk48xx"
#define SPI_LMK48XX_NUMSLAVES 1
#define SPI_LMK48XX_PREFIX "spi_"
#define SPI_LMK48XX_SPAN 32
#define SPI_LMK48XX_SYNC_REG_DEPTH 2
#define SPI_LMK48XX_TARGETCLOCK 1250000u
#define SPI_LMK48XX_TARGETSSDELAY "0.0"
#define SPI_LMK48XX_TYPE "altera_avalon_spi"


/*
 * spi_temp configuration
 *
 */

#define ALT_MODULE_CLASS_spi_temp altera_avalon_spi
#define SPI_TEMP_BASE 0x720001a0
#define SPI_TEMP_CLOCKMULT 1
#define SPI_TEMP_CLOCKPHASE 0
#define SPI_TEMP_CLOCKPOLARITY 0
#define SPI_TEMP_CLOCKUNITS "Hz"
#define SPI_TEMP_DATABITS 8
#define SPI_TEMP_DATAWIDTH 16
#define SPI_TEMP_DELAYMULT "1.0E-9"
#define SPI_TEMP_DELAYUNITS "ns"
#define SPI_TEMP_EXTRADELAY 0
#define SPI_TEMP_INSERT_SYNC 0
#define SPI_TEMP_IRQ 12
#define SPI_TEMP_IRQ_INTERRUPT_CONTROLLER_ID 0
#define SPI_TEMP_ISMASTER 1
#define SPI_TEMP_LSBFIRST 0
#define SPI_TEMP_NAME "/dev/spi_temp"
#define SPI_TEMP_NUMSLAVES 1
#define SPI_TEMP_PREFIX "spi_"
#define SPI_TEMP_SPAN 32
#define SPI_TEMP_SYNC_REG_DEPTH 2
#define SPI_TEMP_TARGETCLOCK 1250000u
#define SPI_TEMP_TARGETSSDELAY "0.0"
#define SPI_TEMP_TYPE "altera_avalon_spi"


/*
 * sysid configuration
 *
 */

#define ALT_MODULE_CLASS_sysid altera_avalon_sysid_qsys
#define SYSID_BASE 0x20014b00
#define SYSID_ID 1095512374
#define SYSID_IRQ -1
#define SYSID_IRQ_INTERRUPT_CONTROLLER_ID -1
#define SYSID_NAME "/dev/sysid"
#define SYSID_SPAN 8
#define SYSID_TIMESTAMP 1603214124
#define SYSID_TYPE "altera_avalon_sysid_qsys"


/*
 * systime configuration
 *
 */

#define ALT_MODULE_CLASS_systime altera_avalon_timer
#define SYSTIME_ALWAYS_RUN 0
#define SYSTIME_BASE 0x20014ac0
#define SYSTIME_COUNTER_SIZE 32
#define SYSTIME_FIXED_PERIOD 0
#define SYSTIME_FREQ 125000000
#define SYSTIME_IRQ 0
#define SYSTIME_IRQ_INTERRUPT_CONTROLLER_ID 0
#define SYSTIME_LOAD_VALUE 124999
#define SYSTIME_MULT 0.001
#define SYSTIME_NAME "/dev/systime"
#define SYSTIME_PERIOD 1
#define SYSTIME_PERIOD_UNITS "ms"
#define SYSTIME_RESET_OUTPUT 0
#define SYSTIME_SNAPSHOT 1
#define SYSTIME_SPAN 32
#define SYSTIME_TICKS_PER_SEC 1000
#define SYSTIME_TIMEOUT_PULSE_OUTPUT 0
#define SYSTIME_TYPE "altera_avalon_timer"


/*
 * systimestamp configuration
 *
 */

#define ALT_MODULE_CLASS_systimestamp altera_avalon_timer
#define SYSTIMESTAMP_ALWAYS_RUN 0
#define SYSTIMESTAMP_BASE 0x20014ae0
#define SYSTIMESTAMP_COUNTER_SIZE 32
#define SYSTIMESTAMP_FIXED_PERIOD 0
#define SYSTIMESTAMP_FREQ 125000000
#define SYSTIMESTAMP_IRQ 1
#define SYSTIMESTAMP_IRQ_INTERRUPT_CONTROLLER_ID 0
#define SYSTIMESTAMP_LOAD_VALUE 124999
#define SYSTIMESTAMP_MULT 0.001
#define SYSTIMESTAMP_NAME "/dev/systimestamp"
#define SYSTIMESTAMP_PERIOD 1
#define SYSTIMESTAMP_PERIOD_UNITS "ms"
#define SYSTIMESTAMP_RESET_OUTPUT 0
#define SYSTIMESTAMP_SNAPSHOT 1
#define SYSTIMESTAMP_SPAN 32
#define SYSTIMESTAMP_TICKS_PER_SEC 1000
#define SYSTIMESTAMP_TIMEOUT_PULSE_OUTPUT 0
#define SYSTIMESTAMP_TYPE "altera_avalon_timer"


/*
 * uart_usb configuration
 *
 */

#define ALT_MODULE_CLASS_uart_usb altera_avalon_uart
#define UART_USB_BASE 0x72000200
#define UART_USB_BAUD 115200
#define UART_USB_DATA_BITS 8
#define UART_USB_FIXED_BAUD 1
#define UART_USB_FREQ 12500000
#define UART_USB_IRQ 13
#define UART_USB_IRQ_INTERRUPT_CONTROLLER_ID 0
#define UART_USB_NAME "/dev/uart_usb"
#define UART_USB_PARITY 'N'
#define UART_USB_SIM_CHAR_STREAM ""
#define UART_USB_SIM_TRUE_BAUD 0
#define UART_USB_SPAN 32
#define UART_USB_STOP_BITS 1
#define UART_USB_SYNC_REG_DEPTH 2
#define UART_USB_TYPE "altera_avalon_uart"
#define UART_USB_USE_CTS_RTS 1
#define UART_USB_USE_EOP_REGISTER 0


/*
 * xcvr_reconfig configuration
 *
 */

#define ALT_MODULE_CLASS_xcvr_reconfig alt_xcvr_reconfig
#define XCVR_RECONFIG_BASE 0x20014800
#define XCVR_RECONFIG_IRQ -1
#define XCVR_RECONFIG_IRQ_INTERRUPT_CONTROLLER_ID -1
#define XCVR_RECONFIG_NAME "/dev/xcvr_reconfig"
#define XCVR_RECONFIG_SPAN 512
#define XCVR_RECONFIG_TYPE "alt_xcvr_reconfig"

#endif /* __SYSTEM_H_ */
