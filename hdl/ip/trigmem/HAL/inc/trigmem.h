#ifndef __TRIGMEM_H__
#define __TRIGMEM_H__

#include <alt_types.h>
#include <io.h>

#ifdef __cplusplus
extern "C"
{
#endif /* __cplusplus */

#define TRIGMEM_SELF_TRIG	1

#define TRIGMEM_WRITE_REG_CTRL(base, reg, val) IOWR(base, reg, val)
#define TRIGMEM_WRITE_REG_CTRL_CH(base, ch, reg, val) IOWR(base, TRIGMEM_NUM_CTRL_BASE_REGS+(ch*TRIGMEM_NUM_CTRL_CH_REGS)+reg, val)

#define TRIGMEM_READ_REG_CTRL(base, reg) IORD(base, reg)
#define TRIGMEM_READ_REG_CTRL_CH(base, ch, reg) IORD(base, TRIGMEM_NUM_CTRL_BASE_REGS+ (ch*TRIGMEM_NUM_CTRL_CH_REGS)+reg)

#define TRIGMEM_READ_REG_STAT(base, reg) IORD(base, reg)
#define TRIGMEM_READ_REG_STAT_CH(base, ch, reg) IORD(base, TRIGMEM_NUM_STAT_BASE_REGS+(ch*TRIGMEM_NUM_STAT_CH_REGS)+reg)

typedef struct {
	alt_u32 ctrl_base;
	alt_u32 stat_base;
	alt_u32 data_base;
	alt_u16 depth;
	alt_u8  width;
	alt_u8  width_in_bytes;
	alt_u16 num_buffer;
	alt_u16 num_ch;
	alt_u16 buff_offset;
	alt_u16 data_offset;
} tTriggerMem;

typedef struct {
	alt_u8 triggerable;
	alt_u8 triggered;
	alt_u8 filled;
	alt_u16 fill_count;
	alt_u16 trig_point;
	alt_u16 stop_point;
	alt_u16 samples_to_read;
} tTriggerStatus;

void TRIGMEM_Init(tTriggerMem* tmem, alt_u32 ctrl_base, alt_u32 stat_base, alt_u32 data_base);
void TRIGMEM_ForceGlobalTrigger(tTriggerMem* tmem);
void TRIGMEM_SetChannel(tTriggerMem* tmem, alt_u16 ch, alt_u16 trigger_point, alt_u16 stop_point);
void TRIGMEM_GetInfo(tTriggerMem* tmem);
void TRIGMEM_GetChannelInfo(tTriggerMem* tmem, alt_u16 ch, tTriggerStatus* status);
alt_u32 TRIGMEM_IsChannelTriggered(tTriggerMem* tmem, alt_u16 ch);
alt_u32 TRIGMEM_IsChannelFilled(tTriggerMem* tmem, alt_u16 ch);
void TRIGMEM_ResetChannel(tTriggerMem* tmem, alt_u16 ch);
void TRIGMEM_TriggerChannel(tTriggerMem* tmem, alt_u16 ch);
alt_u16 TRIGMEM_GetChannelBuffer(tTriggerMem* tmem, alt_u16 ch, alt_u16 buffer, alt_u8* data, alt_u16 len, alt_u32 self_trig);

volatile void* TRIGMEM_GetChannelLocation(tTriggerMem* tmem, alt_u16 ch, alt_u16 buffer);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __TRIGMEM_H__ */
