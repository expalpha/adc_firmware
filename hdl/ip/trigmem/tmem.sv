 module tmem (
	clk,
	rst,
	soft_rst,
	d,
	trigger,
	
	stop_point,
	trigger_point,
	
	triggered,
	triggerable,
	filled,
	fill_count,
	
	rd_addr,
	rd_q
);

parameter MEM_DEPTH = 256;
parameter SZ_DATA_IN   	= 16;
parameter SZ_DATA_OUT	= 16;


localparam SZ_MEM 	= $clog2(MEM_DEPTH);		// Depth of each memory buffer
localparam SZ_CNT		= $clog2(MEM_DEPTH+1);
localparam SZ_ADDR_IN = SZ_MEM;
localparam SZ_ADDR_OUT = $clog2((MEM_DEPTH * SZ_DATA_IN) / SZ_DATA_OUT);

localparam RAM_WORDS = (SZ_DATA_IN < SZ_DATA_OUT) ? MEM_DEPTH*SZ_DATA_IN/SZ_DATA_OUT : MEM_DEPTH*SZ_DATA_OUT/SZ_DATA_IN;

input wire clk;
input wire rst;
input wire [SZ_DATA_IN-1:0] d;
input wire soft_rst;	// for 
input wire trigger;	// doubles as lock as well
input wire [SZ_MEM-1:0] stop_point;
input wire [SZ_MEM-1:0] trigger_point;
input wire [SZ_ADDR_OUT-1:0] rd_addr;

output reg triggerable;			// We have past the trigger_point, allowed to trigger 
output reg triggered;	// Triggered, read-out can start! 
output reg filled;				// Finished writing, once read-out is done, trigger memory should be reset
output reg [SZ_CNT-1:0] fill_count;		// Number of samples written out
output wire [SZ_DATA_OUT-1:0] rd_q;

wire int_rst = soft_rst | rst;
wire pre_trigger = ~(triggered | (trigger & triggerable));

reg [SZ_CNT-1:0] rd_pos_start; // data_in must always be at least as large as data_out, so we over-size rd_pos_start to avoid a warning
reg wr_ena;
reg [SZ_CNT-1:0] wr_cnt;
reg [SZ_MEM-1:0] wr_pos;
reg [SZ_ADDR_OUT-1:0] rd_pos;
reg [SZ_MEM-1:0] r_trig_ena_cnt;


// Trigger available logic 
always@(posedge int_rst, posedge clk) begin 
	if(int_rst) begin 
		triggered <= 1'b0;	
		triggerable <= 1'b0;
		r_trig_ena_cnt <= {SZ_MEM{1'b0}};
	end else begin 		
		if(r_trig_ena_cnt < trigger_point) begin 
			triggered 	<= 1'b0;	
			// We want triggerable to be high for one clock if the trigger is present immediately
			triggerable <= (r_trig_ena_cnt == (trigger_point - 1'b1)) ? 1'b1 : 1'b0;
			r_trig_ena_cnt <= r_trig_ena_cnt + 1'b1;
		end else begin 
			triggered 	<= (trigger) ? 1'b1 : triggered;
			triggerable <= (trigger) ? 1'b0 : (triggered) ? 1'b0 : 1'b1;
			r_trig_ena_cnt <= r_trig_ena_cnt;			
		end
	end
end

// Write and readback logic
always@(posedge int_rst, posedge clk) begin 
	if(int_rst) begin 
		wr_pos <= {SZ_MEM{1'b0}};
		wr_cnt 	<= {SZ_MEM{1'b0}};
		filled 		<= 1'b0;
		wr_ena	<= 1'b1;
		fill_count <= {SZ_CNT{1'b0}};
	end else begin 
		fill_count <= wr_cnt + 1'b1;
		// Have we triggered yet?
		if(pre_trigger) begin 
			wr_pos <= wr_pos + 1'b1;
			wr_ena <= 1'b1; // just keep writing to the memory
			wr_cnt  <= trigger_point;
			filled <= 1'b0;
		end else begin 
			if((wr_cnt < stop_point) && (wr_cnt < MEM_DEPTH)) begin 
				wr_ena <= 1'b1;
				wr_cnt <= wr_cnt + 1'b1;
				wr_pos <= wr_pos + 1'b1;
				filled <= 1'b0;
			end else begin 
				wr_ena <= 1'b0;
				wr_cnt <= wr_cnt;
				wr_pos <= wr_pos;
				filled <= 1'b1;
			end		
		end
	end
end

generate 
if(SZ_DATA_OUT != SZ_DATA_IN) begin 
	always@(posedge int_rst, posedge clk) begin 
		if(int_rst) begin 
			rd_pos_start <= {SZ_CNT{1'b0}};
			rd_pos	<= {SZ_ADDR_OUT{1'b0}};			
		end else begin 
			rd_pos_start <= ({1'b0, wr_pos} - wr_cnt) >> $clog2((SZ_DATA_OUT / SZ_DATA_IN));
			rd_pos <= rd_pos_start[SZ_ADDR_OUT-1:0] + rd_addr;			
		end
	end 
end else begin 
	always@(posedge int_rst, posedge clk) begin 
		if(int_rst) begin 
			rd_pos_start <= {SZ_CNT{1'b0}};
			rd_pos	<= {SZ_ADDR_OUT{1'b0}};			
		end else begin 
			rd_pos_start <= ({1'b0, wr_pos} - wr_cnt);
			rd_pos <= rd_pos_start[SZ_ADDR_OUT-1:0] + rd_addr;			
		end
	end 
end
endgenerate

// Inferrred RAM block 
mixed_width_ram #(
	.WORDS(RAM_WORDS),
	.WW(SZ_DATA_IN),
	.RW(SZ_DATA_OUT)
) ram_inst (
	.clk		(clk),
	.wdata	( d ),
	.waddr	( wr_pos ),
	.raddr	( rd_pos ),
	.we		( wr_ena ),
	.q			( rd_q )
);
	

endmodule
