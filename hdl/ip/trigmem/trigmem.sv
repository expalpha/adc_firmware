module trigmem (
	// Wishbone/Avalon Interface
	clk, 
	rst, 
	
	s0_address, 
	s0_readdata, 
	s0_read,
	s0_readdatavalid, 

	s1_address, 
	s1_byteenable,
	s1_readdata, 
	s1_writedata,
	s1_write, 
	s1_readdatavalid, 
	s1_read,
	
	s2_address, 
	s2_readdata, 
	s2_read,
	
	// Conduits	
	trig_in,
	data_in
);

parameter NUM_CH 	= 16;
parameter NUM_BUFF	= 1;
parameter NUM_DEPTH	 = 1024;
parameter SZ_DATA_WIDTH = 16;

localparam NUM_MEM_CH = NUM_CH*NUM_BUFF;
localparam SZ_WIDTH 	= 32; // always NIOS 32bit bus size

// Control Registers
localparam CTRL_GLOBAL_TRIGGER = 0;
localparam NUM_CTRL_BASE_REGS = 1;

localparam CTRL_TRIG_POINT		= 0;
localparam CTRL_STOP_POINT	= 1;
localparam CTRL_FORCE_TRIG	= 2;
localparam CTRL_SOFT_RESET	= 3;
localparam NUM_CTRL_CH_REGS = 4;
localparam NUM_CTRL_REGS		= NUM_CTRL_BASE_REGS+(NUM_CTRL_CH_REGS*NUM_CH);

// Status Registers		

localparam STAT_DEPTH				= 0;
localparam STAT_N_BUF				= 1;
localparam STAT_N_CH					= 2;
localparam STAT_DATA_WIDTH	= 3;
localparam NUM_STAT_BASE_REGS = 4;

localparam STAT_TRIGERRABLE	= 0;
localparam STAT_TRIG_STATUS	= 1;
localparam STAT_FILL_STATUS	= 2;
localparam STAT_FILL_COUNT		= 3;
localparam NUM_STAT_CH_REGS 		= 4;

localparam NUM_STAT_REGS = NUM_STAT_BASE_REGS+(NUM_STAT_CH_REGS*NUM_CH);

localparam SZ_STAT				= $clog2(NUM_STAT_REGS);
localparam SZ_CTRL				= $clog2(NUM_CTRL_REGS);
localparam SZ_MEM_IN_32	= $clog2(NUM_DEPTH * SZ_DATA_WIDTH / SZ_WIDTH);
localparam SZ_BUFF				= $clog2(NUM_DEPTH);
localparam SZ_CNT					= $clog2(NUM_DEPTH+1);
localparam SZ_N_BUF			= $clog2(NUM_BUFF);
localparam SZ_N_CH				= $clog2(NUM_CH);
localparam SZ_ADDR_STAT	= SZ_STAT;
localparam SZ_ADDR_CTRL	= SZ_CTRL;
localparam SZ_ADDR_MUX	= (SZ_N_BUF + SZ_N_CH);
localparam SZ_ADDR_MEM	= SZ_ADDR_MUX + SZ_MEM_IN_32;


// Clock and Resets
input        										clk;
input 												rst;

// Conduits
input  [NUM_MEM_CH-1:0][SZ_DATA_WIDTH-1:0] 	data_in;
input  [NUM_CH-1:0] 					trig_in;

// Avalon 0 - Status
input  [SZ_ADDR_STAT-1:0] 	s0_address;	// lower address bits
output [SZ_WIDTH-1:0] 			s0_readdata;	// data bus input
input        										s0_read;	// valid bus cycle input
output      										s0_readdatavalid;	// bus cycle acknowledge output

// Avalon 1 - Control 
input  [SZ_ADDR_CTRL-1:0] 	s1_address;	// lower address bits
input [3:0] s1_byteenable;
input        										s1_write;	// write enable input
input  [SZ_WIDTH-1:0] 				s1_writedata;	// data bus output
input        										s1_read;	// valid bus cycle input
output [SZ_WIDTH-1:0] 			s1_readdata;	// data bus input
output      										s1_readdatavalid;	// bus cycle acknowledge output

// Avalon 2 - Memory, 4 cycle delayed reads
input  [SZ_ADDR_MEM-1:0] 		s2_address;	// lower address bits
input        										s2_read;	// valid bus cycle input
output [SZ_WIDTH-1:0] 	s2_readdata;	// data bus input

wire global_trigger;
wire [NUM_CTRL_REGS-1:0][SZ_WIDTH-1:0]	ctrl;
wire [NUM_STAT_REGS-1:0][SZ_WIDTH-1:0]	status;
wire [NUM_CH-1:0][SZ_BUFF-1:0] 													trig_point;
wire [NUM_CH-1:0][SZ_BUFF-1:0] 													stop_point;
wire [NUM_CH-1:0][NUM_BUFF-1:0][0:0]										filled;
wire [NUM_CH-1:0][NUM_BUFF-1:0][0:0]										triggered;
wire [NUM_CH-1:0][NUM_BUFF-1:0][0:0]										triggerable;
wire [NUM_CH-1:0][NUM_BUFF-1:0][SZ_CNT-1:0]					fill_count;
wire [NUM_CH-1:0]																				force_trig;
wire [NUM_CH-1:0]																				soft_rst;
wire [NUM_CH*NUM_BUFF-1:0][SZ_WIDTH-1:0] 					data;

reg [SZ_ADDR_MUX-1:0] read_addr0;
reg [SZ_ADDR_MUX-1:0] read_addr;

assign s2_readdata = data[read_addr];

assign status[STAT_N_CH]			= NUM_CH;
assign status[STAT_N_BUF]		= NUM_BUFF;
assign status[STAT_DEPTH]		= NUM_DEPTH;
assign status[STAT_DATA_WIDTH] = SZ_DATA_WIDTH;	
	
// Pipeline read address to match delay in readout	
always@(posedge rst, posedge clk) begin 
	if(rst) begin 
		read_addr0 <= 0;
		read_addr  <= 0;
	end else begin 
		read_addr0 <= s2_address[SZ_ADDR_MEM-1 -: SZ_ADDR_MUX];
		read_addr  <= read_addr0;
	end
end 

genvar n;
genvar i;
generate 
for(n=0; n<NUM_CH; n++) begin: TRIG_CH_GEN
	assign global_trigger = ctrl[CTRL_GLOBAL_TRIGGER][0];
	// Control
	assign trig_point[n] 	= ctrl[NUM_CTRL_BASE_REGS+(n*NUM_CTRL_CH_REGS)+CTRL_TRIG_POINT][SZ_BUFF-1:0];
	assign stop_point[n]	= ctrl[NUM_CTRL_BASE_REGS+(n*NUM_CTRL_CH_REGS)+CTRL_STOP_POINT][SZ_BUFF-1:0];
	assign force_trig[n] 		= ctrl[NUM_CTRL_BASE_REGS+(n*NUM_CTRL_CH_REGS)+CTRL_FORCE_TRIG][0];
	assign soft_rst[n] 		= ctrl[NUM_CTRL_BASE_REGS+(n*NUM_CTRL_CH_REGS)+CTRL_SOFT_RESET][0];
	// Status
	assign status[NUM_STAT_BASE_REGS+(n*NUM_STAT_CH_REGS)+STAT_TRIGERRABLE]		= triggerable[n][0];
	assign status[NUM_STAT_BASE_REGS+(n*NUM_STAT_CH_REGS)+STAT_TRIG_STATUS] 	= triggered[n][0];
	assign status[NUM_STAT_BASE_REGS+(n*NUM_STAT_CH_REGS)+STAT_FILL_STATUS]  	= filled[n][0];
	assign status[NUM_STAT_BASE_REGS+(n*NUM_STAT_CH_REGS)+STAT_FILL_COUNT]	  	= fill_count[n][0];

	// Swap for ease of passing in additional waveforms
	for(i=0; i<NUM_BUFF; i++) begin: TMEM_BUFF_GEN						
		tmem #(
			.MEM_DEPTH	( NUM_DEPTH ),
			.SZ_DATA_IN			( SZ_DATA_WIDTH ),
			.SZ_DATA_OUT		( SZ_WIDTH )
		) tmem_inst (
			.clk						( clk ),
			.rst						( rst ),
			.soft_rst				( soft_rst[n] ),
			.d						( data_in[(n*NUM_BUFF)+i] ),
			.trigger				( trig_in[n]  | force_trig[n] | global_trigger),
			.trigger_point	( trig_point[n] ),
			.stop_point 		( stop_point[n] ),
			.triggered 			( triggered[n][i] ),
			.triggerable		( triggerable[n][i] ),
			.filled 					( filled[n][i] ),
			.fill_count			( fill_count[n][i] ),
			.rd_addr			( s2_address[0 +: SZ_MEM_IN_32] ), // drop lower 2 bits (byte addressing!)
			.rd_q					( data[(n*NUM_BUFF)+i] )
		);			
	end
end
endgenerate 

avalon_stat_slave #(
	.NUM_STAT_REGS(NUM_STAT_REGS),
	.SZ_WIDTH(SZ_WIDTH)
) trigmem_stat (	
	.clk						( clk ), 
	.rst						( rst ), 
	.address				( s0_address ),
	.readdata			( s0_readdata ), 
	.read					( s0_read ), 
	.readdatavalid	( s0_readdatavalid ), 
	.status 				( status )
);

avalon_ctrl_slave #(
	.NUM_CTRL_REGS(NUM_CTRL_REGS),
	.SZ_WIDTH(SZ_WIDTH)
) trigmem_ctrl (
	.clk						( clk ), 
	.rst						( rst ), 
	.address				( s1_address ),
	.byteenable		( s1_byteenable ),
	.write					( s1_write ), 
	.writedata			( s1_writedata ),
	.read					( s1_read  ), 
	.readdata			( s1_readdata ), 
	.readdatavalid	( s1_readdatavalid ), 
	.ctrl 					( ctrl )
);

endmodule
