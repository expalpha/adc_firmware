/*
 * Copyright (c) 2014, TRIUMF
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * 	  this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of TRIUMF.
 *
 */

#include <drivers/inc/ad9249.h>
#include <altera_avalon_spi.h>
#include <altera_avalon_pio_regs.h>

void AD9249_Write(alt_u32 io_base, alt_u32 io_tri, alt_u32 spi_base, alt_u32 cs, alt_u32 reg, alt_u32 data) {
	alt_u8 tx_data[3];

	tx_data[0] = (reg & 0x0F00) >> 8;
	tx_data[1] = (reg & 0x00FF);
	tx_data[2] = data;

	// Enable output
	IOWR_ALTERA_AVALON_PIO_SET_BITS(io_base, io_tri);

	alt_avalon_spi_command(spi_base, cs, 1, (alt_u8*)&tx_data[0], 0, 0, 0);
	alt_avalon_spi_command(spi_base, cs, 1, (alt_u8*)&tx_data[1], 0, 0, 0);
	alt_avalon_spi_command(spi_base, cs, 1, (alt_u8*)&tx_data[2], 0, 0, 0);

}

alt_u32 AD9249_Read(alt_u32 io_base, alt_u32 io_tri, alt_u32 spi_base, alt_u32 cs, alt_u32 reg) {
	alt_u8 tx_data[3];
	alt_u8 data;

	tx_data[0] = 0x80 | ((reg & 0x0F00) >> 8);
	tx_data[1] = (reg & 0x00FF);
	tx_data[2] = 0;

	IOWR_ALTERA_AVALON_PIO_SET_BITS(io_base, io_tri);

	alt_avalon_spi_command(spi_base, cs, 1, (alt_u8*)&tx_data[0], 0, 0, 0);
	alt_avalon_spi_command(spi_base, cs, 1, (alt_u8*)&tx_data[1], 0, 0, 0);

	IOWR_ALTERA_AVALON_PIO_CLEAR_BITS(io_base, io_tri);

	alt_avalon_spi_command(spi_base, cs, 0, (alt_u8*)&tx_data[2], 1, &data, 0);

	IOWR_ALTERA_AVALON_PIO_SET_BITS(io_base, io_tri);

	return data;
}
