/*
 * Copyright (c) 2016, TRIUMF
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * 	  this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of TRIUMF.
 *
 * Created by: Bryerton Shaw
 * Created on: May 16, 2016
 */

#ifndef EEPROM_24AA02E48_H
#define EEPROM_24AA02E48_H

#include <alt_types.h>

#ifdef __cplusplus
extern "C"
{
#endif /* __cplusplus */

#define EEPROM_24AA02E48_INSTANCE(name, dev) extern int alt_no_storage
#define EEPROM_24AA02E48_INIT(name, dev) while (0)

#define EEPROM_24AA02E48_CTRL_CODE		0x50
#define EEPROM_24AA02E48_ADDR_EUI48		0xFA

typedef struct {
	alt_u8 field[6];
} tEUI48;

typedef struct {
	alt_u8 field[8];
} tEUI64;
	
void EEPROM_24AA02E48_WriteByte(alt_u32 i2c_base, alt_u8 addr,  alt_u8 data);
void EEPROM_24AA02E48_WritePage(alt_u32 i2c_base, alt_u8 addr,  alt_u8* data, alt_u8 data_len);

alt_u8 EEPROM_24AA02E48_ReadByte(alt_u32 i2c_base, alt_u8 addr);
void EEPROM_24AA02E48_ReadPage(alt_u32 i2c_base, alt_u8 addr, alt_u8* data, alt_u8 data_len);

void EEPROM_24AA02E48_GetEUI48(alt_u32 i2c_base, tEUI48* eui);
void EEPROM_24AA02E48_GetEUI64(alt_u32 i2c_base, tEUI64* eui);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* EEPROM_24AA02E48_H */
