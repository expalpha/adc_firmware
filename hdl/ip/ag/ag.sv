`default_nettype none
module ag #(
	parameter SZ_DATA = 32
)(
  // Avalon Interface
  input wire clk, 
  input wire rst, 

  // Avalon 0 - Status Registers 
  input  wire [SZ_ADDR_STAT-1:0] 	s0_address, // lower address bits
  input  wire       			s0_read,    // valid bus cycle input
  input	 wire [3:0]			s0_byteenable,
  output wire [SZ_DATA-1:0] 		s0_readdata,	  // data bus input
  output wire      			s0_readdatavalid, // bus cycle acknowledge output

  // Avalon 1 - Control Registers
  input  wire [SZ_ADDR_CTRL-1:0] 	s1_address,	// lower address bits
  input  wire       			s1_read,	// valid bus cycle input
  output wire [SZ_DATA-1:0] 		s1_readdata,	// data bus input
  output wire      			s1_readdatavalid,	// bus cycle acknowledge output
  input  wire [3:0] 			s1_byteenable,
  input  wire       			s1_write,	// write enable input
  input  wire [SZ_DATA-1:0] 		s1_writedata,	// data bus output
  
  // trigger thresholds
  output wire [15:0]   adc16_threshold,
  output wire [15:0]   adc32_threshold,

  // trigger bits
  input  wire [15:0]   adc16_bits,
  input  wire [31:0]   adc32_bits,

  // trigger counters
  output wire          clk_counter,
  input  wire [31:0]   adc16_counter,
  input  wire [31:0]   adc32_counter,

  // DAC control
  output wire [31:0]   dac_data,
  output wire [31:0]   dac_ctrl,
  output wire [31:0]   dac_ctrl_a,
  output wire [31:0]   dac_ctrl_b,
  output wire [31:0]   dac_ctrl_c,
  output wire [31:0]   dac_ctrl_d,

  // channel suppression thresholds
  output wire [15:0]   adc16_sthreshold,
  output wire [15:0]   adc32_sthreshold,

  // general control
  output wire [31:0]   ctrl_a,
  output wire [31:0]   ctrl_b,
  output wire [31:0]   ctrl_c,
  output wire [31:0]   ctrl_d,
  output wire [31:0]   ctrl_e,
  output wire [31:0]   ctrl_f,
  
  input  wire [31:0]   stat_a,
  input  wire [31:0]   stat_b,
  input  wire [31:0]   stat_c,
  input  wire [31:0]   stat_d
);

// Control Registers
localparam ADC16_THRESHOLD	= 0;
localparam ADC32_THRESHOLD	= 1;
localparam DAC_DATA		= 2;
localparam DAC_CTRL		= 3;
localparam DAC_CTRL_A		= 4;
localparam DAC_CTRL_B		= 5;
localparam DAC_CTRL_C		= 6;
localparam DAC_CTRL_D		= 7;
localparam ADC16_STHRESHOLD	= 8;
localparam ADC32_STHRESHOLD	= 9;
localparam CTRL_A	        = 10;
localparam CTRL_B	        = 11;
localparam CTRL_C	        = 12;
localparam CTRL_D	        = 13;
localparam CTRL_E	        = 14;
localparam CTRL_F               = 15;
localparam NUM_CTRL_REGS	= 16;

// Status Registers
localparam ADC16_BITS		= 0;
localparam ADC32_BITS		= 1;
localparam ADC16_COUNTER	= 2;
localparam ADC32_COUNTER	= 3;
localparam STAT_A	        = 4;
localparam STAT_B	        = 5;
localparam STAT_C	        = 6;
localparam STAT_D	        = 7;
localparam NUM_STAT_REGS	= 8;

localparam SZ_STAT = $clog2(NUM_STAT_REGS);
localparam SZ_CTRL = $clog2(NUM_CTRL_REGS);
localparam SZ_ADDR_STAT = SZ_STAT;
localparam SZ_ADDR_CTRL = SZ_CTRL;

// Control and status registers

wire [NUM_CTRL_REGS-1:0][SZ_DATA-1:0] ctrl;
wire [NUM_STAT_REGS-1:0][SZ_DATA-1:0] status;

// Connect Control Registers

assign adc16_threshold = ctrl[ADC16_THRESHOLD][15:0];
assign adc32_threshold = ctrl[ADC32_THRESHOLD][15:0];

assign dac_data = ctrl[DAC_DATA][31:0];
assign dac_ctrl = ctrl[DAC_CTRL][31:0];

assign dac_ctrl_a = ctrl[DAC_CTRL_A][31:0];
assign dac_ctrl_b = ctrl[DAC_CTRL_B][31:0];
assign dac_ctrl_c = ctrl[DAC_CTRL_C][31:0];
assign dac_ctrl_d = ctrl[DAC_CTRL_D][31:0];

assign adc16_sthreshold = ctrl[ADC16_STHRESHOLD][15:0];
assign adc32_sthreshold = ctrl[ADC32_STHRESHOLD][15:0];

assign ctrl_a = ctrl[CTRL_A][31:0];
assign ctrl_b = ctrl[CTRL_B][31:0];
assign ctrl_c = ctrl[CTRL_C][31:0];
assign ctrl_d = ctrl[CTRL_D][31:0];
assign ctrl_e = ctrl[CTRL_E][31:0];
assign ctrl_f = ctrl[CTRL_F][31:0];

// Connect status registers
   
// Synchronize incoming signals to NIOS clock
synchronizer #( .SZ_DATA( 16 ) ) sync_adc16_bits_ts ( .clk	( clk ),	.rst	( rst ), .d ( adc16_bits ),	.q ( status[ADC16_BITS][15:0] ));
synchronizer #( .SZ_DATA( 32 ) ) sync_adc32_bits_ts ( .clk	( clk ),	.rst	( rst ), .d ( adc32_bits ),	.q ( status[ADC32_BITS][31:0] ));

   assign clk_counter = clk;
   assign status[ADC16_COUNTER] = adc16_counter;
   assign status[ADC32_COUNTER] = adc32_counter;

   synchronizer #(.SZ_DATA(32)) sync_stat_a(.clk(clk),.rst(rst),.d(stat_a),.q(status[STAT_A][31:0]));
   synchronizer #(.SZ_DATA(32)) sync_stat_b(.clk(clk),.rst(rst),.d(stat_b),.q(status[STAT_B][31:0]));
   synchronizer #(.SZ_DATA(32)) sync_stat_c(.clk(clk),.rst(rst),.d(stat_c),.q(status[STAT_C][31:0]));
   synchronizer #(.SZ_DATA(32)) sync_stat_d(.clk(clk),.rst(rst),.d(stat_d),.q(status[STAT_D][31:0]));

// avalon bus interfaces

avalon_stat_slave #(
	.NUM_STAT_REGS(NUM_STAT_REGS),
	.SZ_WIDTH(SZ_DATA)
) ag_stat (	
	.clk		( clk ), 
	.rst		( rst ), 
	.address	( s0_address ),
	.byteenable	( s0_byteenable ),
	.read		( s0_read ), 
	.readdata	( s0_readdata ),
	.readdatavalid	( s0_readdatavalid  ), 
	.status 	( status )
);

avalon_ctrl_slave #(
	.NUM_CTRL_REGS(NUM_CTRL_REGS),
	.SZ_WIDTH(SZ_DATA)
) ag_ctrl (
	.clk		( clk ), 
	.rst		( rst ), 
	.address	( s1_address ),
	.write		( s1_write ), 
	.writedata	( s1_writedata ),
	.byteenable     ( s1_byteenable ),
	.read		( s1_read ), 
	.readdata	( s1_readdata ), 
	.readdatavalid	( s1_readdatavalid ), 
	.ctrl		( ctrl )
);

endmodule
