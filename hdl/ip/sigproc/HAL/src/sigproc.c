#include <alt_types.h>
#include <unistd.h>
#include <string.h>
#include <sigproc_regs.h>
#include <sigproc.h>

alt_u32 SIGPROC_Read(alt_u32 base, alt_u32 id) {
	return IORD(base, id);
}

void SIGPROC_Write(alt_u32 base, alt_u32 id, alt_u32 data) {
	IOWR(base, id, data);
}

alt_u32 SIGPROC_STAT_CH_Read(alt_u32 base, alt_u32 ch, alt_u32 id) {
	return IORD(base, SIGPROC_NUM_STAT_BASE_REGS + (ch*SIGPROC_NUM_STAT_CH_REGS) + id);
}

alt_u32 SIGPROC_CTRL_CH_Read(alt_u32 base, alt_u32 ch, alt_u32 id) {
	return IORD(base, SIGPROC_NUM_CTRL_BASE_REGS + (ch*SIGPROC_NUM_CTRL_CH_REGS) + id);
}

void SIGPROC_CTRL_CH_Write(alt_u32 base, alt_u32 ch, alt_u32 id, alt_u32 data) {
	IOWR(base, SIGPROC_NUM_CTRL_BASE_REGS + (ch*SIGPROC_NUM_CTRL_CH_REGS) + id, data);
}
