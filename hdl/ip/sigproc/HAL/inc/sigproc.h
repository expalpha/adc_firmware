#ifndef __SIGPROC_H__
#define __SIGPROC_H__

#include <alt_types.h>
#include <io.h>

#ifdef __cplusplus
extern "C"
{
#endif /* __cplusplus */

alt_u32 SIGPROC_Read(alt_u32 base, alt_u32 id);
void SIGPROC_Write(alt_u32 base, alt_u32 id, alt_u32 data);

alt_u32 SIGPROC_STAT_CH_Read(alt_u32 base, alt_u32 ch, alt_u32 id);
alt_u32 SIGPROC_CTRL_CH_Read(alt_u32 base, alt_u32 ch, alt_u32 id);
void SIGPROC_CTRL_CH_Write(alt_u32 base, alt_u32 ch, alt_u32 id, alt_u32 data);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SIGPROC_H__ */
