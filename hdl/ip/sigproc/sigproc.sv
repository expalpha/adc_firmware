module sigproc (
	// Wishbone/Avalon Interface
	clk, 
	rst, 
	
	s0_address, 
	s0_byteenable,
	s0_readdata, 
	s0_read,
	s0_readdatavalid, 

	s1_address, 
	s1_byteenable,
	s1_readdata, 
	s1_writedata,
	s1_write, 
	s1_readdatavalid, 
	s1_read,
	
	// Conduits	
	int_trigger,
	run_status,
	timestamp,
	
	ch_ena,
	ch_type,
	ch_trig_delay,
	ch_trig_point,
	ch_stop_point,
	
	ch_stat_trig_in,
	ch_stat_trig_accepted,
	ch_stat_trig_drop_busy,
	ch_stat_trig_drop_full
);

parameter NUM_CH 			= 16;
parameter SZ_TIMESTAMP 	= 48;
parameter WAVE_DEPTH 	= 1024; // number of samples available in the sp_channel

localparam TRIGGER_DELAY_MAX 	= 1024;
localparam SZ_DELAY 	= $clog2(TRIGGER_DELAY_MAX);
localparam SZ_WAVE 	= $clog2(WAVE_DEPTH);
localparam SZ_WIDTH 	= 32; // always NIOS 32bit bus size

// Control Registers
localparam CTRL_INT_TRIGGER			= 0;
localparam NUM_CTRL_BASE_REGS 		= 1;

localparam CTRL_CH_ENA					= 0;
localparam CTRL_CH_TYPE					= 1;
localparam CTRL_CH_TRIG_DELAY 		= 2;
localparam CTRL_CH_TRIG_POINT			= 3;
localparam CTRL_CH_STOP_POINT			= 4;
localparam NUM_CTRL_CH_REGS 			= 5;
localparam NUM_CTRL_REGS				= NUM_CTRL_BASE_REGS+(NUM_CTRL_CH_REGS*NUM_CH);

// Status Registers		
localparam STAT_NUM_CHANNELS			= 0;
localparam STAT_RUN						= 1;
localparam STAT_TIMESTAMP_LO			= 2;
localparam STAT_TIMESTAMP_HI			= 3;
localparam NUM_STAT_BASE_REGS			= 4;

localparam STAT_CH_TRIG_IN				= 0;
localparam STAT_CH_TRIG_ACPTED		= 1;
localparam STAT_CH_TRIG_DROP_BUSY	= 2;
localparam STAT_CH_TRIG_DROP_FULL	= 3;
localparam NUM_STAT_CH_REGS 			= 4;

localparam NUM_STAT_REGS = NUM_STAT_BASE_REGS+(NUM_STAT_CH_REGS*NUM_CH);

localparam SZ_STAT		= $clog2(NUM_STAT_REGS);
localparam SZ_CTRL		= $clog2(NUM_CTRL_REGS);
localparam SZ_ADDR_STAT	= SZ_STAT;
localparam SZ_ADDR_CTRL	= SZ_CTRL;

// Clock and Resets
input wire	clk;
input wire	rst;

// Conduits
output wire int_trigger;
	       
output wire [NUM_CH-1:0] ch_ena;
output wire [NUM_CH-1:0] ch_type;
output wire [NUM_CH-1:0][SZ_DELAY-1:0] ch_trig_delay;
output wire [NUM_CH-1:0][SZ_WAVE-1:0] ch_trig_point;
output wire [NUM_CH-1:0][SZ_WAVE-1:0] ch_stop_point;

input wire run_status;
input wire [SZ_TIMESTAMP-1:0] timestamp;
	
input wire [NUM_CH-1:0][SZ_WIDTH-1:0] ch_stat_trig_in;
input wire [NUM_CH-1:0][SZ_WIDTH-1:0] ch_stat_trig_accepted;
input wire [NUM_CH-1:0][SZ_WIDTH-1:0] ch_stat_trig_drop_busy;
input wire [NUM_CH-1:0][SZ_WIDTH-1:0] ch_stat_trig_drop_full;

// Avalon 0 - Status
input  [SZ_ADDR_STAT-1:0] 	s0_address;	// lower address bits
input  [3:0]					s0_byteenable;
output [SZ_WIDTH-1:0] 		s0_readdata;	// data bus input
input        					s0_read;	// valid bus cycle input
output      					s0_readdatavalid;	// bus cycle acknowledge output

// Avalon 1 - Control 
input  [SZ_ADDR_CTRL-1:0] 	s1_address;	// lower address bits
input  [3:0]					s1_byteenable;
input        					s1_write;	// write enable input
input  [SZ_WIDTH-1:0] 		s1_writedata;	// data bus output
input        					s1_read;	// valid bus cycle input
output [SZ_WIDTH-1:0] 		s1_readdata;	// data bus input
output      					s1_readdatavalid;	// bus cycle acknowledge output

wire [NUM_CTRL_REGS-1:0][SZ_WIDTH-1:0]	ctrl;
wire [NUM_STAT_REGS-1:0][SZ_WIDTH-1:0]	status;

assign status[STAT_NUM_CHANNELS]	= NUM_CH;
assign status[STAT_RUN]				= {31'h0, run_status};
assign status[STAT_TIMESTAMP_LO]	= timestamp[31:0];	
assign status[STAT_TIMESTAMP_HI]	= {16'h0, timestamp[47:32]};

// One-shot the internal trigger
oneshot trigger_int ( 
	.clk	( clk ),
	.rst	( rst ),
	.d		( ctrl[CTRL_INT_TRIGGER][0] ),
	.q		( int_trigger )
);  

genvar n;
generate 
for(n=0; n<NUM_CH; n++) begin: CH_GEN
	// Control
	assign ch_ena[n] 				= ctrl[NUM_CTRL_BASE_REGS+(n*NUM_CTRL_CH_REGS)+CTRL_CH_ENA][0];
	assign ch_type[n]				= ctrl[NUM_CTRL_BASE_REGS+(n*NUM_CTRL_CH_REGS)+CTRL_CH_TYPE][0];
	assign ch_trig_delay[n] 	= ctrl[NUM_CTRL_BASE_REGS+(n*NUM_CTRL_CH_REGS)+CTRL_CH_TRIG_DELAY][SZ_DELAY-1:0];
	assign ch_trig_point[n] 	= ctrl[NUM_CTRL_BASE_REGS+(n*NUM_CTRL_CH_REGS)+CTRL_CH_TRIG_POINT][SZ_WAVE-1:0];
	assign ch_stop_point[n] 	= ctrl[NUM_CTRL_BASE_REGS+(n*NUM_CTRL_CH_REGS)+CTRL_CH_STOP_POINT][SZ_WAVE-1:0];
	
	// Status
	assign status[NUM_STAT_BASE_REGS+(n*NUM_STAT_CH_REGS)+STAT_CH_TRIG_IN]				= ch_stat_trig_in[n][SZ_WIDTH-1:0];
	assign status[NUM_STAT_BASE_REGS+(n*NUM_STAT_CH_REGS)+STAT_CH_TRIG_ACPTED] 		= ch_stat_trig_accepted[n][SZ_WIDTH-1:0];
	assign status[NUM_STAT_BASE_REGS+(n*NUM_STAT_CH_REGS)+STAT_CH_TRIG_DROP_BUSY]  	= ch_stat_trig_drop_busy[n][SZ_WIDTH-1:0];
	assign status[NUM_STAT_BASE_REGS+(n*NUM_STAT_CH_REGS)+STAT_CH_TRIG_DROP_FULL]	  	= ch_stat_trig_drop_full[n][SZ_WIDTH-1:0];
end
endgenerate 

avalon_stat_slave #(
	.NUM_STAT_REGS(NUM_STAT_REGS),
	.SZ_WIDTH(SZ_WIDTH)
) sp_stat (	
	.clk				( clk ), 
	.rst				( rst ), 
	.address			( s0_address ),
	.byteenable		( s0_byteenable ),
	.readdata		( s0_readdata ), 
	.read				( s0_read ), 
	.readdatavalid	( s0_readdatavalid ), 
	.status 			( status )
);

avalon_ctrl_slave #(
	.NUM_CTRL_REGS(NUM_CTRL_REGS),
	.SZ_WIDTH(SZ_WIDTH)
) sp_ctrl (
	.clk				( clk ), 
	.rst				( rst ), 
	.address			( s1_address ),
	.byteenable		( s1_byteenable ),
	.write			( s1_write ), 
	.writedata		( s1_writedata ),
	.read				( s1_read  ), 
	.readdata		( s1_readdata ), 
	.readdatavalid	( s1_readdatavalid ), 
	.ctrl 			( ctrl )
);

endmodule
