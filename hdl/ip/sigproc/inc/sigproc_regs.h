#ifndef __SIGPROC_REGS_H__
#define __SIGPROC_REGS_H__

#define SIGPROC_REG_CTRL_INT_TRIGGER 		   ( 0 )
#define SIGPROC_NUM_CTRL_BASE_REGS 			   ( 1 )

#define SIGPROC_REG_CTRL_CH_ENA				   ( 0 )
#define SIGPROC_REG_CTRL_CH_TYPE			      ( 1 )
#define SIGPROC_REG_CTRL_CH_TRIG_DELAY	      ( 2 )
#define SIGPROC_REG_CTRL_CH_TRIG_POINT	      ( 3 )
#define SIGPROC_REG_CTRL_CH_STOP_POINT	      ( 4 )
#define SIGPROC_NUM_CTRL_CH_REGS 			   ( 5 )

// Status Registers
#define SIGPROC_REG_STAT_NUM_CH				   ( 0 )
#define SIGPROC_REG_STAT_RUN				      ( 1 )
#define SIGPROC_REG_STAT_TIMESTAMP_LO		   ( 2 )
#define SIGPROC_REG_STAT_TIMESTAMP_HI		   ( 3 )
#define SIGPROC_NUM_STAT_BASE_REGS 			   ( 4 )

#define SIGPROC_REG_STAT_CH_TRIG_IN			   ( 0 )
#define SIGPROC_REG_STAT_CH_TRIG_ACPTD	      ( 1 )
#define SIGPROC_REG_STAT_CH_TRIG_DROP_BUSY	( 2 )
#define SIGPROC_REG_STAT_CH_TRIG_DROP_FULL	( 3 )
#define SIGPROC_NUM_STAT_CH_REGS			      ( 4 )

#endif /* __SIGPROC_REGS_H__ */
