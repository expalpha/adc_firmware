create_driver fabric_udp_stream_driver

set_sw_property hw_class_name fabric_udp_stream

set_sw_property version 1.0

set_sw_property min_compatible_hw_version 1.0

set_sw_property auto_initialize false

set_sw_property bsp_subdirectory drivers

add_sw_property c_source HAL/src/fabric_udp_stream.c

add_sw_property include_source HAL/inc/fabric_udp_stream.h
add_sw_property include_source inc/fabric_udp_stream_regs.h

add_sw_property supported_bsp_type HAL
add_sw_property supported_bsp_type UCOSII
