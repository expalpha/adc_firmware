# TCL File Generated by Component Editor 14.0
# Wed Feb 25 16:27:19 PST 2015
# DO NOT MODIFY


# 
# fabric_udp_stream "Fabric UDP Stream Inserter" v1.0
#  2015.02.25.16:27:19
# 
# 

# 
# request TCL package from ACDS 14.0
# 
package require -exact qsys 14.0


# 
# module fabric_udp_stream
# 
set_module_property DESCRIPTION ""
set_module_property NAME fabric_udp_stream
set_module_property VERSION 1.0
set_module_property INTERNAL false
set_module_property OPAQUE_ADDRESS_MAP true
set_module_property AUTHOR ""
set_module_property DISPLAY_NAME "Fabric UDP Stream Inserter"
set_module_property INSTANTIATE_IN_SYSTEM_MODULE true
set_module_property EDITABLE true
set_module_property REPORT_TO_TALKBACK false
set_module_property ALLOW_GREYBOX_GENERATION false
set_module_property REPORT_HIERARCHY false


# 
# file sets
# 
add_fileset QUARTUS_SYNTH QUARTUS_SYNTH "" ""
set_fileset_property QUARTUS_SYNTH TOP_LEVEL fabric_udp_stream
set_fileset_property QUARTUS_SYNTH ENABLE_RELATIVE_INCLUDE_PATHS false
set_fileset_property QUARTUS_SYNTH ENABLE_FILE_OVERWRITE_MODE false
add_fileset_file fabric_udp_stream.v VERILOG PATH fabric_udp_stream.v TOP_LEVEL_FILE
add_fileset_file udp_payload_inserter.v VERILOG PATH udp_payload_inserter.v
add_fileset_file alignment_pad_inserter.v VERILOG PATH alignment_pad_inserter.v


# 
# parameters
# 


# 
# display items
# 

# 
# connection point clk
# 
add_interface clk clock end
set_interface_property clk clockRate 0
set_interface_property clk ENABLED true
set_interface_property clk EXPORT_OF ""
set_interface_property clk PORT_NAME_MAP ""
set_interface_property clk CMSIS_SVD_VARIABLES ""
set_interface_property clk SVD_ADDRESS_GROUP ""

add_interface_port clk csi_clock_clk clk Input 1

# 
# connection point rst
# 
add_interface rst reset end
set_interface_property rst associatedClock clk
set_interface_property rst synchronousEdges DEASSERT
set_interface_property rst ENABLED true
set_interface_property rst EXPORT_OF ""
set_interface_property rst PORT_NAME_MAP ""
set_interface_property rst CMSIS_SVD_VARIABLES ""
set_interface_property rst SVD_ADDRESS_GROUP ""

add_interface_port rst csi_clock_reset reset Input 1


# 
# connection point s0
# 
add_interface s0 avalon end
set_interface_property s0 addressUnits WORDS
set_interface_property s0 associatedClock clk
set_interface_property s0 associatedReset rst
set_interface_property s0 bitsPerSymbol 8
set_interface_property s0 burstOnBurstBoundariesOnly false
set_interface_property s0 burstcountUnits WORDS
set_interface_property s0 explicitAddressSpan 0
set_interface_property s0 holdTime 0
set_interface_property s0 linewrapBursts false
set_interface_property s0 maximumPendingReadTransactions 0
set_interface_property s0 maximumPendingWriteTransactions 0
set_interface_property s0 readLatency 0
set_interface_property s0 readWaitTime 1
set_interface_property s0 setupTime 0
set_interface_property s0 timingUnits Cycles
set_interface_property s0 writeWaitTime 0
set_interface_property s0 ENABLED true
set_interface_property s0 EXPORT_OF ""
set_interface_property s0 PORT_NAME_MAP ""
set_interface_property s0 CMSIS_SVD_VARIABLES ""
set_interface_property s0 SVD_ADDRESS_GROUP ""

add_interface_port s0 avs_s0_write write Input 1
add_interface_port s0 avs_s0_read read Input 1
add_interface_port s0 avs_s0_address address Input 4
add_interface_port s0 avs_s0_byteenable byteenable Input 4
add_interface_port s0 avs_s0_writedata writedata Input 32
add_interface_port s0 avs_s0_readdata readdata Output 32
set_interface_assignment s0 embeddedsw.configuration.isFlash 0
set_interface_assignment s0 embeddedsw.configuration.isMemoryDevice 0
set_interface_assignment s0 embeddedsw.configuration.isNonVolatileStorage 0
set_interface_assignment s0 embeddedsw.configuration.isPrintableDevice 0

# 
# connection point snk0
# 
add_interface snk0 avalon_streaming end
set_interface_property snk0 associatedClock clk
set_interface_property snk0 associatedReset rst
set_interface_property snk0 dataBitsPerSymbol 8
set_interface_property snk0 symbolsPerBeat 4
set_interface_property snk0 errorDescriptor ""
set_interface_property snk0 firstSymbolInHighOrderBits true
set_interface_property snk0 maxChannel 0
set_interface_property snk0 readyLatency 0
set_interface_property snk0 ENABLED true
set_interface_property snk0 EXPORT_OF ""
set_interface_property snk0 PORT_NAME_MAP ""
set_interface_property snk0 CMSIS_SVD_VARIABLES ""
set_interface_property snk0 SVD_ADDRESS_GROUP ""

add_interface_port snk0 asi_snk0_valid valid Input 1
add_interface_port snk0 asi_snk0_ready ready Output 1
add_interface_port snk0 asi_snk0_data data Input 32
add_interface_port snk0 asi_snk0_empty empty Input 2
add_interface_port snk0 asi_snk0_startofpacket startofpacket Input 1
add_interface_port snk0 asi_snk0_endofpacket endofpacket Input 1



# 
# connection point src0
# 
add_interface src0 avalon_streaming start
set_interface_property src0 associatedClock clk
set_interface_property src0 associatedReset rst
set_interface_property src0 dataBitsPerSymbol 8
set_interface_property src0 symbolsPerBeat 4
set_interface_property src0 errorDescriptor ""
set_interface_property src0 firstSymbolInHighOrderBits true
set_interface_property src0 maxChannel 0
set_interface_property src0 readyLatency 0
set_interface_property src0 ENABLED true
set_interface_property src0 EXPORT_OF ""
set_interface_property src0 PORT_NAME_MAP ""
set_interface_property src0 CMSIS_SVD_VARIABLES ""
set_interface_property src0 SVD_ADDRESS_GROUP ""

add_interface_port src0 aso_src0_valid valid Output 1
add_interface_port src0 aso_src0_ready ready Input 1
add_interface_port src0 aso_src0_data data Output 32
add_interface_port src0 aso_src0_empty empty Output 2
add_interface_port src0 aso_src0_startofpacket startofpacket Output 1
add_interface_port src0 aso_src0_endofpacket endofpacket Output 1





