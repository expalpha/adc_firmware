module board #(
	parameter SZ_DATA = 32,
	parameter SZ_TIMESTAMP = 48,
	parameter NUM_ADC = 4,
	parameter NUM_FMC_ADC = 4,
	parameter REF_CLK_FREQ = 12500000
)(
	// Avalon Interface
	clk, 
	rst, 
	
	s0_address, 
	s0_read,
	s0_readdata, 
	s0_readdatavalid, 
	s0_byteenable,

	s1_address, 
	s1_read,
	s1_readdata, 
	s1_readdatavalid, 
	s1_byteenable,
	s1_write, 
	s1_writedata,
		
	// Conduits
	clk_eth,
	clk_adc,
	clk_fmc_adc,
	clk_esata,
	clk_nim,
	clk_clean0,
	clk_clean1,
	
	// Control Signals
	temp_rst,
	adc_sync,
	sfp_tx_disable,
	reset_serdes,
	nim_trig_ena,
	nim_trig_inv,
	esata_trig_ena,
	esata_trig_inv,
	
	// Status Signal Inputs
	lmk_clkin_sel,
	refclk_sel,
	lmk_statusld,
	sfp_los,	
	sfp_tx_fault,
	usb_bus,
	adc_locked,
	adc_aligned,
	fmc_adc_locked,
	fmc_adc_aligned,
	sd_detect,
	
	nim_trig_in,
	esata_trig_in,
	
	fpga_temp_rdbk,
	fpga_timestamp,

	pll_ethernet_locked,
	fmc_present_n,

	// Other
	chipid_data,
	chipid_valid,
	mac_addr,
	force_run,
	module_id
);

// Control Registers
localparam TEMP_RST			= 0;
localparam ADC_SYNC			= 1;
localparam SFP_TX_DISABLE	= 2;
localparam RESET_SERDES		= 3;
localparam NIM_TRIG_ENA		= 4;
localparam NIM_TRIG_INV		= 5;
localparam ESATA_TRIG_ENA 	= 6;
localparam ESATA_TRIG_INV	= 7;
localparam MAC_ADDR1		= 8;
localparam MAC_ADDR0		= 9;
localparam MODULE_ID		= 10;
localparam FORCE_RUN		= 11;
localparam NUM_CTRL_REGS	= 12;

// Status Registers
localparam CHIPID_LO			= 0;
localparam CHIPID_HI			= 1;
localparam FPGA_TIMESTAMP		= 2;
localparam FPGA_TEMP			= 3;
localparam LMK_STATUSLD			= 4;
localparam SFP_LOS				= 5;
localparam SFP_TX_FAULT			= 6;
localparam FREQ_ADC_CNT			= 7;
localparam FREQ_FMC_ADC_CNT 	= 8;
localparam FREQ_ESATA_CNT 		= 9;
localparam FREQ_NIM_CNT 		= 10;
localparam ADC_LOCKED			= 11;
localparam ADC_ALIGNED			= 12;
localparam ADC_LOCK_CNT0 		= 13;
localparam ADC_LOCK_CNT1 		= 14;
localparam ADC_LOCK_CNT2 		= 15;
localparam ADC_LOCK_CNT3 		= 16;
localparam ADC_ALIGN_CNT0 		= 17;
localparam ADC_ALIGN_CNT1 		= 18;
localparam ADC_ALIGN_CNT2 		= 19;
localparam ADC_ALIGN_CNT3 		= 20;

localparam FMC_LOCKED			= 21;
localparam FMC_ALIGNED			= 22;
localparam FMC_LOCK_CNT0 		= 23;
localparam FMC_LOCK_CNT1 		= 24;
localparam FMC_LOCK_CNT2 		= 25;
localparam FMC_LOCK_CNT3 		= 26;
localparam FMC_ALIGN_CNT0 		= 27;
localparam FMC_ALIGN_CNT1 		= 28;
localparam FMC_ALIGN_CNT2 		= 29;
localparam FMC_ALIGN_CNT3 		= 30;

localparam USB_BUS				= 31;
localparam SD_DETECT			= 32;
localparam NIM_TRIG_STAT		= 33;
localparam NIM_TRIG_CNT			= 34;
localparam ESATA_TRIG_STAT		= 35;
localparam ESATA_TRIG_CNT		= 36;
localparam PLL_ETHERNET_LOCKED	= 37;
localparam FREQ_CLEAN0_CNT 		= 38;
localparam FREQ_CLEAN1_CNT 		= 39;
localparam FREQ_ETH_CNT			= 40;
localparam FMC_PRESENTn			= 41;
localparam REFCLK_SEL			= 42;
localparam LMK_CLKIN_SEL		= 43;
localparam NUM_STAT_REGS		= 44;

localparam SZ_STAT = $clog2(NUM_STAT_REGS);
localparam SZ_CTRL = $clog2(NUM_CTRL_REGS);
localparam SZ_ADDR_STAT = SZ_STAT;
localparam SZ_ADDR_CTRL = SZ_CTRL;

// Clock and Resets
input        				clk;
input 						rst;

// Avalon 0 - Status Registers 
input  [SZ_ADDR_STAT-1:0] 	s0_address;	// lower address bits
input        				s0_read;	// valid bus cycle input
input	[3:0]						s0_byteenable;
output [SZ_DATA-1:0] 		s0_readdata;	// data bus input
output      				s0_readdatavalid;	// bus cycle acknowledge output

// Avalon 1 - Control Registers
input  [SZ_ADDR_CTRL-1:0] 	s1_address;	// lower address bits
input        				s1_read;	// valid bus cycle input
output [SZ_DATA-1:0] 		s1_readdata;	// data bus input
output      				s1_readdatavalid;	// bus cycle acknowledge output
input  [3:0] 				s1_byteenable;
input        				s1_write;	// write enable input
input  [SZ_DATA-1:0] 		s1_writedata;	// data bus output

// Conduits
output wire					temp_rst;
output wire	 				adc_sync;
output wire					sfp_tx_disable;
output wire					reset_serdes;
output wire 				nim_trig_ena;
output wire 				nim_trig_inv;
output wire 				esata_trig_ena;
output wire 				esata_trig_inv;
output wire [47:0]			mac_addr;
output wire [7:0]			module_id;
output wire 				force_run;

input wire					clk_eth;
input wire 					clk_adc;
input wire 					clk_fmc_adc;
input wire					clk_esata;
input wire					clk_nim;
input wire					clk_clean0;
input wire					clk_clean1;
input wire [NUM_ADC-1:0] 	adc_locked;			// 1 signal per ADC
input wire [NUM_ADC-1:0] 	adc_aligned;		// 1 signal per ADC (one FCO)
input wire [NUM_FMC_ADC-1:0] 	fmc_adc_locked;			// 1 signal per ADC
input wire [NUM_FMC_ADC-1:0] 	fmc_adc_aligned;		// 1 signal per ADC (one FCO)
input wire [1:0]			lmk_statusld;
input wire					sfp_los;
input wire 					sfp_tx_fault;
input wire					usb_bus;
input wire					sd_detect;
input wire [7:0]			fpga_temp_rdbk;
input wire [31:0]			fpga_timestamp;
input wire [63:0] 		chipid_data;
input wire 					chipid_valid;
input wire					nim_trig_in;
input wire					esata_trig_in;
input wire					pll_ethernet_locked;
input wire					fmc_present_n;
input wire [1:0] 			lmk_clkin_sel;
input wire [1:0] 			refclk_sel;


wire [NUM_CTRL_REGS-1:0][SZ_DATA-1:0] ctrl;
wire [NUM_STAT_REGS-1:0][SZ_DATA-1:0] status;
wire [7:0] w_fpga_temp_rdbk;

// Setup Control Registers
assign temp_rst						= ctrl[TEMP_RST]		[0];
assign adc_sync						= ctrl[ADC_SYNC]		[0];
assign sfp_tx_disable				= ctrl[SFP_TX_DISABLE]	[0];
assign reset_serdes					= ctrl[RESET_SERDES]	[0];
assign nim_trig_ena					= ctrl[NIM_TRIG_ENA]	[0];
assign nim_trig_inv					= ctrl[NIM_TRIG_INV]	[0];
assign esata_trig_ena				= ctrl[ESATA_TRIG_ENA]	[0];
assign esata_trig_inv				= ctrl[ESATA_TRIG_INV]	[0];
assign mac_addr						= {ctrl[MAC_ADDR1][15:0], ctrl[MAC_ADDR0][31:0]};
assign force_run					= ctrl[FORCE_RUN][0];
assign module_id					= ctrl[MODULE_ID][7:0];

// Synchronize incoming signals to NIOS clock
synchronizer #( .SZ_DATA( 32 ) ) sync_fpga_ts			( .clk	( clk ),	.rst	( rst ), .d ( fpga_timestamp ),		.q ( status[FPGA_TIMESTAMP][31:0] ));
synchronizer #( .SZ_DATA( 32 ) ) sync_fpga_lo			( .clk	( clk ),	.rst	( rst ), .d ( chipid_data[31:0] ),	.q ( status[CHIPID_LO][31:0] ));
synchronizer #( .SZ_DATA( 32 ) ) sync_fpga_hi			( .clk	( clk ),	.rst	( rst ), .d ( chipid_data[63:32] ),	.q ( status[CHIPID_HI][31:0] ));
synchronizer #( .SZ_DATA( 2 ) ) sync_lmk_status			( .clk	( clk ),	.rst	( rst ), .d ( lmk_statusld ),			.q ( {status[LMK_STATUSLD][8], status[LMK_STATUSLD][0]} ));
synchronizer #( .SZ_DATA( 1 ) ) sync_sfp_los			( .clk	( clk ),	.rst	( rst ), .d ( sfp_los ),				.q ( status[SFP_LOS][0] ));
synchronizer #( .SZ_DATA( 1 ) ) sync_sfp_tx_fault		( .clk	( clk ),	.rst	( rst ), .d ( sfp_tx_fault ),			.q ( status[SFP_TX_FAULT][0] ));
synchronizer #( .SZ_DATA( 1 ) ) sync_usb_bus			( .clk	( clk ),	.rst	( rst ), .d ( usb_bus ),				.q ( status[USB_BUS][0] ));
synchronizer #( .SZ_DATA( 1 ) ) sync_sd_detect			( .clk	( clk ),	.rst	( rst ), .d ( sd_detect ),				.q ( status[SD_DETECT][0] ));
synchronizer #( .SZ_DATA( 8 ) ) sync_fpga_temp			( .clk	( clk ),	.rst	( rst ), .d ( fpga_temp_rdbk ),		.q ( status[FPGA_TEMP][7:0] ));
synchronizer #( .SZ_DATA( 4 ) ) sync_adc_locked			( .clk	( clk ),	.rst	( rst ), .d ( adc_locked ),			.q ( {status[ADC_LOCKED][24], status[ADC_LOCKED][16], status[ADC_LOCKED][8], status[ADC_LOCKED][0]} ));
synchronizer #( .SZ_DATA( 4 ) ) sync_adc_aligned 		( .clk	( clk ),	.rst	( rst ), .d ( adc_aligned ),			.q ( {status[ADC_ALIGNED][24], status[ADC_ALIGNED][16], status[ADC_ALIGNED][8], status[ADC_ALIGNED][0]} ));
synchronizer #( .SZ_DATA( 4 ) ) sync_fmc_locked			( .clk	( clk ),	.rst	( rst ), .d ( fmc_adc_locked ),		.q ( {status[FMC_LOCKED][24], status[FMC_LOCKED][16], status[FMC_LOCKED][8], status[FMC_LOCKED][0]} ));
synchronizer #( .SZ_DATA( 4 ) ) sync_fmc_aligned 		( .clk	( clk ),	.rst	( rst ), .d ( fmc_adc_aligned ),		.q ( {status[FMC_ALIGNED][24], status[FMC_ALIGNED][16], status[FMC_ALIGNED][8], status[FMC_ALIGNED][0]} ));
synchronizer #( .SZ_DATA( 1 ) ) sync_nim_in 			( .clk	( clk ),	.rst	( rst ), .d ( nim_trig_in ),			.q ( status[NIM_TRIG_STAT][0] ));
synchronizer #( .SZ_DATA( 1 ) ) sync_esata_in 			( .clk	( clk ),	.rst	( rst ), .d ( esata_trig_in ),		.q ( status[ESATA_TRIG_STAT][0] ));
synchronizer #( .SZ_DATA( 1 ) ) sync_ethernet_pll 		( .clk	( clk ),	.rst	( rst ), .d ( pll_ethernet_locked ),.q ( status[PLL_ETHERNET_LOCKED][0] ));
synchronizer #( .SZ_DATA( 1 ) ) sync_fmc_presentn 		( .clk	( clk ),	.rst	( rst ), .d ( fmc_present_n ),		.q ( status[FMC_PRESENTn][0] ));
synchronizer #( .SZ_DATA( 2 ) ) sync_lmk_clkin_sel 		( .clk	( clk ),	.rst	( rst ), .d ( lmk_clkin_sel ),		.q ( status[LMK_CLKIN_SEL][1:0] ));
synchronizer #( .SZ_DATA( 2 ) ) sync_refclk_sel 		( .clk	( clk ),	.rst	( rst ), .d ( refclk_sel ),		.q ( status[REFCLK_SEL][1:0] ));

// Frequency Counters
freq_counter #( 
	.WIDTH(SZ_DATA), .REF_FREQ(REF_CLK_FREQ) 
) freq_count_fpga_fmc_adc (
	.clk		( clk ), .rst		( rst ),	.clk_freq( clk_fmc_adc ), 	.q ( status[FREQ_FMC_ADC_CNT] ) 
);

freq_counter #( 
	.WIDTH(SZ_DATA), .REF_FREQ(REF_CLK_FREQ) 
) freq_count_fpga_adc (
	.clk		( clk ), .rst		( rst ),	.clk_freq( clk_adc ), 	.q ( status[FREQ_ADC_CNT] ) 
);

freq_counter #( 
	.WIDTH(SZ_DATA), .REF_FREQ(REF_CLK_FREQ) 
) freq_count_esata (
	.clk		( clk ), .rst		( rst ),	.clk_freq( clk_esata ), 	.q ( status[FREQ_ESATA_CNT] ) 
);

freq_counter #( 
	.WIDTH(SZ_DATA), .REF_FREQ(REF_CLK_FREQ) 
) freq_count_nim (
	.clk		( clk ), .rst		( rst ),	.clk_freq( clk_nim ), 	.q ( status[FREQ_NIM_CNT] ) 
);

freq_counter #( 
	.WIDTH(SZ_DATA), .REF_FREQ(REF_CLK_FREQ) 
) freq_count_clean0 (
	.clk		( clk ), .rst		( rst ),	.clk_freq( clk_clean0 ), 	.q ( status[FREQ_CLEAN0_CNT] ) 
);

freq_counter #( 
	.WIDTH(SZ_DATA), .REF_FREQ(REF_CLK_FREQ) 
) freq_count_clean1 (
	.clk		( clk ), .rst		( rst ),	.clk_freq( clk_clean1 ), 	.q ( status[FREQ_CLEAN1_CNT] ) 
);

freq_counter #( 
	.WIDTH(SZ_DATA), .REF_FREQ(REF_CLK_FREQ) 
) freq_count_eth (
	.clk		( clk ), .rst		( rst ),	.clk_freq( clk_eth ), 	.q ( status[FREQ_ETH_CNT] ) 
);

pattern_counter #(
	.SZ_COUNTER(SZ_DATA),
	.SZ_PATTERN(2)
) nim_in_counter (
	.clk				( clk ),
	.clk_cap 	( clk  ),
	.rst				( rst ),
	.pattern 	( 2'b01 ),
	.d				( nim_trig_in ),
	.q				( status[NIM_TRIG_CNT] )
);			

pattern_counter #(
	.SZ_COUNTER(SZ_DATA),
	.SZ_PATTERN(2)
) esata_in_counter (
	.clk				( clk ),
	.clk_cap 	( clk ),
	.rst				( rst ),
	.pattern 	( 2'b01 ),
	.d				( esata_trig_in ),
	.q				( status[ESATA_TRIG_CNT] )
);		

genvar n;

// ADC Signals 
generate 
	for(n=0; n<NUM_ADC; n++) begin: generate_adc_loss_counters
		pattern_counter #(
			.SZ_COUNTER(SZ_DATA),
			.SZ_PATTERN(2)
		) adc_lock_counter (
			.clk		( clk ),
			.clk_cap ( clk_adc ),
			.rst		( rst | reset_serdes),
			.pattern ( 2'b10 ),
			.d			( adc_locked[n] ),
			.q			( status[ADC_LOCK_CNT0+n] )
		);
		
		pattern_counter #(
			.SZ_COUNTER(SZ_DATA),
			.SZ_PATTERN(2)
		) adc_align_counter (
			.clk		( clk ),
			.clk_cap ( clk_adc ),
			.rst		( rst | reset_serdes ),
			.pattern ( 2'b10 ),
			.d			( adc_aligned[n] ),
			.q			( status[ADC_ALIGN_CNT0+n] )
		);
		
	end 
endgenerate 

// FMC ADC Signals 
generate 
	for(n=0; n<NUM_FMC_ADC; n++) begin: generate_fmc_adc_loss_counters
		pattern_counter #(
			.SZ_COUNTER(SZ_DATA),
			.SZ_PATTERN(2)
		) fmc_adc_lock_counter (
			.clk		( clk ),
			.clk_cap ( clk_fmc_adc ),
			.rst		( rst  | reset_serdes),
			.pattern ( 2'b10 ),
			.d			( fmc_adc_locked[n] ),
			.q			( status[FMC_LOCK_CNT0+n] )
		);
		
		pattern_counter #(
			.SZ_COUNTER(SZ_DATA),
			.SZ_PATTERN(2)
		) fmc_adc_align_counter (
			.clk		( clk ),
			.clk_cap ( clk_fmc_adc ),
			.rst		( rst  | reset_serdes),
			.pattern ( 2'b10 ),
			.d			( fmc_adc_aligned[n] ),
			.q			( status[FMC_ALIGN_CNT0+n] )
		);
		
	end 
endgenerate 

avalon_stat_slave #(
	.NUM_STAT_REGS(NUM_STAT_REGS),
	.SZ_WIDTH(SZ_DATA)
) board_stat (	
	.clk							( clk ), 
	.rst							( rst ), 
	.address				( s0_address ),
	.byteenable			( s0_byteenable ),
	.read						( s0_read ), 
	.readdata			( s0_readdata ),
	.readdatavalid	( s0_readdatavalid  ), 
	.status 					( status )
);

avalon_ctrl_slave #(
	.NUM_CTRL_REGS(NUM_CTRL_REGS),
	.SZ_WIDTH(SZ_DATA)
) board_ctrl (
	.clk							( clk ), 
	.rst							( rst ), 
	.address				( s1_address ),
	.write					( s1_write ), 
	.writedata			( s1_writedata ),
	.byteenable     ( s1_byteenable ),
	.read						( s1_read ), 
	.readdata			( s1_readdata ), 
	.readdatavalid	( s1_readdatavalid ), 
	.ctrl						( ctrl )
);

endmodule
