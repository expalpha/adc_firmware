// TRIUMF Electronics Group
// GRIF-16 Rev.1 module for AlphaG TPC detector
// FMC-ADC32 Rev.1.1 module for AlpgaG TPC detector

//
// Clock frequencies from Timequest:
//
// Clock CLK_125MHzT : Freq = 125.0
// Clock CLK_125MHzB : Freq = 125.0
//
// Clock FPGA_ClnClk0 : Freq = 125.0
// Clock FPGA_ClnClk1 : Freq = 125.0
//
// Clock NIM_CLK : Freq = 62.5
// Clock eSATA_CLK : Freq = 62.5
//
// CLK_100MHz1 - 100 MHz - where is this used?
// CLK_100MHz2 - 100 MHz - where is this used?
//
//
// PLL pll_data, refclk FPGA_ClnClk[1], 125 MHz:
//
// Clock pll_data|data_pll_inst|altera_pll_i|general[0].gpll~PLL_OUTPUT_COUNTER|divclk : Freq = 125.0 : clk_125
// Clock pll_data|data_pll_inst|altera_pll_i|general[1].gpll~PLL_OUTPUT_COUNTER|divclk : Freq = 100.0 : clk_100
// Clock pll_data|data_pll_inst|altera_pll_i|general[2].gpll~PLL_OUTPUT_COUNTER|divclk : Freq = 62.5  : clk_62_5
// Clock pll_data|data_pll_inst|altera_pll_i|general[3].gpll~PLL_OUTPUT_COUNTER|divclk : Freq = 50.0  : clk_50
// Clock pll_data|data_pll_inst|altera_pll_i|general[4].gpll~PLL_OUTPUT_COUNTER|divclk : Freq = 12.5  : clk_12_5
//
// adc16 receive clocks
//
// ADC_DCO_A - ADC_DCO[0] - 400 MHz
// ADC_DCO_B - ADC_DCO[1] - 400 MHz
// ADC_DCO_C - ADC_DCO[2] - 400 MHz
// ADC_DCO_D - ADC_DCO[3] - 400 MHz
//
// adc32 receive clocks
//
// ADC32_DCO_A -period "437.5 MHz" - FMC_ADC1_DCO1 - 437.5 MHz = 7*62.5 MHz
// ADC32_DCO_B -period "437.5 MHz" - FMC_ADC1_DCO2
// ADC32_DCO_C -period "437.5 MHz" - FMC_ADC2_DCO1
// ADC32_DCO_D -period "437.5 MHz" - FMC_ADC2_DCO2
//
// adc16 recovered receive clocks (100 Ms/s, 100 MHz)
//
// Clock adc16_inst|gen_adc[0].serdes_inst|ALTLVDS_RX_component|auto_generated|pll_sclk~PLL_OUTPUT_COUNTER|divclk : Freq = 100.0
// Clock adc16_inst|gen_adc[1].serdes_inst|ALTLVDS_RX_component|auto_generated|pll_sclk~PLL_OUTPUT_COUNTER|divclk : Freq = 100.0
// Clock adc16_inst|gen_adc[2].serdes_inst|ALTLVDS_RX_component|auto_generated|pll_sclk~PLL_OUTPUT_COUNTER|divclk : Freq = 100.0
// Clock adc16_inst|gen_adc[3].serdes_inst|ALTLVDS_RX_component|auto_generated|pll_sclk~PLL_OUTPUT_COUNTER|divclk : Freq = 100.0
//
// adc32 recovered receive clocks (62.5 Ms/s, 125 MHz)
//
// Clock ADC32_A_OUT : Freq = 125.0
// Clock ADC32_B_OUT : Freq = 125.0
// Clock ADC32_C_OUT : Freq = 125.0
// Clock ADC32_D_OUT : Freq = 125.0
//
// Clock ag_sas|sas_block|gx_inst|gx_xcvr_inst|gen_native_inst.av_xcvr_native_insts[0].gen_bonded_group_native.av_xcvr_native_inst|inst_av_pcs|ch[0].inst_av_pcs_ch|inst_av_hssi_8g_tx_pcs|wys|txpmalocalclk : Freq = 125.0
// Clock ag_sas|sas_block|gx_inst|gx_xcvr_inst|gen_native_inst.av_xcvr_native_insts[1].gen_bonded_group_native.av_xcvr_native_inst|inst_av_pcs|ch[0].inst_av_pcs_ch|inst_av_hssi_8g_tx_pcs|wys|txpmalocalclk : Freq = 125.0
// Clock ag_sas|sas_block|gx_inst|gx_xcvr_inst|gen_native_inst.av_xcvr_native_insts[2].gen_bonded_group_native.av_xcvr_native_inst|inst_av_pcs|ch[0].inst_av_pcs_ch|inst_av_hssi_8g_tx_pcs|wys|txpmalocalclk : Freq = 125.0
// Clock ag_sas|sas_block|gx_inst|gx_xcvr_inst|gen_native_inst.av_xcvr_native_insts[3].gen_bonded_group_native.av_xcvr_native_inst|inst_av_pcs|ch[0].inst_av_pcs_ch|inst_av_hssi_8g_tx_pcs|wys|txpmalocalclk : Freq = 125.0
// Clock ag_sas|sas_block|gx_inst|gx_xcvr_inst|gen_native_inst.av_xcvr_native_insts[4].gen_bonded_group_native.av_xcvr_native_inst|inst_av_pcs|ch[0].inst_av_pcs_ch|inst_av_hssi_8g_tx_pcs|wys|txpmalocalclk : Freq = 125.0
// Clock ag_sas|sas_block|gx_inst|gx_xcvr_inst|gen_native_inst.av_xcvr_native_insts[5].gen_bonded_group_native.av_xcvr_native_inst|inst_av_pcs|ch[0].inst_av_pcs_ch|inst_av_hssi_8g_tx_pcs|wys|txpmalocalclk : Freq = 125.0
// Clock ag_sas|sas_block|gx_inst|gx_xcvr_inst|gen_native_inst.av_xcvr_native_insts[6].gen_bonded_group_native.av_xcvr_native_inst|inst_av_pcs|ch[0].inst_av_pcs_ch|inst_av_hssi_8g_tx_pcs|wys|txpmalocalclk : Freq = 125.0
// Clock ag_sas|sas_block|gx_inst|gx_xcvr_inst|gen_native_inst.av_xcvr_native_insts[7].gen_bonded_group_native.av_xcvr_native_inst|inst_av_pcs|ch[0].inst_av_pcs_ch|inst_av_hssi_8g_tx_pcs|wys|txpmalocalclk : Freq = 125.0
//
// Clock mgmnt_qsys_inst|eth_tse|i_custom_phyip_0|A5|transceiver_core|gen.av_xcvr_native_insts[0].gen_bonded_group.av_xcvr_native_inst|inst_av_pcs|ch[0].inst_av_pcs_ch|inst_av_hssi_8g_rx_pcs|wys|rcvdclkpma : Freq = 125.0
// Clock mgmnt_qsys_inst|eth_tse|i_custom_phyip_0|A5|transceiver_core|gen.av_xcvr_native_insts[0].gen_bonded_group.av_xcvr_native_inst|inst_av_pcs|ch[0].inst_av_pcs_ch|inst_av_hssi_8g_tx_pcs|wys|txpmalocalclk : Freq = 125.0
// Clock mgmnt_qsys_inst|eth_tse|i_custom_phyip_0|A5|transceiver_core|gen.av_xcvr_native_insts[0].gen_bonded_group.av_xcvr_native_inst|inst_av_pma|av_rx_pma|rx_pmas[0].rx_pma.rx_pma_deser|clk90b : Freq = 125.0
//
// Clock mgmnt_qsys_inst|mem_ddr|pll0|pll1~PLL_OUTPUT_COUNTER|divclk : Freq = 500.0
// Clock mgmnt_qsys_inst|mem_ddr|pll0|pll6~PLL_OUTPUT_COUNTER|divclk : Freq = 71.4285714286
// Clock mgmnt_qsys_inst|mem_ddr|pll0|pll7~PLL_OUTPUT_COUNTER|divclk : Freq = 23.8095238095
//
// Clock altera_reserved_tck : Freq = 30.000300003
//
// Clock transfers to investigate and fix:
//
// - tangle of mgmnt_qsys_inst|mem_ddr|pll0|pll1~PLL_OUTPUT_COUNTER|divclk in management:mgmnt_qsys_inst|management_mem_ddr:mem_ddr|altera_mem_if_hard_memory_controller_top_arriav
// - CLK_125MHzT and clk_12_5 via management:mgmnt_qsys_inst|management_pio_init_output:pio_init_output|data_out[5] to management:mgmnt_qsys_inst|management_spi_lmk48xx:spi_lmk48xx|MISO_reg (looks like "init" nios and "normal" nios are driving the LMK SPI pins from different clocks.

`default_nettype none

module alpha16_top (
    // SYSTEM CLOCKS
    input wire [1:0] 	  FPGA_ClnClk,  // [1] and [0] 125 MHz clocks, [4:2] - Upper three all connected to Transceiver REFCLKs, only useful for Transceiver Clocking, enable if required
    input wire 		  CLK_125MHzT,  // 125 MHz clock
    input wire 		  CLK_125MHzB,  // not used
    //input  wire         CLK_125MHz1L, // Doesn't connect to FPGA in design!
    input wire 		  CLK_125MHz3L, // eth_tse_pcs_ref_clk
    input wire [2:1] 	  CLK_100MHz,   // CLK_100MHz[0] is only useful for Transceiver Clocking, enable if required

    output wire 	  CLK_FPGA,

    // Front panel eSATA connector
    input wire 		  eSATA_SYNC,
    input wire 		  eSATA_CLK,

    //input  wire         CLK0_M2C,
    //output wire         CLK0_C2M,

    // GIGABIT TRANSCEIVERS
    input wire 		  SFP_RX,
    output wire 	  SFP_TX,

    /*
    // Pin Error between ALPHA16rev1 and FMC-32 Rev1 means
    // we have to short DAC2 and DAC1's SCK and SDI pins in order to work. 
    output wire		FMC_SPI_DAC1_SCK,    
    output wire		FMC_SPI_DAC1_SDI,
    input wire 		FMC_SPI_DAC1_SDO,
    */
    output wire 	  FMC_SPI_DAC1_CSn,

    output wire 	  FMC_SPI_DAC2_SCK,
    output wire 	  FMC_SPI_DAC2_CSn,
    output wire 	  FMC_SPI_DAC2_SDI,
    input wire 		  FMC_SPI_DAC2_SDO,
	 
    /*
    // input  wire [1:0]  GBTCLK_M2C,  	// xCVR Reference Clock
    input  wire	[3:3]     DP_M2C,
    output wire	[3:3]     DP_C2M,
    */

    input wire 		  FMC_ADC2_A1,
    input wire 		  FMC_ADC2_B1,
    input wire 		  FMC_ADC2_C1,
    input wire 		  FMC_ADC2_D1,
    input wire 		  FMC_ADC2_E1,
    input wire 		  FMC_ADC2_F1,
    input wire 		  FMC_ADC2_G1,
    input wire 		  FMC_ADC2_H1,

    input wire 		  FMC_ADC2_A2,
    input wire 		  FMC_ADC2_B2,
    input wire 		  FMC_ADC2_C2,
    input wire 		  FMC_ADC2_D2,
    input wire 		  FMC_ADC2_E2,
    input wire 		  FMC_ADC2_F2,
    input wire 		  FMC_ADC2_G2,
    input wire 		  FMC_ADC2_H2,

    input wire 		  FMC_ADC2_FCO1,
    input wire 		  FMC_ADC2_FCO2,
    input wire 		  FMC_ADC2_DCO1,
    input wire 		  FMC_ADC2_DCO2,

    input wire 		  FMC_ADC1_A1,
    input wire 		  FMC_ADC1_B1,
    input wire 		  FMC_ADC1_C1,
    input wire 		  FMC_ADC1_D1,
    input wire 		  FMC_ADC1_E1,
    input wire 		  FMC_ADC1_F1,
    input wire 		  FMC_ADC1_G1,
    input wire 		  FMC_ADC1_H1,

    input wire 		  FMC_ADC1_A2,
    input wire 		  FMC_ADC1_B2,
    input wire 		  FMC_ADC1_C2,
    input wire 		  FMC_ADC1_D2,
    input wire 		  FMC_ADC1_E2,
    input wire 		  FMC_ADC1_F2,
    input wire 		  FMC_ADC1_G2,
    input wire 		  FMC_ADC1_H2,

    input wire 		  FMC_ADC1_FCO1,
    input wire 		  FMC_ADC1_FCO2,
    input wire 		  FMC_ADC1_DCO1,
    input wire 		  FMC_ADC1_DCO2,

    inout wire 		  FMC_ADC_SDIO1,
    inout wire 		  FMC_ADC_SDIO2,
    output wire 	  FMC_ADC_DIR1,
    output wire 	  FMC_ADC_DIR2,
    output wire 	  FMC_ADC_SCK,
    output wire 	  FMC_ADC_CS0n,
    output wire 	  FMC_ADC_CS1n,
    output wire 	  FMC_ADC_CS2n,
    output wire 	  FMC_ADC_CS3n,

    output wire 	  FMC_ADC_SYNC,


    // QUAD-ADC's
    output wire [3:0] 	  ADC_CSn,
    output wire 	  ADC_SCK,
    inout wire 		  ADC_SDIO,
    output wire 	  ADC_SYNC,
    input wire [3:0] 	  ADC_FCO,
    input wire [3:0] 	  ADC_DCO,
    input wire [3:0][1:0] ADC_DA,
    input wire [3:0][1:0] ADC_DB,
    input wire [3:0][1:0] ADC_DC,
    input wire [3:0][1:0] ADC_DD,

    // DAC
    inout wire 		  DAC_SMB_SCL,
    inout wire 		  DAC_SMB_SDA,
    output wire [13:0] 	  DAC_D, /* synthesis useioff = 1 */
    output wire 	  DAC_XOR,
    output wire 	  DAC_SELIQ, /* synthesis useioff = 1 */
    output wire 	  DAC_TORB,
    output wire 	  DAC_PD,

    // SFP CONTROL AND STATUS
    input wire 		  SFP_TX_FAULT,
    output wire 	  SFP_TX_DISABLE,
    inout wire 		  SFP_SCL,
    inout wire 		  SFP_SDA,
    output wire 	  SFP_MODDET,
    output wire 	  SFP_RATESELECT,
    input wire 		  SFP_LOS,

    output wire 	  TEMP_RSTn,
    output wire 	  TEMP_SCK,
    output wire 	  TEMP_SDI,
    output wire 	  TEMP_CSn,
    input wire 		  TEMP_SDO,

    // DDR3
    output wire [13:0] 	  DDR3_A,
    output wire [2:0] 	  DDR3_BA,
    output wire 	  DDR3_CASn,
    output wire 	  DDR3_CK, // DIFFERENTIAL
    output wire 	  DDR3_CK_n, // DIFFERENTIAL
    output wire 	  DDR3_CKE,
    output wire 	  DDR3_CSn,
    output wire [3:0] 	  DDR3_DM,
    inout wire [31:0] 	  DDR3_DQ,
    inout wire [3:0] 	  DDR3_DQS, // DIFFERENTIAL
    inout wire [3:0] 	  DDR3_DQS_n, // DIFFERENTIAL
    output wire 	  DDR3_ODT,
    output wire 	  DDR3_RASn,
    output wire 	  DDR3_RESETn,
    output wire 	  DDR3_WEn,
    output wire 	  DDR3_VTT_EN,
    input wire 		  DDR3_PG,
    input wire 		  DDR3_OCT_RZQ6,

    // SD CARD
    input wire 		  SD_DETECT,
    inout wire [3:0] 	  SD_DAT,
    inout wire 		  SD_CMD,
    output wire 	  SD_CLK,

    // CLOCK CLEANER
    output wire [1:0] 	  LMK_CLKinSEL,
    output wire 	  LMK_CSn,
    input wire 		  LMK_MISO, // defaults to reset on powerup
    output wire 	  LMK_SCLK,
    output wire 	  LMK_MOSI,
    output wire 	  LMK_SYNC,
    input wire [2:1] 	  LMK_StatusLD,

    // MAC
    inout wire 		  MAC_SCL,
    inout wire 		  MAC_SDA,

    // DISPLAY
    output wire 	  DISP_RSTn,
    inout wire 		  DISP_SCL,
    inout wire 		  DISP_SDA,

    inout wire 		  FMC_SCL,
    inout wire 		  FMC_SDA,
    /*
    // FMC MEZZANINE
    input	 wire				FMC_PRSNTn,
    input	 wire	[15:0]	FMC_HA,   	// HA00,01,02,03,04,05,06,07,10,11,14,17,18,21,22,23
    output wire	[9:0]		FMC_HB, 	   // ACTUALLY HB00, 01, 06, 07, 10, 11, 14, 15, 17, 18
    */
    input wire [7:0] 	  DP_M2C,
    output wire [7:0] 	  DP_C2M,

    // USB - Note UART pins are reversed, so TX<->RX and CTSn<->RTSn when connected to QSYS
    input wire 		  USB_CBUS0,
    output wire 	  USB_CTSn,
    input wire 		  USB_RTSn,
    output wire 	  USB_RXD,
    input wire 		  USB_TXD,

    // MISCELLANEOUS
    output wire 	  FMC_SW_EN,

    output wire [1:0] 	  REFCLK_SEL,

    // Front panel LEMO connectors
    input wire 		  NIM_CLK,
    input wire 		  NIM_TRIG,

    // Front panel LEDs
    output wire [3:0] 	  FP_LED,

    input wire 		  FMC_PRSNTn,
    input wire 		  CPU_RESETn
);

parameter NUM_ADC 		= 4;
parameter NUM_ADC_CH  	= 16;
parameter NUM_FMC 		= 4;
parameter NUM_FMC_CH  	= 32;
parameter NUM_SAS		= 1;
parameter SZ_ADC_DATA	= 16;
parameter SZ_TIMESTAMP 	= 48;
parameter WF_DEPTH		= 1024;
parameter STARTUP_DELAY_IN_TICKS 	= 256;
parameter TRIGGER_DELAY_MAX 		= 1024;

localparam SZ_DELAY = $clog2(TRIGGER_DELAY_MAX);
localparam SZ_WAVE = $clog2(WF_DEPTH);

wire [91:0] 	sfp_reconfig_from_xvcr;
wire [139:0] 	sfp_reconfig_to_xvcr;
wire chipid_valid;
wire [63:0] chipid_data;
wire led_link;
wire led_crs;
wire clock_synced;
wire led_trigger_input;
wire led_crs_delayed;
wire [47:0] mac_addr;
wire CLK_125MHz  /* synthesis keep */;

// Hardcoded output pins
assign FMC_SW_EN 	= 1; // turn on VADJ regulator (and 2.5V to VADJ off)
assign SD_DAT[1] 	= 1'bz;
assign SD_DAT[2] 	= 1'bz;
assign DDR3_VTT_EN 	= 1'b1; // required
assign SFP_RATESELECT = 1'b0; // Low for 1x gigabit speeds (aka reduced bandwidth)
assign SFP_MODDET 	 = 1'b0; // Must be grounded/low

assign DISP_RSTn 	= 1'b0;

   // Drive the DAC from qsys

   wire [31:0] ag_dac_data;
   wire [31:0] ag_dac_ctrl;

   // In Rev1 boards, FPGA_ClnClk[2] is connected to transceiver block,
   // not accessible in the fpga logic.
   //wire        clk_dac = FPGA_ClnClk[2];
   // the DAC clock should be set to 125 MHz, then hope
   // it is in good phase with the main 125 MHz clock.
   //wire        clk_dac = clk_125;
   wire        clk_dac = FPGA_ClnClk[1];

   wire [13:0] dac_d_out;
   wire        dac_seliq_out;

   ag_dac ag_dac
     (
      .clk_625(clk_fmc),
      .clk_125(clk_125),
      .reset(global_rst),
      .clk_dac(clk_dac),
      .dac_xor(DAC_XOR),
      .dac_torb(DAC_TORB),
      .dac_pd(DAC_PD),
      .dac_seliq(dac_seliq_out),
      .dac_d(dac_d_out),
      .dac_ctrl(ag_dac_ctrl),
      .dac_data(ag_dac_data),
      .trigger(esata_trigger)
      );

   always_ff@ (posedge clk_dac) begin
      DAC_D <= dac_d_out;
      DAC_SELIQ <= dac_seliq_out;
   end

// SPI Tristate and multi chipselecting logic
wire w_fmc_adc_tri;
wire w_fmc_adc_miso;
wire w_fmc_adc_mosi;
wire fmc_adc_csn_all;
wire [3:0] fmc_adc_csn;
	 
assign FMC_ADC_DIR1 = w_fmc_adc_tri;
assign FMC_ADC_DIR2 = w_fmc_adc_tri;
	 
assign FMC_ADC_SDIO1   = (w_fmc_adc_tri) ? w_fmc_adc_mosi : 1'bz;
assign FMC_ADC_SDIO2   = (w_fmc_adc_tri) ? w_fmc_adc_mosi : 1'bz;

wire fmc_adc_sdio1_in = ((FMC_ADC_CS0n == 1'b0) || (FMC_ADC_CS1n == 1'b0)) ? FMC_ADC_SDIO1 : 1'b0;
wire fmc_adc_sdio2_in = ((FMC_ADC_CS2n == 1'b0) || (FMC_ADC_CS3n == 1'b0)) ? FMC_ADC_SDIO2 : 1'b0;

assign w_fmc_adc_miso = (w_fmc_adc_tri) ? 1'b0 : ( fmc_adc_sdio1_in | fmc_adc_sdio2_in );

assign FMC_ADC_CS3n = fmc_adc_csn[3] & fmc_adc_csn_all;
assign FMC_ADC_CS2n = fmc_adc_csn[2] & fmc_adc_csn_all;
assign FMC_ADC_CS1n = fmc_adc_csn[1] & fmc_adc_csn_all;
assign FMC_ADC_CS0n = fmc_adc_csn[0] & fmc_adc_csn_all;

wire w_adc_tri;
wire w_adc_miso;
wire w_adc_mosi;
wire adc_csn_all;
wire [3:0] adc_csn;

assign ADC_SDIO   = (w_adc_tri) ? w_adc_mosi : 1'bz;
assign w_adc_miso = (w_adc_tri) ? 1'b0 : ADC_SDIO;
assign ADC_CSn[3] = adc_csn[3] & adc_csn_all;
assign ADC_CSn[2] = adc_csn[2] & adc_csn_all;
assign ADC_CSn[1] = adc_csn[1] & adc_csn_all;
assign ADC_CSn[0] = adc_csn[0] & adc_csn_all;

// Useful settings, shouldn't need changing
// Delay LEDs so we can see them
oneshot_extend #( .TICKS_TO_EXTEND(3750000) ) extend_led_crs ( .clk( clk_125 ), .rst( global_rst ), .d( led_crs ), 		  		.q( led_crs_delayed ) );
oneshot_extend #( .TICKS_TO_EXTEND(5000000) ) extend_trigger ( .clk( clk_adc ), .rst( clk_adc_rst ), .d( trigger_input_adc ), 	.q( led_trigger_input ) );

assign FP_LED[3] = led_link ^ led_crs_delayed; // Turn on LED if link is active, then blink it on activity. May need to insert delayer on activity signal
assign FP_LED[2] = run_active; 		// Currently taking data
assign FP_LED[1] = led_trigger_input; 	// Trigger occurred (from any source)
assign FP_LED[0] = clock_synced;	// Should show if clock is synced

// -------------------------
// STARTUP RESET & PLL BLOCK
// -------------------------

// If the button is pressed, we stay in reset until it's been let go for DELAY_CNT clock ticks
// Also does a good job of holding everything in reset for a bit on power-up
wire poweron_rst_n;
//unused - wire poweron_rst = ~poweron_rst_n;

// Initial power up delay
delayer #(
    .DELAY_CNT( STARTUP_DELAY_IN_TICKS )
) initial_rst_delay (
    .clk	( CLK_125MHzT ),
    .rst	( 1'b0 ),
    .d		( 1'b1 ),
    .q		( poweron_rst_n )
);

wire main_pll_rst_n;
wire main_pll_rst = ~main_pll_rst_n;

// Generate full range of required clocks, once the cleaner is locked and setup
// We can switch from our local clock
wire pll_data_locked;
wire clk_125;
wire clk_100;
wire clk_62_5;
wire clk_50;
wire clk_12_5;
data_pll pll_data (
    .rst			( main_pll_rst ),	// held in reset until clock cleaner is done being programmed!
    .locked 	( pll_data_locked ),
    .refclk		( FPGA_ClnClk[1] ),
    .outclk_0 	( clk_125 ),	// NIOS, Ethernet, SATA
    .outclk_1 	( clk_100 ),	// ADC16 clock
    .outclk_2	( clk_62_5 ),	// Possible FMC ADC32 clock
    .outclk_3 	( clk_50 ),		// used for SDCARD, and perhaps FMC ADC32
    .outclk_4	( clk_12_5 ) 	// used for sync'd reset, and IO
);

// Lets do some clock setup
assign CLK_FPGA = clk_12_5; // This should never change, it is the internal 125 in use everywhere (and will be sync'd if the clock cleaner is selected)
wire clk_sys = clk_100; //clk_125;
wire clk_adc = clk_100;
wire clk_fmc = clk_62_5;
wire clk_io  = clk_12_5;

// Delay initial power-up after data_pll comes up, and let a reset occur on
// Synchronize reset to 12.5 clock, which is a multiple of all the others
// So we've now fully synchronized all our resets with one fell swoop. Handy.
// If the CPU_RESETn button is pressed, we will reset the system (without resetting the PLLs!)
wire global_rst_n;
wire global_rst  = ~global_rst_n;
delayer #(
    .DELAY_CNT( STARTUP_DELAY_IN_TICKS )
) pll_rst_delay (
    .clk	( clk_12_5 ),
    .rst	( ~(poweron_rst_n & CPU_RESETn) ),
    .d		( pll_data_locked ),
    .q		( global_rst_n )
);

wire reset_serdes; // controlled by nios, the serdes must be reset after the clock cleaner is locked
wire reset_serdes_synced;
synchronizer #(
    .SZ_DATA( 1 )
) sync_serdes_reset	(
    .clk	( clk_12_5 ),
    .rst	( global_rst ),
    .d 	    ( reset_serdes ),
    .q 	    ( reset_serdes_synced )
);

   wire reset_serdes_adc16;
   wire reset_serdes_adc32;

   wire reset_serdes_clk_adc;
   wire reset_serdes_clk_fmc;

   synchronizer
     #(
       .SZ_DATA( 1 )
       ) sync_serdes_reset_clk_adc
       (
        .clk	( clk_adc ),
        .rst	( global_rst ),
        .d 	( reset_serdes_adc16 | reset_serdes_synced ),
        .q 	( reset_serdes_clk_adc )
        );

   synchronizer
     #(
       .SZ_DATA( 1 )
       ) sync_serdes_reset_clk_fmc
       (
        .clk	( clk_fmc ),
        .rst	( global_rst ),
        .d 	( reset_serdes_adc32 | reset_serdes_synced ),
        .q 	( reset_serdes_clk_fmc )
        );
   
wire clk_fmc_rst = global_rst;
wire clk_fmc_rst_n = ~clk_fmc_rst;
wire clk_adc_rst = global_rst;
wire clk_adc_rst_n = ~clk_adc_rst;

// ------------------------------------------------
// Synchronizer block for various top level signals
// ------------------------------------------------

// WARNING: There is a missing phase aligner between the raw FPGA_ClnClk[0]
// and the clk_12_5, without this, we can't guarantee the phase of the internal
// clocks relative to the ones going in to the clock cleaner

wire adc_sync;
wire adc_sync_synced;
synchronizer #(
    .SZ_DATA( 1 )
) sync_adc_sync	(
    .clk	( clk_12_5 ),
    .rst	( global_rst ),
    .d 	( adc_sync ),
    .q 	( adc_sync_synced )
);
// FMC ADC32 Sync
reg  r_fmc_sync;
assign FMC_ADC_SYNC = r_fmc_sync;
always@(negedge clk_fmc) begin
    r_fmc_sync <= adc_sync_synced;
end

// ADC16 Sync
reg  r_adc_sync;
assign ADC_SYNC = r_adc_sync;
always@(negedge clk_adc) begin
    r_adc_sync <= adc_sync_synced;
end

// Don't sync these signals yes, we'll do it in a second...
wire nim_trig_ena;
wire nim_trig_inv;
wire esata_trig_ena;
wire esata_trig_inv;
wire nim_trigger   = ((NIM_TRIG ^ nim_trig_inv) & nim_trig_ena);
wire esata_trigger = ((eSATA_SYNC ^ esata_trig_inv) & esata_trig_ena);

// Synchronize external signals with the clocks

wire nim_trigger_sync_clk_adc;
wire nim_trigger_sync_clk_fmc;

wire esata_trigger_sync_clk_adc;
wire esata_trigger_sync_clk_fmc;

synchronizer #(.SZ_DATA(1)) sync_nim_sync_adc(.clk(clk_adc), .rst(clk_adc_rst), .d(nim_trigger), .q(nim_trigger_sync_clk_adc));
synchronizer #(.SZ_DATA(1)) sync_nim_sync_fmc(.clk(clk_fmc), .rst(clk_fmc_rst), .d(nim_trigger), .q(nim_trigger_sync_clk_fmc));

synchronizer #(.SZ_DATA(1)) sync_esata_sync_adc(.clk(clk_adc), .rst(clk_adc_rst), .d(esata_trigger), .q(esata_trigger_sync_clk_adc));
synchronizer #(.SZ_DATA(1)) sync_esata_sync_fmc(.clk(clk_fmc), .rst(clk_fmc_rst), .d(esata_trigger), .q(esata_trigger_sync_clk_fmc));

// One shot logic to synchronize NIM signal to ADC32 clock

wire nim_trigger_os_625;
oneshot trigger_nim_os_625 (
    .clk	( clk_fmc ),
    .rst	( clk_fmc_rst ),
    .d		( nim_trigger_sync_clk_fmc ),
    .q		( nim_trigger_os_625 )
);

   
// One shot logic to synchronize eSATA signal to ADC32 clock
wire esata_trigger_os_625;
oneshot trigger_esata_os_625 (
    .clk	( clk_fmc ),
    .rst	( clk_fmc_rst ),
    .d		( esata_trigger_sync_clk_fmc ),
    .q		( esata_trigger_os_625 )
);

// Trigger on either signal in
// @TODO: Add a count here to cou
wire fmc_int_trigger;
wire trigger_input_fmc  = nim_trigger_os_625 | esata_trigger_os_625 | fmc_int_trigger;

// One shot logic to synchronize NIM signal to ADC16 clock
wire nim_trigger_os_100;
oneshot trigger_nim_os_100 (
    .clk	( clk_adc ),
    .rst	( clk_adc_rst ),
    .d		( nim_trigger_sync_clk_adc ),
    .q		( nim_trigger_os_100 )
);

wire esata_trigger_os_100;
oneshot trigger_esata_os_100 (
    .clk	( clk_adc ),
    .rst	( clk_adc_rst ),
    .d		( esata_trigger_sync_clk_adc ),
    .q		( esata_trigger_os_100 )
);

wire adc_int_trigger;
wire trigger_input_adc  = nim_trigger_os_100 | esata_trigger_os_100 | adc_int_trigger;

// Quartus Build Timestamp
wire [31:0] fpga_timestamp;
build_timestamp build_timestamp_inst (
    .data_out( fpga_timestamp )
);

// Reset run on start of run (run_active going high)
wire run_rst;
wire force_run;
wire run_active;

// Synchronize run_active signal, currently only received via board
synchronizer #(
    .SZ_DATA( 1 )
) sync_run	(
    .clk	( clk_12_5 ),
    .rst	( global_rst ),
    .d 		( force_run ),
    .q 		( run_active )
);

oneshot run_oneshot (
    .clk 	( clk_12_5 ),
    .rst 	( global_rst ),
    .d	  	( run_active ),
    .q   	( run_rst )
);

// Synchronize module_id signal, currently only received via board
wire [7:0] module_id;
wire [7:0] board_module_id;
synchronizer #(
    .SZ_DATA( 8 )
) sync_module_id (
    .clk	( clk_12_5 ),
    .rst	( global_rst ),
    .d 		( board_module_id ),
    .q 		( module_id )
);

// 100MHz timestamp, converted to 125MHz to allow it to be compared to
// the 50/62.5MHz clock of the FMC ADCs, and also the FEAMs later
// which also use a 125MHz timestamp
wire [SZ_TIMESTAMP-1:0] timestamp_100;
ts_100_to_125 #(
    .SZ_TS( SZ_TIMESTAMP )
) ts_100_to_125 (
    .rst	( run_rst ),
    .ena	( run_active ),
    .clk	( clk_adc ),
    .q		( timestamp_100 ),
    .frac	( )
);

wire [SZ_TIMESTAMP-1:0] adc_timestamp = timestamp_100;

// 62.5MHz timestamp, converted to 125MHz
// This is pretty straightforward, the bottom bit is always zero and we count
// one bit left, to mimic the count of a 125MHz clock
wire [SZ_TIMESTAMP-1:0] timestamp_625;
ts_625_to_125 #(
    .SZ_TS( SZ_TIMESTAMP )
) ts_626_to_125 (
    .rst	( run_rst ),
    .ena	( run_active ),
    .clk	( clk_fmc ),
    .q		( timestamp_625 )
);

wire [SZ_TIMESTAMP-1:0] fmc_timestamp = timestamp_625;

// ADC SERDES + TRAINER for AD9253 4 Channel, 14 bit, 100 MS/s
// The incoming raw 14-bit ADC data is trained then phase aligned into to our internal 'data' clock domain
wire [NUM_ADC-1:0]						adc_locked;
wire [NUM_ADC-1:0]						adc_aligned;
wire [NUM_ADC_CH-1:0][SZ_ADC_DATA-1:0]	adc_data_out;
adc_data_aligner16 adc16_inst (
    .clk			( clk_adc ),		// Internal Data clock domain
    .rst			( clk_adc_rst | reset_serdes_clk_adc ),	// Reset
    .fco			( ADC_FCO ),  		// Frame Clock, only data trained against
    .dco			( {ADC_DCO[3], ADC_DCO[2], ADC_DCO[1], ADC_DCO[0]} ),  		// Data Clock
    .d				({{ADC_DD[3], ADC_DC[3], ADC_DB[3], ADC_DA[3]},
                      {ADC_DD[2], ADC_DC[2], ADC_DB[2], ADC_DA[2]},
                      {ADC_DD[1], ADC_DC[1], ADC_DB[1], ADC_DA[1]},
                      {ADC_DD[0], ADC_DC[0], ADC_DB[0], ADC_DA[0]}}),
    .ext_sync       ( adc16_aligner_sync ),
    .clk_sel_async  ( adc16_aligner_clk_sel ),
    .phff_sel_async ( adc16_aligner_phff_sel ),
    .locked         ( adc_locked ),	// ADC SERDES PLL Locked?
    .aligned        ( adc_aligned ),	// ADC SERDES DATA is Aligned? (FCO = 0xF0)
    //.q				( adc_data_out )	// ADC SERDES DATA Clocked on clk_adc
    .adc_data_out   ( adc_data_out )	// ADC SERDES DATA Clocked on clk_adc
);

// ADC SERDES + TRAINER for AD9249 8 Channel, 14 bit, 62.5 MS/s
// The incoming raw 14-bit ADC data is trained then phase aligned into to our internal 'data' clock domain
wire [NUM_FMC-1:0]						fmc_locked;
wire [NUM_FMC-1:0]						fmc_aligned;
wire [NUM_FMC_CH-1:0][SZ_ADC_DATA-1:0]	fmc_data_out;
//wire [NUM_FMC_CH-1:0][SZ_ADC_DATA-1:0]	fmc_data_out_pre;
wire [NUM_FMC-1:0]						fmc_rx_clk;

// Note: in adc32_inst FMC_ADC1_DCO1 is used twice and FMC_ADC1_DCO2 is not used (complaint
// from quartus timequest). This is due to a mistake on the FMC-ADC32-Rev1 board which
// routes this clock to a pin that cannot be used. K.O. & B.S.

adc_data_aligner32 adc32_inst (
    .clk			( clk_fmc ),		// Internal Data clock domain
    .rst			( clk_fmc_rst | reset_serdes_clk_fmc),	// Reset
    .fco			( { FMC_ADC2_FCO2, FMC_ADC2_FCO1, FMC_ADC1_FCO2, FMC_ADC1_FCO1 } ),  		// Frame Clock, only data trained against
    .dco			( { FMC_ADC2_DCO2, FMC_ADC2_DCO1, FMC_ADC1_DCO2, FMC_ADC1_DCO1 } ),  		// Data Clock
    .d				( {{FMC_ADC2_H2, FMC_ADC2_G2, FMC_ADC2_F2, FMC_ADC2_E2, FMC_ADC2_D2, FMC_ADC2_C2, FMC_ADC2_B2, FMC_ADC2_A2 },
                       {FMC_ADC2_H1, FMC_ADC2_G1, FMC_ADC2_F1, FMC_ADC2_E1, FMC_ADC2_D1, FMC_ADC2_C1, FMC_ADC2_B1, FMC_ADC2_A1 },
                       {FMC_ADC1_H2, FMC_ADC1_G2, FMC_ADC1_F2, FMC_ADC1_E2, FMC_ADC1_D2, FMC_ADC1_C2, FMC_ADC1_B2, FMC_ADC1_A2 },
                       {FMC_ADC1_H1, FMC_ADC1_G1, FMC_ADC1_F1, FMC_ADC1_E1, FMC_ADC1_D1, FMC_ADC1_C1, FMC_ADC1_B1, FMC_ADC1_A1 }}),
    .ext_sync       ( adc32_aligner_sync ),
    .clk_sel_async  ( adc32_aligner_clk_sel ),
    .phff_sel_async ( adc32_aligner_phff_sel ),
    .locked         ( fmc_locked ),	// ADC SERDES PLL Locked?
    .aligned        ( fmc_aligned ),	// ADC SERDES DATA is Aligned? (FCO = 0xF0)
    .rx_clk(fmc_rx_clk),			       
    .adc_data_out   ( fmc_data_out )	// ADC SERDES DATA Clocked on clk_adc
);

   wire        adc16_wf_pattern_ok;
   wire        adc16_wf_pattern4_ok;
   wire [15:0] adc16_wf_pattern4_ok_bits;

   wire        adc32_wf_pattern_ok;
   wire        adc32_wf_pattern4_ok;
   wire [31:0] adc32_wf_pattern4_ok_bits;

// Remap ADC1 channels to match inputs
//assign fmc_data_out[0] = fmc_data_out_pre[0];
//assign fmc_data_out[1] = fmc_data_out_pre[8];
//assign fmc_data_out[2] = fmc_data_out_pre[1];
//assign fmc_data_out[3] = fmc_data_out_pre[9];
//assign fmc_data_out[4] = fmc_data_out_pre[2];
//assign fmc_data_out[5] = fmc_data_out_pre[10];
//assign fmc_data_out[6] = fmc_data_out_pre[3];
//assign fmc_data_out[7] = fmc_data_out_pre[11];
//assign fmc_data_out[8] = fmc_data_out_pre[4];
//assign fmc_data_out[9] = fmc_data_out_pre[12];
//assign fmc_data_out[10] = fmc_data_out_pre[5];
//assign fmc_data_out[11] = fmc_data_out_pre[13];
//assign fmc_data_out[12] = fmc_data_out_pre[6];
//assign fmc_data_out[13] = fmc_data_out_pre[14];
//assign fmc_data_out[14] = fmc_data_out_pre[7];
//assign fmc_data_out[15] = fmc_data_out_pre[15];

// Remap ADC2 channels to match inputs
//assign fmc_data_out[16] = fmc_data_out_pre[16];
//assign fmc_data_out[17] = fmc_data_out_pre[24];
//assign fmc_data_out[18] = fmc_data_out_pre[17];
//assign fmc_data_out[19] = fmc_data_out_pre[25];
//assign fmc_data_out[20] = fmc_data_out_pre[18];
//assign fmc_data_out[21] = fmc_data_out_pre[26];
//assign fmc_data_out[22] = fmc_data_out_pre[19];
//assign fmc_data_out[23] = fmc_data_out_pre[27];
//assign fmc_data_out[24] = fmc_data_out_pre[20];
//assign fmc_data_out[25] = fmc_data_out_pre[28];
//assign fmc_data_out[26] = fmc_data_out_pre[21];
//assign fmc_data_out[27] = fmc_data_out_pre[29];
//assign fmc_data_out[28] = fmc_data_out_pre[22];
//assign fmc_data_out[29] = fmc_data_out_pre[30];
//assign fmc_data_out[30] = fmc_data_out_pre[23];
//assign fmc_data_out[31] = fmc_data_out_pre[31];


// Signal processing for ADC16
wire [NUM_ADC_CH-1:0] 					adc_trigger_out;
wire [NUM_ADC_CH-1:0] 					adc_ch_type;
wire [NUM_ADC_CH-1:0] 					adc_ch_ena;
wire [NUM_ADC_CH-1:0][SZ_DELAY-1:0] 	adc_ch_trig_delay;
wire [NUM_ADC_CH-1:0][SZ_WAVE-1:0] 		adc_ch_trig_point;
wire [NUM_ADC_CH-1:0][SZ_WAVE-1:0] 		adc_ch_stop_point;
wire [NUM_ADC_CH-1:0][31:0] 			adc_ch_stat_trig_in;
wire [NUM_ADC_CH-1:0][31:0] 			adc_ch_stat_trig_accepted;
wire [NUM_ADC_CH-1:0][31:0] 			adc_ch_stat_trig_drop_busy;
wire [NUM_ADC_CH-1:0][31:0] 			adc_ch_stat_trig_drop_full;
wire 									adc_sp_rdy;
wire 									adc_sp_sop;
wire 									adc_sp_eop;
wire 									adc_sp_val;
wire [31:0] 							adc_sp_dat;
wire [1:0]							adc_sp_empty;
wire									adc_sp_avail;

sp_top #(
    .NUM_CH				( NUM_ADC_CH ),
    .SZ_TIMESTAMP		( SZ_TIMESTAMP ),
    .WAVE_DEPTH			( WF_DEPTH ),
    .TRIGGER_DELAY_MAX( TRIGGER_DELAY_MAX )
) adc16_sp (
    .clk					( clk_adc ),
    .rst					( clk_adc_rst ),
    .run					( run_active ),
    .timestamp				( adc_timestamp ),
    .module_id				( module_id ),
    .hw_id					( mac_addr ),
    .fw_id					( fpga_timestamp ),
    .ch_sthr_const              ( ag_adc16_sthreshold ),
    .ch_ctrl_const              ( ag_ctrl_a ),
    .ch_stat_out                ( ag_stat_a ),
    .ch_ena					( adc_ch_ena ),
    .ch_trig_in				( {NUM_ADC_CH{trigger_input_adc}} ),
    .ch_trig_delay			( adc_ch_trig_delay ),
    .ch_trig_out			( adc_trigger_out ),
    .ch_type				( adc_ch_type ),
    .ch_wf_in				( adc_data_out ),
    .ch_trig_point			( adc_ch_trig_point ),
    .ch_stop_point			( adc_ch_stop_point ),
    .wf_pattern_ok_out                  ( adc16_wf_pattern_ok ),
    .wf_pattern4_ok_out                 ( adc16_wf_pattern4_ok ),
    .wf_pattern4_ok_bits_out            ( adc16_wf_pattern4_ok_bits ),
    .ch_stat_trig_in		( adc_ch_stat_trig_in ),
    .ch_stat_trig_accepted	( adc_ch_stat_trig_accepted ),
    .ch_stat_trig_drop_busy	( adc_ch_stat_trig_drop_busy ),
    .ch_stat_trig_drop_full	( adc_ch_stat_trig_drop_full ),
    .rd_clk     ( clk_sys ),
    .rd_clk_rst ( global_rst ),
    .rd_rdy     ( adc_sp_rdy ),
    .rd_sop     ( adc_sp_sop ),
    .rd_eop     ( adc_sp_eop ),
    .rd_val     ( adc_sp_val ),
    .rd_empty   ( adc_sp_empty ),
    .rd_dat     ( adc_sp_dat ),
    .rd_avail   ( adc_sp_avail )
);

// Signal processing for FMC ADC32
wire [NUM_FMC_CH-1:0] 					fmc_trigger_out;
wire [NUM_FMC_CH-1:0] 					fmc_ch_type;
wire [NUM_FMC_CH-1:0] 					fmc_ch_ena;
wire [NUM_FMC_CH-1:0][SZ_DELAY-1:0] 	fmc_ch_trig_delay;
wire [NUM_FMC_CH-1:0][SZ_WAVE-1:0] 		fmc_ch_trig_point;
wire [NUM_FMC_CH-1:0][SZ_WAVE-1:0] 		fmc_ch_stop_point;
wire [NUM_FMC_CH-1:0][31:0] 			fmc_ch_stat_trig_in;
wire [NUM_FMC_CH-1:0][31:0] 			fmc_ch_stat_trig_accepted;
wire [NUM_FMC_CH-1:0][31:0] 			fmc_ch_stat_trig_drop_busy;
wire [NUM_FMC_CH-1:0][31:0] 			fmc_ch_stat_trig_drop_full;
wire 									fmc_sp_rdy;
wire 									fmc_sp_sop;
wire 									fmc_sp_eop;
wire 									fmc_sp_val;
wire [31:0] 							fmc_sp_dat;
wire [1:0]								fmc_sp_empty;
wire									fmc_sp_avail;
sp_top #(
    .NUM_CH				( NUM_FMC_CH ),
    .SZ_TIMESTAMP		( SZ_TIMESTAMP ),
    .WAVE_DEPTH			( WF_DEPTH ),
    .TRIGGER_DELAY_MAX( TRIGGER_DELAY_MAX )
) adc32_sp (
    .clk						( clk_fmc ),
    .rst						( clk_fmc_rst ),
    .run						( run_active ),
    .timestamp					( fmc_timestamp ),
    .module_id					( module_id ),
    .hw_id						( mac_addr ),
    .fw_id						( fpga_timestamp ),
    .ch_sthr_const                      ( ag_adc32_sthreshold ),
    .ch_ctrl_const                      ( ag_ctrl_b ),
    .ch_stat_out                        ( ag_stat_b ),
    .ch_ena						( fmc_ch_ena ),
    .ch_trig_in					( {NUM_FMC_CH{trigger_input_fmc}} ),
    .ch_trig_delay				( fmc_ch_trig_delay ),
    .ch_trig_out				( fmc_trigger_out ),
    .ch_type					( fmc_ch_type ),
    .ch_wf_in					( fmc_data_out ),
    .ch_trig_point				( fmc_ch_trig_point ),
    .ch_stop_point				( fmc_ch_stop_point ),
    .wf_pattern_ok_out                  ( adc32_wf_pattern_ok ),
    .wf_pattern4_ok_out                 ( adc32_wf_pattern4_ok ),
    .wf_pattern4_ok_bits_out            ( adc32_wf_pattern4_ok_bits ),
    .ch_stat_trig_in			( fmc_ch_stat_trig_in ),
    .ch_stat_trig_accepted		( fmc_ch_stat_trig_accepted ),
    .ch_stat_trig_drop_busy		( fmc_ch_stat_trig_drop_busy ),
    .ch_stat_trig_drop_full		( fmc_ch_stat_trig_drop_full ),
    .rd_clk     ( clk_sys ),
    .rd_clk_rst ( global_rst ),
    .rd_rdy     ( fmc_sp_rdy ),
    .rd_sop     ( fmc_sp_sop ),
    .rd_eop     ( fmc_sp_eop ),
    .rd_val     ( fmc_sp_val ),
    .rd_empty   ( fmc_sp_empty ),
    .rd_dat     ( fmc_sp_dat ),
    .rd_avail   ( fmc_sp_avail )
);


st_mux #(
	.WIDTH( 32 ),
	.NUM_CH( 2 ) // just two channels, ADC16 and FMC-ADC32
) channel_arbiter (
	.clk     ( clk_sys ),
	.rst     ( global_rst | ag_ctrl_ab_reset ),
	.d_dat   ( {adc_sp_dat,   fmc_sp_dat} ),
	.d_sop   ( {adc_sp_sop,   fmc_sp_sop} ),
	.d_eop   ( {adc_sp_eop,   fmc_sp_eop} ),
	.d_val   ( {adc_sp_val,   fmc_sp_val} ),
	.d_req   ( {adc_sp_avail, fmc_sp_avail} ),
	.d_ack   ( {adc_sp_rdy,   fmc_sp_rdy} ),	
	.d_empty ( {adc_sp_empty, fmc_sp_empty} ),
	.q_ch		( ),
	.q_ch_oh	( ),
	.q_dat   ( sp_dat ),
	.q_sop   ( sp_sop ),
	.q_eop   ( sp_eop ),
	.q_val   ( sp_val ),
	.q_empty ( sp_empty ),
	.q_ack   ( sp_rdy )
);

wire sp_val;
wire sp_rdy;
wire [31:0] sp_dat;	
wire [1:0] sp_empty;
wire sp_sop;
wire sp_eop;

   wire        udp_val;
   wire        udp_rdy;
   wire [31:0] udp_dat;	
   wire [1:0]  udp_epy;
   wire        udp_sop;
   wire        udp_eop;

   st_store_and_forward_fifo
     #(
       .DEPTH( 1024 )
       ) udp_fifo
       ( 
	 .clk ( clk_sys ),
	 .rst ( global_rst | ag_ctrl_ab_reset ),
	 
	 .snk_dat    ( sp_dat ),
	 .snk_val    ( sp_val ),
	 .snk_sop    ( sp_sop ),
	 .snk_eop    ( sp_eop ),
	 .snk_rdy    ( sp_rdy ),
	 .snk_err    ( ),
	 .snk_empty  ( sp_empty ),
	 
	 .src_dat    ( udp_dat ),
	 .src_val    ( udp_val ),
	 .src_sop    ( udp_sop ),
	 .src_eop    ( udp_eop ),
	 .src_empty  ( udp_epy ),
	 .src_rdy    ( udp_rdy ),
	 .src_avail  (  ) // indicates there is a packet to be transmitted
         );
	
   wire        udp_event_val;
   wire        udp_event_rdy;
   wire [31:0] udp_event_dat;
   wire [1:0]  udp_event_epy;
   wire        udp_event_sop;
   wire        udp_event_eop;
   
   // Take the packets and add their length to the start of the packet
   // for the ALtera UDP Offload engine to work properly
   packet_length_prepender
     #(
       .PKT_DEPTH( 368 ), // 368 words * 4 bytes per word = 1472 bytes, defaults to correct size for 1500 MTU, 28 bytes for IP+UDP header
       .NUM_PACKETS( 3 )	
       ) udp_len_prepender
       (
	.clk		( clk_sys ),
	.rst		( global_rst | ag_ctrl_ab_reset ),	
	.in_sop		( udp_sop ),
	.in_eop		( udp_eop ),
	.in_dat		( udp_dat ),
	.in_val		( udp_val ),
	.in_empty	( udp_epy ),
	.in_rdy		( udp_rdy ),
	.out_sop	( udp_event_sop ),
	.out_eop	( udp_event_eop ),
	.out_val	( udp_event_val ),
	.out_dat	( udp_event_dat ),
	.out_empty	( udp_event_epy ),
	.out_rdy	( udp_event_rdy )
        );
   
// Grab temperature
wire fpga_temp_rst;
wire fpga_temp_done;
wire [7:0] fpga_temp;
wire [7:0] fpga_temp_rdbk;
read_alt_temp arriav_temp_inst (
    .clk			( clk_io ),
    .rst			( global_rst ),
    .q				( fpga_temp_rdbk ),
    .temp_rst		( fpga_temp_rst ),
    .temp_done		( fpga_temp_done ),
    .temp_rdbk		( fpga_temp )
);

wire clk_sfp_recovered_to_pll;
wire pll_eth_locked;

wire init_lmk_miso;
wire init_lmk_mosi;
wire init_lmk_sclk;
wire init_lmk_csn;

wire main_lmk_miso;
wire main_lmk_mosi;
wire main_lmk_sclk;
wire main_lmk_csn;

assign init_lmk_miso = (main_pll_rst_n) ? 1'b0 : LMK_MISO;
assign main_lmk_miso = (main_pll_rst_n) ? LMK_MISO : 1'b0;

assign LMK_MOSI = (main_pll_rst_n) ? main_lmk_mosi : init_lmk_mosi;
assign LMK_SCLK = (main_pll_rst_n) ? main_lmk_sclk : init_lmk_sclk;
assign LMK_CSn = (main_pll_rst_n) ? main_lmk_csn  : init_lmk_csn;

   wire [15:0] ag_adc16_threshold;
   wire [15:0] ag_adc32_threshold;
   
   wire [15:0] ag_adc16_bits;
   wire [31:0] ag_adc32_bits;
   
   wire        ag_clk_counter;
   wire [31:0] ag_adc16_counter;
   wire [31:0] ag_adc32_counter;

   wire [31:0] ag_dac_ctrl_a;
   wire [31:0] ag_dac_ctrl_b;
   wire [31:0] ag_dac_ctrl_c;
   wire [31:0] ag_dac_ctrl_d;

   wire [15:0] ag_adc16_sthreshold; // adc16 channel suppression threshold
   wire [15:0] ag_adc32_sthreshold; // adc32 channel suppression threshold

   wire [31:0] ag_ctrl_a; // adc16 control ch_ctrl
   wire [31:0] ag_ctrl_b; // adc32 control ch_ctrl
   wire [31:0] ag_ctrl_c; // alphag control
   wire [31:0] ag_ctrl_d; // adc16 and adc32 serializer and aligner control
   wire [31:0] ag_ctrl_e;
   wire [31:0] ag_ctrl_f;

   wire        ag_ctrl_a_reset        = ag_ctrl_a[31];
   wire        ag_ctrl_b_reset        = ag_ctrl_b[31];

   wire        ag_ctrl_ab_reset = ag_ctrl_a_reset & ag_ctrl_b_reset;

   // not used                          ag_ctrl_d[31];
   assign      reset_serdes_adc16     = ag_ctrl_d[30];
   wire        adc16_aligner_sync     = ag_ctrl_d[29];
   wire        adc16_aligner_clk_sel  = ag_ctrl_d[28];
   wire [3:0]  adc16_aligner_phff_sel = ag_ctrl_d[19:16];

   // not used                          ag_ctrl_d[15];
   assign      reset_serdes_adc32     = ag_ctrl_d[14];
   wire        adc32_aligner_sync     = ag_ctrl_d[13];
   wire        adc32_aligner_clk_sel  = ag_ctrl_d[12];
   wire [3:0]  adc32_aligner_phff_sel = ag_ctrl_d[3:0];

   wire [31:0] ag_stat_a; // adc16 status ch_stat_out
   wire [31:0] ag_stat_b; // adc32 status ch_stat_out
   wire [31:0] ag_stat_cx; // alphag block status
   wire [31:0] ag_stat_c;
   wire [31:0] ag_stat_d;

   // adc16 does not have ADC test pattern controls, pattern bits go nowhere.
   //wire        adc16_wf_pattern4_ok;
   //wire [15:0] adc16_wf_pattern4_ok_bits,

   assign ag_stat_c[31] = adc32_wf_pattern4_ok;
   assign ag_stat_c[30] = adc16_wf_pattern4_ok;
   assign ag_stat_c[29] = adc32_wf_pattern_ok;
   assign ag_stat_c[28] = adc16_wf_pattern_ok;
   assign ag_stat_c[27:16] = ag_stat_cx[27:16]; // alphag block status
   assign ag_stat_c[15:0] = adc16_wf_pattern4_ok_bits;
   assign ag_stat_d = adc32_wf_pattern4_ok_bits;
   
management mgmnt_qsys_inst (
    .clk_fmc_adc_clk_clk        ( clk_fmc ),         // clk_fmc_adc_clk.clk
    .clk_fmc_adc_rst_reset_n    ( clk_fmc_rst_n ),   // clk_fmc_adc_rst.reset_n
    .clk_adc_clk_clk            ( clk_adc ),         // clk_adc_clk.clk
    .clk_adc_rst_reset_n        ( clk_adc_rst_n ),   // clk_adc_rst.reset_n
    .clk_125_clk_clk            ( clk_sys ),         // clk_125_clk.clk
    .clk_125_rst_reset_n        ( global_rst_n ),    // clk_125_rst.reset_n
    .clk_50_clk_clk             ( clk_50 ),          // clk_50_clk.clk
    .clk_50_rst_reset_n         ( global_rst_n ),    // clk_50_rst.reset_n
    .clk_12_5_clk_clk           ( clk_12_5 ),        // clk_12_5_clk.clk
    .clk_12_5_rst_reset_n       ( global_rst_n ),    // clk_12_5_rst.reset_n
		
    // NIOS for Clock Cleaner Init	
    .clk_init_clk               ( CLK_125MHzT ),     // clk_init.clk
    .rst_init_reset_n           ( poweron_rst_n ),   // rst_init.reset_n
    .pio_init_output_export     ( { main_pll_rst_n, REFCLK_SEL, LMK_CLKinSEL, LMK_SYNC} ), // pio_init_output.export
    .pio_init_input_export      ( LMK_StatusLD ),    // pio_init_input.export
    .spi_clockcleaner_external_MISO           		( init_lmk_miso ),
    .spi_clockcleaner_external_MOSI           		( init_lmk_mosi ),
    .spi_clockcleaner_external_SCLK           		( init_lmk_sclk ),
    .spi_clockcleaner_external_SS_n           		( init_lmk_csn ), 	     
    .xcvr_reconfig_busy_reconfig_busy          		( ),
    .eth_reconfig_to_xcvr_reconfig_to_xcvr   		( sfp_reconfig_to_xvcr ),
    .eth_reconfig_from_xcvr_reconfig_from_xcvr   	( sfp_reconfig_from_xvcr ),
    .eth_tse_mac_misc_ff_tx_crc_fwd                 ( 1'b0 ),
    .eth_tse_mac_misc_xon_gen                       ( 1'b0 ),
    .eth_tse_mac_misc_xoff_gen                      ( 1'b0 ),
    .eth_tse_status_led_crs                	( led_crs ),
    .eth_tse_status_led_link               	( led_link ),
    .eth_tse_status_led_col                	( ),
    .eth_tse_status_led_an                 	( ),
    .eth_tse_status_led_char_err           	( ),
    .eth_tse_status_led_disp_err           	( ),
    .eth_tse_serdes_control_rx_recovclkout      ( clk_sfp_recovered_to_pll ),
    .eth_tse_serdes_control_reconfig_togxb      ( sfp_reconfig_to_xvcr ),
    .eth_tse_serdes_control_reconfig_fromgxb    ( sfp_reconfig_from_xvcr ),
    .eth_tse_serial_connection_rxp              ( SFP_RX ),
    .eth_tse_serial_connection_txp              ( SFP_TX ),
    .eth_tse_pcs_ref_clk_clock_connection_clk   ( CLK_125MHz3L ),
    .spi_adc_external_MISO                    	( w_adc_miso ),
    .spi_adc_external_MOSI                    	( w_adc_mosi ),
    .spi_adc_external_SCLK                    	( ADC_SCK ),
    .spi_adc_external_SS_n                    	( {adc_csn_all, adc_csn} ),
    .spi_fmc_adc_MISO                           ( w_fmc_adc_miso),
    .spi_fmc_adc_MOSI                           ( w_fmc_adc_mosi),
    .spi_fmc_adc_SCLK                           ( FMC_ADC_SCK ),
    .spi_fmc_adc_SS_n                           ( {fmc_adc_csn_all, fmc_adc_csn} ),
    /*
    .spi_fmc_dac1_MISO                          ( FMC_SPI_DAC1_SDO ),
    .spi_fmc_dac1_MOSI                          ( FMC_SPI_DAC1_SDI ),
    .spi_fmc_dac1_SCLK                          ( FMC_SPI_DAC1_SCK ),
    .spi_fmc_dac1_SS_n                          ( FMC_SPI_DAC1_CSn ),
    */
    .spi_fmc_dac2_MISO                          ( FMC_SPI_DAC2_SDO ),
    .spi_fmc_dac2_MOSI                          ( FMC_SPI_DAC2_SDI ),
    .spi_fmc_dac2_SCLK                          ( FMC_SPI_DAC2_SCK ),
    .spi_fmc_dac2_SS_n                          ( {FMC_SPI_DAC2_CSn, FMC_SPI_DAC1_CSn} ),

    .spi_temp_external_MISO    ( TEMP_SDO ),
    .spi_temp_external_MOSI    ( TEMP_SDI ),
    .spi_temp_external_SCLK    ( TEMP_SCK ),
    .spi_temp_external_SS_n    ( TEMP_CSn ),	 
    .spi_lmk48xx_MISO          ( main_lmk_miso ),
    .spi_lmk48xx_MOSI          ( main_lmk_mosi ),
    .spi_lmk48xx_SCLK          ( main_lmk_sclk ),
    .spi_lmk48xx_SS_n          ( main_lmk_csn ), 	     	 
    .i2c_dac2655_scl_pad_io    ( DAC_SMB_SCL ),
    .i2c_dac2655_sda_pad_io    ( DAC_SMB_SDA ),
    .i2c_fmc_scl_pad_io        ( FMC_SCL ),
    .i2c_fmc_sda_pad_io        ( FMC_SDA ),
    .i2c_disp_scl_pad_io       ( DISP_SCL ),
    .i2c_disp_sda_pad_io       ( DISP_SDA ),
    .i2c_sfp_scl_pad_io        ( SFP_SCL ),
    .i2c_sfp_sda_pad_io        ( SFP_SDA ),
    .i2c_mac_scl_pad_io        ( MAC_SCL ),
    .i2c_mac_sda_pad_io        ( MAC_SDA ),
    .temp_out_tsdcalo          ( fpga_temp ),
    .temp_done_tsdcaldone      ( fpga_temp_done ),
    .temp_clear_reset          ( fpga_temp_rst ),
    .temp_clk_clk              ( clk_io ),
    .pio_adc_out_export        ( w_adc_tri ),
    .pio_fmc_adc_out_export    ( w_fmc_adc_tri ),
    .pio_fpled_export          ( clock_synced ),
    .chip_id_valid             ( chipid_valid ),
    .chip_id_data              ( chipid_data ),
    .chipid_clkin_clk          ( clk_io ),
    .chipid_reset_reset        ( global_rst ),
    .sdcard_external_b_SD_cmd                       ( SD_CMD ),
    .sdcard_external_b_SD_dat                       ( SD_DAT[0] ),
    .sdcard_external_b_SD_dat3                      ( SD_DAT[3] ),
    .sdcard_external_o_SD_clock                     ( SD_CLK ),
    .uart_usb_external_rxd                          ( USB_TXD ),
    .uart_usb_external_txd                          ( USB_RXD ),
    .uart_usb_external_cts_n                        ( USB_RTSn ),
    .uart_usb_external_rts_n                        ( USB_CTSn ),
    .mem_ddr_pll_ref_clk_clk                        ( CLK_125MHzT ),
    .mem_ddr_global_reset_reset_n                   ( poweron_rst_n ),
    .memory_mem_a                                   ( DDR3_A ),
    .memory_mem_ba                                  ( DDR3_BA ),
    .memory_mem_ck                                  ( DDR3_CK ),
    .memory_mem_ck_n                                ( DDR3_CK_n ),
    .memory_mem_cke                                 ( DDR3_CKE ),
    .memory_mem_cs_n                                ( DDR3_CSn ),
    .memory_mem_dm                                  ( DDR3_DM ),
    .memory_mem_ras_n                               ( DDR3_RASn ),
    .memory_mem_cas_n                               ( DDR3_CASn ),
    .memory_mem_we_n                                ( DDR3_WEn ),
    .memory_mem_reset_n                             ( DDR3_RESETn ),
    .memory_mem_dq                                  ( DDR3_DQ ),
    .memory_mem_dqs                                 ( DDR3_DQS ),
    .memory_mem_dqs_n                               ( DDR3_DQS_n ),
    .memory_mem_odt                                 ( DDR3_ODT ),
    .oct_rzqin                                      ( DDR3_OCT_RZQ6 ),
    .adc_data_inserter_valid                        ( udp_event_val ),
    .adc_data_inserter_ready                        ( udp_event_rdy ),
    .adc_data_inserter_data                         ( udp_event_dat ),
    .adc_data_inserter_empty                        ( udp_event_epy ),
    .adc_data_inserter_startofpacket                ( udp_event_sop ),
    .adc_data_inserter_endofpacket                  ( udp_event_eop ),
    .board_temp_rst                                 ( TEMP_RSTn ),
    .board_adc_sync                                 ( adc_sync ),
    .board_lmk_clkin_sel                            ( LMK_CLKinSEL ),
    .board_refclk_sel                               ( REFCLK_SEL ),
    .board_sfp_tx_disable                           ( SFP_TX_DISABLE ),
    .board_reset_serdes                             ( reset_serdes ),
    .board_nim_trig_ena			( nim_trig_ena ),
    .board_nim_trig_inv			( nim_trig_inv ),
    .board_nim_trig_in			( nim_trigger ),
    .board_esata_trig_ena		( esata_trig_ena ),
    .board_esata_trig_inv		( esata_trig_inv ),
    .board_esata_trig_in		( esata_trigger ),
    .board_clk_eth                                  ( clk_sfp_recovered_to_pll ),
    .board_clk_esata                                ( eSATA_CLK  ),
    .board_clk_nim                                  ( NIM_CLK ),
    .board_clk_clean0                               ( FPGA_ClnClk[0] ),
    .board_clk_clean1                               ( FPGA_ClnClk[1] ),
    .board_adc_locked                               ( adc_locked ),
    .board_adc_aligned                              ( adc_aligned ),
    .board_fmc_present_n                           ( FMC_PRSNTn ),
    .board_fmc_adc_locked                           ( fmc_locked ),
    .board_fmc_adc_aligned                          ( fmc_aligned ),
    .board_sd_detect				( SD_DETECT ),
    .board_lmk_statusld                             ( LMK_StatusLD ),
    .board_sfp_los                                  ( SFP_LOS ),
    .board_sfp_tx_fault                             ( SFP_TX_FAULT ),
    .board_usb_bus                                  ( USB_CBUS0 ),
    .board_fpga_temp_rdbk                           ( fpga_temp_rdbk ),
    .board_fpga_timestamp                           ( fpga_timestamp ),
    .board_chipid_data                              ( chipid_data ),
    .board_chipid_valid                             ( chipid_valid ),
    .board_mac_addr			( mac_addr ),
    .board_force_run			( force_run ),
    .board_module_id			( board_module_id ),
    .board_pll_ethernet_locked		( pll_eth_locked ),
    .ag_adc16_threshold                         (ag_adc16_threshold),
    .ag_adc32_threshold                         (ag_adc32_threshold),
    .ag_adc16_bits                              (ag_adc16_bits),
    .ag_adc32_bits                              (ag_adc32_bits),
    .ag_dac_data                                (ag_dac_data),
    .ag_dac_ctrl                                (ag_dac_ctrl),
    .ag_dac_ctrl_a          (ag_dac_ctrl_a),
    .ag_dac_ctrl_b          (ag_dac_ctrl_b),
    .ag_dac_ctrl_c          (ag_dac_ctrl_c),
    .ag_dac_ctrl_d          (ag_dac_ctrl_d),
    .ag_adc16_sthreshold    (ag_adc16_sthreshold),
    .ag_adc32_sthreshold    (ag_adc32_sthreshold),
    .ag_ctrl_a              (ag_ctrl_a),
    .ag_ctrl_b              (ag_ctrl_b),
    .ag_ctrl_c              (ag_ctrl_c),
    .ag_ctrl_d              (ag_ctrl_d),
    .ag_ctrl_e              (ag_ctrl_e),
    .ag_ctrl_f              (ag_ctrl_f),
    .ag_clk_counter         (ag_clk_counter),
    .ag_adc16_counter       (ag_adc16_counter),
    .ag_adc32_counter       (ag_adc32_counter),
    .ag_stat_a              (ag_stat_a),
    .ag_stat_b              (ag_stat_b),
    .ag_stat_c              (ag_stat_c),
    .ag_stat_d              (ag_stat_d),
    .alpha_sigproc_adc_int_trigger                  ( adc_int_trigger ),
    .alpha_sigproc_adc_run_status                   ( run_active ),
    .alpha_sigproc_adc_timestamp                    ( adc_timestamp ),
    .alpha_sigproc_adc_ch_stat_trig_in              ( adc_ch_stat_trig_in ),
    .alpha_sigproc_adc_ch_stat_trig_accepted        ( adc_ch_stat_trig_accepted ),
    .alpha_sigproc_adc_ch_stat_trig_drop_busy       ( adc_ch_stat_trig_drop_busy ),
    .alpha_sigproc_adc_ch_stat_trig_drop_full       ( adc_ch_stat_trig_drop_full ),
    .alpha_sigproc_adc_ch_ena                       ( adc_ch_ena ),
    .alpha_sigproc_adc_ch_type                      ( adc_ch_type ),
    .alpha_sigproc_adc_ch_trig_delay                ( adc_ch_trig_delay ),
    .alpha_sigproc_adc_ch_trig_point                ( adc_ch_trig_point ),
    .alpha_sigproc_adc_ch_stop_point                ( adc_ch_stop_point  ),
    .alpha_sigproc_fmc_adc_int_trigger              ( fmc_int_trigger ),
    .alpha_sigproc_fmc_adc_run_status               ( run_active ),
    .alpha_sigproc_fmc_adc_timestamp              	( fmc_timestamp ),
    .alpha_sigproc_fmc_adc_ch_stat_trig_in        	( fmc_ch_stat_trig_in ),
    .alpha_sigproc_fmc_adc_ch_stat_trig_accepted  	( fmc_ch_stat_trig_accepted ),
    .alpha_sigproc_fmc_adc_ch_stat_trig_drop_busy 	( fmc_ch_stat_trig_drop_busy ),
    .alpha_sigproc_fmc_adc_ch_stat_trig_drop_full 	( fmc_ch_stat_trig_drop_full ),
    .alpha_sigproc_fmc_adc_ch_ena                 	( fmc_ch_ena ),
    .alpha_sigproc_fmc_adc_ch_type                	( fmc_ch_type ),
    .alpha_sigproc_fmc_adc_ch_trig_delay          	( fmc_ch_trig_delay ),
    .alpha_sigproc_fmc_adc_ch_trig_point          	( fmc_ch_trig_point ),
    .alpha_sigproc_fmc_adc_ch_stop_point          	( fmc_ch_stop_point  ),
    .ethernet_packet_mux_in2_valid                 ( 1'b0 ),
    .ethernet_packet_mux_in2_ready                 ( ),
    .ethernet_packet_mux_in2_data                  ( 32'h0 ),
    .ethernet_packet_mux_in2_startofpacket         ( 1'b0 ),
    .ethernet_packet_mux_in2_endofpacket           ( 1'b0 ),
    .ethernet_packet_mux_in2_empty                 ( 2'b00 ),
    .ethernet_packet_mux_in3_valid                 ( 1'b0 ),
    .ethernet_packet_mux_in3_ready                 ( ),
    .ethernet_packet_mux_in3_data                  ( 32'h0 ),
    .ethernet_packet_mux_in3_startofpacket         ( 1'b0 ),
    .ethernet_packet_mux_in3_endofpacket           ( 1'b0 ),
    .ethernet_packet_mux_in3_empty                 ( 2'b00 ),
    .ethernet_packet_mux_in4_valid                 ( 1'b0 ),
    .ethernet_packet_mux_in4_ready                 ( ),
    .ethernet_packet_mux_in4_data                  ( 32'h0 ),
    .ethernet_packet_mux_in4_startofpacket         ( 1'b0 ),
    .ethernet_packet_mux_in4_endofpacket           ( 1'b0 ),
    .ethernet_packet_mux_in4_empty                 ( 2'b00 )
);

   // ALPHA-g trigger functions go here

   wire [63:0] sas_bits;

   alphag
     #(
       .NUM_ADC_CH(NUM_ADC_CH),
       .NUM_FMC_CH(NUM_FMC_CH),
       .SZ_ADC_DATA(SZ_ADC_DATA),
       .SZ_FMC_DATA(SZ_ADC_DATA)
       )
   ag
     (
      .clk_adc100(clk_adc),
      .data_adc100(adc_data_out), // adc16 adc data to discriminator (100 Ms/s)
      .clk_adc625(clk_fmc),
      .data_adc625(fmc_data_out), // adc32 adc data to discriminator (62.5 Ms/s)
      //.clk_625(),
      .clk_125(clk_125),
      .reset(global_rst),
      .esata_clk(eSATA_CLK),
      .esata_trig(eSATA_SYNC),
      .nim_clk(NIM_CLK),
      .nim_trig(NIM_TRIG),
      .module_id(module_id),
      .board_id(module_id[3:0]),
      .ctrl     ( ag_ctrl_c ),
      .stat_out ( ag_stat_cx ),
      .adc16_threshold(ag_adc16_threshold), // adc16 discriminator
      .adc32_threshold(ag_adc32_threshold), // adc32 discriminator
      .adc16_bits_out      ( ag_adc16_bits    ), // adc16 trigger bits to NIOS
      .adc32_bits_out      ( ag_adc32_bits    ), // adc32 trigger bits to NIOS
      .clk_counter         ( ag_clk_counter   ), // clock for the adc16 and adc32 counters
      .adc16_counter_out   ( ag_adc16_counter ),
      .adc32_counter_out   ( ag_adc32_counter ),
      .sas_bits_out        ( sas_bits         )  // trigger data to TRG
      );

   // 125MHz serial data to TRG via FMC-ADC32 SATA connector or FMC-MiniSAS connector
   
   sas_links ag_sas
     (
      .sfp_clk(clk_125),
      .reset(global_rst),
      .FMCX_RX(DP_M2C[7:0]),
      .FMCX_TX(DP_C2M[7:0]),
      .sas_bits(sas_bits)
      );

endmodule
