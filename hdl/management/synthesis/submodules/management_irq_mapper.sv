// (C) 2001-2017 Intel Corporation. All rights reserved.
// Your use of Intel Corporation's design tools, logic functions and other 
// software and tools, and its AMPP partner logic functions, and any output 
// files any of the foregoing (including device programming or simulation 
// files), and any associated documentation or information are expressly subject 
// to the terms and conditions of the Intel Program License Subscription 
// Agreement, Intel MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your use is for the 
// sole purpose of programming logic devices manufactured by Intel and sold by 
// Intel or its authorized distributors.  Please refer to the applicable 
// agreement for further details.


// $Id: //acds/rel/17.0std/ip/merlin/altera_irq_mapper/altera_irq_mapper.sv.terp#1 $
// $Revision: #1 $
// $Date: 2017/01/22 $
// $Author: swbranch $

// -------------------------------------------------------
// Altera IRQ Mapper
//
// Parameters
//   NUM_RCVRS        : 17
//   SENDER_IRW_WIDTH : 32
//   IRQ_MAP          : 0:4,1:3,2:8,3:9,4:10,5:11,6:14,7:5,8:2,9:0,10:1,11:7,12:12,13:13,14:15,15:6,16:17
//
// -------------------------------------------------------

`timescale 1 ns / 1 ns

module management_irq_mapper
(
    // -------------------
    // Clock & Reset
    // -------------------
    input clk,
    input reset,

    // -------------------
    // IRQ Receivers
    // -------------------
    input                receiver0_irq,
    input                receiver1_irq,
    input                receiver2_irq,
    input                receiver3_irq,
    input                receiver4_irq,
    input                receiver5_irq,
    input                receiver6_irq,
    input                receiver7_irq,
    input                receiver8_irq,
    input                receiver9_irq,
    input                receiver10_irq,
    input                receiver11_irq,
    input                receiver12_irq,
    input                receiver13_irq,
    input                receiver14_irq,
    input                receiver15_irq,
    input                receiver16_irq,

    // -------------------
    // Command Source (Output)
    // -------------------
    output reg [31 : 0] sender_irq
);


    always @* begin
	sender_irq = 0;

        sender_irq[4] = receiver0_irq;
        sender_irq[3] = receiver1_irq;
        sender_irq[8] = receiver2_irq;
        sender_irq[9] = receiver3_irq;
        sender_irq[10] = receiver4_irq;
        sender_irq[11] = receiver5_irq;
        sender_irq[14] = receiver6_irq;
        sender_irq[5] = receiver7_irq;
        sender_irq[2] = receiver8_irq;
        sender_irq[0] = receiver9_irq;
        sender_irq[1] = receiver10_irq;
        sender_irq[7] = receiver11_irq;
        sender_irq[12] = receiver12_irq;
        sender_irq[13] = receiver13_irq;
        sender_irq[15] = receiver14_irq;
        sender_irq[6] = receiver15_irq;
        sender_irq[17] = receiver16_irq;
    end

endmodule

