module fabric_udp_stream (
	csi_clock_clk,
	csi_clock_reset,

	avs_s0_write,
	avs_s0_read,
	avs_s0_address,
	avs_s0_byteenable,
	avs_s0_writedata,
	avs_s0_readdata,

	aso_src0_valid,
	aso_src0_ready,
	aso_src0_data,
	aso_src0_empty,
	aso_src0_startofpacket,
	aso_src0_endofpacket,
	
	asi_snk0_valid,
	asi_snk0_ready,
	asi_snk0_data,
	asi_snk0_empty,
	asi_snk0_startofpacket,
	asi_snk0_endofpacket
);

// clock interface
input  wire         csi_clock_clk;
input  wire         csi_clock_reset;
       
// slave interface
input  wire         avs_s0_write;
input  wire         avs_s0_read;
input  wire [3:0]   avs_s0_address;
input  wire [3:0]   avs_s0_byteenable;
input  wire [31:0]  avs_s0_writedata;
output wire [31:0]  avs_s0_readdata;
       
// source interface
output wire             aso_src0_valid;
input  wire             aso_src0_ready;
output wire [31:0]  aso_src0_data;
output wire [1:0]   aso_src0_empty;
output wire          aso_src0_startofpacket;
output wire          aso_src0_endofpacket;
       
// sink interface
input  wire         asi_snk0_valid;
output wire         asi_snk0_ready;
input  wire [31:0]  asi_snk0_data;
input  wire [1:0]   asi_snk0_empty;
input  wire         asi_snk0_startofpacket;
input  wire         asi_snk0_endofpacket;

wire valid;
wire ready;
wire [31:0] data;
wire [1:0] empty;
wire sop;
wire eop;

udp_payload_inserter udp_payload (
	.csi_clock_clk			( csi_clock_clk ),
	.csi_clock_reset		( csi_clock_reset ),
		
	.avs_s0_write			( avs_s0_write ),
	.avs_s0_read			( avs_s0_read ),
	.avs_s0_address			( avs_s0_address ),
	.avs_s0_byteenable		( avs_s0_byteenable ),
	.avs_s0_writedata		( avs_s0_writedata ),
	.avs_s0_readdata		( avs_s0_readdata ),
		
	.aso_src0_valid			( valid ),
	.aso_src0_ready			( ready ),
	.aso_src0_data			( data ),
	.aso_src0_empty			( empty ),
	.aso_src0_startofpacket ( sop ),
	.aso_src0_endofpacket	( eop ),
	
	.asi_snk0_valid			( asi_snk0_valid ),
	.asi_snk0_ready			( asi_snk0_ready ),
	.asi_snk0_data			( asi_snk0_data ),
	.asi_snk0_empty			( asi_snk0_empty ),
	.asi_snk0_startofpacket	( asi_snk0_startofpacket ),
	.asi_snk0_endofpacket	( asi_snk0_endofpacket )
);

alignment_pad_inserter alignment_padder ( 
    .csi_clock_clk			( csi_clock_clk ),
    .csi_clock_reset		( csi_clock_reset ),
    
    // source interface
    .aso_src0_valid			( aso_src0_valid ),
    .aso_src0_ready			( aso_src0_ready ),
    .aso_src0_data			( aso_src0_data ),
    .aso_src0_empty			( aso_src0_empty ),
    .aso_src0_startofpacket	( aso_src0_startofpacket ),
    .aso_src0_endofpacket	( aso_src0_endofpacket ),
    
    // sink interface
    .asi_snk0_valid			( valid ),
    .asi_snk0_ready			( ready ),
    .asi_snk0_data			( data ),
    .asi_snk0_empty			( empty ),
    .asi_snk0_startofpacket	( sop ),
    .asi_snk0_endofpacket	( eop )
);

endmodule

