`default_nettype none
module ag_dac
  (
   input wire 	      clk_625, // 62.5 MHz clock (ext clock)
   input wire 	      clk_125, // 125 MHz clock (ethernet)
   input wire 	      reset,

   // DAC clock
   input wire 	      clk_dac,

   // DAC output pins
   output wire 	      dac_xor,
   output wire 	      dac_torb,
   output wire 	      dac_pd,
   output wire 	      dac_seliq,
   output wire [13:0] dac_d,

   // controls from qsys
   input wire [31:0]  dac_ctrl,
   input wire [31:0]  dac_data,

   // trigger input
   input wire 	      trigger
   );


   // drive the DAC control and data pins
   
   assign dac_pd	= ~dac_ctrl[0]; // DAC power down
   wire   dac_seliq_x	= dac_ctrl[1]; // DAC select I or Q outputs
   assign dac_xor	= dac_ctrl[2]; // invert DAC data
   assign dac_torb 	= dac_ctrl[3]; // two's complement data format
   wire   pulser_enable = dac_ctrl[4]; // enable DAC pulser driven by trigger
   wire   ramp_enable   = dac_ctrl[5]; // enable DAC linear ramp

   reg [13:0] dac_d_reg;
   reg 	      dac_seliq_reg;
  
   assign dac_d = dac_d_reg;
   assign dac_seliq = dac_seliq_reg;
  
   // sync trigger with the dac clock

   reg 	    trigger_sync;
   reg 	    trigger1;

   always@ (posedge clk_dac) begin
      trigger1 <= trigger;
      trigger_sync <= trigger1;
   end

   // compute the dac pulse
   
   wire [13:0] dac_d_pulse = (pulser_enable & !trigger_sync) ? 14'b0: dac_data[13:0];

   // state machine to ramp up and down
  
   reg [3:0]   state;
   //assign state_out = state;
   parameter
     st_wait       = 0,
     st_start_up   = 1,
     st_ramp_up    = 2,
     st_start_top  = 3,
     st_top        = 4,
     st_start_down = 5,
     st_ramp_down  = 6,
     st_done       = 7;
   
   reg [15:0]  d_base; // base line
   reg [15:0]  d_max; // pulse maximum
   reg [15:0]  d_up; // ramp up rate
   reg [15:0]  d_down; // ramp down rate
   
   reg [15:0]  d_out;
   wire [15:0] d_amp_up = d_max - d_out;
   wire [15:0] d_amp_down = d_out - d_base;
   
   reg [15:0]  top_len;
   reg [15:0]  top_cnt;

   reg 	       flipflop;

   always_ff@ (posedge clk_dac or posedge reset) begin
      if (reset) begin
	 state = st_wait;
         d_out <= 0;
         d_base <= 0;
         d_max <= 0;
         d_up <= 0;
         d_down <= 0;
         top_len <= 0;
         top_cnt <= 0;
	 flipflop <= 0;
      end else begin // if (reset)
	 flipflop <= ~flipflop;
	 if (flipflop) begin
	    case (state)
	      st_wait: begin
		 d_base <= dac_data[31:16];
		 d_max <= dac_data[15:0];
		 d_out <= d_base;
		 d_up[15:14] <= 0;
		 d_up[13:6] <= dac_ctrl[31:24];
		 d_up[5:1] <= 0;
		 d_up[0] <= 1; // ensure up counter is never zero
		 d_down[15:14] <= 0;
		 d_down[13:6] <= dac_ctrl[23:16];
		 d_down[5:1] <= 0;
		 d_down[0] <= 1; // ensure down counter is never zero
		 top_len[15:8] <= 0;
		 top_len[7:0] <= dac_ctrl[15:8];
		 if (trigger_sync && ramp_enable) begin
		    state <= st_start_up;
		 end
	      end
	      st_start_up: begin
		 state <= st_ramp_up;
		 top_cnt <= top_len;
	      end
	      st_ramp_up: begin
		 if (top_cnt) begin
		    top_cnt <= top_cnt - 16'h1;
		    if (d_up < d_amp_up) begin
		       d_out <= d_out + d_up;
		    end else begin
		       d_out <= d_max;
		       state <= st_top;
		    end
		 end else begin
		    state <= st_start_down;
		 end
	      end
	      st_top: begin
		 if (top_cnt) begin
		    top_cnt <= top_cnt - 16'h1;
		 end else begin
		    state <= st_start_down;
		 end
	      end
	      st_start_down: begin
		 state <= st_ramp_down;
	      end
	      st_ramp_down: begin
		 if (d_down < d_amp_down) begin
		    d_out <= d_out - d_down;
		 end else begin
		    d_out <= d_base;
		    state <= st_done;
		 end
	      end
	      st_done: begin
		 if (!trigger_sync) begin
		    state <= st_wait;
		 end
	      end
	      default: begin
		 state <= st_done;
	      end
	    endcase // case (state)
	 end // if (flipflop)
      end // else: !if(reset)
   end // always@ (posedge clk_dac or posedge reset)
   
   // DAC output mux
   
   always_ff@ (posedge clk_dac) begin
      dac_seliq_reg <= dac_seliq_x;
      if (pulser_enable) begin
	 dac_d_reg <= dac_d_pulse;
      end else if (ramp_enable) begin
	 dac_d_reg <= d_out[13:0];
      end else begin
	 dac_d_reg[13:0] <= dac_data[13:0];
      end
   end

endmodule // ag_dac
