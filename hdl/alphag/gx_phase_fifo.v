// not really a fifo - but if read remains a little behind write, behaves like one
// (also reset sequence is simpler than real fifo)
// using 4 word mem buffer with reads at 0,1,2,3,0 ... and writes at 2,3,0,1,2 ...
module gx_phase_fifo ( Rclk, RdEn, data_out, Wclk, WrEn, data_in, RSTn );

parameter DWIDTH = 9; // default for gx_interface

input  wire Rclk, Wclk, RdEn, WrEn, RSTn;
output wire [DWIDTH-1:0] data_out;
input  wire [DWIDTH-1:0] data_in;

reg [1:0] read_addr; reg [1:0] write_addr;
always @ (posedge Rclk or negedge RSTn) begin
   if (!RSTn)      read_addr <= 2'h0;
   else if( RdEn ) read_addr <= read_addr + 2'h1;
	else            read_addr <= read_addr;
end

always @ (posedge Wclk or negedge RSTn) begin
   if (!RSTn)      write_addr <= 2'h2; // read_addr must be < write_addr
   else if( WrEn ) write_addr <= write_addr + 2'h1;
	else            write_addr <= write_addr;
end

gx_phase_fifo_dpram #(.DWIDTH(DWIDTH)) fifo_mem (
   .wrclock(Wclk),   .wren(WrEn),   .wraddress(write_addr),   .data(data_in),	
   .rdclock(Rclk),                  .rdaddress(read_addr ),     .q(data_out)
);

endmodule
