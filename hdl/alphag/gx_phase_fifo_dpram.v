module gx_phase_fifo_dpram ( wrclock, wraddress, wren, data, rdclock, rdaddress, q);

parameter DWIDTH = 9; // default for gx_interface

input [DWIDTH-1:0] data;  output [DWIDTH-1:0] q;
input   [1:0] rdaddress;  input[1:0] wraddress;  input wren;
input           rdclock;  input        wrclock;

altsyncram	altsyncram_component (  .aclr0 (1'b0),  .aclr1 (1'b0),  .eccstatus (),
   .clock0(wrclock),  .address_a(wraddress),  .wren_a(wren),   .data_a(data),  .rden_a (1'b1),  .q_a (),
   .clock1(rdclock),  .address_b(rdaddress),  .rden_b(1'b1),   .q_b ( q ),     .wren_b (1'b0),  .data_b ({DWIDTH{1'b1}}),
   .clocken0(1'b1),   .clocken1(1'b1),        .clocken2(1'b1),                 .clocken3(1'b1),
   .byteena_a(1'b1),  .byteena_b(1'b1),       .addressstall_a(1'b0),           .addressstall_b(1'b0)
);

defparam
altsyncram_component.address_aclr_b = "NONE",
altsyncram_component.address_reg_b = "CLOCK1",
altsyncram_component.clock_enable_input_a = "BYPASS",
altsyncram_component.clock_enable_input_b = "BYPASS",
altsyncram_component.clock_enable_output_b = "BYPASS",
altsyncram_component.intended_device_family = "Arria V",
altsyncram_component.lpm_type = "altsyncram",
altsyncram_component.numwords_a = 4,
altsyncram_component.numwords_b = 4,
altsyncram_component.operation_mode = "DUAL_PORT",
altsyncram_component.outdata_aclr_b = "NONE",
altsyncram_component.outdata_reg_b = "CLOCK1",
altsyncram_component.power_up_uninitialized = "FALSE",
altsyncram_component.ram_block_type = "MLAB",
altsyncram_component.widthad_a = 2,
altsyncram_component.widthad_b = 2,
altsyncram_component.width_a = DWIDTH,
altsyncram_component.width_b = DWIDTH,
altsyncram_component.width_byteena_a = 1;

endmodule
