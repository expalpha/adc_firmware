`default_nettype none
module alphag
  (
   input wire 					reset,
   input wire 					clk_625, // 62.5 MHz clock (ext clock)
   input wire 					clk_125, // 125 MHz clock (ethernet)

   // 100 MHz 16ch onboard ADC 

   input wire 					clk_adc100,
   input wire [NUM_ADC_CH-1:0][SZ_ADC_DATA-1:0] data_adc100,

   // 62.5 MHz 32ch FMC ADC
   
   input wire 					clk_adc625,
   input wire [NUM_FMC_CH-1:0][SZ_FMC_DATA-1:0] data_adc625,

   // esata link
   
   input wire 					esata_clk,
   input wire 					esata_trig,
   
   // NIM inputs

   input wire 					nim_clk,
   input wire 					nim_trig,

   // module ID from ESPER, 8 bits

   input wire [7:0] 				module_id,

   // board ID, 0..15

   input wire [3:0] 				board_id,

   // adc16 and adc32 discriminator thresholds
						
   input wire [15:0] 				adc16_threshold,
   input wire [15:0] 				adc32_threshold,

   // control and status registers

   input  wire [31:0]                           ctrl,
   output wire [31:0]                           stat_out,

   // output of the adc16 and adc32 discriminators

   output wire [15:0] 				adc16_bits_out,
   output wire [31:0]				adc32_bits_out,

   input  wire                                  clk_counter,   // NIOS 12.5 MHz clock
   output wire [31:0] 				adc16_counter_out,
   output wire [31:0]				adc32_counter_out,

   // output bits

   output wire [63:0] 				sas_bits_out

   );

   assign stat_out = 0;

   parameter NUM_ADC_CH;
   parameter NUM_FMC_CH;
   parameter SZ_ADC_DATA;
   parameter SZ_FMC_DATA;

   // per-wire discriminators, 100 MHz ADC

   wire [15:0] thr = adc16_threshold;
   wire        pol = !adc16_threshold[15];
   wire [15:0] trig_adc100;

   wire        ena100 = (|adc16_threshold); // enable if threshold is non-zero

   ag_trig_one trig0(.clk(clk_adc100), .threshold(thr), .polarity_pos(pol), .adc_stream(data_adc100[0]), .trig(trig_adc100[0]));
   ag_trig_one trig1(.clk(clk_adc100), .threshold(thr), .polarity_pos(pol), .adc_stream(data_adc100[1]), .trig(trig_adc100[1]));
   ag_trig_one trig2(.clk(clk_adc100), .threshold(thr), .polarity_pos(pol), .adc_stream(data_adc100[2]), .trig(trig_adc100[2]));
   ag_trig_one trig3(.clk(clk_adc100), .threshold(thr), .polarity_pos(pol), .adc_stream(data_adc100[3]), .trig(trig_adc100[3]));
   
   ag_trig_one trig4(.clk(clk_adc100), .threshold(thr), .polarity_pos(pol), .adc_stream(data_adc100[4]), .trig(trig_adc100[4]));
   ag_trig_one trig5(.clk(clk_adc100), .threshold(thr), .polarity_pos(pol), .adc_stream(data_adc100[5]), .trig(trig_adc100[5]));
   ag_trig_one trig6(.clk(clk_adc100), .threshold(thr), .polarity_pos(pol), .adc_stream(data_adc100[6]), .trig(trig_adc100[6]));
   ag_trig_one trig7(.clk(clk_adc100), .threshold(thr), .polarity_pos(pol), .adc_stream(data_adc100[7]), .trig(trig_adc100[7]));
   
   ag_trig_one trig8(.clk(clk_adc100), .threshold(thr), .polarity_pos(pol), .adc_stream(data_adc100[8]), .trig(trig_adc100[8]));
   ag_trig_one trig9(.clk(clk_adc100), .threshold(thr), .polarity_pos(pol), .adc_stream(data_adc100[9]), .trig(trig_adc100[9]));
   ag_trig_one trig10(.clk(clk_adc100), .threshold(thr), .polarity_pos(pol), .adc_stream(data_adc100[10]), .trig(trig_adc100[10]));
   ag_trig_one trig11(.clk(clk_adc100), .threshold(thr), .polarity_pos(pol), .adc_stream(data_adc100[11]), .trig(trig_adc100[11]));
   
   ag_trig_one trig12(.clk(clk_adc100), .threshold(thr), .polarity_pos(pol), .adc_stream(data_adc100[12]), .trig(trig_adc100[12]));
   ag_trig_one trig13(.clk(clk_adc100), .threshold(thr), .polarity_pos(pol), .adc_stream(data_adc100[13]), .trig(trig_adc100[13]));
   ag_trig_one trig14(.clk(clk_adc100), .threshold(thr), .polarity_pos(pol), .adc_stream(data_adc100[14]), .trig(trig_adc100[14]));
   ag_trig_one trig15(.clk(clk_adc100), .threshold(thr), .polarity_pos(pol), .adc_stream(data_adc100[15]), .trig(trig_adc100[15]));

   assign adc16_bits_out = trig_adc100;

   wire        grand_or_adc100 = |trig_adc100;
   wire [4:0]  mult16_adc100;

   integer     i;
   always_comb begin
      mult16_adc100 = 0;
      for(i=0; i<16; i=i+1) begin
	 mult16_adc100 = mult16_adc100 + trig_adc100[i];
      end
   end

   reg grand_or_clk100;
   reg [4:0] mult16_clk100;
   always_ff@ (posedge clk_adc100) begin
      grand_or_clk100 <= grand_or_adc100;
      mult16_clk100 <= mult16_adc100;
   end

   // per-wire discriminators, 62.5 MHz ADC

   //wire [15:0] thr_adc625 = 16'h2000;
   //wire [15:0] thr_adc625 = { module_id[7:4], 12'h000 };
   wire [15:0] thr_adc625 = adc32_threshold;
   wire        pol_adc625 = !adc32_threshold[15];
   wire [31:0] trig_adc625;

   wire        ena625 = (|adc32_threshold); // enable if threshold is non-zero

   ag_trig_one ag_trig625_00(.clk(clk_adc625), .threshold(thr_adc625), .polarity_pos(pol_adc625), .adc_stream(data_adc625[0]), .trig(trig_adc625[0]));
   ag_trig_one ag_trig625_01(.clk(clk_adc625), .threshold(thr_adc625), .polarity_pos(pol_adc625), .adc_stream(data_adc625[1]), .trig(trig_adc625[1]));
   ag_trig_one ag_trig625_02(.clk(clk_adc625), .threshold(thr_adc625), .polarity_pos(pol_adc625), .adc_stream(data_adc625[2]), .trig(trig_adc625[2]));
   ag_trig_one ag_trig625_03(.clk(clk_adc625), .threshold(thr_adc625), .polarity_pos(pol_adc625), .adc_stream(data_adc625[3]), .trig(trig_adc625[3]));

   ag_trig_one ag_trig625_04(.clk(clk_adc625), .threshold(thr_adc625), .polarity_pos(pol_adc625), .adc_stream(data_adc625[4]), .trig(trig_adc625[4]));
   ag_trig_one ag_trig625_05(.clk(clk_adc625), .threshold(thr_adc625), .polarity_pos(pol_adc625), .adc_stream(data_adc625[5]), .trig(trig_adc625[5]));
   ag_trig_one ag_trig625_06(.clk(clk_adc625), .threshold(thr_adc625), .polarity_pos(pol_adc625), .adc_stream(data_adc625[6]), .trig(trig_adc625[6]));
   ag_trig_one ag_trig625_07(.clk(clk_adc625), .threshold(thr_adc625), .polarity_pos(pol_adc625), .adc_stream(data_adc625[7]), .trig(trig_adc625[7]));

   ag_trig_one ag_trig625_08(.clk(clk_adc625), .threshold(thr_adc625), .polarity_pos(pol_adc625), .adc_stream(data_adc625[8]), .trig(trig_adc625[8]));
   ag_trig_one ag_trig625_09(.clk(clk_adc625), .threshold(thr_adc625), .polarity_pos(pol_adc625), .adc_stream(data_adc625[9]), .trig(trig_adc625[9]));
   ag_trig_one ag_trig625_10(.clk(clk_adc625), .threshold(thr_adc625), .polarity_pos(pol_adc625), .adc_stream(data_adc625[10]), .trig(trig_adc625[10]));
   ag_trig_one ag_trig625_11(.clk(clk_adc625), .threshold(thr_adc625), .polarity_pos(pol_adc625), .adc_stream(data_adc625[11]), .trig(trig_adc625[11]));

   ag_trig_one ag_trig625_12(.clk(clk_adc625), .threshold(thr_adc625), .polarity_pos(pol_adc625), .adc_stream(data_adc625[12]), .trig(trig_adc625[12]));
   ag_trig_one ag_trig625_13(.clk(clk_adc625), .threshold(thr_adc625), .polarity_pos(pol_adc625), .adc_stream(data_adc625[13]), .trig(trig_adc625[13]));
   ag_trig_one ag_trig625_14(.clk(clk_adc625), .threshold(thr_adc625), .polarity_pos(pol_adc625), .adc_stream(data_adc625[14]), .trig(trig_adc625[14]));
   ag_trig_one ag_trig625_15(.clk(clk_adc625), .threshold(thr_adc625), .polarity_pos(pol_adc625), .adc_stream(data_adc625[15]), .trig(trig_adc625[15]));

   ag_trig_one ag_trig625_16(.clk(clk_adc625), .threshold(thr_adc625), .polarity_pos(pol_adc625), .adc_stream(data_adc625[16]), .trig(trig_adc625[16]));
   ag_trig_one ag_trig625_17(.clk(clk_adc625), .threshold(thr_adc625), .polarity_pos(pol_adc625), .adc_stream(data_adc625[17]), .trig(trig_adc625[17]));
   ag_trig_one ag_trig625_18(.clk(clk_adc625), .threshold(thr_adc625), .polarity_pos(pol_adc625), .adc_stream(data_adc625[18]), .trig(trig_adc625[18]));
   ag_trig_one ag_trig625_19(.clk(clk_adc625), .threshold(thr_adc625), .polarity_pos(pol_adc625), .adc_stream(data_adc625[19]), .trig(trig_adc625[19]));

   ag_trig_one ag_trig625_20(.clk(clk_adc625), .threshold(thr_adc625), .polarity_pos(pol_adc625), .adc_stream(data_adc625[20]), .trig(trig_adc625[20]));
   ag_trig_one ag_trig625_21(.clk(clk_adc625), .threshold(thr_adc625), .polarity_pos(pol_adc625), .adc_stream(data_adc625[21]), .trig(trig_adc625[21]));
   ag_trig_one ag_trig625_22(.clk(clk_adc625), .threshold(thr_adc625), .polarity_pos(pol_adc625), .adc_stream(data_adc625[22]), .trig(trig_adc625[22]));
   ag_trig_one ag_trig625_23(.clk(clk_adc625), .threshold(thr_adc625), .polarity_pos(pol_adc625), .adc_stream(data_adc625[23]), .trig(trig_adc625[23]));

   ag_trig_one ag_trig625_24(.clk(clk_adc625), .threshold(thr_adc625), .polarity_pos(pol_adc625), .adc_stream(data_adc625[24]), .trig(trig_adc625[24]));
   ag_trig_one ag_trig625_25(.clk(clk_adc625), .threshold(thr_adc625), .polarity_pos(pol_adc625), .adc_stream(data_adc625[25]), .trig(trig_adc625[25]));
   ag_trig_one ag_trig625_26(.clk(clk_adc625), .threshold(thr_adc625), .polarity_pos(pol_adc625), .adc_stream(data_adc625[26]), .trig(trig_adc625[26]));
   ag_trig_one ag_trig625_27(.clk(clk_adc625), .threshold(thr_adc625), .polarity_pos(pol_adc625), .adc_stream(data_adc625[27]), .trig(trig_adc625[27]));

   ag_trig_one ag_trig625_28(.clk(clk_adc625), .threshold(thr_adc625), .polarity_pos(pol_adc625), .adc_stream(data_adc625[28]), .trig(trig_adc625[28]));
   ag_trig_one ag_trig625_29(.clk(clk_adc625), .threshold(thr_adc625), .polarity_pos(pol_adc625), .adc_stream(data_adc625[29]), .trig(trig_adc625[29]));
   ag_trig_one ag_trig625_30(.clk(clk_adc625), .threshold(thr_adc625), .polarity_pos(pol_adc625), .adc_stream(data_adc625[30]), .trig(trig_adc625[30]));
   ag_trig_one ag_trig625_31(.clk(clk_adc625), .threshold(thr_adc625), .polarity_pos(pol_adc625), .adc_stream(data_adc625[31]), .trig(trig_adc625[31]));

   assign adc32_bits_out = trig_adc625;

   // this does not work!!!
   //genvar      ii;
   //generate
   //for (ii=1; ii<32;  ii=ii+1) begin :generate_ag_trig625
   //ag_trig_one ag_trig625_[ii](.clk(clk_adc625), .threshold(thr_adc625), .polarity_pos(pol_adc625), .adc_stream(data_adc625[ii]), .trig(trig_adc625[ii]));
   //end
   //endgenerate
   
   wire        grand_or_adc625 = |trig_adc625;
   wire [4:0]  mult32_adc625;

   always_comb begin
      mult32_adc625 = 0;
      for(i=0; i<32; i=i+1) begin
	 mult32_adc625 = mult32_adc625 + trig_adc625[i];
      end
   end

   reg grand_or_clk625;
   reg [4:0] mult32_clk625;
   always_ff@ (posedge clk_adc625) begin
      grand_or_clk625 <= grand_or_adc625;
      mult32_clk625 <= mult32_adc625;
   end

   // increment counters
   reg gor100_clk_counter;
   reg gor100_clk_counter1;
   reg gor100_clk_counter2;
   reg gor100_clk_counterP;
   reg gor625_clk_counter;
   reg gor625_clk_counter1;
   reg gor625_clk_counter2;
   reg gor625_clk_counterP;
   always_ff@ (posedge clk_counter) begin
      // NOTE: these synchronizers will not work,
      // the grand-or signals need to be stretched: 1 clock pulses
      // will be missed by the very slow NIOS 12.5 MHz clock!
      
      // synchronize the 100 MHz signals
      gor100_clk_counter1 <= grand_or_clk100;
      gor100_clk_counter2 <= gor100_clk_counter1;
      gor100_clk_counter  <= gor100_clk_counter2;
      
      // synchronize the 62.5 MHz signals
      gor625_clk_counter1 <= grand_or_clk625;
      gor625_clk_counter2 <= gor625_clk_counter1;
      gor625_clk_counter  <= gor625_clk_counter2;

      // save previous value to detect leading edges
      gor100_clk_counterP <= gor100_clk_counter;
      gor625_clk_counterP <= gor625_clk_counter;

      if (reset) begin
         adc16_counter_out <= 0;
         adc32_counter_out <= 0;
      end else begin
         if (gor100_clk_counter & !gor100_clk_counterP) begin
            adc16_counter_out <= adc16_counter_out + 1;
         end
         if (gor625_clk_counter & !gor625_clk_counterP) begin
            adc32_counter_out <= adc32_counter_out + 1;
         end
      end
   end

   // drive the sas output bits to TRG
   
   assign sas_bits_out[15:0]  = (ena100) ? trig_adc100[15:0] : 16'b0;
   assign sas_bits_out[47:16] = (ena625) ? trig_adc625[31:0] : 32'b0;
   
   assign sas_bits_out[48] = grand_or_clk100;
   assign sas_bits_out[49] = mult16_clk100[0];
   assign sas_bits_out[50] = mult16_clk100[1];
   assign sas_bits_out[51] = |mult16_clk100[4:2];

   assign sas_bits_out[52] = grand_or_clk625;
   assign sas_bits_out[53] = mult32_clk625[0];
   assign sas_bits_out[54] = mult32_clk625[1];
   assign sas_bits_out[55] = |mult32_clk625[4:2];

   assign sas_bits_out[56] = esata_clk   ^ ctrl[0];
   assign sas_bits_out[57] = esata_trig  ^ ctrl[1];
   assign sas_bits_out[58] = nim_clk     ^ ctrl[2];
   assign sas_bits_out[59] = nim_trig    ^ ctrl[3];

   assign sas_bits_out[63:60] = board_id[3:0];
   
endmodule // alphag
