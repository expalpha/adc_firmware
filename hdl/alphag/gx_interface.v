module gx_interface #(parameter NCHAN = 1) ( input  wire RSTn, // 1 chan is default for 8bit 1Gbit interface
   input  wire rx_inclk,     rx_outclk,   input  wire tx_inclk,     tx_outclk,
   input  wire [  NCHAN-1:0] rx_ctrlin,   input  wire [  NCHAN-1:0] tx_ctrlin,
   output wire [  NCHAN-1:0] rx_ctrlout,  output wire [  NCHAN-1:0] tx_ctrlout,
   input  wire [8*NCHAN-1:0] rx_datain,   input  wire [8*NCHAN-1:0] tx_datain,
   output wire [8*NCHAN-1:0] rx_dataout,  output wire [8*NCHAN-1:0] tx_dataout
);

gx_phase_fifo #(.DWIDTH(8*NCHAN+NCHAN)) rx4g_fifo_1 ( .RSTn(RSTn),
   .Rclk(rx_outclk ), .RdEn(1'b1),  .data_out({rx_ctrlout,rx_dataout}),
   .Wclk(rx_inclk),   .WrEn(1'b1),  .data_in ({rx_ctrlin, rx_datain })
);

gx_phase_fifo #(.DWIDTH(8*NCHAN+NCHAN)) tx4g_fifo_0 ( .RSTn(RSTn),
   .Rclk(tx_outclk),  .RdEn(1'b1),   .data_out({tx_ctrlout,tx_dataout}),  
   .Wclk(tx_inclk),   .WrEn(1'b1),   .data_in ({tx_ctrlin, tx_datain })
);

endmodule
