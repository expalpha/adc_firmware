module sas_links
  (
   input wire 	     reset,
   input wire 	     sfp_clk,
   input wire [7:0]  FMCX_RX,
   output wire [7:0] FMCX_TX,
   input wire [63:0] sas_bits
   ); 

   reg 		     cout;
   reg [7:0] 	     bout;
   
   reg [4:0] 	     cnt;
   reg [63:0] 	     latched_bits;

   always_ff@ (posedge sfp_clk or posedge reset) begin
      if (reset) begin
	 cnt <= 0;
	 cout <= 0;
	 bout <= 0;
	 latched_bits[63:0] <= 64'h0;
      end else begin
	 latched_bits[63:0] <= latched_bits[63:0] | sas_bits[63:0];
	 case (cnt)
	   0: begin
	      bout <= {4'h0,latched_bits[3:0]};
	      cout <= 0;
	      latched_bits[3:0] <= sas_bits[3:0];
	      cnt <= cnt + 1'b1;
	   end
	   1: begin
	      bout <= {4'h1,latched_bits[7:4]};
	      cout <= 0;
	      latched_bits[7:4] <= sas_bits[7:4];
	      cnt <= cnt + 1'b1;
	   end
	   2: begin
	      bout <= {4'h2,latched_bits[11:8]};
	      cout <= 0;
	      latched_bits[11:8] <= sas_bits[11:8];
	      cnt <= cnt + 1'b1;
	   end
	   3: begin
	      bout <= {4'h3,latched_bits[15:12]};
	      cout <= 0;
	      latched_bits[15:12] <= sas_bits[15:12];
	      cnt <= cnt + 1'b1;
	   end
	   4: begin
	      bout <= {4'h4,latched_bits[19:16]};
	      cout <= 0;
	      latched_bits[19:16] <= sas_bits[19:16];
	      cnt <= cnt + 1'b1;
	   end
	   5: begin
	      bout <= {4'h5,latched_bits[23:20]};
	      cout <= 0;
	      latched_bits[23:20] <= sas_bits[23:20];
	      cnt <= cnt + 1'b1;
	   end
	   6: begin
	      bout <= {4'h6,latched_bits[27:24]};
	      cout <= 0;
	      latched_bits[27:24] <= sas_bits[27:24];
	      cnt <= cnt + 1'b1;
	   end
	   7: begin
	      bout <= {4'h7,latched_bits[31:28]};
	      cout <= 0;
	      latched_bits[31:28] <= sas_bits[31:28];
	      cnt <= cnt + 1'b1;
	   end
	   8: begin
	      bout <= {4'h8,latched_bits[35:32]};
	      cout <= 0;
	      latched_bits[35:32] <= sas_bits[35:32];
	      cnt <= cnt + 1'b1;
	   end
	   9: begin
	      bout <= {4'h9,latched_bits[39:36]};
	      cout <= 0;
	      latched_bits[39:36] <= sas_bits[39:36];
	      cnt <= cnt + 1'b1;
	   end
	   10: begin
	      bout <= {4'hA,latched_bits[43:40]};
	      cout <= 0;
	      latched_bits[43:40] <= sas_bits[43:40];
	      cnt <= cnt + 1'b1;
	   end
	   11: begin
	      bout <= {4'hB,latched_bits[47:44]};
	      cout <= 0;
	      latched_bits[47:44] <= sas_bits[47:44];
	      cnt <= cnt + 1'b1;
	   end
	   12: begin
	      bout <= {4'hC,latched_bits[51:48]};
	      cout <= 0;
	      latched_bits[51:48] <= sas_bits[51:48];
	      cnt <= cnt + 1'b1;
	   end
	   13: begin
	      bout <= {4'hD,latched_bits[55:52]};
	      cout <= 0;
	      latched_bits[55:52] <= sas_bits[55:52];
	      cnt <= cnt + 1'b1;
	   end
	   14: begin
	      bout <= {4'hE,latched_bits[59:56]};
	      cout <= 0;
	      latched_bits[59:56] <= sas_bits[59:56];
	      cnt <= cnt + 1'b1;
	   end
	   15: begin
	      bout <= {4'hF,latched_bits[63:60]};
	      cout <= 0;
	      latched_bits[63:60] <= sas_bits[63:60];
	      cnt <= cnt + 1'b1;
	   end
	   16: begin
	      bout <= 8'hBC; // K.28.5 comma symbol
	      cout <= 1;
	      cnt <= cnt + 1'b1;
	   end
	   17: begin
	      bout <= 8'h3C; // K.28.1 comma symbol
	      cout <= 1;
	      cnt <= 0;
	   end
	   default: begin
	      if (cnt[0] == 0) begin
		 bout <= 8'hBC; // K.28.5 comma symbol
		 cout <= 1;
	      end else begin
		 bout <= 8'h3C; // K.28.1 comma symbol
		 cout <= 1;
	      end
	      cnt <= cnt + 1'b1;
	   end
	 endcase
      end // else: !if(reset)
   end // always_ff@

   wire [63:0] 	      gx_TxDataSas = {bout,bout,bout,bout,bout,bout,bout,bout};
   wire [7:0] 	      tx_ctrlena_sas = {cout,cout,cout,cout,cout,cout,cout,cout};

   gx_arriav_block #(.NCHAN(8), .RATE(1) ) sas_block
     (
      .ref_clk(sfp_clk),
      .reset(reset),
      //.rx_dataout(   gx_RxDataSas[31:  0]),
      //.rx_ctrldet(rx_ctrldet_sas[3: 0]),
      .tx_datain(gx_TxDataSas),
      .tx_ctrlen(tx_ctrlena_sas),
      .FMCX_RX(FMCX_RX),
      .FMCX_TX(FMCX_TX)
      );

endmodule
