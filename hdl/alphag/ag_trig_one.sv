//
// detector for ADC pulses
//
module ag_trig_one
  (
   input wire 	     reset,
   input wire 	     clk,
   input wire [15:0] adc_stream,
   input wire [15:0] threshold,
   input wire 	     polarity_pos, 
   output reg [15:0] baseline_out,
   output reg [15:0] adc_out,
   output reg 	     trig,
   output wire 	     trig_pos,
   output wire 	     trig_neg
   );

   // compute the baseline

   reg [15:0] 			adc_in;
   reg [15:0] 			adc0a;
   reg [15:0] 			adc0b;
   reg [15:0] 			adc0c;
   reg [15:0] 			adc0d;
   reg [18:0] 			adc0;
   reg [18:0] 			adc1;
   reg [18:0] 			adc2;
   reg [18:0] 			adc3;
   reg [18:0] 			adc4;
   reg [18:0] 			adc5;
   reg [18:0] 			adc6;
   reg [18:0] 			adc7;
   reg [18:0] 			adc_sum;

   always_ff@ (posedge clk) begin
      adc_in <= adc_stream + 16'h8000; // convert from signed to unsigned centered at 0x8000

      adc0a <= adc_in;
      adc0b <= adc0a;
      adc0c <= adc0b;
      adc0d <= adc0c;

      adc0 <= adc0d;
      adc1 <= adc0;
      adc2 <= adc1;
      adc3 <= adc2;
      adc4 <= adc3;
      adc5 <= adc4;
      adc6 <= adc5;
      adc7 <= adc6;

      adc_sum <= adc0 + adc1 + adc2 + adc3 + adc4 + adc5 + adc6 + adc7;
   end

   wire [15:0] adc_baseline = adc_sum[18:3]; // divide by 8
   wire [15:0] adc_value = adc_in - adc_baseline + 16'h8000;

   assign trig_neg = (adc_value < (16'h8000 + threshold));
   assign trig_pos = (adc_value > (16'h8000 + threshold));

   always_ff@ (posedge clk) begin
      adc_out <= adc_value;
      baseline_out <= adc_baseline;
      if (polarity_pos) begin
	 trig <= trig_pos;
      end else begin
	 trig <= trig_neg;
      end
   end
   
endmodule // alphag
