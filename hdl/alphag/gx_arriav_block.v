// comment this line to fail fit:    wire [255:0] gx_RxDataSas;   wire [175:0] gx_TxDataSas;
module gx_arriav_block #( parameter NCHAN = 1, parameter RATE = 1,  parameter POLINV = 0) (
   input  wire                    ref_clk,    input  wire                    reset,
   input  wire [8*NCHAN*RATE-1:0] tx_datain,  output wire [8*NCHAN*RATE-1:0] rx_dataout,
   input  wire [  NCHAN*RATE-1:0] tx_ctrlen,  output wire   [NCHAN*RATE-1:0] rx_ctrldet,
   input  wire        [NCHAN-1:0] FMCX_RX,    output wire        [NCHAN-1:0] FMCX_TX
);
// ** stratix ratematch will not work here as have all the extra bits
//    also need alt_data in/out for the non-data monitoring bits

genvar i,j;
generate
wire [8*NCHAN*RATE-1:0] gx_RxDataOut;    wire [8*NCHAN*RATE-1:0] gx_TxDataIn;
wire   [NCHAN*RATE-1:0] gx_RxCtrlOut;    wire   [NCHAN*RATE-1:0] gx_TxCtrlIn;
for(i=0; i<NCHAN; i=i+1) begin :if_chan
   gx_interface #(.NCHAN(RATE)) gx_if (  .RSTn(pll_locked[i]), // all data on tx_outclk either by our or altera ratematch
       .rx_inclk(       tx_clkout[i]),                   .tx_outclk(     tx_clkout[ i]),
		 .rx_outclk(           ref_clk),                   .tx_inclk(            ref_clk),
	    .rx_ctrlin(gx_RxCtrlOut[  RATE*(i+1)-1:   RATE*i]),  .rx_ctrlout(  rx_ctrldet[  RATE*(i+1)-1:   RATE*i]),
		 .rx_datain(gx_RxDataOut[8*RATE*(i+1)-1: 8*RATE*i]),  .rx_dataout(  rx_dataout[8*RATE*(i+1)-1: 8*RATE*i]),
	    .tx_ctrlin(   tx_ctrlen[  RATE*(i+1)-1:   RATE*i]),  .tx_ctrlout( gx_TxCtrlIn[  RATE*(i+1)-1:   RATE*i]),
		 .tx_datain(   tx_datain[8*RATE*(i+1)-1: 8*RATE*i]),  .tx_dataout( gx_TxDataIn[8*RATE*(i+1)-1: 8*RATE*i])
   );
end
wire     [64*NCHAN-1:0] gx_rawRxDataOut; wire     [44*NCHAN-1:0] gx_rawTxDataIn;  wire [64*NCHAN-1:0] gx_RmRxDataOut;
if( RATE == 1 )
   assign gx_rawRxDataOut = gx_RmRxDataOut;      // ratematching already done for us
else
   for(i=0; i<NCHAN; i=i+1) begin :ratematch_chan  // or have to do it ourselves
      rate_fifo fifo (           .reset(!pll_locked[i]),                    //aclr
         .wrclk(rx_clkout[i]),   .data_in ( gx_RmRxDataOut[64*i+63:64*i]),  //data 64 rxbits per chan
         .rdclk(tx_clkout[i]),   .data_out(gx_rawRxDataOut[64*i+63:64*i])   //   q
      );
   end
endgenerate

wire       [NCHAN-1:0]    rx_patalign /* synthesis keep */;
wire  [NCHAN*RATE-1:0]      rx_patdet /* synthesis keep */;  wire [NCHAN*RATE-1:0] rx_syncstatus    /* synthesis keep */;
wire  [NCHAN*RATE-1:0] rx_disperr_out /* synthesis keep */;  wire [NCHAN*RATE-1:0] rx_errdetect_out /* synthesis keep */;
gx_realign gxrealign ( // monitor errors and realign when needed (e.g. cable reconnect)
   .clk(                    ref_clk),  .rx_enarealign(      rx_patalign),
   .rx_errdetect(  rx_errdetect_out),  .rx_disperr(      rx_disperr_out),
   .rx_patdetect(         rx_patdet),  .rx_syncstatus(    rx_syncstatus)            // should keep count of realignments
);
wire [NCHAN-1:0] pll_locked;  wire [NCHAN-1:0] pll_powerdown;  wire [NCHAN-1:0] rx_lockedtodata;
wire [NCHAN-1:0]  rx_clkout;  wire [NCHAN-1:0]     tx_clkout;
gx_arriav_xcvr #(.NCHAN(NCHAN), .RATE(RATE), .POLINV(POLINV)) gx_inst (
   .tx_pll_refclk(              ref_clk), .rx_cdr_refclk(              ref_clk),
   .tx_std_coreclkin(         tx_clkout), .rx_std_coreclkin(         tx_clkout), // need both as txclkout
   .tx_std_clkout(            tx_clkout), .rx_std_clkout(            rx_clkout),
   .pll_locked(              pll_locked), .pll_powerdown(        pll_powerdown),
   .tx_analogreset(      tx_analogreset), .rx_analogreset(      rx_analogreset),
	.tx_digitalreset(    tx_digitalreset), .rx_digitalreset(    rx_digitalreset),
   .tx_cal_busy(            tx_cal_busy), .rx_cal_busy(            rx_cal_busy),
   .reconfig_to_xcvr(          gx_cfgin), .reconfig_from_xcvr(       gx_cfgout),
	.tx_serial_data(             FMCX_TX), .rx_serial_data(             FMCX_RX),
   .tx_parallel_data(    gx_rawTxDataIn), .rx_parallel_data(    gx_RmRxDataOut),
   .rx_is_lockedtodata( rx_lockedtodata)//, .rx_std_wa_patternalign(rx_std_wa_patternalign)
);
wire [NCHAN-1:0] rx_digitalreset;  wire [NCHAN-1:0] rx_analogreset;  wire [NCHAN-1:0] rx_cal_busy;  wire [NCHAN-1:0] rx_ready;
wire [NCHAN-1:0] tx_digitalreset;  wire [NCHAN-1:0] tx_analogreset;  wire [NCHAN-1:0] tx_cal_busy;  wire [NCHAN-1:0] tx_ready;
gx_arriav_reset #(.num_chan(NCHAN)) gx_reset_inst (
   .clock(                  ref_clk),                     .reset(reset),
   .pll_locked(          pll_locked), .pll_powerdown(    pll_powerdown),
   .tx_analogreset(  tx_analogreset), .rx_analogreset(  rx_analogreset),
	.tx_digitalreset(tx_digitalreset), .rx_digitalreset(rx_digitalreset),
   .tx_cal_busy(        tx_cal_busy), .rx_cal_busy(        rx_cal_busy),
   .tx_ready(              tx_ready), .rx_ready(              rx_ready),
   .pll_select(/*log2(NCHAN)*/ 4'h0), .rx_is_lockedtodata({NCHAN{|(rx_lockedtodata)}})
); // **entire block resets if ANY rx_is_lockedtodata go low** => if some cables unattached,
   //    do not use unused channels lock signals or they'll reset the in-use channels
wire reconfig_busy;  wire [70*2*NCHAN-1:0] gx_cfgin;  wire [46*2*NCHAN-1:0] gx_cfgout; // 70 and 46 bits per interface
gx_arriav_reconfig #(.num_iface(NCHAN*2)) gx_reconfig_inst ( // 2 interfaces per channel: TX and PLL
   .mgmt_clk_clk(           ref_clk), .mgmt_rst_reset(            reset),
   .reconfig_to_xcvr(      gx_cfgin), .reconfig_from_xcvr(    gx_cfgout),
	.reconfig_busy(    reconfig_busy)
);

/////////////////////  xcvr bus bit packing/unpacking   ////////////////////////////

// xcvr_io: the bits for up to rate=4 always exist, even if unused, i.e. always 64/44 bits per channel
// 32+4bit Rx: the 36 valid bits out of each 64 are [0-8,16-24,32-40,48-56] [64-71,] [128-135,] [192-200,208-216,224-232,240-248]
// 32+4bit Tx: the 36 valid bits out of each 44 are [0-8,11-19,22-30,33-41] [44-51,] [ 88- 95,] [132-140,143-151,154-162,165-173]
// followed by 9:rx_errdetect, 10:rx_syncstatus, 11:rx_disperr, 12:rx_patndetct, 13:NA, 14:rx_rmfifostat, 15:rx_runningdisp
// 
// the channel in use is gx_bits 132-40,192-200 i.e. xcvr chan3 in 0-3
// these map to bits 31-24 for rate1 or 127-96 for rate4
// 
generate
for(j=0; j<NCHAN; j=j+1) begin :nchan_assign
   for(i=0; i<RATE;  i=i+1) begin :rate_assign
	   localparam k = j*RATE+i;    // packed parameter offset
	   localparam r = 64*j + 16*i; // gx rx parameter offset
	   localparam t = 44*j + 11*i; // gx tx parameter offset
      assign gx_rawTxDataIn [t+10     :  t] = {2'h0,gx_TxCtrlIn[k],gx_TxDataIn[k*8+7:k*8]};
      assign gx_RxDataOut   [8*(k+1)-1:8*k] =  gx_rawRxDataOut[r+ 7:r];
      assign gx_RxCtrlOut     [k]           =  gx_rawRxDataOut[r+ 8];
      assign rx_errdetect_out [k]           =  gx_rawRxDataOut[r+ 9];
      assign rx_syncstatus    [k]           =  gx_rawRxDataOut[r+10];
      assign rx_disperr_out   [k]           =  gx_rawRxDataOut[r+11];
      assign rx_patdet        [k]           =  gx_rawRxDataOut[r+12];
   end
end
endgenerate

endmodule
