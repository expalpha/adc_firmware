// monitor errors and realign when needed (e.g. cable reconnect)
module gx_realign (
   input  wire clk,                  output reg  [ 3:0] rx_enarealign,
	input  wire [15:0] rx_errdetect,  input  wire [15:0] rx_disperr,
   input  wire [15:0] rx_patdetect,  input  wire [15:0] rx_syncstatus
);
// stratix4 word-aligner responds to +ve edge on rx_enarealign

reg [15:0] ok_count; reg [15:0] err_count;
always @ (posedge clk) begin
   rx_enarealign <= 4'h0;  ok_count <= ok_count;  err_count <= err_count;
	if( rx_errdetect == 16'h0 && rx_disperr == 16'h0 ) begin
	   ok_count <= ok_count + 16'h1;
		if( err_count != 16'h0 && ok_count >= 16'h3f ) begin // reduce error by 1 every 64 good words
		   err_count <= err_count - 16'h1;                   // => > 2% or so error rate will continually resync
			ok_count <= 16'h0;                                //     (a few times per second)
		end
	end else begin
   	err_count <= err_count + 16'h1;
		if( err_count >= 16'hFFF0 ) begin                    // too many errors - resync
		   err_count <= 16'h0;  rx_enarealign <= 4'hF; 
		end
	end
end

endmodule
