// This module takes a 100MHz clock and outputs a 125MHz equivalent timestamp, with a 2 bit fractional portion
// Expressed from a 100MHz counters point of view, a 125MHz clock 'jumps' every 4th count, due to the 8ns vs 10ns periods of the clocks 
// The 125MHz clock counts 5 for every 4 of the 100MHz counts. 
// 100MHz 	125MHz 	Fraction 
//	0			0			0.00 		
//	1			1			0.25 
//	2			2 			0.50
//	3			3			0.75
//	4			5			1.00 

module ts_100_to_125 (
	rst, 	// asynchronous reset 
	clk, 	// 100 MHz clock signal 
	ena,	// Enable timestamping
	q,		// Timestamp in 125MHz 
	frac	// 2 bit fractional output, to properly order timestamp within 125Mhz period
			// The fractional count is equal to N*2ns, ie: 01 = 2ns, 10 = 4ns, 11 = 6ns 
			// To get the exact time passed since starting the timestamp use the following formula:
			// (q * 8ns) + (frac * 2ns) = time passed 
);

parameter SZ_TS = 48;

input  wire rst; 
input  wire clk;
input  wire ena;
output reg  [SZ_TS-1:0] q;
output reg  [1:0] frac;

always@(posedge rst, posedge clk) begin 
	if(rst) begin
		q 		<= {{SZ_TS}{1'b0}};
		frac 	<= 2'b00;
	end else begin 
		if(ena) begin 
			frac 	<= frac + 1'b1; // logic requires roll-over
			if(frac == 2'b11) begin 
				q <= q + 2'b10; // double-count 
			end else begin
				q <= q + 2'b01; // single-count 
			end
		end else begin
			frac <= frac;
			q <= q;
		end
	end 
end 

endmodule

