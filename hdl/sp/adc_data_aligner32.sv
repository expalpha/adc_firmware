module adc_data_aligner32 (	
	// ADC SERDES Output
	clk,
	rst,
	rx_clk,
	//q,
	adc_data_out,
		
	// Diagnostics
	locked,
	aligned,
        ext_sync,
        clk_sel_async,
        phff_sel_async,
	
	// ADC Input lines
	fco,  
	dco,  
	d
);

parameter NUM_ADC 		= 4; // Number of ADCs
parameter NUM_CH	 		= 8; // Number of Channels per ADC
parameter SZ_DATA_IN 	= 1; // Number of bits per ADC channel pre-deserializer
parameter SZ_FCO_IN  	= 1; // Number of bits in FCO pre-deserializer
parameter SZ_DS_RATE	 	= 7; // Deserializer rate (in-to-out bits ratio)

localparam TOTAL_CH		= NUM_ADC*NUM_CH;
localparam SZ_FCO	 = (SZ_FCO_IN * SZ_DS_RATE);  // Outgoing FCO size
localparam SZ_DATA = (SZ_DATA_IN * SZ_DS_RATE); // Outgoing Data size
localparam NUM_CH_IN = (NUM_CH * SZ_DATA_IN) + (SZ_FCO_IN); // 8 data lines + FCO 
localparam SZ_DATA_OUT = 16; // we add in two bits to align it to 16

input wire  									  		clk;
input wire  					  				  		rst;
input wire [NUM_ADC-1:0][SZ_FCO_IN-1:0]  		fco;
input wire [NUM_ADC-1:0] 					  		dco;
input wire [NUM_ADC-1:0][NUM_CH-1:0][SZ_DATA_IN-1:0] d;

input wire ext_sync;
input wire clk_sel_async;
input wire [NUM_ADC-1:0] phff_sel_async;

output wire [NUM_ADC-1:0]  						locked;
output wire [NUM_ADC-1:0]  						aligned;
output wire [NUM_ADC-1:0]							rx_clk;
//output wire [NUM_ADC-1:0][NUM_CH-1:0][SZ_DATA_OUT-1:0] 		q;
output wire [TOTAL_CH-1:0][SZ_DATA_OUT-1:0] 		adc_data_out;

wire [NUM_ADC-1:0][NUM_CH-1:0][SZ_DATA-1:0] 	adc_data;
wire [NUM_ADC-1:0][SZ_FCO-1:0]    			  	adc_fco;	// only used internally to align
wire [NUM_ADC-1:0]								int_aligned;
wire [NUM_ADC-1:0]					 			int_bitslip;
wire [NUM_ADC-1:0]  							int_locked;


genvar n;
genvar i;

   wire all_channels_aligned_async0 = &int_aligned;
   reg  all_channels_aligned_clk;
   reg  all_channels_aligned_clk1;
   reg  all_channels_aligned_clk2;

   always_ff @(posedge clk) begin 
      all_channels_aligned_clk1 <= all_channels_aligned_async0;
      all_channels_aligned_clk2 <= all_channels_aligned_clk1;
      all_channels_aligned_clk  <= all_channels_aligned_clk2;
   end 		
   
   reg [NUM_ADC-1:0] all_channels_aligned;

   // select straight clock or inverted clock
   wire xclk = (xclk_sel) ? clk : !clk;
   
   reg  xclk_sel;
   reg  xclk_sel1;
   reg  xclk_sel2;

   always_ff @(posedge clk) begin
      // got burned in sp_suppress, now make sure all signals are explicitely clocked! K.O.
      xclk_sel1 <= clk_sel_async;
      xclk_sel2 <= xclk_sel1;
      xclk_sel  <= xclk_sel2;
   end

generate
for(n=0; n<NUM_ADC; n++) begin: gen_adc

	wire int_rst = ~int_locked[n];
	wire [1:0] word_aligned;
	wire [1:0] bitslip;
	
	// Outgoing status bits
	synchronizer #( .SZ_DATA( 1 ) ) sync_adc_locked		( .clk	( clk ),	.rst	( rst ), .d ( int_locked[n] ),	.q ( locked[n] ));
	synchronizer #( .SZ_DATA( 1 ) ) sync_adc_aligned 	( .clk	( clk ),	.rst	( rst ), .d ( int_aligned[n] ),	.q ( aligned[n] ));	
	
	// Instantiate hard IP SERDES block 
	adc32_serdes serdes_inst (
		.pll_areset 		( rst ),
		.rx_channel_data_align	( {NUM_CH_IN{int_bitslip[n]}} ),
		.rx_in 			( {d[n], fco[n]} ),
		.rx_inclock		( dco[n] ), 
		.rx_out 		( { adc_data[n], adc_fco[n] } ),
		.rx_outclock 		( rx_clk[n] ),
		.rx_locked 		( int_locked[n] )
	);
	
	// Trainer works in SERDES clock domain, trains continously, and solely against FCO
	adc_trainer #(
		.NUM_CH(1),
		.SZ_CH(SZ_DS_RATE)
	) train_hi_inst (
		.clk			( rx_clk[n] ),	
		.training	        ( int_locked[n] /* 1'b1 */),
		.aligned		( word_aligned[1] ),
		.bitslip		( bitslip[1] ),
		.pattern		( 7'b1111111 ),
		.d_in			( adc_fco[n] )		
	);
	
	adc_trainer #(
		.NUM_CH(1),
		.SZ_CH(SZ_DS_RATE)
	) train_lo_inst (
		.clk			( rx_clk[n] ),	
		.training	        ( int_locked[n] /* 1'b1 */),
		.aligned		( word_aligned[0] ),
		.bitslip		( bitslip[0] ),
		.pattern		( 7'b0000000 ),
		.d_in			( adc_fco[n] )		
	);	
	
	// we are aligned if EITHER trainer thinks its good, because an all 0 or 1 pattern is possible when unlocked, we AND with the lock signal
	assign int_aligned[n] = int_locked[n] & (word_aligned[1] | word_aligned[0]); 
	
	// bitslip if either complains, they must be out for longer than 2 clocks in order for this to occur	
	assign int_bitslip[n] = bitslip[1] | bitslip[0]; 

        // sync all_channels_aligned_async to per-ADC clocks
        reg all_channels_aligned1;
        reg all_channels_aligned2;
   
        always_ff @(posedge rx_clk[n]) begin 
           all_channels_aligned1 <= all_channels_aligned_clk;
           all_channels_aligned2 <= all_channels_aligned1;
           all_channels_aligned[n] <= all_channels_aligned2;
	end 

        reg phff_sel;
        reg phff_sel1;
        reg phff_sel2;

        always_ff @(posedge clk) begin
           // got burned in sp_suppress, now make sure all signals are explicitely clocked! K.O.
           phff_sel1 <= phff_sel_async[n];
           phff_sel2 <= phff_sel1;
           phff_sel <= phff_sel2;
        end
		
	for(i=0; i<NUM_CH; i++) begin: gen_adc_ch	
		reg [(SZ_DATA*2)-1:0] adc_data_concatted; 
		
		//reg [2:0] rd_req;
		//wire rd_empty;		
	        //wire rd_active = (rd_req == 3'b111) ? 1'b1 : 1'b0;
		wire wr_active = all_channels_aligned[n] & word_aligned[0];	
			
		// Read Logic
		//always@(posedge clk) begin 
		//	rd_req <= {rd_req[1:0], ~rd_empty};
		//end
		
		// We have to re-combine the ADC32 data, it wants a 14 deep serdes, but altera does not allow us to create one in hardware
		// So instead, we've created a 7 deep SERDES, and then aligned and concatenated the results
		always@(posedge rx_clk[n]) begin 
			adc_data_concatted <= { adc_data_concatted[SZ_DATA-1:0], adc_data[n][i] }; // shift in channel data 
		end 		
		
		phase_fifo adc_phase_fifo_inst (
			.aclr		( int_rst | ext_sync ),
			.data		( {adc_data_concatted, 2'b00} ), 
			.wrclk		( rx_clk[n] ),
			.wrreq		( wr_active ),
			.wrfull		( ),
			.rdclk		( xclk /* clk */),
			.rdempty	( /* rd_empty */ ),
			.rdreq		( 1'b1 /* rd_active */ ),
			.q		( phff /* q[n][i][SZ_DATA_OUT-1:0] */ )
		);

	   reg [SZ_DATA_OUT-1:0] phff;
	   reg [SZ_DATA_OUT-1:0] phff1;
	   reg [SZ_DATA_OUT-1:0] phff2;

	   always_ff @(posedge clk) begin
              // sync from maybe inverted clock to straight clock
              phff1 <= phff;
              // delay by 1 clock
              phff2 <= phff1;
           end

           // select delay by 1 clock or delay by 2 clocks
	   //assign q[n][i] = (phff_sel) ? phff2 : phff1;
	   assign adc_data_out[n*NUM_CH+i] = (phff_sel) ? phff2 : phff1;
			
	end 
end
endgenerate

endmodule
