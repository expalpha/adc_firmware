//-----------------------------------------------------------------------------
// CRC module for data[31:0] ,   crc[15:0]=1+x^2+x^15+x^16;
//-----------------------------------------------------------------------------
module crc16_for_sp (
	clk,
	rst,
	crc_en,
	d,
	q
);

input wire clk;
input wire rst;
input wire crc_en;
input wire [31:0] d;
output wire [15:0] q;

reg [15:0] lfsr_q;
reg [15:0] lfsr_c;

assign q = lfsr_q;

always_comb begin
	lfsr_c[0] = lfsr_q[0] ^ lfsr_q[1] ^ lfsr_q[2] ^ lfsr_q[3] ^ lfsr_q[4] ^ lfsr_q[5] ^ lfsr_q[6] ^ lfsr_q[7] ^ lfsr_q[8] ^ lfsr_q[9] ^ lfsr_q[10] ^ lfsr_q[11] ^ lfsr_q[14] ^ lfsr_q[15] ^ d[0] ^ d[1] ^ d[2] ^ d[3] ^ d[4] ^ d[5] ^ d[6] ^ d[7] ^ d[8] ^ d[9] ^ d[10] ^ d[11] ^ d[12] ^ d[13] ^ d[15] ^ d[16] ^ d[17] ^ d[18] ^ d[19] ^ d[20] ^ d[21] ^ d[22] ^ d[23] ^ d[24] ^ d[25] ^ d[26] ^ d[27] ^ d[30] ^ d[31];
	lfsr_c[1] = lfsr_q[0] ^ lfsr_q[1] ^ lfsr_q[2] ^ lfsr_q[3] ^ lfsr_q[4] ^ lfsr_q[5] ^ lfsr_q[6] ^ lfsr_q[7] ^ lfsr_q[8] ^ lfsr_q[9] ^ lfsr_q[10] ^ lfsr_q[11] ^ lfsr_q[12] ^ lfsr_q[15] ^ d[1] ^ d[2] ^ d[3] ^ d[4] ^ d[5] ^ d[6] ^ d[7] ^ d[8] ^ d[9] ^ d[10] ^ d[11] ^ d[12] ^ d[13] ^ d[14] ^ d[16] ^ d[17] ^ d[18] ^ d[19] ^ d[20] ^ d[21] ^ d[22] ^ d[23] ^ d[24] ^ d[25] ^ d[26] ^ d[27] ^ d[28] ^ d[31];
	lfsr_c[2] = lfsr_q[0] ^ lfsr_q[12] ^ lfsr_q[13] ^ lfsr_q[14] ^ lfsr_q[15] ^ d[0] ^ d[1] ^ d[14] ^ d[16] ^ d[28] ^ d[29] ^ d[30] ^ d[31];
	lfsr_c[3] = lfsr_q[1] ^ lfsr_q[13] ^ lfsr_q[14] ^ lfsr_q[15] ^ d[1] ^ d[2] ^ d[15] ^ d[17] ^ d[29] ^ d[30] ^ d[31];
	lfsr_c[4] = lfsr_q[0] ^ lfsr_q[2] ^ lfsr_q[14] ^ lfsr_q[15] ^ d[2] ^ d[3] ^ d[16] ^ d[18] ^ d[30] ^ d[31];
	lfsr_c[5] = lfsr_q[1] ^ lfsr_q[3] ^ lfsr_q[15] ^ d[3] ^ d[4] ^ d[17] ^ d[19] ^ d[31];
	lfsr_c[6] = lfsr_q[2] ^ lfsr_q[4] ^ d[4] ^ d[5] ^ d[18] ^ d[20];
	lfsr_c[7] = lfsr_q[3] ^ lfsr_q[5] ^ d[5] ^ d[6] ^ d[19] ^ d[21];
	lfsr_c[8] = lfsr_q[4] ^ lfsr_q[6] ^ d[6] ^ d[7] ^ d[20] ^ d[22];
	lfsr_c[9] = lfsr_q[5] ^ lfsr_q[7] ^ d[7] ^ d[8] ^ d[21] ^ d[23];
	lfsr_c[10] = lfsr_q[6] ^ lfsr_q[8] ^ d[8] ^ d[9] ^ d[22] ^ d[24];
	lfsr_c[11] = lfsr_q[7] ^ lfsr_q[9] ^ d[9] ^ d[10] ^ d[23] ^ d[25];
	lfsr_c[12] = lfsr_q[8] ^ lfsr_q[10] ^ d[10] ^ d[11] ^ d[24] ^ d[26];
	lfsr_c[13] = lfsr_q[9] ^ lfsr_q[11] ^ d[11] ^ d[12] ^ d[25] ^ d[27];
	lfsr_c[14] = lfsr_q[10] ^ lfsr_q[12] ^ d[12] ^ d[13] ^ d[26] ^ d[28];
	lfsr_c[15] = lfsr_q[0] ^ lfsr_q[1] ^ lfsr_q[2] ^ lfsr_q[3] ^ lfsr_q[4] ^ lfsr_q[5] ^ lfsr_q[6] ^ lfsr_q[7] ^ lfsr_q[8] ^ lfsr_q[9] ^ lfsr_q[10] ^ lfsr_q[13] ^ lfsr_q[14] ^ lfsr_q[15] ^ d[0] ^ d[1] ^ d[2] ^ d[3] ^ d[4] ^ d[5] ^ d[6] ^ d[7] ^ d[8] ^ d[9] ^ d[10] ^ d[11] ^ d[12] ^ d[14] ^ d[15] ^ d[16] ^ d[17] ^ d[18] ^ d[19] ^ d[20] ^ d[21] ^ d[22] ^ d[23] ^ d[24] ^ d[25] ^ d[26] ^ d[29] ^ d[30] ^ d[31];
end

always @(posedge clk, posedge rst) begin
	if(rst) begin
		lfsr_q <= {16{1'b1}};
	end else begin
		lfsr_q <= crc_en ? lfsr_c : lfsr_q;
	end
end

endmodule
