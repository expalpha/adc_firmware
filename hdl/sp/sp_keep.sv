//
// channel suppression for ADC data (copy from the PWB project)
//
`default_nettype none
module sp_keep
  (
   input wire 	           reset,
   input wire 	           clk,
   input wire [SZ_DATA1:0] threshold,
   input wire [SZ_DATA1:0] adc_a,
   input wire [SZ_DATA1:0] adc_b,
   input wire              adc_strobe,
   output reg              ready_out,
   output reg [SZ_DATA1:0] baseline_out,
   output reg [SZ_DATA1:0] adc_out,
   output reg 	           trig_out,
   output wire 	           trig_pos_out,
   output wire 	           trig_neg_out
   );

   parameter SZ_DATA = 16;

   localparam SZ_DATA1 = SZ_DATA-1;

   // width of ADC signal

   //parameter ADC_NBITS = 12;
   //parameter ADC_NBITS1 = ADC_NBITS-1;

   // compute the baseline

   localparam NSAMPLES = 64; // baseline is sum of 64 samples
   localparam NEXT = $clog2(NSAMPLES); // 6 bits of overflow
   localparam NBITS = SZ_DATA + NEXT; // 12+6 = 18 bits of adc sum

   localparam NEXT1 = NEXT-1;
   localparam NBITS1 = NBITS-1;

   reg signed [NBITS1:0] baseline_sum;
   reg [7:0]             baseline_count;
   reg                   baseline_ready;

   assign ready_out = baseline_ready;

   // construct sign-extended adc value
   wire signed [NBITS1:0] adc_a_sext;
   assign adc_a_sext[SZ_DATA1:0] = adc_a[SZ_DATA1:0];
   assign adc_a_sext[NBITS1:SZ_DATA] = (adc_a[SZ_DATA1]?{NEXT{1'b1}}:{NEXT{1'b0}});

   wire signed [NBITS1:0] adc_b_sext;
   assign adc_b_sext[SZ_DATA1:0] = adc_b[SZ_DATA1:0];
   assign adc_b_sext[NBITS1:SZ_DATA] = (adc_b[SZ_DATA1]?{NEXT{1'b1}}:{NEXT{1'b0}});

   wire signed [SZ_DATA1:0] adc_a_value = adc_a - baseline_out; // signed!
   wire signed [SZ_DATA1:0] adc_b_value = adc_b - baseline_out; // signed!

   wire trig_neg_a = baseline_ready & (adc_a_value <= $signed(-threshold)); // signed!
   wire trig_pos_a = baseline_ready & (adc_a_value >= $signed(threshold)); // signed!

   wire trig_neg_b = baseline_ready & (adc_b_value <= $signed(-threshold)); // signed!
   wire trig_pos_b = baseline_ready & (adc_b_value >= $signed(threshold)); // signed!

   assign trig_neg_out = trig_neg_a | trig_neg_b;
   assign trig_pos_out = trig_pos_a | trig_pos_b;

   wire trig = trig_pos_out | trig_neg_out;

   always_ff@ (posedge clk) begin
      if (reset) begin
         baseline_sum <= 0;
         baseline_count <= 0;
         baseline_ready <= 0;
         adc_out <= 0;
         baseline_out <= 0;
         trig_out <= 0;
      end else if (adc_strobe) begin
         if (baseline_ready) begin
            adc_out  <= adc_a_value;
            trig_out <= trig;
         end if (baseline_count == NSAMPLES) begin
            baseline_out[SZ_DATA1:0] <= baseline_sum[NBITS1:NEXT]; // div by 64
            baseline_ready <= 1;
            adc_out  <= adc_a_value;
            trig_out <= trig;
         end else begin
            baseline_sum <= baseline_sum + adc_a_sext + adc_b_sext; // signed add
            baseline_count <= baseline_count + 8'h02;
            baseline_ready <= 0;
            adc_out  <= adc_a_value;
            trig_out <= trig;
         end
      end
   end // always_ff@ (posedge clk)
   
endmodule
