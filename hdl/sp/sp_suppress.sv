`default_nettype none
module sp_suppress
  (
   input wire clk, // clk_adc 100 MHz, clk_fmc 62.5 MHz
   input wire rst,

   // fifo write
   input wire val,
   input wire [33:0] data,

   // suppression config
   input wire enable,
   input wire [SZ_OUT-1:0] end_count,
   input wire keep_bit,
   input wire [11:0] keep_last,
   input wire        keep_force,
   input wire [11:0] keep_more,

   // suppression start and done
   //input wire start,
   output reg done_out,

   // backpressure
   output wire ready_out,

   // debug
   //input wire debug0,
   //input wire debug1,
   //input wire debug2,
   //input wire debug3,
   output wire debug_out,

   // Avalon-ST out
   input wire                    rd_clk, // clk_sys 125 MHz
   input wire                    rd_clk_rst,
   input wire                    rd_ack,
   output wire                   rd_sop,
   output wire                   rd_eop,
   output wire                   rd_val,
   output wire [1:0]             rd_empty,
   output wire [31:0]            rd_dat
   );

   parameter SZ_OUT = 9999;

   // hand-built mux for debug_out:
   //
   //wire   dout000X = (debug0) ? xempty : running;
   //wire   dout001X = (debug0) ? zfifo_empty : zready;
   //wire   dout00XX = (debug1) ? dout001X : dout000X;

   //wire   dout010X = (debug0) ? 1'b0      : 1'b0;
   //wire   dout011X = (debug0) ? (xkeep_bit|(|xkeep_last)|xdone|(|cnt)) : 1'b0;
   //wire   dout01XX = (debug1) ? dout011X : dout010X;
   //
   //wire   dout0XXX = (debug2) ? dout01XX : dout00XX;
   //
   //assign debug_out = dout0XXX;

   assign debug_out = running;

   // data flow:
   // { data, val } -> xfifo -> { xdata } -> read loop -> { ydata, yval } -> ch_fifo -> { zdata } -> rd_dat & co

   wire          isop       = data[33] & val;
   wire          ieop       = data[32] & val;
   //wire        i31        = data[31] & val; // unused
   //wire        i30        = data[30] & val; // unused
   //wire        ikeep_ena  = data[29] & val;
   //wire        ikeep_bit  = data[28] & val;
   //wire [11:0] ikeep_last = data[27:16] & val;
   //wire [15:0] ibaseline  = data[15:0]  & val;

   wire                          xrd_ack;
   wire                          xempty;
   wire [33:0]                   xdata;

   //assign xrd_ack = zready & !xempty;
   assign xrd_ack = zready & running & !xempty;

   scfifo #(
       .LPM_WIDTH ( 34 ),
       .LPM_NUMWORDS ( 384 ), // max udp packet 1500 bytes hold 700 adc samples is around 300 data words
       .LPM_SHOWAHEAD ( "ON" )
       ) xfifo
     (
      .sclr    ( rst ),
      .clock   ( clk ),
      .wrreq   ( val ),
      .data    ( data ),
      
      .rdreq   ( xrd_ack ),
      .empty   ( xempty ),
      .q       ( xdata )
      );

   //// FIFO size is 512. (too big, max udp packet 1500 bytes
   //// hold 700 adc samples is around 300 data words)
   //
   //suppress_fifo xfifo
   //  (
   //   .sclr    ( rst ),
   //   .clock   ( clk ),
   //   .wrreq   ( val ),
   //   .data    ( data ),
   //   
   //   .rdreq   ( xrd_ack ),
   //   .empty   ( xempty ),
   //   .q       ( xdata )
   //   );

   wire xsop    = xdata[33] & !xempty;
   wire xeop    = xdata[32] & !xempty;

   reg          yval;
   reg [33:0]   ydata;

   reg          running;
   reg          xdone;
   reg          xkeep_bit;
   reg [11:0]   xkeep_last;
   reg [11:0]   cnt;

   //
   // explanation of state machine:
   //
   // copy data from xfifo to ch_fifo:
   //
   // copied is 8 words of the event header (starts with xsop)
   // copied is 2 words of data (the first 4 adc samples)
   // copied is xkeep_last words of data (kept samples)
   // copied is keep_more words of data (additional samples)
   // following words are dropped
   // copied is 1 word of event footer (word with xeop).
   //
   // copying cannot start until the sp_channel state
   // machines goes through the waveform and computes
   // the value of "keep_last".
   //
   // because xkeep_bit and xkeep_last are not stored
   // in a fifo, we can only process one packet at a time,
   // and we have to handshake with the sp_channel state machine:
   //
   // sp_channel starts
   // sends event header (sop)
   // we see isop, clear xdone, clear running
   // sp_channel finishes looping over the waveform,
   // sends event footer (eop)
   // we see ieop, latch xkeep_bit, xkeep_last, set running
   // sp_channel sits in S15_DONE state, waiting for xdone
   // while in "running" state, we copy data from xfifo to ch_fifo
   // we see xeop (and there will be no more data in xfifo),
   // we copy the event footer, clear running, set xdone
   // sp_channel state machine sees xdone, goes to the idle state
   //

   always_ff @(posedge clk) begin
      if (rst) begin
         // reset
         running <= 0;
         cnt <= 0;
         yval <= 0;
         xdone <= 0;
         xkeep_bit  <= 0;
         xkeep_last <= 12'd0;
      end else if (isop) begin
         // event header pushed into xfifo
         cnt  <= 0;
         xdone <= 0;
         running <= 0;
         yval <= 0;
      end else if (ieop) begin
         // event footer pushed into xfifo
         // capture keep_bit and start copying
         running <= 1;
         //xkeep_bit  <= ikeep_bit | keep_force;
         //xkeep_last <= ikeep_last;
         xkeep_bit  <= keep_bit | keep_force;
         xkeep_last <= keep_last;
         cnt     <= 0;
         xdone   <= 0;
      end else if (running & xempty) begin
         // kludge: we should never get here,
         // before we see xempty, we should see xeop
         // and stop running. K.O. 28-OCT-2020
         running <= 0;
         xdone   <= 1;
         yval    <= 0;
         ydata   <= 0;
      end else if (zready & running & !xempty) begin
         if (xsop) begin
            // xfifo pops the event header
            running <= 1;
            xdone   <= 0;
            cnt     <= 12'd1;
            yval    <= 1;
            ydata   <= xdata;
         end else if (xeop) begin
            // xfifo pops the event footer, stop running
            running <= 0;
            xdone   <= 1;
            cnt     <= cnt + 12'd1;
            yval    <= 1;
            ydata   <= xdata;
         end else if (cnt < 3) begin // copy the short packet header
            // xfifo pops additional event header words
            running <= 1;
            xdone   <= 0;
            cnt     <= cnt + 12'd1;
            yval    <= 1;
            ydata   <= xdata;
         end else if (xkeep_bit & (cnt < xkeep_last + keep_more + 10)) begin
            // xfifo pops adc samples, keep as many as requested
            // copy 10 dwords of header (8 dwords) and 4 adc samples (2 dwords)
            // copy keep_last dwords of adc samples (2 samples/dword)
            // copy keep_more dwords of adc samples (2 samples/dword)
            running <= 1;
            xdone   <= 0;
            cnt     <= cnt + 12'd1;
            yval    <= 1;
            ydata   <= xdata;
         end else begin
            // xfifo pops adc samples beyound (xkeep_last + keep_more)
            running <= 1;
            xdone   <= 0;
            cnt     <= cnt + 12'd1;
            yval    <= 0;
            ydata   <= 0;
         end
      end else begin // if (zready & running & !xempty)
         // idle cycle
         yval  <= 0;
         ydata <= 0;
      end
   end

   assign done_out   = (enable) ? xdone  : 1'b1;
   //assign ready_out  = (enable) ? xempty : zavail;
   assign ready_out  = (enable) ? zavail3000 : zavail;

   wire        zval  = (enable) ? yval   : val;
   wire [33:0] zdata = (enable) ? ydata  : data;
   
   wire        zfifo_empty;
   wire [11:0] zwords_used;
   wire [33:0] rd_data;
   
   assign rd_val    = ~zfifo_empty;
   assign rd_sop    = rd_data[33] & !zfifo_empty;
   assign rd_eop    = rd_data[32] & !zfifo_empty;
   assign rd_empty  = 2'b00;
   assign rd_dat    = rd_data[31:0];

   // FIFO size is 4096 words
   
   channel_fifo ch_fifo
     (
      .aclr    ( rst ),
      .wrclk   ( clk ),
      .wrreq   ( zval ),
      .wrusedw ( zwords_used ),
      .data    ( zdata ),
      
      .rdclk   ( rd_clk ),
      .rdreq   ( rd_ack ),
      .rdempty ( zfifo_empty ),
      .q       ( rd_data )
      );

   wire [11:0] r_space_reqd = end_count + 12'd12; // give a bit of extra space for synchronization delay
   wire [11:0] r_space_left = 12'd1023 - zwords_used;
   wire zavail = (r_space_left > r_space_reqd) ? 1'b1 : 1'b0;

   wire zready = (zwords_used < 12'd4000);
   wire zavail3000 = (zwords_used < 12'd3000);
   
endmodule
