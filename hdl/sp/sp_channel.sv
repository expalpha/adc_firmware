`default_nettype none
module sp_channel
  (
   input wire clk, // clk_adc 100 MHz, clk_fmc 62.5 MHz
   input wire rst,
   input wire ena,
   input wire run_start,

   input wire ch_type, // BScint or Anode
   
   input wire [47:0] hw_id, // 64 bit unique id from ArriaV chipid
   input wire [31:0] fw_id, // 32 bit build timestamp 
   input wire [7:0]  module_id, // Set at runtime by NIOS/communication link
   input wire [SZ_TIMESTAMP-1:0] timestamp, // time of trigger

   // misc controls
   input wire [31:0]             ch_ctrl_const,
   input wire [15:0]             ch_sthr_const,

   // debug output
   output wire                   debug_out,
   
   input wire                    trig_in,
   output wire                   trig_out,
   input wire [SZ_DELAY-1:0]     trig_delay,
   
   // ADC samples data stream
   input wire [SZ_DATA-1:0]      wf_in,
   input wire [SZ_WAVE-1:0]      wf_trig_point,
   input wire [SZ_WAVE-1:0]      wf_stop_point,

   // ADC samples pattern check
   output wire                   wf_pattern4_ok_out,
   output wire                   wf_pattern4_AAA8_out,
   output wire                   wf_pattern4_5554_out,
   
   // Avalon-ST out
   input wire                    rd_clk, // clk_sys 125 MHz
   input wire                    rd_clk_rst,
   input wire                    rd_ack,
   output wire                   rd_sop,
   output wire                   rd_eop,
   output wire                   rd_val,
   output wire [1:0]             rd_empty,
   output wire [31:0]            rd_dat,
   
   // Statistics
   output reg [31:0]             stat_trig_in,
   output reg [31:0]             stat_trig_accepted,
   output reg [31:0]             stat_trig_drop_busy, // trigger occurred while we were writing the current trigger to mem
   output reg [31:0]             stat_trig_drop_full  // can't trigger, no room in buffer
   );

   reg [31:0]                    ch_ctrl;
   reg [15:0]                    ch_sthr;

   always_ff @(posedge clk) begin 
      ch_ctrl <= ch_ctrl_const;
      ch_sthr <= ch_sthr_const;
   end
   
   wire ch_ctrl_supp_enable    = ch_ctrl[0]; // enable data suppression
   wire ch_ctrl_force_keep     = ch_ctrl[1]; // force the keep_bit
   //wire debug0 = ch_ctrl[4];
   //wire debug1 = ch_ctrl[5];
   //wire debug2 = ch_ctrl[6];
   //wire debug3 = ch_ctrl[7];
   wire [11:0] ch_ctrl_supp_keep_more = ch_ctrl[27:16]; // how many words to keep after the pulse
   wire ch_ctrl_reset          = ch_ctrl[31]; // reset everything (used in sp_top.sv)
   
   localparam PACKET_TYPE    = 8'h01;
   localparam PACKET_VERSION = 8'h03;
   
   parameter CH_ID             = 0;
   parameter WAVE_DEPTH        = 1024;
   parameter SZ_TIMESTAMP      = 48;
   parameter TRIGGER_DELAY_MAX = 1024;
   
   localparam SZ_DATA  = 16;
   localparam SZ_DELAY = $clog2(TRIGGER_DELAY_MAX);
   localparam SZ_WAVE  = $clog2(WAVE_DEPTH);
   localparam SZ_OUT   = SZ_WAVE - 1;
   localparam SZ_CNT   = $clog2(WAVE_DEPTH+1);
   
   wire [31:0]                   wf_data;
   wire                          filled;
   wire                          triggerable;
   wire                          triggered;
   wire [SZ_WAVE-1:0]            wf_len;
   
   reg                           val;
   reg [33:0]                    data;
   reg                           r_done;
   reg [11:0]                    sample_cnt;
   reg [15:0]                    packet_cnt;
   reg [SZ_OUT-1:0]              rd_pos;
   reg                           r_trigger;
   reg [SZ_DELAY-1:0]            delay_cnt;
   reg [SZ_TIMESTAMP-1:0]        r_timestamp;
   
   integer                       trigger_offset;
   
   assign trig_out = r_trigger;
   
   always_ff @(posedge rst, posedge clk) begin 
      if(rst) begin
	 r_trigger <= 1'b0;
	 delay_cnt <= {SZ_DELAY{1'b0}};
	 r_timestamp <= {SZ_TIMESTAMP{1'b0}};
         
      end else begin 
	 r_timestamp <= r_timestamp;
	 
	 // Don't touch timestamp on run start, a packet may still be being finished assembling, and restarting the timestamp would corrupt it
	 if(run_start) begin 
	    stat_trig_in <= {32{1'b0}};		
	    r_trigger <= 1'b0;
	    delay_cnt <= {SZ_DELAY{1'b0}};
	 end else begin 
	    if(trig_in & ena) begin 
	       stat_trig_in <= stat_trig_in + 1'b1;
	       
	       if(delay_cnt == {SZ_DELAY{1'b0}}) begin
		  
		  // Capture timestamp here, so we can relate timestamps to events
		  // Even if the trigger itself is delayed!
		  r_timestamp <= timestamp;
		  
		  if(trig_delay == {SZ_DELAY{1'b0}}) begin
		     r_trigger <= 1'b1;
		     delay_cnt <= {SZ_DELAY{1'b0}};
		  end else begin
		     r_trigger <= 1'b0;
		     delay_cnt <= trig_delay;
		  end
	       end else begin 
		  r_trigger <= 1'b0; // count missed trigger 
		  delay_cnt <= delay_cnt - 1'b1; // we are not at zero yet 
	       end
	    end else begin 
	       if(delay_cnt > 1) begin 
		  r_trigger <= 1'b0;
		  delay_cnt <= delay_cnt - 1'b1;
	       end else if(delay_cnt == 1) begin 
		  r_trigger <= 1'b1;
		  delay_cnt <= delay_cnt - 1'b1;
	       end else begin 
		  r_trigger <= 1'b0;
		  delay_cnt <= {SZ_DELAY{1'b0}};
	       end
	    end 		
	 end 
      end
   end	
   
   enum int unsigned { 
	               S0_IDLE 		= 0,
	               S1_HEADER0	= 1,
	               S2_HEADER1	= 2,
	               S3_HEADER2	= 4,
	               S4_HEADER3	= 8,
	               S5_HEADER4	= 16,
	               S6_HEADER5	= 32,
	               S7_HEADER6	= 64,	
	               S8_HEADER7	= 128,	
	               S8_HEADER7_RDY	= 256,
	               S9_WAIT0		= 512,
	               S10_WAIT1	= 1024,
	               S11_DATA		= 2048,
	               S12_FOOTER0	= 4096,
	               S13_FOOTER1	= 8192,
	               //S14_CRC16	= 16384,
	               S15_DONE 	= 32768
                       } state, next_state;
   
   wire accepted_trigger = (r_trigger && triggerable && r_fifo_avail);
   
   always_comb begin : next_state_logic
      next_state = S0_IDLE;
      case(state)
	S0_IDLE: 		next_state = accepted_trigger ? S1_HEADER0 : S0_IDLE;				
	S1_HEADER0: 		next_state = S2_HEADER1;
	S2_HEADER1: 		next_state = S3_HEADER2;
	S3_HEADER2: 		next_state = S4_HEADER3;
	S4_HEADER3: 		next_state = S5_HEADER4;
	S5_HEADER4: 		next_state = S6_HEADER5;
	S6_HEADER5: 		next_state = S7_HEADER6;
	S7_HEADER6: 		next_state = (ready_for_readout) ? S8_HEADER7_RDY : S8_HEADER7;
	S8_HEADER7: 		next_state = (ready_for_readout) ? S10_WAIT1 : S9_WAIT0;
	S8_HEADER7_RDY: 	next_state = S11_DATA;
	S9_WAIT0: 		next_state = (ready_for_readout) ? S10_WAIT1 : S9_WAIT0;
	S10_WAIT1:		next_state = S11_DATA;
	S11_DATA:		next_state = (rd_pos < end_count) ? S11_DATA : S12_FOOTER0;
	S12_FOOTER0: 		next_state = S13_FOOTER1;
	S13_FOOTER1: 		next_state = S15_DONE;
	//S14_CRC16:		next_state = S15_IDLE;
	S15_DONE:		next_state = (supp_done) ? S0_IDLE : S15_DONE;
      endcase
   end
   
   always_ff @(posedge rst, posedge clk) begin
      if (rst) begin 
	 state <= S0_IDLE;
      end else begin
	 state <= next_state;
      end
   end
   
   reg clear_counters;
   //reg r_fifo_avail;
   //reg [11:0] r_space_left;
   //reg [11:0] r_space_reqd;
   wire r_fifo_avail;
   
   wire [SZ_CNT-1:0] fill_count;
   
   wire [SZ_OUT-1:0] end_count = wf_stop_point[1 +: SZ_OUT]; // equivalent to wf_stop_point >> 1
   wire              ready_for_readout = (fill_count > end_count) ? 1'b1 : 1'b0; // if more than half the buffer has been written, we can start the readout

   reg               supp_reset;
   reg               supp_strobe;
   reg [SZ_DATA-1:0] supp_adc_a;
   reg [SZ_DATA-1:0] supp_adc_b;
   wire              keep_this;
   reg               keep_bit;
   reg [11:0]        keep_count;
   reg [11:0]        keep_last;
   //reg               supp_start;
   reg               supp_done;
   
   always_ff @(posedge rst, posedge clk) begin
      if (rst) begin 
	 r_done 		<= 1'b1;
	 clear_counters		<= 1'b0;
	 stat_trig_accepted	<= 32'h0;
	 stat_trig_drop_busy 	<= 32'h0;
	 stat_trig_drop_full 	<= 32'h0;		
	 val 			<= 1'b0;
	 data 			<= 34'h0;		
	 rd_pos			<= {SZ_OUT{1'b0}};
         supp_reset             <= 1;
         supp_strobe            <= 0;
         keep_bit               <= 0;
         keep_count             <= 0;
         keep_last              <= 0;
         //supp_start             <= 0;
      end else begin 
	 // Clear the counters on run start, but don't do so immediately, wait till next idle state!
	 clear_counters <= (run_start) ? 1'b1 : clear_counters;
	 
	 stat_trig_accepted 	<= stat_trig_accepted;
	 stat_trig_drop_busy 	<= stat_trig_drop_busy;
	 stat_trig_drop_full 	<= stat_trig_drop_full;
	 
	 //r_space_reqd           <= end_count + 12'd12; // give a bit of extra space for synchronization delay
	 //r_space_left           <= 12'd1023 - words_used;
	 //r_fifo_avail           <= (r_space_left > r_space_reqd) ? 1'b1 : 1'b0;
	 
	 packet_cnt             <= packet_cnt;
	 sample_cnt             <= sample_cnt;
	 trigger_offset         <= trigger_offset;
	 
	 if (accepted_trigger) begin 
	    stat_trig_accepted <= stat_trig_accepted + 1'b1;
	 end 
	 
	 if (r_trigger) begin
	    if (!triggerable) begin 
	       stat_trig_drop_busy <= stat_trig_drop_busy + 1'b1;
	    end else if (!r_fifo_avail) begin
	       stat_trig_drop_full <= stat_trig_drop_full + 1'b1;
	    end 
	 end
         
         
	 case(state)
	   S0_IDLE: begin 
	      if(clear_counters) begin 
		 clear_counters 	<= 1'b0;
		 stat_trig_accepted	<= 32'h0;
		 stat_trig_drop_busy 	<= 32'h0;
		 stat_trig_drop_full 	<= 32'h0;				
	      end 
	      
	      r_done 	<= 1'b0;
	      val 	<= 1'b0;
	      data 	<= 34'h0;
	      packet_cnt     <= (wf_stop_point[0] == 1'b0) ? ({wf_stop_point[SZ_WAVE-1:0],1'b0})+(16'd34) : ({wf_stop_point[SZ_WAVE-1:0], 1'b0})+((16'd32)); // if stop_point is even, we will get an ODD number of samples, so add two-bytes of padding. This will reverse when we add a CRC on the end!
	      sample_cnt     <= wf_stop_point + 1'b1;
	      trigger_offset <= $signed({1'b0,trig_delay}) - $signed({1'b0,wf_trig_point});
	      rd_pos         <= {SZ_OUT{1'b0}};
              supp_reset     <= 1;
              supp_strobe    <= 0;
              keep_bit       <= 0;
              keep_count     <= 0;
              keep_last      <= 0;
              //supp_start     <= 0;
	   end
	   S1_HEADER0: begin
	      val 	<= 1'b1;
	      data	<= { 1'b1, 1'b0, PACKET_TYPE[7:0], PACKET_VERSION[7:0], stat_trig_accepted[15:0] };
	      rd_pos	<= rd_pos;
              supp_reset     <= 1;
              supp_strobe    <= 0;
              keep_bit       <= 0;
              keep_count     <= 0;
              keep_last      <= 0;
              //supp_start     <= 0;
	   end 
	   S2_HEADER1: begin 
	      val 	<= 1'b1;
              data      <= { 1'b0, 1'b0, module_id[7:0], ch_type, CH_ID[6:0], 4'h0, sample_cnt[11:0] };
	      rd_pos	<= rd_pos;
              supp_reset     <= 1;
              supp_strobe    <= 0;
              keep_bit       <= 0;
              keep_count     <= 0;
              keep_last      <= 0;
              //supp_start     <= 0;
	   end 
	   S3_HEADER2: begin 
	      val 	<= 1'b1;
	      data	<= { 1'b0, 1'b0, r_timestamp[31:0] };
	      rd_pos	<= rd_pos;
              supp_reset     <= 1;
              supp_strobe    <= 0;
              keep_bit       <= 0;
              keep_count     <= 0;
              keep_last      <= 0;
              //supp_start     <= 0;
	   end 
	   S4_HEADER3: begin 
	      val 	<= 1'b1;
	      data	<= { 1'b0, 1'b0, 16'h0000, hw_id[47:32] }; // MSB MAC Address
	      rd_pos	<= rd_pos;
              supp_reset     <= 1;
              supp_strobe    <= 0;
              keep_bit       <= 0;
              keep_count     <= 0;
              keep_last      <= 0;
              //supp_start     <= 0;
	   end 
	   S5_HEADER4: begin 
	      val 	<= 1'b1;
	      data	<= { 1'b0, 1'b0, hw_id[31:0] };	// LSB MAC Address
	      rd_pos	<= rd_pos;
              supp_reset     <= 1;
              supp_strobe    <= 0;
              keep_bit       <= 0;
              keep_count     <= 0;
              keep_last      <= 0;
              //supp_start     <= 0;
	   end 
	   S6_HEADER5: begin 
	      val 	<= 1'b1;
	      data	<= { 1'b0, 1'b0, {(32-(SZ_TIMESTAMP-32)){1'b0}},r_timestamp[(SZ_TIMESTAMP-1) : 32] }; // for now we'll reserve the remainder of this word until timestamp size is finalized
	      rd_pos	<= rd_pos;
              supp_reset     <= 1;
              supp_strobe    <= 0;
              keep_bit       <= 0;
              keep_count     <= 0;
              keep_last      <= 0;
              //supp_start     <= 0;
	   end 
	   S7_HEADER6: begin 
	      val 	<= 1'b1;
	      data	<= { 1'b0, 1'b0, trigger_offset[31:0] };
	      rd_pos	<= (ready_for_readout) ? rd_pos + 1'b1 : rd_pos;
              supp_reset     <= 1;
              supp_strobe    <= 0;
              keep_bit       <= 0;
              keep_count     <= 0;
              keep_last      <= 0;
              //supp_start     <= 0;
	   end 	
	   S8_HEADER7: begin 
	      val 	<= 1'b1;
	      data 	<= { 1'b0, 1'b0, fw_id[31:0] }; // build timestamp (acts as FW version)
	      rd_pos	<= (ready_for_readout) ? rd_pos + 1'b1 : rd_pos;
              supp_reset     <= 1;
              supp_strobe    <= 0;
              keep_bit       <= 0;
              keep_count     <= 0;
              keep_last      <= 0;
              //supp_start     <= 0;
	   end	
	   S8_HEADER7_RDY: begin 
	      val 	<= 1'b1;
	      data 	<= { 1'b0, 1'b0, fw_id[31:0] }; // build timestamp (acts as FW version)
	      rd_pos	<= rd_pos + 1'b1;
              supp_reset     <= 1;
              supp_strobe    <= 0;
              keep_bit       <= 0;
              keep_count     <= 0;
              keep_last      <= 0;
              //supp_start     <= 0;
	   end				
	   S9_WAIT0: begin 
	      val 	<= 1'b0;
	      data	<= 34'h0;
	      rd_pos	<= (ready_for_readout) ? rd_pos + 1'b1 : rd_pos;
              supp_reset     <= 0;
              supp_strobe    <= 0;
              //supp_start     <= 0;
	   end 
	   S10_WAIT1: begin 
	      val 	<= 1'b0;
	      data	<= 34'h0;
	      rd_pos	<= rd_pos + 1'b1;
              supp_reset     <= 0;
              supp_strobe    <= 0;
              keep_bit       <= keep_bit | keep_this;
              //supp_start     <= 0;
	   end 
	   S11_DATA: begin // if last packet, pad and send out, otherwise put data in MSB, then send out next time
	      r_done	<= 1'b0;
	      val 	<= 1'b1;
	      data	<= { 1'b0, 1'b0, wf_data[15:0], wf_data[31:16] };
	      rd_pos	<= rd_pos + 1'b1;
              supp_reset     <= 0;
              supp_strobe    <= 1;
              supp_adc_a     <= wf_data[15:0];
              supp_adc_b     <= wf_data[31:16];
              keep_bit       <= keep_bit | keep_this;
              keep_count     <= keep_count + 12'b1;
              if (keep_this) begin
                 keep_last   <= keep_count;
              end
              //supp_start     <= 0;
	   end 
	   S12_FOOTER0: begin 
	      r_done	<= 1'b0;
	      val 	<= 1'b1;
	      data	<= { 1'b0, 1'b0, wf_data[15:0], wf_data[31:16] };
	      rd_pos	<= rd_pos;			
              supp_reset     <= 0;
              supp_strobe    <= 0;
              keep_bit       <= keep_bit | keep_this;
              //supp_start     <= 1;
	   end
	   S13_FOOTER1: begin 
	      r_done	<= 1'b1;
	      val 	<= 1'b1;
	      //data	<= { 1'b0, 1'b1, wf_data[15:0], wf_data[31:16] };
	      data	<= { 1'b0, 1'b1, /* data */ 1'b1, 1'b1, ch_ctrl_supp_enable, keep_bit, keep_last[11:0], supp_baseline[15:0] };
	      rd_pos	<= rd_pos;			
              supp_reset     <= 0;
              supp_strobe    <= 0;
              keep_bit       <= keep_bit | keep_this;
              //supp_start     <= 0;
	   end	
	   //S14_CRC16: begin 
	   // r_done	<= 1'b1;
	   // val	<= 1'b1;
	   // data	<= { 1'b0, 1'b1, crc_value, 16'h0 };		
	   // rd_pos	<= rd_pos;					
           // supp_reset     <= 0;
           // supp_strobe    <= 0;
           // keep_bit       <= keep_bit | keep_this;
           // supp_start     <= 0;
	   //end
	   S15_DONE: begin 
	      r_done	<= 1'b0;
	      val 	<= 1'b0;
	      data	<= 34'h0;
	      rd_pos	<= rd_pos;	
              supp_reset     <= 0;
              supp_strobe    <= 0;
              keep_bit       <= keep_bit;
              //supp_start     <= 0;
	   end	
	 endcase
      end
   end
   
   //wire [31:0] crc_data = (state == S1_HEADER0) ? { 16'h0, data[15:0] } : data[31:0];
   //wire [15:0] crc_value;
   //
   //crc16_for_sp crc
   //  (
   //   .clk	( clk ),
   //   .rst	( rst | r_done ),
   //   .crc_en	( val ),
   //   .d	( crc_data ),
   //   .q	( crc_value )
   //   );

   
   tmem #(
	  .MEM_DEPTH( WAVE_DEPTH ),
	  .SZ_DATA_IN( SZ_DATA ),
	  .SZ_DATA_OUT( 32 )
          ) waveform_mem
     (
      .clk		( clk ),
      .rst		( rst | ch_ctrl_reset ),
      .soft_rst		( r_done ),
      .d		( wf_in ),
      .trigger		( accepted_trigger ),	
      .trigger_point	( wf_trig_point ),
      .stop_point	( wf_stop_point ),	
      .triggered	( ),
      .triggerable	( triggerable ),
      .filled		( ), // Filled and fill_count may be useful if we go to 2x readout
      .fill_count	( fill_count ),	
      .rd_addr		( rd_pos ),
      .rd_q		( wf_data )
      );

   wire        supp_ready;
   wire        trig_pos;
   wire        trig_neg;
   wire [SZ_DATA-1:0] supp_baseline;
   wire [SZ_DATA-1:0] supp_adc;
   
   sp_keep #(
             .SZ_DATA( SZ_DATA )
             ) sp_keep
     (
      .reset ( supp_reset ),
      .clk   ( clk ),
      .threshold     ( ch_sthr[SZ_DATA-1:0] ),
      .adc_a         ( supp_adc_a ),
      .adc_b         ( supp_adc_b ),
      .adc_strobe    ( supp_strobe    ),
      .ready_out     ( supp_ready     ),
      .baseline_out  ( supp_baseline  ),
      .adc_out       ( supp_adc ),
      .trig_out      ( keep_this ),
      .trig_pos_out  ( trig_pos ),
      .trig_neg_out  ( trig_neg )
      );

   sp_suppress #(
                 .SZ_OUT(SZ_OUT)
                 ) sp_suppress
     (
      .rst     ( rst | ch_ctrl_reset ),
      .clk     ( clk ),

      // write data
      .val     ( val ),
      .data    ( data ),

      // backpressure
      .ready_out  ( r_fifo_avail ),

      // data suppression
      .enable     ( ch_ctrl_supp_enable ),
      .end_count  ( end_count  ),
      .keep_bit   ( keep_bit | ch_ctrl_force_keep ),
      .keep_last  ( keep_last  ),
      .keep_force ( ch_ctrl_force_keep ),
      .keep_more  ( ch_ctrl_supp_keep_more ),

      //.start      ( supp_start ),
      .done_out   ( supp_done  ),

      //.debug0     ( debug0 ),
      //.debug1     ( debug1 ),
      //.debug2     ( debug2 ),
      //.debug3     ( debug3 ),
      .debug_out  ( debug_out ),

      // Avalon-ST out
      .rd_clk     ( rd_clk ), // clk_sys 125 MHz
      .rd_clk_rst ( rd_clk_rst | ch_ctrl_reset ),
      .rd_ack     ( rd_ack ),
      .rd_sop     ( rd_sop ),
      .rd_eop     ( rd_eop ),
      .rd_val     ( rd_val ),
      .rd_empty   ( rd_empty ),
      .rd_dat     ( rd_dat )
      );

   // detect ADC test pattern 4 "alt checkerboard":
   // data is alternating words 0xAAA8 and 0x5554.

   reg                wfAAA8;
   reg                wf5554;
   reg [3:0]          wfCounter;
   reg                wfGood;
   reg [15:0]         wfPREV;

   always_ff @(posedge clk) begin
      wfPREV <= wf_in;
      if (((wf_in == 16'hAAA8) & (wfPREV == 16'h5554)) |
          ((wf_in == 16'h5554) & (wfPREV == 16'hAAA8))) begin
         // the two words should alternate
         // check that they alternate for 16 cycles,
         // and set the "good" flag.
         if (wfCounter == 4'hF) begin
            wfGood <= 1;
         end else begin
            wfCounter <= wfCounter + 4'd1;
         end
      end else begin
         // both up means one of the words got repeated,
         // both down means one of the words is missing
         wfCounter <= 0;
         wfGood <= 0;
      end

      wfAAA8 <= (wf_in == 16'hAAA8);
      wf5554 <= (wf_in == 16'h5554);
   end
   
   assign wf_pattern4_ok_out = wfGood;
   assign wf_pattern4_AAA8_out = wfAAA8;
   assign wf_pattern4_5554_out = wf5554;

endmodule
