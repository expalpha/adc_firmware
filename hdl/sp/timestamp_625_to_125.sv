// This module takes a 62.5MHz clock and outputs a 125MHz equivalent timestamp
// AKA it counts at twice the rate of the clock
module ts_625_to_125 (
	rst, 	// asynchronous reset 
	clk, 	// 62.5 MHz clock signal
	ena,	// Enable timestamping
	q		// Timestamp in 125MHz 
);

parameter SZ_TS = 48;

input  wire rst; 
input  wire clk;
input  wire ena;
output reg  [SZ_TS-1:0] q;

always@(posedge rst, posedge clk) begin 
	if(rst) begin
		q 	<= {{SZ_TS}{1'b0}};
	end else begin
		if(ena) begin 
			q[0] 	<= 1'b0;
			q[SZ_TS-1:1] <= q[SZ_TS-1:1] + 1'b1;
		end else begin
			q <= q;
		end
	end 
end 

endmodule
