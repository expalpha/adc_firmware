module adc_data_aligner16 (	
	// ADC SERDES Output
	clk,
	rst,
	rx_clk,
	//q,
	adc_data_out,
		
	// Diagnostics
	locked,
	aligned,
        ext_sync,
        clk_sel_async,
        phff_sel_async,
	
	// ADC Input lines
	fco,  
	dco,  
	d
);

parameter PATTERN 		= 8'hF0;
parameter NUM_ADC 		= 4; // Number of ADCs
parameter NUM_CH	 		= 4; // Number of Channels per ADC
parameter SZ_DATA_IN 	= 2; // Number of bits per ADC channel pre-deserializer
parameter SZ_FCO_IN  	= 1; // Number of bits in FCO pre-deserializer
parameter SZ_DS_RATE	 	= 8; // Deserializer rate (in-to-out bits ratio)

localparam TOTAL_CH		= NUM_ADC*NUM_CH;
localparam SZ_FCO	 = (SZ_FCO_IN * SZ_DS_RATE);  // Outgoing FCO size
localparam SZ_DATA = (SZ_DATA_IN * SZ_DS_RATE); // Outgoing Data size
localparam NUM_CH_IN = (NUM_CH * SZ_DATA_IN) + (SZ_FCO_IN); // 8 data lines + FCO 

input wire  									  		clk;
input wire  					  				  		rst;
input wire [NUM_ADC-1:0][SZ_FCO_IN-1:0]  		fco;
input wire [NUM_ADC-1:0] 					  		dco;
input wire [NUM_ADC-1:0][NUM_CH-1:0][SZ_DATA_IN-1:0] d;

input wire ext_sync;
input wire clk_sel_async;
input wire [NUM_ADC-1:0] phff_sel_async;

output wire [NUM_ADC-1:0]  						locked;
output wire [NUM_ADC-1:0]  						aligned;
output wire [NUM_ADC-1:0]							rx_clk;
//output wire [NUM_ADC-1:0][NUM_CH-1:0][SZ_DATA-1:0] 		q;
output wire [TOTAL_CH-1:0][SZ_DATA-1:0]	adc_data_out;

wire [NUM_ADC-1:0][NUM_CH-1:0][SZ_DATA-1:0] 	adc_data;
wire [NUM_ADC-1:0][SZ_FCO-1:0]    			  	adc_fco;	// only used internally to align
wire [NUM_ADC-1:0]									int_aligned;
wire [NUM_ADC-1:0]					 				bitslip;
wire [NUM_ADC-1:0]  									int_locked;
wire [NUM_ADC-1:0][NUM_CH-1:0][SZ_DATA-1:0] 	d_out;

genvar n;
genvar i;

   wire    all_channels_aligned_async0 = &int_aligned;
   reg     all_channels_aligned_clk;
   reg     all_channels_aligned_clk1;
   reg     all_channels_aligned_clk2;
   
   always_ff @(posedge clk) begin 
      all_channels_aligned_clk1 <= all_channels_aligned_async0;
      all_channels_aligned_clk2 <= all_channels_aligned_clk1;
      all_channels_aligned_clk  <= all_channels_aligned_clk2;
   end 		

   reg [NUM_ADC-1:0] all_channels_aligned;

   //reg               xclk_sel;
   //reg               xclk_sel1;
   //reg               xclk_sel2;
   
   //always_ff @(posedge clk) begin
   //   // got burned in sp_suppress, now make sure all signals are explicitely clocked! K.O.
   //   xclk_sel1 <= clk_sel_async;
   //   xclk_sel2 <= xclk_sel1;
   //   xclk_sel  <= xclk_sel2;
   //end

   // select straight clock or inverted clock
   //wire              xclk = (xclk_sel) ? !clk : clk;

generate
for(n=0; n<NUM_ADC; n++) begin: gen_adc

	wire int_rst = ~int_locked[n];
	
	// Outgoing status bits
	synchronizer #( .SZ_DATA( 1 ) ) sync_adc_locked		( .clk	( clk ),	.rst	( rst ), .d ( int_locked[n] ),	.q ( locked[n] ));
	synchronizer #( .SZ_DATA( 1 ) ) sync_adc_aligned 	( .clk	( clk ),	.rst	( rst ), .d ( int_aligned[n] ),	.q ( aligned[n] ));	
	
	// Instantiate hard IP SERDES block 
	adc_serdes serdes_inst (
		.pll_areset 		( rst ),
		.rx_channel_data_align	( {NUM_CH_IN{bitslip[n]}} ),
		.rx_in 			( {d[n], fco[n]} ),
		.rx_inclock		( dco[n] ), 
		.rx_out 		( { adc_data[n], adc_fco[n] } ),
		.rx_outclock 		( rx_clk[n] ),
		.rx_locked 		( int_locked[n] )
	);

	// Trainer works in SERDES clock domain, trains continously, and solely against FCO
	adc_trainer #(
		.NUM_CH(1),
		.SZ_CH(SZ_FCO)
	) train_inst (
		.clk			( rx_clk[n] ),	
		.training	        ( int_locked[n] ),
		.aligned		( int_aligned[n] ),
		.bitslip		( bitslip[n] ),
		.pattern		( PATTERN ),
		.d_in			( adc_fco[n] )		
	);

        // sync all_channels_aligned_async to per-ADC clocks
        reg all_channels_aligned1;
        reg all_channels_aligned2;
   
        always@(posedge rx_clk[n]) begin 
           all_channels_aligned1 <= all_channels_aligned_clk;
           all_channels_aligned2 <= all_channels_aligned1;
           all_channels_aligned[n] <= all_channels_aligned2;
	end

        //// select straight clock or inverted clock
        //wire xclk = (xclk_sel) ? !clk : clk;

        //reg xclk_sel;
        //reg xclk_sel1;
        //reg xclk_sel2;
   
        reg phff_sel;
        reg phff_sel1;
        reg phff_sel2;

        always_ff @(posedge clk) begin
           //// got burned in sp_suppress, now make sure all signals are explicitely clocked! K.O.
           //xclk_sel1 <= clk_sel_async;
           //xclk_sel2 <= xclk_sel1;
           //xclk_sel  <= xclk_sel2;
           
           // got burned in sp_suppress, now make sure all signals are explicitely clocked! K.O.
           phff_sel1 <= phff_sel_async[n];
           phff_sel2 <= phff_sel1;
           phff_sel  <= phff_sel2;
        end
		
	for(i=0; i<NUM_CH; i++) begin: gen_adc_ch	
	
	        //reg [3:0] rd_req;
		//wire rd_empty;		
		//wire rd_active = (rd_req == 3'b111) ? 1'b1 : 1'b0;
		wire wr_active = all_channels_aligned[n] & int_locked[n];	
		
		// Read Logic
		//always@(posedge clk) begin 
		//	rd_req <= {rd_req[1:0], ~rd_empty};
		//end
						
		phase_fifo adc_phase_fifo_inst (
			.aclr		( int_rst | ext_sync ),
			.data		( adc_data[n][i] ), 
			.wrclk		( rx_clk[n] ),
			.wrreq		( wr_active ),
			.rdclk		( clk /* xclk */ ),
			.rdempty	( /* rd_empty */ ),
			.rdreq		( 1'b1 /* rd_active */ ),	
			.q		( phff /* q[n][i][SZ_DATA-1:0] */ )
		);

           reg [SZ_DATA-1:0] phff;
	   reg [SZ_DATA-1:0] phff1;
	   reg [SZ_DATA-1:0] phff2;

	   always_ff @(posedge clk) begin
              // sync from maybe inverted clock to straight clock
              phff1 <= phff;
              // delay by 1 clock
              phff2 <= phff1;
           end

           // select delay by 1 clock or delay by 2 clocks
	   //assign q[n][i] = (phff_sel) ? phff2 : phff1;
	   assign adc_data_out[n*NUM_CH+i] = (phff_sel) ? phff2 : phff1;
				
	end 
end
endgenerate

endmodule
