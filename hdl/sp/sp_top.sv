`default_nettype none
module sp_top (
	clk,
	rst,
	
	run,
	timestamp,
	module_id,
	hw_id,
	fw_id,

        ch_sthr_const,
        ch_ctrl_const,
        ch_stat_out,
	
	ch_ena,
	ch_trig_in,
	ch_trig_delay,
	ch_trig_out,
	ch_type,
	ch_wf_in,
	ch_trig_point,
	ch_stop_point,
	
        wf_pattern_ok_out,
        wf_pattern4_ok_out,
        wf_pattern4_ok_bits_out,

	rd_clk,
	rd_clk_rst,
	
	rd_rdy,
	rd_sop,
	rd_eop,
	rd_val,
	rd_empty,
	rd_dat,
	rd_avail,
	
	// Statistics
	ch_stat_trig_in,
	ch_stat_trig_accepted,
	ch_stat_trig_drop_busy,
	ch_stat_trig_drop_full
);

parameter SZ_TIMESTAMP = 48;
parameter WAVE_DEPTH = 1024;
parameter NUM_CH = 16;
parameter TRIGGER_DELAY_MAX = 1024;

localparam SZ_DATA = 16;
localparam SZ_DELAY = $clog2(TRIGGER_DELAY_MAX);
localparam SZ_WAVE = $clog2(WAVE_DEPTH);
localparam SZ_CNT  = $clog2(WAVE_DEPTH+1);

input wire clk;
input wire rst;
input wire run;

input wire [SZ_TIMESTAMP-1:0] timestamp;
input wire  [7:0] module_id; // Set at runtime by NIOS/communication link
input wire  [47:0] hw_id; // 64 bit unique id from ArriaV chipid
input wire  [31:0] fw_id; // 32 bit build timestamp 

input  wire [15:0] ch_sthr_const; // channel suppression threshold
input  wire [31:0] ch_ctrl_const; // control register
output wire [31:0] ch_stat_out; // status register

//assign ch_stat_out = 32'h00000000;

input wire  [NUM_CH-1:0] ch_ena;
input wire  [NUM_CH-1:0] ch_trig_in;
input wire  [NUM_CH-1:0][SZ_DELAY-1:0] ch_trig_delay;
output wire [NUM_CH-1:0] ch_trig_out;
input wire  [NUM_CH-1:0] ch_type;
input wire  [NUM_CH-1:0][SZ_DATA-1:0] ch_wf_in;
input wire  [NUM_CH-1:0][SZ_WAVE-1:0] ch_trig_point;
input wire  [NUM_CH-1:0][SZ_WAVE-1:0] ch_stop_point;

   output wire                        wf_pattern_ok_out;
   output wire                        wf_pattern4_ok_out;
   output wire [NUM_CH-1:0]           wf_pattern4_ok_bits_out;
	
input wire 	rd_clk;
input wire  rd_clk_rst;
input wire  rd_rdy;
output wire rd_sop;
output wire rd_eop;
output wire rd_val;
output wire rd_avail;
output wire [1:0] rd_empty;
output wire [31:0] rd_dat;

output wire [NUM_CH-1:0][31:0] 	ch_stat_trig_in;
output wire [NUM_CH-1:0][31:0] 	ch_stat_trig_accepted;
output wire [NUM_CH-1:0][31:0] 	ch_stat_trig_drop_busy;
output wire [NUM_CH-1:0][31:0] 	ch_stat_trig_drop_full;

wire ch_ctrl_reset = ch_ctrl_const[31]; // reset everything

reg [1:0] run_cap;
wire run_os;
wire running;

oneshot run_oneshot (
	.clk ( clk ),
	.rst ( rst ),
	.d	  ( run ),
	.q   ( run_os )
);

wire [NUM_CH-1:0] ch_rdy;
wire [NUM_CH-1:0] ch_sop;
wire [NUM_CH-1:0] ch_eop;
wire [NUM_CH-1:0] [1:0] ch_empty;
wire [NUM_CH-1:0] ch_val;
wire [NUM_CH-1:0][31:0] ch_dat;

wire fifo_empty;
wire [11:0] words_used;
wire [31:0] active_ch_dat;

wire active_ch_sop;
wire active_ch_eop;
wire active_ch_rdy;
wire active_ch_val;
wire [1:0] active_ch_empty;


st_store_and_forward_fifo #(
	.DEPTH( 1024 )
) sp_fifo ( 
	.clk ( rd_clk ),
	.rst ( rd_clk_rst | ch_ctrl_reset ),
		
	.snk_dat ( active_ch_dat ),
	.snk_val ( active_ch_val ),
	.snk_sop ( active_ch_sop ),
	.snk_eop ( active_ch_eop ),
	.snk_rdy ( active_ch_rdy ),
	.snk_err ( ),
	.snk_empty ( active_ch_empty ),
	
	.src_dat ( rd_dat ),
	.src_val ( rd_val ),
	.src_sop ( rd_sop ),
	.src_eop ( rd_eop ),
	.src_empty (rd_empty ),
	.src_rdy (rd_rdy ),
	.src_avail ( rd_avail ) // indicates there is a packet to be transmitted
);


st_mux #(
	.WIDTH( 32 ),
	.NUM_CH( NUM_CH )
) channel_arbiter (
	.clk		( rd_clk ),
	.rst		( rd_clk_rst | ch_ctrl_reset ),
	.d_dat	( ch_dat ),
	.d_sop	( ch_sop ),
	.d_eop	( ch_eop ),
	.d_val	( ch_val ),
	.d_req	( ch_val ),
	.d_ack	( ch_rdy ),	
	.d_empty ( ch_empty ),
	.q_ch		( ),
	.q_ch_oh	( ),
	.q_dat	( active_ch_dat ),
	.q_sop	( active_ch_sop ),
	.q_eop	( active_ch_eop ),
	.q_val	( active_ch_val ),
	.q_empty	( active_ch_empty ),
	.q_ack	( active_ch_rdy )
);

wire [47:0] hw_id_sync;
synchronizer #(
    .SZ_DATA( 48 )
) sync_hwid	(
    .clk	( clk ),
    .rst	( rst ),
    .d 	( hw_id ),
    .q 	( hw_id_sync )
);

wire [31:0] fw_id_sync;
synchronizer #(
    .SZ_DATA( 32 )
) sync_fwid	(
    .clk	( clk ),
    .rst	( rst ),
    .d 	( fw_id ),
    .q 	( fw_id_sync )
);

wire [7:0] mod_id_sync;
synchronizer #(
    .SZ_DATA( 8 )
) sync_modid	(
    .clk	( clk ),
    .rst	( rst ),
    .d 	( module_id ),
    .q 	( mod_id_sync )
);

   wire [NUM_CH-1:0] wf_pattern4_chan_ok;
   wire [NUM_CH-1:0] wf_pattern4_AAA8;
   wire [NUM_CH-1:0] wf_pattern4_5554;

   wire [NUM_CH-1:0] wf_pattern_ok_bits;
	
genvar n;
generate
for(n=0; n<NUM_CH; n++) begin : gen_sp_ch
	sp_channel #(
		.CH_ID( n ),
		.TRIGGER_DELAY_MAX( TRIGGER_DELAY_MAX ),
		.WAVE_DEPTH( WAVE_DEPTH ),
		.SZ_TIMESTAMP( SZ_TIMESTAMP )
	) channel (
		.clk						( clk ),
		.rst						( rst | ch_ctrl_reset ),
		.ena						( ch_ena[n] & run ),	
		.run_start				( run_os ),
		.trig_in					( ch_trig_in[n] ),
		.trig_delay				( ch_trig_delay[n] ),
		.trig_out				( ch_trig_out[n] ),	
		.ch_type					( ch_type[n] ), // BScint or Anode
		.hw_id					( hw_id_sync ),
		.fw_id					( fw_id_sync ),
		.module_id				( mod_id_sync ),
		.timestamp				( timestamp ),		
                .ch_ctrl_const                          ( ch_ctrl_const ),
                .ch_sthr_const                          ( ch_sthr_const ),
                .debug_out                              ( ch_stat_out[n] ),
		.wf_in					( ch_wf_in[n] ),
		.wf_trig_point			( ch_trig_point[n] ),
		.wf_stop_point			( ch_stop_point[n] ),
                .wf_pattern4_ok_out             ( wf_pattern4_chan_ok[n] ),
                .wf_pattern4_AAA8_out           ( wf_pattern4_AAA8[n] ),
                .wf_pattern4_5554_out           ( wf_pattern4_5554[n] ),
		.rd_clk					( rd_clk ),
		.rd_clk_rst				( rd_clk_rst | ch_ctrl_reset ),
		.rd_ack					( ch_rdy[n] ),
		.rd_sop					( ch_sop[n] ),
		.rd_eop					( ch_eop[n] ),
		.rd_val					( ch_val[n] ),
		.rd_dat					( ch_dat[n] ),	
		.rd_empty				( ch_empty[n] ),
		.stat_trig_in			( ch_stat_trig_in[n] ),
		.stat_trig_accepted	( ch_stat_trig_accepted[n] ),
		.stat_trig_drop_busy	( ch_stat_trig_drop_busy[n] ),
		.stat_trig_drop_full	( ch_stat_trig_drop_full[n] )	
	);

   assign wf_pattern_ok_bits[n] = ( ch_wf_in[n] == ch_wf_in[0]);
end 
endgenerate

   // detector for the ADC test pattern 4 "Alt Checkerboard"
   // alternating 0xAAAA and 0x5555 truncated to 14 bits

   wire wf_pattern4_all_chan_ok = (&wf_pattern4_chan_ok);

   // pattern4 summary okey:
   // all channels should be okey,
   // all channels should be in step: all AAA8 or all 5554.
   wire wf_pattern4_ok = (wf_pattern4_all_chan_ok) & ((&wf_pattern4_AAA8) | (&wf_pattern4_5554));

   wire wf_pattern_ok = (&wf_pattern_ok_bits);

   reg [3:0] wf_pattern_ok_counter;
   reg [3:0] wf_pattern4_ok_counter;
   
   reg [NUM_CH-1:0] wf_pattern4_ok_bits;

   always_ff @(posedge clk) begin 
      if (!wf_pattern4_all_chan_ok) begin
         // if some channels do not see the pattern, show them
         wf_pattern4_ok_bits <= wf_pattern4_chan_ok;
      end else if (wf_pattern4_AAA8[0]) begin
         // all channels see the pattern, so AAA8[0] toggles up and down
         // if all channels toggle in sync, output will be 0xFFFFFFFF,
         // if some channels toggle out of sync, they will show as zero bits.
         wf_pattern4_ok_bits <= wf_pattern4_AAA8;
      end

      if (wf_pattern_ok) begin
         if (wf_pattern_ok_counter == 4'hF) begin
            wf_pattern_ok_out <= 1;
         end else begin
            wf_pattern_ok_counter <= wf_pattern_ok_counter + 4'h1;
            wf_pattern_ok_out <= 0;
         end
      end else begin
         wf_pattern_ok_out <= 0;
         wf_pattern_ok_counter <= 4'h0;
      end

      if (wf_pattern4_ok) begin
         if (wf_pattern4_ok_counter == 4'hF) begin
            wf_pattern4_ok_out <= 1;
         end else begin
            wf_pattern4_ok_counter <= wf_pattern4_ok_counter + 4'h1;
            wf_pattern4_ok_out <= 0;
         end
      end else begin
         wf_pattern4_ok_out <= 0;
         wf_pattern4_ok_counter <= 4'h0;
      end
   end

   // pattern4_ok per-channel bits
   assign wf_pattern4_ok_bits_out = wf_pattern4_ok_bits;

endmodule 
